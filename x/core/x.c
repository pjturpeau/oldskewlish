/**
 * x/core/x.c
 * 
 * Copyleft (c) Pierre-Jean Turpeau 04/2020
 * <pierrejean AT turpeau DOT net>
 */
#ifndef X_CORE_X_C
#define X_CORE_X_C

#include <x/x.h>

# include <string.h> /* memset, memcpy, ... */
# include <stdlib.h> /* RAND_MAX, ... */

typedef enum {
    DISPLAY_WINDOW,
    DISPLAY_FULLSCREEN_DESKTOP, /* keep desktop resolution */
    DISPLAY_FULLSCREEN_NATIVE, /* switch to the closest resolution */
}
windowing_mode_t;

typedef struct video_driver_t
{
    /* video data */
    u8 *backbuffer;
    u8 *frontbuffer;
    v2s * warping;
    palette_t * palette;

    /* display properties */
    bool with_vsync;
    int pixel_scale;
    int pitch;
    windowing_mode_t display_mode;

    s32 x_offset;
    s32 y_offset;

    /* frame rate history w/ simple moving average for FPS computation */
#define FRAME_COUNT_BITS 6 /* bits used for easier modulo */
#define FRAME_MAX_COUNT (1<<FRAME_COUNT_BITS)
    int frame_count;
    int fps;
    float frame_time_history[FRAME_MAX_COUNT];
    float frame_load_history[FRAME_MAX_COUNT];
    float frame_time_sum;
    bool show_fps;
}
video_driver_t;

extern video_driver_t video;

/* ---[ INLINED IMPLEMENTATIONS ]------------------------------------------- */

/* -------------------------------------------------------------------------
 * Video (inlined operations)
 * ------------------------------------------------------------------------- */

static inline void set_pixel(int x, int y, u8 color) {
    assert(x >= 0 && y >= 0 && x < W && y < H);
    video.backbuffer[x+y*W] = color;
}

static inline void set_pixel_clipped(int x, int y, u8 color) {
    if(x >= 0 && y >= 0 && x < W && y < H)
        video.backbuffer[x+y*W] = color;
}

static inline u8 get_pixel(int x, int y) {
    assert(x >= 0 && y >= 0 && x < W && y < H);
    return video.backbuffer[x+y*W];
}

static inline void ClearScreen(u8 clear_color) {
    memset(video.backbuffer, clear_color, W * H);
}

static inline u8 * backbuffer() {
    return video.backbuffer;
}

static inline u8 * frontbuffer() {
    return video.frontbuffer;
}

/* -------------------------------------------------------------------------
 * Numbers (inlined operations)
 * ------------------------------------------------------------------------- */

static inline int random_s32(int min, int max) {
    return min + rand() / (RAND_MAX / (max - min + 1));
}

static inline float GetFloatRandomValue(float min, float max) {
    return min + ((float) rand() / (float) RAND_MAX) * (max - min);
}

static inline void seed_buffer_u8(u8 * buffer, int size, int min, int max) {
    for(;size > 0; size--) *(buffer++) = random_s32(min, max); 
}

static inline float lerp_f32(float from, float to, float ratio) {
    return from + (to - from) * ratio;
}

static inline s32 lerp_s32(s32 from, s32 to, float ratio) {
    return from + (to - from) * ratio;
}

/* ---[ IMPLEMENTATION ]---------------------------------------------------- */

# ifdef X_IMPLEMENTATION

#  include <time.h>
#  include <math.h>
#  include <stdio.h>

static int _last_event = EVT_NONE;

video_driver_t video;

/* ---[ FORWARD PlATFORM DEFINITIONS]--------------------------------------- */

static void init_platform();
static void terminate_platform();
static void update_platform_buttons();
static int last_platform_event();
static void print_system_infos();
static void open_video();
static void update_video();
static u64 init_time();
static float get_elapsed_time_and_restart(u64 * time);
static float get_elapsed_time(const u64 * time);
void toggle_windowing_mode(windowing_mode_t mode);

/* -------------------------------------------------------------------------
 * Color operations
 * ------------------------------------------------------------------------- */

/** rgb as 0xRRGGBB value */
void set_color(int index, u32 rgb888) {
    assert(index >= 0 && index < MAX_COLORS );
    video.palette->colors[index].v = rgb888;
}

void linear_gradient(int from_index, u32 from_rgb, int to_index, u32 to_rgb)
{
    if( to_index < from_index ) {
        int a = from_index; from_index = to_index; to_index = a;
        u32 b = from_rgb; from_rgb = to_rgb; to_rgb = b;
    }
                        
    if( from_index >= MAX_COLORS || to_index >= MAX_COLORS
        || from_index < 0 || to_index < 0 || from_index == to_index )
            return;

    float d = 1.f / (float) (to_index - from_index);

    pix32 from, to, c;
    
    from.v = from_rgb;
    to.v = to_rgb;
 
    for(int i = from_index; i <= to_index; i ++ ) {
        float ratio = (float) (i - from_index) * d;
        c.r = lerp_s32(from.r, to.r, ratio);
        c.g = lerp_s32(from.g, to.g, ratio);
        c.b = lerp_s32(from.b, to.b, ratio);
        set_color(i, c.v);
   }
}

/* -------------------------------------------------------------------------
 * Palette operations
 * ------------------------------------------------------------------------- */

extern palette_t * create_palette() {
    return (palette_t *) mem_alloc_cache_align(sizeof(palette_t));
}

extern void delete_palette(palette_t * palette) {
    mem_free(palette);
}

void clear_palette(u32 rgb) {
    for(int i = 0; i < MAX_COLORS; i++)
        video.palette->colors[i].v = rgb;
}

void save_palette(palette_t * palette_out) {
    memcpy(palette_out->colors, video.palette, sizeof(palette_t));
}

void restore_palette(const palette_t * palette) {
    memcpy(video.palette, palette->colors, sizeof(palette_t));
}

void copy_palette(palette_t * palette, const palette_t * src) {
    memcpy(palette, src, sizeof(palette_t));
}

void fade_to(const palette_t * from, const palette_t * to, float factor)
{
    pix32 color;
    for(int i = 0; i < MAX_COLORS; i++) {
        color.r = lerp_s32(from->colors[i].r, to->colors[i].r, factor);
        color.g = lerp_s32(from->colors[i].g, to->colors[i].g, factor);
        color.b = lerp_s32(from->colors[i].b, to->colors[i].b, factor);

        set_color(i, color.v);
    }
}

/** simple helper function to display the color palette */
static void draw_palette() {
    u8 * video = frontbuffer();
    int start = (W - 256) / 2; 
    for( int y = 0; y < H; y++) {
        for( int x = 0; x < W; x++) {
            if( x < start )
                video[x + y * W] = 0;
            else if ( x >= start + 256)
                video[x + y * W] = 255;
            else
                video[x + y * W] = x - start; 
        }
    }

    int sz = 1, cx, cy;
    do {
        sz++;
        cx = W/sz;
        cy = H/sz;
    } while(cx * cy > MAX_COLORS);
    sz--;
    cx = W/sz;

    ClearScreen(0);

    video = backbuffer();

    for(int color = 0; color < MAX_COLORS; color++) {
        video = backbuffer() + ((color / cx) * sz) * W + (color % cx) * sz;
        for(int k = 0; k < sz-1; k++)
            memset(&video[k*W], color, sz-1);
    }
}

/* from https://lospec.com/palette-list/bubblegum-16 */
static const u32 default_pal[] = {
    0x16171a, 0x7f0622, 0xd62411, 0xff8426, 0xffd100, 0xfafdff, 0xff80a4,
    0xff2674, 0x94216a, 0x430067, 0x234975, 0x68aed4, 0xbfff3c, 0x10d275,
    0x007899, 0x002859
};

static void init_default_palette() {
    for(int i = 0; i < sizeof(default_pal)/sizeof(u32); i++)
        set_color(i, default_pal[i]);
}

/* -------------------------------------------------------------------------
 * Images operations
 * ------------------------------------------------------------------------- */

image_t * create_image(int w, int h) {
    image_t * image = (image_t *) mem_alloc_cache_align(sizeof(image_t));
    assert(image);

    image->w = w;
    image->h = h;
    image->transparent_color = -1;
    image->pixels = (u8 *) mem_alloc_cache_align(w*h);
    assert(image->pixels);
    image->masks = NULL;
    image->palette = NULL;
    image->color_count = 0;
    return image;
}

void delete_image(image_t * image) {
    if(image->palette) delete_palette(image->palette);
    if(image->pixels) mem_free(image->pixels);
    if(image->masks) mem_free(image->masks);
    mem_free(image);
}


/* -------------------------------------------------------------------------
 * Interactivity
 * ------------------------------------------------------------------------- */

int last_event() {
    int return_value = _last_event;
    _last_event = EVT_NONE;
    return return_value;
}

static bool button_state[LAST_BUTTON-1];

bool button(button_t b) {
    assert(b < LAST_BUTTON);
    return button_state[b];
}

/* -------------------------------------------------------------------------
 * Numbers (non-inlined operations)
 * ------------------------------------------------------------------------- */

/* https://www.coranac.com/tonc/text/fixed.htm */
s32 lerp_lut_s32(const s32 lut[], int x, const int bits) {
    int xa = x >> bits;
    int ya = lut[xa];
    int yb = lut[xa + 1];
    return ya + ((yb - ya) * (x - (xa << bits)) >> bits);
}

/* -------------------------------------------------------------------------
 * Video & main loop
 * ------------------------------------------------------------------------- */

static void init_video()
{   
    memset(&video, 0, sizeof(video_driver_t));

    /* single framebuffer by default */
    video.backbuffer = video.frontbuffer = (u8 *) mem_alloc_cache_align(W*H);

    video.warping = NULL;

# ifdef EMULATE_FRONTBUFFER

#  warning "Frontbuffer emulation enabled"
    video.frontbuffer = (u8 *) mem_alloc_cache_align(W*H);

    console_debug("emulate frontbuffer = %X / backbuffer = %d",
        video.frontbuffer, video.backbuffer);

# endif /* EMULATE_FRONTBUFFER */

    video.palette = create_palette();

    video.display_mode = DISPLAY_WINDOW; /* start in window mode by default */
    video.with_vsync = true; /* use vsync by default */
    video.pixel_scale = 2;

    video.frame_count = 0;
    video.frame_time_sum = 0;
    video.show_fps = false;
    video.fps = 0;
    memset(video.frame_time_history, 0, sizeof(video.frame_time_history));
    memset(video.frame_load_history, 0, sizeof(video.frame_load_history));

    video.x_offset = 0;
    video.y_offset = 0;
}

static inline void swap_buffers() {
    u8 * tmp = video.frontbuffer;
    video.frontbuffer = video.backbuffer;
    video.backbuffer = tmp;
}

void enable_warping() {
    video.warping = (v2s *) mem_alloc_cache_align(W*H*sizeof(v2s));

    v2s * warp = video.warping;

     for(int j = 0; j < H; j++) {

        float yy = j / (float) H - .5f; /* range -0.5..0.5 */

        for(int i = 0; i < W; i++) {
            float x, y;

            x = (i - W * .5f) / (H * .5f);

            x /= (2.f * W / (float) H);

            x = x * (1.02f + (yy * yy * .5f));
            y = yy * (1.02f + (x * x * .5f));

            x += .5f;
            y += .5f;

            warp->u = W * x;
            warp->v = H * y;

            warp++;
        }
    }
}

void disable_warping() {
    if(video.warping)
        mem_free(video.warping);
    video.warping = NULL;
}

static float _shake_intensity = 0.f;

void shake(int amount) {
    _shake_intensity += amount;
}

static inline void update_shake() {
    if(_shake_intensity == 0.f) {
        video.x_offset = 0;
        video.y_offset = 0;
        return;
    }
    video.x_offset = GetFloatRandomValue(0, _shake_intensity) - _shake_intensity*.5f;
    video.y_offset = GetFloatRandomValue(0, _shake_intensity) - _shake_intensity*.5f;

    _shake_intensity *= .9f;
    if(_shake_intensity < .3f) _shake_intensity = 0.f;
}

/**
 * time management inspired by:
 * http://devcry.heiho.net/html/2015/20150211-rock-solid-frame-rates.html
 *
 * for precise physics in games, you should move to:
 * https://gafferongames.com/post/fix_your_timestep/
 */
void main_loop(update_func_t rendering_func)
{
    bool palette = false;
    bool pause = false;

    u64 inner_time = init_time();

    while(true)
    {
        update_platform_buttons();

        int event = last_platform_event();
        switch( event )
        {
            case KEY_ESC:
            case EVT_QUIT:
                return;

            case KEY_ALT_ENTER:
            case KEY_CTRL_ENTER:
                if(video.display_mode == DISPLAY_WINDOW) {
                    if(event == KEY_ALT_ENTER)
                        toggle_windowing_mode(DISPLAY_FULLSCREEN_DESKTOP);
                    else
                        toggle_windowing_mode(DISPLAY_FULLSCREEN_NATIVE);
                } else
                    toggle_windowing_mode(DISPLAY_WINDOW);
                break;

            case 'c': /* show color palette */
                palette = !palette;
                if( !palette ) ClearScreen(0);
                break;

            case 'p': /* pause */
                pause = !pause;
                if( !pause )
                    inner_time = init_time();
                break;

            case 's': /* screenshot */
                screenshot();
                break;

            case 'w':
                if(video.warping) disable_warping(); else enable_warping();
                break;

            case KEY_TAB: video.show_fps = ! video.show_fps; break;

            default: /* delegate */
                _last_event = event;
                break;
        }

        if(!pause) {
            float time_step = get_elapsed_time_and_restart(&inner_time);

            rendering_func(time_step);

#  if !defined(DISABLE_LOAD_TRACKING)
            float rendering_duration = get_elapsed_time(&inner_time);

            video.frame_time_sum -= video.frame_time_history[video.frame_count];
            video.frame_time_sum += time_step;

            video.frame_time_history[video.frame_count] = time_step;
            video.frame_load_history[video.frame_count] = rendering_duration;

            video.fps = FRAME_MAX_COUNT / (video.frame_time_sum * 0.001f);

            video.frame_count = (video.frame_count + 1) & (FRAME_MAX_COUNT-1);
#  endif

            update_shake();
        }

        if(palette)
            draw_palette();

#  ifdef EMULATE_FRONTBUFFER
        swap_buffers();
#  endif

        update_video();
    }
}

/* see http://paulbourke.net/dataformats/tga/ */
void save_tga(void * data, int w, int h, int bits) {
    if(bits != 24) {
        console_error("save_tga: format not supported yet (bits=%d)", bits);
        return;
    }

    FILE *fp = fopen("screenshot.tga", "wb"); 
    if(!fp) {
        console_error("save_tga: failed to create screenshot.tga");
        return;
    }

    u32 * data32 = (u32 *) data;

    putc(0, fp); /* no Identification Field */
    putc(0, fp); /* no Color Map */
    putc(2, fp); /* Data Type 2 = Unmapped RGB */

    /* Ignored Color Map data */
    putc(0, fp); putc(0, fp); putc(0, fp); putc(0, fp); putc(0, fp); 

    putc(0, fp); putc(0, fp); /* X origin */
    putc(0, fp); putc(0, fp); /* Y origin */

    putc(w & 0xFF, fp); /* WIDTH low byte */
    putc((w>>8)&0xFF, fp); /* WIDTH high byte */

    putc(h & 0xFF, fp); /* HEIGHT low byte */
    putc((h>>8)&0xFF, fp); /* HEIGHT high byte */

    putc(24, fp); /* 24 BPPP */
    putc(0, fp); /* Image descriptor byte */

    pix32 c;
    for(int k = 0; k < w*h; k++) {
        c.v = data32[k];

        putc(c.b, fp);
        putc(c.g, fp);
        putc(c.r, fp);
    }

    fclose(fp);
    console("screenshot.tga created!");
}

/* -------------------------------------------------------------------------
 * CLI
 * ------------------------------------------------------------------------- */

static int cli_argc;
static char **cli_argv;

const char * check_args(const char * opt) {
    for(int i = 0; i < cli_argc; i++) {
        if( ! strcmp(opt, cli_argv[i]) ) {
            i++;
            if(i < cli_argc) {
                if(cli_argv[i][0] != '-')
                    return cli_argv[i];
                else
                    return "";
            }
            else
                return "";
        }
    }
    return NULL;
}

static void console_usage(const char * name) {
    console("%s options:", name);
    console("\t-debug         force debug level infos");
    console("\t-fullres       fullscreen (switch to the closest resolution)");
    console("\t-fullscreen    fullscreen desktop (keep current resolution");
    console("\t-infos         print platform infos on console");
    console("\t-novsync       disable synchronization to vertical retrace");
    console("\t-window        force windowing display");
    console("\t-zoom <n>      window mode pixel size (default: %d)",
        video.pixel_scale);
    console(" ");
    terminate_platform();
    exit(0);
}

static void read_cli_options(int argc, char ** argv) {
    for(int i = 0; i < argc; i++) {

        if( ! strcmp(argv[i], "-novsync") ) {
            console_debug("CLI: no vsync");
            video.with_vsync = false;
        }
        else if ( ! strcmp(argv[i], "-window") ) {
            console_debug("CLI: window mode");
            video.display_mode = DISPLAY_WINDOW;
        }
        else if ( ! strcmp(argv[i], "-zoom") ) {
            if( i < argc-1 ) {
                video.pixel_scale = atoi(argv[i+1]);
                if(video.pixel_scale == 0)
                    console_error("-zoom: invalid argument: not a number or out of range");
            } else {
                console_error("-zoom: missing argument");;
            }
        }
        else if ( ! strcmp(argv[i], "-infos") ) {
            print_system_infos();
            terminate_platform();
            exit(0);
        }
        else if ( ! strcmp(argv[i], "-fullscreen") ) {
            console_debug("CLI: fullscreen desktop mode");
            video.display_mode = DISPLAY_FULLSCREEN_DESKTOP;
        }
        else if ( ! strcmp(argv[i], "-fullres") ) {
            console_debug("CLI: fullscreen display mode");
            video.display_mode = DISPLAY_FULLSCREEN_NATIVE;
        }
        else if (  ! strcmp(argv[i], "-h")
                || ! strcmp(argv[i], "-?")
                || ! strcmp(argv[i], "-help")
                || ! strcmp(argv[i], "--help") ) {
            console_usage(argv[0]);
        }
    }
}

/* -------------------------------------------------------------------------
 * Executable bootstrap
 * ------------------------------------------------------------------------- */

/** Forward declaration of the user-defined entry point. */
extern void start(void);

/**
 * By providing the default 'main()' function the framework ensures everything
 * is correctly initialized such as:
 *  - seeding random numbers
 *  - initialize platform specific drivers
 *  - parse default CLI options
 *  - setup the default palette
 *  - create video buffers
 * 
 * It then calls the user-defined 'start()' function.
 */
int main(int argc, char **argv)
{
    cli_argc = argc;
    cli_argv = argv;

    srand(time(NULL)); /* seed random generator */

    init_platform();

    /* catch debug option before anything else after platform init */
    if( check_args("-debug") ) {
        console_debug("CLI: window mode");
        enable_console_debug();
    }

    init_video();

    read_cli_options(argc, argv);

    open_video();

    init_default_palette();

    ClearScreen(0);

    start();

    console("fps = %d", video.fps);

    terminate_platform();

    return EXIT_SUCCESS;
}

/* -------------------------------------------------------------------------
 * Include platform driver according to compilation options
 * ------------------------------------------------------------------------- */

// # ifdef USE_SDL2
#  include <x/core/sdl_drv.c>
// # endif

# endif /* X_IMPLEMENTATION */

#endif /* X_CORE_X_C */
