/**
 * player.h Copyleft (c) Pierre-Jean Turpeau 12/2023 <pierrejean AT turpeau DOT
 * net>
 *
 * Initialize and mix protracker data structures.
 *
 * Inspired by:
 *  - http://manual.freeshell.org/tracker/Html/The_Protracker_file_format.html
 *  - https://greg-kennedy.com/tracker/modformat.html
 *  - http://coppershade.org/articles/More!/Topics/Protracker_File_Format/
 *
 * TODO
 *  - better separation of data viz: e.g., remove printf() and feed dataviz
 *    buffer if allocated by the user...
 *  - merge player.h replay code into protracker
 */
#ifndef X_SND_PLAYER_H
#define X_SND_PLAYER_H

#include <x/snd/protracker.h>
#include <x/util/bytereader.h>

/* ---[ DEFINITIONS ]------------------------------------------------------- */

typedef struct
{
    mod_song_t mod;

    /* for data viz */
    u32 mixing_audio_chunks;
    s16 *channel_audio_buf[MOD_CHANNELS_MAXCOUNT];
    s16 *mono_audio_buf;

    s16 *audio_stream;
}
mod_player_t;

/* ---[ DECLARATIONS ]------------------------------------------------------ */

extern void mod_player_load(
                        mod_player_t * player,
                        u32 mixing_frequency,
                        u32 mixing_chunk_count,
                        u8 mixing_channels,
                        u8* data,
                        u32 data_size );

extern void mod_player_free(mod_player_t *player);

/* to be called at the desired rate so that the module player fills the
 * output_audio_stream with song audio data */
extern void mod_player_mix_audio_callback(
                            mod_player_t *player,
                            u8 *output_audio_stream,
                            u32 output_size );

/* ---[ IMPLEMENTATION ]---------------------------------------------------- */

# ifdef X_IMPLEMENTATION

void mod_player_load(
                mod_player_t * player,
                u32 mixing_frequency, 
                u32 mixing_chunk_count, 
                u8 mixing_channels, 
                u8* data, 
                u32 data_size) {

    player->mod.mixing_frequency = mixing_frequency;
    player->mod.mixing_channels = mixing_channels;
    player->mixing_audio_chunks = mixing_chunk_count;

    for(int i = 0; i < MOD_CHANNELS_MAXCOUNT; i++)
        player->channel_audio_buf[i] = (s16*) malloc(mixing_chunk_count * sizeof(s16));

    player->mono_audio_buf = (s16*) malloc(mixing_chunk_count * sizeof(s16));

    for(int i =0; i <16; i++)
        player->mod.unknown_fx[i] = 0;

    mod_set_tempo(&(player->mod), MOD_DEFAULT_BPM);
    mod_set_speed(&(player->mod), MOD_DEFAULT_SPEED);

    player->mod.replay_sequence_index = 0;
    player->mod.replay_pattern_id = 0;
    player->mod.replay_position_in_pattern = 0;
    player->mod.mixing_count_remaining = player->mod.mixing_count_before_tick;

    // player->mod.sample_pos_incr
    //     = ((MOD_AMIGA_C2_FREQ * MOD_AMIGA_C2_PERIOD)
    //         / player->mod.mixing_frequency)
    //             << MOD_SAMPLE_FP_SHIFT;

    player->mod.sample_pos_incr = (MOD_AMIGA_PAUL_PAL_FREQ / player->mod.mixing_frequency) << MOD_SAMPLE_FP_SHIFT;

    printf("Mixing freq: %d, C2 Freq: %.1f, Sample Incr: %u (%d:%d fixed point = %u <=> %ux%d)\n",
        player->mod.mixing_frequency,
        ((float)MOD_AMIGA_PAUL_PAL_FREQ) / ((float)MOD_AMIGA_C2_NOTE_PERIOD),
        player->mod.sample_pos_incr >> MOD_SAMPLE_FP_SHIFT,
        32-MOD_SAMPLE_FP_SHIFT, MOD_SAMPLE_FP_SHIFT,
        player->mod.sample_pos_incr,
        player->mod.sample_pos_incr >> MOD_SAMPLE_FP_SHIFT, 1<<MOD_SAMPLE_FP_SHIFT);

    memset(&(player->mod.channels), 0, sizeof(player->mod.channels));

    bytereader_t reader;
    bytereader_init(&reader, data, data_size, true);

    bytereader_seek(&reader, 0);
    bytereader_read_string(&reader, player->mod.title, sizeof(player->mod.title));

    // Check MOD type
    // --------------

    player->mod.number_of_samples = 15;
    player->mod.number_of_channels = 4;

    bytereader_seek(&reader, MOD_SIGNATURE_OFFSET);
    player->mod.signature = bytereader_read_u32(&reader);

    switch(player->mod.signature)
    {
    case MOD_SIGNATURE_MK1:
    case MOD_SIGNATURE_MK2:
    case MOD_SIGNATURE_FLT4:
    case MOD_SIGNATURE_4CHN:
        player->mod.number_of_samples = 31;
        break;

    case MOD_SIGNATURE_6CHN:
        player->mod.number_of_samples = 31;
        player->mod.number_of_channels = 6;
        break;

    case MOD_SIGNATURE_FLT8:
    case MOD_SIGNATURE_8CHN:
        player->mod.number_of_samples = 31;
        player->mod.number_of_channels = 8;
        break;
    }

    // Read samples data
    // -----------------

    bytereader_seek(&reader, MOD_SAMPLE_OFFSET);

    for(u32 i = 0; i < player->mod.number_of_samples; i++)
    {
        mod_sample_t *sample = &(player->mod.samples[i]);

        bytereader_read_string(&reader, sample->name, MOD_SAMPLE_NAME_LEN);

        /* stored as the count of 16 bits words in MOD file */
        sample->length = bytereader_read_u16(&reader) * 2;

        sample->finetune = bytereader_read_u8(&reader);
        sample->volume = bytereader_read_u8(&reader);

        // dunno if its the right way to normalize.
        // seems it's needed however, according to tech docs.
        sample->volume = sample->volume > 64 ? 64 : sample->volume;

        sample->loop_offset = bytereader_read_u16(&reader) * 2;
        sample->loop_length = bytereader_read_u16(&reader) * 2;

        // Patch for old MOD with offset in bytes not words
        if( sample->loop_offset + sample->loop_length > sample->length )
        {
            sample->loop_length = sample->length - sample->loop_offset;
        }
    }

    player->mod.song_sequence_length = bytereader_read_u8(&reader);

    bytereader_skip(&reader, 1); // ignore protracker byte #951
    bytereader_read_bytes(&reader, player->mod.song_patterns, MOD_PATTERNS_MAXCOUNT);

    if(player->mod.number_of_samples > 15)
        bytereader_skip(&reader, 4); // ignore already read signature

    /* -=( Lookup for the highest pattern id in the pattern sequence  )=- */

    player->mod.number_of_patterns = 0;

    for(u32 j = 0; j < MOD_PATTERNS_MAXCOUNT; j++)
        if( player->mod.song_patterns[j] > player->mod.number_of_patterns )
            player->mod.number_of_patterns = player->mod.song_patterns[j];

    player->mod.number_of_patterns++;

    /* -=( Read patterns data )=- */

    for(u32 k = 0; k < player->mod.number_of_patterns; k++) {
        for( u32 p = 0; p < MOD_PATTERN_DIVISIONS; p++ ) {
            for(u32 m = 0; m < player->mod.number_of_channels; m++) {
                mod_note_t *note;
                u8 a,b,c,d;

                note = &(player->mod.patterns_notes[k][p][m]);

                a = bytereader_read_u8(&reader);
                b = bytereader_read_u8(&reader);
                c = bytereader_read_u8(&reader);
                d = bytereader_read_u8(&reader);

                // sample number (8 bits)
                note->sample_id = (a & 0xF0) + ((c >> 4) & 0x0F);

                // period (12 bits)
                note->period = ((a & 0x0F) << 8) + b;

                // effect command (4 bits)
                note->fx = c & 0x0F;

                // effect param (8 bits)
                note->fx_param = d;

                // store the note real name
                note->name = mod_note_name(note->period);
            }
        }
    }

    /* -=( Load sample data )=- */

    // TODO: store loaded MOD address only...
    for(u32 n = 0; n < player->mod.number_of_samples; n++) {
        mod_sample_t *sample = &(player->mod.samples[n]);
        if(sample->length > 1) {
            sample->data = (s8 *) bytereader_address(&reader);
            bytereader_skip(&reader, sample->length);
        }
    }

    /* TODO*/
    // console_debug("Error: %d - reader pos: %d\n", reader.error, bytereader_tell(&reader));
    // return ((reader.error != BR_ERR_NONE) && (bytereader_tell(&reader) != data_size) ? false : true;
}

/* few private pre-declarations */
static void _play_song_sequence(mod_player_t *player);
static void _play_pattern_row(mod_player_t *player);
static void _mix_channels_stereo(mod_player_t *player, int16_t *output, int index);
static void _mix_channels_mono(mod_player_t *player, int16_t *output, int index);

void mod_player_mix_audio_callback(
                mod_player_t *player,
                u8 *output_audio_stream,
                u32 output_size) {
    u32 index = 0;

    player->audio_stream = output_audio_stream;

    s16 * audio_stream_16bits = (s16 *) output_audio_stream;
    u32 word_count = output_size / sizeof(s16);

    /* here we want to fill the output mixing buffer with the song audio data */
    while(word_count > 0) {
        player->mod.mixing_count_remaining--;
        if( player->mod.mixing_count_remaining <= 0 ) {
            _play_song_sequence(player);
            player->mod.mixing_count_remaining = player->mod.mixing_count_before_tick;
        }

        switch(player->mod.mixing_channels)
        {
        case 1:
            _mix_channels_mono(player, audio_stream_16bits, index);
            break;

        case 2:
            _mix_channels_stereo(player, audio_stream_16bits, index);
            break;
        }

        index++;

        /* move the destination pointer and decrement size */
        audio_stream_16bits += player->mod.mixing_channels;
        word_count  -= player->mod.mixing_channels;
    }
}


void _play_song_sequence(mod_player_t *player)
{
    static u32 tick = 65535; /* to start the process - speed is 8 bits... */

    tick++;

    /* do we need to process a new row? */
    if( tick >= player->mod.speed )
    {
        /* reset tick to wait before processing the next row */
        tick = 0;

        _play_pattern_row(player);

        if(player->mod.jump_fx)
            // do something
            return;

        /* change position, then change pattern if needed and loop the song in
        the end... */
        player->mod.replay_position_in_pattern++;
        if( player->mod.replay_position_in_pattern >= MOD_PATTERN_DIVISIONS ) {
            player->mod.replay_position_in_pattern = 0;
            player->mod.replay_sequence_index++;
            if( player->mod.replay_sequence_index >= player->mod.song_sequence_length )
                player->mod.replay_sequence_index = 0;
        }
    }
    else
    {
        // In between two notes, channels effects are updated.

        //TODO: _update_effects();

        // TBC : update effect even at tick 0?
    }
}

/* TODO: separate text display from replay routine => fill a output info buffer instead?*/
void _play_pattern_row(mod_player_t *player)
{
    mod_note_t *note;
    mod_sample_t *sample;
    mod_channel_t *channel;

    player->mod.replay_pattern_id = player->mod.song_patterns[player->mod.replay_sequence_index];

    player->mod.jump_fx = 0;

    // prepare channels data with a new row
    for(int i = 0; i < player->mod.number_of_channels; i++) {
        channel = &(player->mod.channels[i]);
        note = &(player->mod.patterns_notes[player->mod.replay_pattern_id][player->mod.replay_position_in_pattern][i]);
        
        channel->replay_last_note = note;

        if(note->period)
            channel->period = note->period;

        if( note->sample_id > 0) {
            sample = &(player->mod.samples[note->sample_id-1]); // 0 means no sample...

            channel->sample_data = sample->data;
            channel->sample_pos  = 0;

            // due to hyper resolution of mixing (e.g. 44KHz vs 8KHz) we need
            // to scale sample length and position using fixed point.
            channel->sample_length   = sample->length << MOD_SAMPLE_FP_SHIFT;
            channel->sample_loop_pos = sample->loop_offset << MOD_SAMPLE_FP_SHIFT;
            channel->sample_loop_len = sample->loop_length << MOD_SAMPLE_FP_SHIFT;

            channel->finetune = sample->finetune;

            // TODO: store note effect for long term management in _play_song_sequence()
            if( note->fx == MOD_FX_SET_VOLUME )
                channel->volume = note->fx_param;
            else
                channel->volume = sample->volume;
        } else
            channel->finetune = 0;


        switch( note->fx ) {
        case MOD_FX_SET_SPEED:
            if( note->fx_param < 32 ) {
                if(note->fx_param == 0)
                    note->fx_param++;
                mod_set_speed(&(player->mod), note->fx_param);
            } else
                mod_set_tempo(&(player->mod), note->fx_param);
            break;

        case MOD_FX_PATTERN_BREAK:
            /* switch to next pattern index and check for song overflow */
            player->mod.replay_sequence_index++;
            if( player->mod.replay_sequence_index >= player->mod.song_sequence_length )
                player->mod.replay_sequence_index = 0;

            /* compute position in the new pattern */
            player->mod.replay_position_in_pattern = ((note->fx_param>>4)&0xF)*10 + (note->fx_param&0xF);

            player->mod.jump_fx = true;
            break;

        case MOD_FX_SET_VOLUME:
            break;

        default:
            if(note->fx || note->fx_param)
                player->mod.unknown_fx[(note->fx)&0xF] = ((note->fx<<8)&0x0F00)+(note->fx_param&0xFF);
            break;
        }
    }
}

void _mix_channels_stereo(mod_player_t *player, int16_t *output, int index)
{
    int32_t channel_chunk;
    int32_t left_audio_chunk = 0;
    int32_t right_audio_chunk = 0;

    for(uint32_t i = 0; i < player->mod.number_of_channels; i++) {
        mod_channel_t *channel = (&player->mod.channels[i]);

        if( channel->period && channel->sample_data != NULL ) {
            channel_chunk =
                channel->sample_data[channel->sample_pos >> MOD_SAMPLE_FP_SHIFT]
                    * channel->volume;

            channel->sample_pos += (player->mod.sample_pos_incr / channel->period);

            // according to one of the doc, you should loop only if the
            // repeat length is greater than 1 (16bits word so => 2 bytes)
            if( channel->sample_loop_len > (2 << MOD_SAMPLE_FP_SHIFT) ) {
                if( channel->sample_pos >= channel->sample_loop_pos + channel->sample_loop_len)
                    channel->sample_pos =
                        channel->sample_loop_pos
                            + (channel->sample_pos % (channel->sample_loop_pos + channel->sample_loop_len));
            } else
                if( channel->sample_pos >= channel->sample_length ) {
                    channel->sample_length = 0;
                    channel->sample_pos = 0;
                }

            // For stereo output:
            //  - MOD channels : 1 2 3 4 5 6 7 8
            //  - audio output : L R R L L R R L
            switch( i & 3 ) {
            case 0:
            case 3:
                left_audio_chunk += channel_chunk;
                break;

            case 1:
            case 2:
                right_audio_chunk += channel_chunk;
                break;
            }
        } else
            channel_chunk = 0;

        if( player->channel_audio_buf[i] )
            player->channel_audio_buf[i][index] = channel_chunk;
    }

    // TODO: global volumec

    left_audio_chunk  = ((left_audio_chunk + player->mod.last_left_chunk) >> 2) <<1;
    right_audio_chunk = ((right_audio_chunk + player->mod.last_right_chunk) >> 2) <<1;

    // crop to 16 bits replay limitations
    if( left_audio_chunk  > INT16_MAX ) left_audio_chunk  = INT16_MAX;
    if( left_audio_chunk  < INT16_MIN ) left_audio_chunk  = INT16_MIN;
    if( right_audio_chunk > INT16_MAX ) right_audio_chunk = INT16_MAX;
    if( right_audio_chunk < INT16_MIN ) right_audio_chunk = INT16_MIN;

    player->mod.last_left_chunk  = left_audio_chunk;
    player->mod.last_right_chunk = right_audio_chunk;

    *output     = left_audio_chunk;
    *(++output) = right_audio_chunk;

    player->mono_audio_buf[index] = (left_audio_chunk + right_audio_chunk) >> 1;
}

void _mix_channels_mono(mod_player_t * player, int16_t *output, int index)
{
    int32_t channel_chunk;
    int32_t audio_chunk = 0;

    for(uint32_t i = 0; i < player->mod.number_of_channels; i++) {
        mod_channel_t *channel = (&player->mod.channels[i]);

        if( channel->period != 0 && channel->sample_data != NULL ) {
            if( channel->period )
                channel->sample_pos += (player->mod.sample_pos_incr / channel->period);

            // according to one of the doc, you should loop only if the
            // repeat length is greater than 1 (16bits word so => 2 bytes)
            if( channel->sample_loop_len > (2 << MOD_SAMPLE_FP_SHIFT)) {
                if( channel->sample_pos >= channel->sample_loop_pos + channel->sample_loop_len)
                    channel->sample_pos =
                        channel->sample_loop_pos
                            + (channel->sample_pos % (channel->sample_loop_pos + channel->sample_loop_len));
            } else
                if( channel->sample_pos >= channel->sample_length ) {
                    channel->sample_data = NULL;
                    channel->sample_pos = 0;
                }

            channel_chunk =
                channel->sample_data[channel->sample_pos >> MOD_SAMPLE_FP_SHIFT]
                    * channel->volume;

            // 127 (max audio chunk) * 64 (max vol) * 8 (max channels) < 64K
            audio_chunk += channel_chunk;
        } else
            channel_chunk = 0;

        if( player->channel_audio_buf[i] )
            player->channel_audio_buf[i][index] = channel_chunk;
    }

    // average volume according to the number of channels
    audio_chunk /= player->mod.number_of_channels;
    //audio_chunk *= 64; // TODO : replace by global volume?

    // crop to 16 bits replay limitations
    if( audio_chunk  > INT16_MAX ) audio_chunk  = INT16_MAX;
    if( audio_chunk  < INT16_MIN ) audio_chunk  = INT16_MIN;

    *output = audio_chunk;

    player->mono_audio_buf[index] = audio_chunk;
}

# endif /* X_IMPLEMENTATION */

#endif /* X_SND_PLAYER_H */
