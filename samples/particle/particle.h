/**
 * samples/particle/particle.h
 * Copyleft (c) Pierre-Jean Turpeau 02/2019
 * <pierrejean AT turpeau DOT net>
 *
 * naive configurable particle system
 */
#ifndef X_PARTICLES_H
#define X_PARTICLES_H

#include <x86/image.h>
#include <x86/draw.h>

#define X_MAX_PARTICLES (256*8)

extern image_t * brush;

typedef struct {
    v2f position;
    v2f velocity;

    /* remember that objects of different mass fall at the same rate */
    v2f acceleration; 

    float speed;
    float angle;
    float lifespan;
    float remaining_life;
    float mass;
}
particle_element_t;

typedef struct
{
    float angle_min;
    float angle_max;
    float speed_min;
    float speed_max;
    float mass_min;
    float mass_max;

    float deathrate; /* per ms */
    float emitter_rate; /* per ms */
    
    v2f acceleration;

    v2s pos_min;
    v2s pos_max;

    uint32_t particles_count;
    particle_element_t particles[X_MAX_PARTICLES];
}
particles_t;

/* ---[ DECLARATIONS ]------------------------------------------------------ */

extern void InitParticles(particles_t * particles);
extern void UpdateParticles(particles_t * p, float dt);
extern void DrawParticles(particles_t * p);

/* ---[ IMPLEMENTATION ]---------------------------------------------------- */

#ifdef X_IMPLEMENTATION

static void _InitOneParticle(particle_element_t * p, int x, int y, float life, float mass, float angle, float speed, v2f * acceleration)
{
    p->position.x = x;
    p->position.y = y;

    p->acceleration.u = acceleration->u;
    p->acceleration.v = acceleration->v;

    p->angle = angle;
    p->speed = speed;
    p->mass  = mass;
    p->lifespan = life;
    p->remaining_life = life;

    p->velocity.u = speed * cos(angle);
    p->velocity.v = -speed * sin(angle);
}

static inline void
_CopyOneParticle(particle_element_t * p, particle_element_t * from) {
    memcpy(p, from, sizeof(particle_element_t));
}

/** returns true if still alive */
static bool _UpdateOneParticle(particle_element_t * p, float deathrate, float dt)
{
    p->remaining_life -= deathrate * dt;

    if(p->remaining_life > 0) {
        p->velocity.x += p->acceleration.x * dt;
        p->velocity.y += p->acceleration.y * dt;

        p->position.x += p->velocity.x * dt;
        p->position.y += p->velocity.y * dt;
        return true;
    }
    return false;
}

static int particle_rendering_mode = 0;

static void _DrawOneParticle(particle_element_t * p) {
    float life_ratio = p->remaining_life / p->lifespan;
    float radius = p->mass * life_ratio;
    uint8_t color = 32 * life_ratio;

    switch(particle_rendering_mode) {
        case 0:
            AdditiveDrawImage_c(brush, p->position.x - brush->w/2, p->position.y - brush->h/2, 255);
            break;
        case 1:
            DrawCircle(p->position.x, p->position.y, radius, 64);
            DrawCircleOutline(p->position.x, p->position.y, radius, 255);
            break;
        case 2:
            AddCircle(p->position.x, p->position.y, radius, color, 255);
            break;
    }
}

void InitParticles(particles_t * p) {
    p->particles_count = 0;
}

void DrawParticles(particles_t * p) {
    for(int i = 0; i < p->particles_count; i++) {
        _DrawOneParticle(&p->particles[i]);
    }
}

static void _PushOneNewParticle(particles_t * p) {
    if(p->particles_count >= X_MAX_PARTICLES) return;

    float angle = GetFloatRandomValue(p->angle_min, p->angle_max);
    float speed = GetFloatRandomValue(p->speed_min, p->speed_max);
    float mass  = GetFloatRandomValue(p->mass_min,  p->mass_max);

    int x = GetIntRandomValue(p->pos_min.x, p->pos_max.x);
    int y = GetIntRandomValue(p->pos_min.y, p->pos_max.y);

    _InitOneParticle(&p->particles[p->particles_count++], x, y, 255, mass, angle, speed, &p->acceleration);
}

void UpdateParticles(particles_t * p, float dt) {
    for( int i = 0; i < dt * p->emitter_rate; i++) {
        _PushOneNewParticle(p);
    }

    for(int i = 0; i < p->particles_count; i++ ) {
        bool alive = _UpdateOneParticle(&p->particles[i], p->deathrate, dt);
        if( ! alive ) {
            p->particles_count--;
            if( i < p->particles_count) {
                _CopyOneParticle(&p->particles[i], &p->particles[p->particles_count]);
                i--; /* to update the moved particle in the next loop */
            }
        } else {
            /* TODO: check out of bound */
        }
    }
}

#endif /* X_IMPLEMENTATION */

#endif /* X_PARTICLES_H */
