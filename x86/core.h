/**
 * x86/core.h
 * 
 * Copyleft (c) Pierre-Jean Turpeau 10/2024
 * <pierrejean AT turpeau DOT net>
 */
#ifndef X86_CORE_H
#define X86_CORE_H

#include <x86/x.h>
typedef struct X86_t {
    struct {
        u8* backbuffer;
        u8* frontbuffer;
        bool with_vsync;
    } Video;

    struct {
        int fps;
        int frame_count;
    } Time;

    struct {
        bool exit_event;
        int last_event;
        bool button_state[LAST_BUTTON-1];
    } Input;

    struct {
        u32 L1_cache_line_bytes;
    } Cpu;

    struct {
        int argc;
        char** argv;
        bool show_fps;
        bool pause;
        int show_palette; // 0=None, 1=Grid, 2=VLines
    } State;

} X86_t;

extern struct X86_t x86;

/* ---[ IMPLEMENTATION ]---------------------------------------------------- */
# ifdef X_IMPLEMENTATION

#  include <string.h> /* memset, memcpy, ... */
#  include <stdlib.h> /* malloc, ... */
#  include <time.h>
#  include <math.h>
#  include <stdio.h>

struct X86_t x86;

/* -------------------------------------------------------------------------
 * Execution
 * ------------------------------------------------------------------------- */

/**
 * Process one command-line argument
 * 
 * It returns NULL if no CLI option matches 'opt'.
 * If 'opt' matches an option in the args list, it returns:
 *  - the pointer to the next string argument if it is a valid value
 *    (i.e., not starting with a dash '-')
 *  - an empty string ("") otherwise.
 */
const char * CheckExeOption(const char * opt) {
    for (int i = 0; i < x86.State.argc ; i++) {
        if (! strcmp(opt, x86.State.argv[i]) ) {
            i++;
            if (i < x86.State.argc) {
                if (x86.State.argv[i][0] != '-')
                    return x86.State.argv[i];
                else
                    return "";
            }
            else
                return "";
        }
    }
    return NULL;
}

# endif /* X_IMPLEMENTATION - inlined functions */

static inline u8 * GetBackbuffer() {
    return x86.Video.backbuffer;
}

static inline u8 * GetFrontbuffer() {
    return x86.Video.frontbuffer;
}

void TakeScreenshot() {
    palette_t palette;
    SavePalette(&palette);
    SaveTGA("screenshot.tga", GetFrontbuffer(), W, H, 8, &palette);
}

/* see http://www.paulbourke.net/dataformats/tga/ */
typedef struct {
   u8  id_length;       // length of the id string located after the header. =0 means no string.
   u8  colormap_type;  // =1 for color mapped datatype
   u8  image_type; 
   u16 colormap_origin; // little endian integer of the first colormap entry
   u16 colormap_length; // little endian size of the colormap
   u8  colormap_depth;  // number of bits of each colormap entry (16, 24, 32)
   u16 x_origin;        // little endian X coordinate of the lower left corner
   u16 y_origin;        // little endian Y coordinate of the lower left corner
   u16 width;           // little endian width of the image
   u16 height;          // little endian height of the image
   u8  bitsperpixel;    // number of bits in a stored pixel
   u8  imagedescriptor; // flags
} tga_header_t;

# define TGA_HEADER_SIZE        18

# define TGA_IMAGETYPE_NONE      0   // no image data
# define TGA_IMAGETYPE_INDEXED   1   // uncompressed, color-mapped image
# define TGA_IMAGETYPE_BGR       2   // uncompressed, BGR image
# define TGA_IMAGETYPE_BW        3   // uncompressed, black & white image

# define TGA_TOPLEFT_ORIGIN     (1<<5)  // bit-5=1 bit-4=0

// write 16 bits data to file in Little Endian format
void fwrite_u16_LE(FILE * f, u16 value) {
    value = U16_LE(value);
    fwrite(&value, 2, 1, f);
}

void fwrite_u8(FILE * f, u8 value) {
    fwrite(&value, 1, 1, f);
}

// see https://www.loc.gov/preservation/digital/formats/fdd/fdd000180.shtml
// see http://tfc.duke.free.fr/coding/tga_specs.pdf
bool SaveTGA(const char * filename, const u8 * pixels, int w, int h, int bits, const palette_t * palette) {
    if (bits != 8 && bits != 24) {
        LogError("SaveTGA: format not supported (bits=%d)", bits);
        return false;
    }

    FILE *f = fopen(filename, "wb"); 
    if (!f) {
        LogError("SaveTGA: failed to create %s", filename);
        return false;
    }

    tga_header_t header;
    header.id_length = 0;
    if (bits == 8) {
        header.colormap_type = 1;
        header.image_type = TGA_IMAGETYPE_INDEXED;
        if (palette) {
            header.colormap_origin = 0;
            header.colormap_length = MAX_COLORS;
            header.colormap_depth = 24;
        } else {
            header.colormap_origin = 0;
            header.colormap_length = 0;
            header.colormap_depth = 0;
        }
        header.bitsperpixel = 8;
    } else {
        header.colormap_type = 0;
        header.image_type = TGA_IMAGETYPE_BGR;
        header.colormap_origin = 0;
        header.colormap_length = 0;
        header.colormap_depth = 0;
        header.bitsperpixel = 24;
    }
    header.x_origin = 0;
    header.y_origin = 0;
    header.width = w;
    header.height = h;
    header.imagedescriptor = TGA_TOPLEFT_ORIGIN;

    fwrite_u8(f, header.id_length);
    fwrite_u8(f, header.colormap_type);
    fwrite_u8(f, header.image_type);
    fwrite_u16_LE(f, header.colormap_origin);
    fwrite_u16_LE(f, header.colormap_length);
    fwrite_u8(f, header.colormap_depth);
    fwrite_u16_LE(f, header.x_origin);
    fwrite_u16_LE(f, header.y_origin);
    fwrite_u16_LE(f, header.width);
    fwrite_u16_LE(f, header.height);
    fwrite_u8(f, header.bitsperpixel);
    fwrite_u8(f, header.imagedescriptor);

    // write colormap only for 8 bits data
    for (int i = 0; i < header.colormap_length; i++) {
        fwrite_u8(f, palette->colors[i].b);
        fwrite_u8(f, palette->colors[i].g);
        fwrite_u8(f, palette->colors[i].r);
    }

    pix32_t p;
    for (int k = 0; k < w*h; k++) {
        if (bits == 8) {
            fwrite_u8(f, pixels[k]);
        } else {
            p = palette->colors[pixels[k]];
            fwrite_u8(f, p.b);
            fwrite_u8(f, p.g);
            fwrite_u8(f, p.r);
        }
    }

    fclose(f);
    LogDebug("SaveTGA: %s created (%dx%d %d-bpp)", filename, w, h, bits);
    return true;
}

bool LoadTGA(image_t * image, palette_t * palette, const u8 * data, int size) {
    byte_reader_t reader;
    byte_reader_t * r = &reader;

    InitBufferReader(r, data, size);
    
    tga_header_t header;
    header.id_length = ReadU8(r);
    header.colormap_type = ReadU8(r);
    header.image_type = ReadU8(r);
    header.colormap_origin = ReadU16_LE(r);
    header.colormap_length = ReadU16_LE(r);
    header.colormap_depth = ReadU8(r);
    header.x_origin = ReadU16_LE(r);
    header.y_origin = ReadU16_LE(r);
    header.width = ReadU16_LE(r);
    header.height = ReadU16_LE(r);
    header.bitsperpixel = ReadU8(r);
    header.imagedescriptor = ReadU8(r);

    SeekCur(r, header.id_length); // skip identification infos, if any...

    LogDebug("LoadTGA: reading TGA file @ %Xh size %d\n", data, size);
    LogDebug("LoadTGA: id_length = %d", header.id_length);
    LogDebug("LoadTGA: has_colormap = %d", header.colormap_type);
    LogDebug("LoadTGA: image_type = %d", header.image_type);
    LogDebug("LoadTGA: colormap_origin = %d", header.colormap_origin);
    LogDebug("LoadTGA: colormap_length = %d", header.colormap_length);
    LogDebug("LoadTGA: colormap_depth = %d", header.colormap_depth);
    LogDebug("LoadTGA: x_origin = %d", header.x_origin);
    LogDebug("LoadTGA: y_origin = %d", header.y_origin);
    LogDebug("LoadTGA: width = %d", header.width);
    LogDebug("LoadTGA: height = %d", header.height);
    LogDebug("LoadTGA: bitsperpixel = %d", header.bitsperpixel);
    LogDebug("LoadTGA: imagedescriptor = %Xh", header.imagedescriptor);

    if (header.bitsperpixel != 8
        && header.colormap_depth != 24
        && header.image_type != TGA_IMAGETYPE_INDEXED) {
        LogError("LoadTGA: format not supported (bits=%d, colormap depth=%d, format=%d)",
            header.bitsperpixel, header.colormap_depth, header.image_type);
        return false;
    }

    image->pixels = MemAlloc(header.width * header.height);

    image->w = header.width;
    image->h = header.height;
    image->masks = NULL;
    image->transparent_color = OPAQUE_COLOR;
    image->color_count = 0;

    for (int i = 0; palette && i < header.colormap_length && i < MAX_COLORS; i++) {
        palette->colors[i].b = ReadU8(r);
        palette->colors[i].g = ReadU8(r);
        palette->colors[i].r = ReadU8(r);
    }

    // skip colormap data if no palette storage is provided
    if (palette == NULL && header.colormap_length > 0)
        SeekCur(r, header.colormap_length * 3);

    image->color_count = 0;
    for (int k = 0; k < header.width * header.height; k++) {
        u8 c = ReadU8(r);
        image->pixels[k] = c;
        if (c > image->color_count)
            image->color_count = c;
    }

    return true;
}


# ifdef X_IMPLEMENTATION

/* -------------------------------------------------------------------------
 * Pixels
 * ------------------------------------------------------------------------- */

# endif /* X_IMPLEMENTATION - inlined functions */

static inline void SetPixel(int x, int y, u8 color) {
    assert(x >= 0 && y >= 0 && x < W && y < H);
    x86.Video.backbuffer[x+y*W] = color;
}

static inline void SetPixel_c(int x, int y, u8 color) {
    if (x >= 0 && y >= 0 && x < W && y < H)
        x86.Video.backbuffer[x+y*W] = color;
}

static inline u8 GetPixel(int x, int y) {
    assert(x >= 0 && y >= 0 && x < W && y < H);
    return x86.Video.backbuffer[x+y*W];
}

static inline void ClearScreen(u8 clear_color) {
    memset(x86.Video.backbuffer, clear_color, W * H);
}

# ifdef X_IMPLEMENTATION

/* -------------------------------------------------------------------------
 * Palette
 * ------------------------------------------------------------------------- */

void ClearPalette(u32 rgb888) {
    for(int i = 0; i < MAX_COLORS; i++)
        SetColorPalette(i, rgb888);
}

void FadePalette(const palette_t * from, const palette_t * to, float factor) {
    pix32_t color;
    for(int i = 0; i < MAX_COLORS; i++) {
        color.r = IntLerp(from->colors[i].r, to->colors[i].r, factor);
        color.g = IntLerp(from->colors[i].g, to->colors[i].g, factor);
        color.b = IntLerp(from->colors[i].b, to->colors[i].b, factor);

        SetColorPalette(i, color.v);
    }
}

/* Simple helper function to display the color palette on the frontbuffer */
void DrawPalette() {
    u8 * video = GetBackbuffer();

    // grid mode
    if(x86.State.show_palette == 1) {
        // compute optimal grid size according to the resolution
        int sz = 1, cx, cy;
        do {
            sz++;
            cx = W/sz;
            cy = H/sz;
        } while(cx * cy > MAX_COLORS);
        sz--;
        cx = W/sz;

        ClearScreen(0);

        for(int color = 0; color < MAX_COLORS; color++) {
            video = GetBackbuffer() + ((color / cx) * sz) * W + (color % cx) * sz;
            for(int k = 0; k < sz-1; k++)
                memset(&video[k*W], color, sz-1);
        }
    }
    // vertical lines
    else if (x86.State.show_palette == 2) {
        u8 line[W];
        int start = (W - 256) / 2;
        // prepare the 1 pixel height line gradient
        for (int x = 0; x < W; x++) {
            if( x < start )
                line[x] = 0;
            else if (x >= start + 256)
                line[x] = 255;
            else
                line[x] = x - start; 
        }
        for( int y = 0; y < H; y++) {
            memcpy((video+y*W), line, 320);
        }
    }
}

void LinearGradientPalette(int from_index, u32 from_rgb, int to_index, u32 to_rgb)
{
    if (to_index < from_index) {
        int a = from_index; from_index = to_index; to_index = a;
        u32 b = from_rgb; from_rgb = to_rgb; to_rgb = b;
    }
                        
    if (from_index >= MAX_COLORS || to_index >= MAX_COLORS
        || from_index < 0 || to_index < 0 || from_index == to_index)
            return;

    float d = 1.f / (float) (to_index - from_index);

    pix32_t from, to, c;
    
    from.v = from_rgb;
    to.v = to_rgb;

    for (int i = from_index; i <= to_index; i ++) {
        float ratio = (float) (i - from_index) * d;
        c.r = IntLerp(from.r, to.r, ratio);
        c.g = IntLerp(from.g, to.g, ratio);
        c.b = IntLerp(from.b, to.b, ratio);
        SetColorPalette(i, c.v);
    }
}

/**
 * from https://www.iquilezles.org/www/articles/palettes/palettes.htm 
 * 
 * params[0] = bias
 * params[1] = scale
 * params[2] = oscillations
 * params[3] = phase
 * 
 * Original operation:
 * a, b, c, & d are rgb vectors, t is time in 0..1 range
 * return a + b*cosf( 6.28318*(c*t+d) );
 */
void TrigonometricGradientPalette(int from_index, int to_index, v3f params[4]) {
    pix32_t color;
    for (int i = from_index; i <= to_index; i++) {
        float t = (float) (i - from_index) / (float) (to_index - from_index);
        color.r = 255 * (params[0].r + params[1].r * cos(M_TAU * (params[2].r * t + params[3].r)));
        color.g = 255 * (params[0].g + params[1].g * cos(M_TAU * (params[2].g * t + params[3].g)));
        color.b = 255 * (params[0].b + params[1].b * cos(M_TAU * (params[2].b * t + params[3].b)));
        SetColorPalette(i, color.v);
    }
}

/**
 * take the 'count' colors from 'start' and darken them to black according
 * to the requested 'levels'.
 */
void DarkenPaletteToBlack(int start, int count, int levels) {
    assert((start + count - 1) < MAX_COLORS);
    assert((levels * count - 1) < MAX_COLORS);
    palette_t tmp;
    SavePalette(&tmp);
    pix32_t new_color;
    for (int j = 1; j < levels; j++) {
        float factor = ((float)(levels - j)) / (float) levels;
        for (int i = start; i < start + count; i++) {
            new_color.r = tmp.colors[i].r * factor;
            new_color.g = tmp.colors[i].g * factor;
            new_color.b = tmp.colors[i].b * factor;
            tmp.colors[i + count * j].v = new_color.v;
        }
    }
    RestorePalette(&tmp);
}

/* -------------------------------------------------------------------------
 * Interactivity
 * ------------------------------------------------------------------------- */

# endif /* X_IMPLEMENTATION */

static inline bool IsPlatformExit() {
    return x86.Input.exit_event;
}

static inline bool IsPaused() {
    return x86.State.pause;
}

# ifdef X_IMPLEMENTATION

int GetLastEvent() {
    int return_value = x86.Input.last_event;
    x86.Input.last_event = EVT_NONE;
    return return_value;
}

bool IsButtonPressed(button_t b) {
    assert(b < LAST_BUTTON);
    return x86.Input.button_state[b];
}

/* -------------------------------------------------------------------------
 * Numbers
 * ------------------------------------------------------------------------- */

# endif /* X_IMPLEMENTATION - inlined functions */

static inline bool IsPowerOfTwo(int x) {
    return (x != 0) && ((x & (x - 1)) == 0);
}

static inline s32 IntLerp(s32 from, s32 to, float ratio) {
    return from + (to - from) * ratio;
}

static inline f32 FloatLerp(float from, float to, float ratio) {
    return from + (to - from) * ratio;
}

static inline s32 GetIntRandomValue(int min, int max) {
    return min + rand() / (RAND_MAX / (max - min + 1));
}

static inline f32 GetFloatRandomValue(float min, float max) {
    return min + ((float) rand() / (float) RAND_MAX) * (max - min);
}

static inline void SeedRandomUInt8Buffer(u8 * buffer, int size, int min, int max) {
    for (;size > 0; size--) {
        *(buffer++) = (u8)GetIntRandomValue(min, max); 
    }
}

# ifdef X_IMPLEMENTATION

/* IntLUTFixedPointLerp = INTeger values LUT (Look-Up_Table) Fixed Point Lerp (Linear Interpolation)
 * Interpolation of indexed values of a discrete Look-Up-Table to improve value
 * accuracy between two indexes based on a higher resolution fixed point index.
 * See https://www.coranac.com/tonc/text/fixed.htm */
s32 IntLUTFixedPointLerp(const s32 lut[], int fp_i, const int bits) {
    int xa = fp_i >> bits;
    int ya = lut[xa];
    int yb = lut[xa + 1];
    return ya + ((yb - ya) * (fp_i - (xa << bits)) >> bits);
}

/* -------------------------------------------------------------------------
 * Console
 * ------------------------------------------------------------------------- */

// platform specific only

/* -------------------------------------------------------------------------
 * Memory
 * ------------------------------------------------------------------------- */

void InitBufferReader(byte_reader_t * reader, const u8 * data, int size) {
    (void)(size); // ignore size for now...
    reader->current = (u8 *)data;
    reader->start = (u8 *)data;
}

u8 ReadU8(byte_reader_t * reader) {
    u8 v = *reader->current;
    reader->current += 1;
    return v;
}

u16 ReadU16(byte_reader_t * reader) {
    u16 v = *((u16 *)reader->current);
    reader->current += 2; 
    return v;
}

u16 ReadU16_LE(byte_reader_t * reader) {
    u16 v = *((u16 *)reader->current);
    reader->current += 2; 
    return U16_LE(v);
}

u32 ReadU32(byte_reader_t * reader) {
    u32 v = *((u32 *)reader->current);
    reader->current += 4;
    return v;
}

void SeekSet(byte_reader_t * reader, int position) {
    reader->current = reader->start + position;   
}

void SeekCur(byte_reader_t * reader, int offset) {
    reader->current += offset;
}


# ifdef WIN32
#  include <x86/core_sdl.h>
# endif

# ifdef MSDOS
#  include <x86/core_dos.h>
# endif

# endif /* X_IMPLEMENTATION */

#endif /* X86_CORE_H */
