/**
 * x86/stdlib_dos.h
 * 
 * Copyleft (c) Pierre-Jean Turpeau 10/2024
 * <pierrejean AT turpeau DOT net>
 */
#ifndef X86_STDLIB_DOS_H
#define X86_STDLIB_DOS_H

#define STDOUT 1
#define STDIN  0

typedef void FILE;


/* -------------------------------------------------------------------------
 * Heap functions
 * ------------------------------------------------------------------------- */
// malloc()
// free()

/* -------------------------------------------------------------------------
 * String functions
 * ------------------------------------------------------------------------- */
// strlen()
// strcpy()
// strcat()
// strncpy()
// vsnprintf()
// printf()

/* -------------------------------------------------------------------------
 * File functions
 * ------------------------------------------------------------------------- */

/* Constants for the SEEKMODE */
#define DOSSeekSet 0
#define DOSSeekCur 1
#define DOS_SEEK_END 2

extern FILE * fopen(const char * name, const char * mode);
extern void fclose (FILE * handle);
extern long fseek (FILE * handle, long offset, char whence);
extern char getch(void);

# ifdef X_IMPLEMENTATION

/* Constants for the OpenMode */
#define DOS_FOPEN_READMODE 0
#define DOS_FOPEN_WRITEMODE 1
#define DOS_FOPEN_READWRITE 2

static FILE * dos_fcreate(char *name);
static FILE * dos_fopen(char *name, char openmode);
static long   dos_fread(FILE * handle, long numbytes, void *dest);

#pragma aux dos_fcreate = "mov ah, 0x03c" "xor ecx, ecx " "int 0x21" parm[edx] modify[eax ecx] value[eax];
#pragma aux dos_fopen = "mov ah, 0x03d" "int 0x21 " parm[edx][al] modify[eax] value[eax];
#pragma aux fclose =    "mov ax, 0x03e00" "int 0x21 " parm[ebx] modify[eax];
#pragma aux fseek =     "mov ah, 0x042"    \
                        "mov ecx, edx"     \
                        "shr ecx, 16"      \
                        "int 0x21 "        \
                        "shl edx, 16"      \
                        "mov dx, ax"       \
                        parm[ebx][edx][al] modify[eax ecx] value[edx];

#pragma aux dos_fread = "mov ah, 0x3f" "int 0x21" parm[ebx][ecx][edx] modify[ebx ecx edx] value[eax]

long dos_fwrite (FILE * handle, long numbytes, void *dest);
#pragma aux dos_fwrite = "mov ah, 0x40" \
                         "int 0x21" \
                        parm[ebx][ecx][edx] modify[ebx ecx edx] value[eax]

#pragma aux getch = "mov ax, 0x0c08" "int 0x21" value[al] modify[eax ebx ecx edx];

# endif /* X_IMPLEMENTATION */

#endif /* X86_STDLIB_DOS_H */
