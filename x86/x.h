/**
 * x86/x.h
 *
 * Copyleft (c) Pierre-Jean Turpeau 10/2024
 * <pierrejean AT turpeau DOT net>
 *
 * use '#define X_IMPLEMENTATION = 1' before including this file in your main
 * file so that the compiler has access to the source code implementation.
 */
#ifndef X86_H
#define X86_H

#include <stdint.h> /* uint8_t, uint16_t, uint32_t */
#include <stdbool.h>
#include <assert.h>

// # ifdef MSDOS
// #  include <tinymath/tnymath2.h>
// # else
// #  include <math.h>
// # endif

# include <x86/default_config.h>

/* ---[ COMMON DEFINITIONS ]------------------------------------------------ */

# if defined(ARCH_X86) || defined(ARCH_X86_64) || defined(_X86_) || defined(__x86_64__)
#  undef X86
#  define X86
# endif

# ifndef M_PI
#  define M_PI 3.14159265358979323846
# endif

# ifndef M_PI_2
#  define M_PI_2 1.57079632679489661923
# endif

# ifndef M_TAU
#  define M_TAU 6.28318530717958647692
# endif

# define DEG_TO_RAD(A) (A / 180.0f * M_PI)

/* from http://sol.gfxile.net/interpolation/ */
# define M_SMOOTHSTEP(x)   ((x) * (x) * (3 - 2 * (x)))
# define M_SMOOTHERSTEP(x) ((x) * (x) * (x) * ((x) * ((x) * 6 - 15) + 10))

# define M_MIN(x, y) (((x) < (y)) ? (x) : (y))
# define M_MAX(x, y) (((x) > (y)) ? (x) : (y))

# define ARRAY_SIZE(SARRAY) (sizeof(SARRAY)/sizeof(SARRAY[0]))

// convert to correct endianness when needed
#  define SWAP_U16(W) (((W<<8)&0xFF00) & ((W>>8)&0x00FF))
#  define SWAP_U32(DW) (((SWAP_U16(DW&0x0FFFF)<<16)&0xFFFF0000) & (SWAP_U16(DW>>16)&0x0FFFF))
# if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
#  define U16_LE(W)  (W)
#  define U32_LE(DW) (DW)
# else
#  define U16_LE(W)  (SWAP_U16(W))
#  define U32_LE(DW) (SWAP_U32(DW))
# endif
/* ---[ COMMON TYPES ]------------------------------------------------------ */

typedef uint8_t  u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

typedef int8_t   s8;
typedef int16_t  s16;
typedef int32_t  s32;
typedef int64_t  s64;

typedef float   f32;
typedef double  f64;

typedef struct {
    s32 x, y, w, h;
} rect_t;

typedef union {
    struct { f32 x, y; };
    struct { f32 u, v; };
} v2f;

typedef union {
    struct { s32 x, y; };
    struct { s32 u, v; };
    struct { s32 w, h; };
} v2s;

typedef union {
    struct { s32 x, y, z; };
    struct { s32 u, v, w; };
    struct { s32 r, g, b; };
} v3s;

typedef union {
    struct { f32 x, y, z; };
    struct { f32 u, v, w; };
    struct { f32 r, g, b; };
    struct { f32 amplitude, speed, shift; };
} v3f;


/* ---[ COLOR & IMAGE DEFINITIONS ]----------------------------------------- */

# define MAX_COLORS 256
# define OPAQUE_COLOR   -1

typedef struct pix32_t {
    union { /* endian dependant color components */
        struct {
#  if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
            u8 b:8, g:8, r:8, a:8;
#  else
            u8 a:8, r:8, g:8, b:8;
#  endif
        };
        u32 v; /* 32-bits color value */
    };
} pix32_t;

typedef struct {
    pix32_t colors[MAX_COLORS];
} palette_t;

typedef struct {
    int w;
    int h;
    int transparent_color;  // OPAQUE_COLOR by default
    int color_count;
    u8 *pixels;
    u8 *masks;              // default is NULL
} image_t;


/* ---[ INTERACTIVITY DEFINITIONS ]----------------------------------------- */

enum {
    EVT_NONE     = 0,

    /* standard ASCII codes */
    KEY_TAB      = 9,
    KEY_ENTER    = 13,
    KEY_ESC      = 27,

    /* non standard ASCII codes */
    KEY_UP       = 0x80,
    KEY_DOWN     = 0x81,
    KEY_LEFT     = 0x82,
    KEY_RIGHT    = 0x84,

    KEY_PAGEUP   = 0x90,
    KEY_PAGEDOWN = 0x91,
    KEY_END      = 0x92,
    KEY_HOME     = 0x93,

    /* non standard shortcuts */
    EVT_QUIT       = -1,
    KEY_CTRL_ENTER = -2,
    KEY_ALT_ENTER  = -3,
};

/** joystick/arrows events */
typedef enum {
    BTN_LEFT    = 0,
    BTN_RIGHT   = 1,
    BTN_UP      = 2,
    BTN_DOWN    = 3,

    LAST_BUTTON
}
button_t;

/* ---[ BYTES & BITS DEFINITIONS ]------------------------------------------ */

// memory only for now. no out-of-bound checks...
typedef struct {
    u8 * start;
    u8 * current;
} byte_reader_t; 


/* ---[ FUNCTIONS ]--------------------------------------------------------- */

/* -------------------------------------------------------------------------
 *  Execution functions
 * ------------------------------------------------------------------------- */

extern void InitPlatform(int argc, const char **argv);      // initialize and reserve platform resources (CLI, timer, ...)
extern void ClosePlatform();                                // free everything and restor initial context
extern void FatalError(const char *fmt, ...);               // Print message onto the console, close resources and terminate
extern void ParseDefaultExeOptions();                       // Parse CLI default options for fast prototyping
extern const char * CheckExeOption(const char * opt);       // Check one single option. NULL if CLI do not matches 'opt'. Pointer to the next valid value (not starting with a dash '-'), or an empty string ("")

/* -------------------------------------------------------------------------
 *  Time functions
 * ------------------------------------------------------------------------- */

extern u32  GetTime();                                      // time in milliseconds since the start of the program
extern void Pause(int ms);                                  // halt execution for some milliseconds

/* -------------------------------------------------------------------------
 *  Video functions
 * ------------------------------------------------------------------------- */

extern void SetVGAMode();                                   // 320x200x8bpp
extern void BeginFrameLoop();                               // initialize time tracking
extern void EndFrameLoop();                                 // blit the backbuffer and update time tracking
static inline void SwapScreenBuffer();
static inline u8* GetBackbuffer();                          // return the video backbuffer
extern void TakeScreenshot();                               // take a screenshot of the frontbuffer
extern bool SaveTGA(const char * filename, const u8 * pixels, int w, int h, int bits, const palette_t * palette); // palette needed when bits==8
extern bool LoadTGA(image_t * image, palette_t * palette, const u8 * data, int size);

/* -------------------------------------------------------------------------
 * Pixel drawing functions
 * ------------------------------------------------------------------------- */

static inline void ClearScreen(u8 clear_color);             // clear the backbuffer using 'clear_color'
static inline void SetPixel(int x, int y, u8 color_index);  // set the backbuffer pixel with the 'color_index' (asserted but unclipped)
static inline void SetPixel_c(int x, int y, u8 color);      // Clipped and unasserted version of SetPixel
static inline u8   GetPixel(int x, int y);                  // returns the backbuffer pixel found at (x,y) coordinates (assserted but unclipped)

/* -------------------------------------------------------------------------
 * Palette operations
 * ------------------------------------------------------------------------- */

extern void SetColorPalette(int index, u32 rgb888);         // configure individual color in the video palette w/ rgb888 as 0xRRGGBB
extern void ClearPalette(u32 rgb);                          // reset the video palette to the given color 
extern void SavePalette(palette_t * palette_out);           // returns a copy of the video palette 
extern void RestorePalette(const palette_t * palette_in);   // Configure the video palette using 'palette_in'
extern void FadePalette(const palette_t * from, const palette_t * to, float factor); // linear interpolation between 'from' and 'to'
extern void LinearGradientPalette(int from_idx, u32 from_rgb, int to_idx, u32 to_rgb); // set the video palette with a linear interpolation of RGB components
extern void TrigonometricGradientPalette(int from_index, int to_index, v3f params[4]); // params are bias, scale, oscillations and phase factors for each rgb component in [0..1] range such as v=bias+scale*cos(2*PI*(oscillation*t+phase))
extern void DarkenPaletteToBlack(int start, int count, int levels); // darken the given range down to levels

extern void DrawPalette();                                  // Draw the active palette for debugging purpose - with [c] key in ExecuteDefaultInteractions()

/* -------------------------------------------------------------------------
 * Interactivity
 * ------------------------------------------------------------------------- */

extern int  GetLastEvent();                                 // returns the last interaction event or EVT_NONE otherwise 
extern bool IsButtonPressed(button_t b);                    // returns true if the given button is in the press state

extern void UpdatePlatformInteractions();
extern void ExecuteDefaultInteractions();                   // Basic actions for fast prototyping (display palette, pause, screenshot)

static inline bool IsPlatformExit();                        // returns true if [ESC] key or [X] button is pressed 
static inline bool IsPaused();                              // returns true if [p] keypress is detected by ExecuteDefaultInteractions()s

/* -------------------------------------------------------------------------
 * Numbers
 * ------------------------------------------------------------------------- */

static inline bool  IsPowerOfTwo(int x);
static inline int   GetIntRandomValue(int min, int max); // (max - min) shall be much much less than RAND_MAX - http://c-faq.com/lib/randrange.html
static inline f32   GetFloatRandomValue(float min, float max);
static inline void  SeedRandomUInt8Buffer(u8 * buffer, int size, int min, int max);
static inline f32   GetFloatRandomValue(float min, float max); // (max - min) shall be much much less than RAND_MAX - http://c-faq.com/lib/randrange.html
static inline s32   IntLerp(s32 from, s32 to, float ratio);  // linear interpolation of signed integers using a float ratio
static inline f32   FloatLerp(f32 from, f32 to, float ratio);  // linear interpolation of float using float ratio
extern        s32   IntLUTFixedPointLerp(const s32 lut[], int fp_i, const int bits); // linear interpolation of Look-Up-Table integer values using fixed point indexing

/* -------------------------------------------------------------------------
 * Console input and outputs
 * ------------------------------------------------------------------------- */

extern void Console(const char * fmt, ...);                 // Log info message with trailing newline
extern void LogDebug(const char * fmt, ...);                // Log debug message with trailing newline
extern void LogError(const char * fmt, ...);                // Log error message with trailing newline

/* -------------------------------------------------------------------------
 * Memory
 * ------------------------------------------------------------------------- */

extern void * MemAllocAlign(int size, int align);           // allocate with any alignment constraint (eg., 64KB for 16bit addressing)
extern void * MemAlloc(int size);                           // cache-aligned memory allocator
extern void   MemFree(void * area);

extern void InitBufferReader(byte_reader_t * reader, const u8 * data, int size);
extern u8   ReadU8(byte_reader_t * reader);
extern u16  ReadU16(byte_reader_t * reader);
extern u16  ReadU16_LE(byte_reader_t * reader);            // Read Little Endian 16-bits word
extern u32  ReadU32(byte_reader_t * reader);
extern void SeekSet(byte_reader_t * reader, int position);
extern void SeekCur(byte_reader_t * reader, int offset);


/* ---[ IMPLEMENTATION ]---------------------------------------------------- */
# include <x86/core.h> /* yes, don't be afraid */

#endif /* X86_H */
