/**
 * x86/image.h
 * 
 * Copyleft (c) Pierre-Jean Turpeau 11/2024
 * <pierrejean AT turpeau DOT net>
 * 
 * Todo
 *  - LoadBMP: support compressed formats
 *  - Masked sprite: try interleaved pixel and mask data to optimize cache access on big sprites
 *  - RLE compressed sprite: to implement and evaluate performance 
 *  - add LoadPCX?
 *  - allow loading from individual file (through generalized callbacks?) or one big data file
 */
#ifndef X86_IMAGE_H
#define X86_IMAGE_H

# include <x86/x.h>

/* -----[ DEFINITIONS ]----------------------------------------------------- */

/* -----[ FUNCTIONS ]------------------------------------------------------- */

/* -------------------------------------------------------------------------
 * Image operations
 * ------------------------------------------------------------------------- */

extern void AllocImagePixels(image_t * img, int w, int h);              // allocate image pixels, masks = NULL
extern void FreeImageData(image_t * img);                               // delete pixels and masks data

extern image_t * CreateImage(int w, int h);                             // allocate pixels only
extern void DeleteImage(image_t * img);                                 // delete pixels and masks
extern void DuplicateImage(image_t * dest, const image_t * src);        // duplicate src image into dest (alloc)
extern void DrawImage_c(const image_t * sprite, int x, int y);          // draw the clipped opaque image to the backbuffer
extern void AdditiveDrawImage_c(const image_t * sprite, int x, int y, int clamp_color); // add image pixel to the backbuffer, clamp colors as needed
extern void VerticalMirrorImage(image_t * img);
extern void HorizontalMirrorImage(image_t * img);
static inline void SetImagePixel(image_t * img, int x, int y, u8 color); // set image pixel - no clipping, no checks
static inline u8   GetImagePixel(const image_t * image, int x, int y);   // get image pixel - no clipping, no checks
static inline void ClearImage(image_t * img, u8 color);                  // Fill the image using 'color'

extern void DrawPixelateImage(const image_t * img, int x, int y, float factor); // draw image with piexalte effect - no clipping checks...

/* -------------------------------------------------------------------------
 * Image loaders
 * ------------------------------------------------------------------------- */

extern void LoadBMP(image_t * img, palette_t * pal, const u8 * bmp_data, int bmp_size); // allocate pixels to load BMP data. Palette is optional (NULL).

/* -------------------------------------------------------------------------
 * Sprite functions
 * ------------------------------------------------------------------------- */

extern void GenerateSpriteMask(image_t * sprite); // allocate and generate mask data - background index must be 0
extern void DrawSprite_c(const image_t * sprite, int x, int y);           // draw the clipped sprite to the backbuffer according to its mask 
extern void DrawSpriteRect_c(const image_t * img, int x, int y,  const rect_t * rect); // draw the specified 'rect' from the sprite 'image'
extern void GenerateQuadraticSprite(u8 * pixels, int w, int h, int color_range, int spot_radius, int smoothing_size); // vertical and horizontal radius are deduced from the image size 


/* -----[ IMPLEMENTATION ]---------------------------------------------------- */
# ifdef X_IMPLEMENTATION

#  include <math.h> // GenerateQuadraticSprite()

void AllocImagePixels(image_t * img, int w, int h) {
    assert(img);
    img->w = w;
    img->h = h;
    img->pixels = (u8 *) MemAlloc(w*h);
    assert(img->pixels);
    img->masks = NULL;
}

void FreeImageData(image_t * img) {
    if (img->pixels) MemFree(img->pixels);
    if (img->masks) MemFree(img->masks);
    img->pixels = NULL;
    img->masks = NULL;
}

void DuplicateImage(image_t * dest, const image_t * src) {
    AllocImagePixels(dest, src->w, src->h);
    memcpy(dest->pixels, src->pixels, src->w*src->h);
}

image_t * CreateImage(int w, int h) {
    image_t * img = (image_t *) MemAlloc(sizeof(image_t));
    assert(img);
    img->w = w;
    img->h = h;
    img->transparent_color = -1;
    img->pixels = (u8 *) MemAlloc(w*h);
    assert(img->pixels);
    img->masks = NULL;
    return img;
}


void DeleteImage(image_t * img) {
    if (img->pixels) MemFree(img->pixels);
    if (img->masks) MemFree(img->masks);
    MemFree(img);
}

static inline void SetImagePixel(image_t * img, int x, int y, u8 color) {
    assert( x >= 0 && y >= 0 && x < img->w && y < img->h);
    img->pixels[y * img->w + x] = color;
}

static inline u8 GetImagePixel(const image_t * image, int x, int y) {
    assert( x >= 0 && y >= 0 && x < image->w && y < image->h);
    return image->pixels[y * image->w + x];
}

static inline void ClearImage(image_t * img, u8 color) {
    memset( img->pixels, color, img->w * img->h);
}

void DrawImage_c(const image_t * img, int x, int y) {
    if(x < -img->w || x > W || y < -img->h || y > H)
        return;

    const int clip_left = (x < 0) ? -x : 0;
    const int clip_top = (y < 0) ? -y : 0;
    const int clip_right = M_MIN(W - x, img->w);
    const int clip_bottom = M_MIN(H - y, img->h);

    for(int j = clip_top; j < clip_bottom; j++) {
        u8 * src = img->pixels + clip_left + j * img->w;
        u8 * dst = GetBackbuffer() + x + clip_left + (y+j) * W;
        memcpy(dst, src, clip_right - clip_left);
    }
}

void AdditiveDrawImage_c(const image_t * sprite, int x, int y, int clamp_color) {
    if(x < -sprite->w || x > W || y < -sprite->h || y > H)
        return;

    const int clip_left = (x < 0) ? -x : 0;
    const int clip_top = (y < 0) ? -y : 0;
    const int clip_right = M_MIN(W - x, sprite->w);
    const int clip_bottom = M_MIN(H - y, sprite->h);

    for(int j = clip_top; j < clip_bottom; j++) {
        u8 * src = sprite->pixels + clip_left + j * sprite->w;
        u8 * dst = GetBackbuffer() + x + clip_left + (y+j) * W;
        for(int i = clip_left; i < clip_right; i++) {
            const int c = *(src++) + *(dst);
            *(dst++) = c > clamp_color ? clamp_color : c;
        }
    }
}

void VerticalMirrorImage(image_t * img) {
    if( img->pixels ) {
        u8 * pix = img->pixels;
        for(int j = 0; j <img->h; j++) {
            for(int i = 0; i < img->w/2; i++) {
                u8 tmp = pix[img->w - 1 - i];
                pix[img->w - 1 - i] = pix[i];
                pix[i] = tmp; 
            }
            pix += img->w;
        }
    }
}

void HorizontalMirrorImage(image_t * img) {
    if( img->pixels ) {
        u8 * pix = img->pixels;
        u8 * pix2 = img->pixels + (img->h -1) * img->w;
        for(int j = 0; j < img->h/2; j++) {
            for(int i = 0; i < img->w; i++) {
                u8 tmp = pix2[i];
                pix2[i] = pix[i];
                pix[i] = tmp; 
            }
            pix += img->w;
            pix2 -= img->w;
        }
    }
}

/**
 * draw image with pixelate effect - the image is supposed to be smaller than
 * the framebuffer - hence no size check, no clipping...
 */
void DrawPixelateImage(const image_t * img, int x, int y, float factor) {
    float inverse_factor = 1.0f / factor;
    u8 * fb;
    u8 * video = GetBackbuffer() + x + y * W;
    int ys, xs; /* source coordinates */
    for(int j = 0; j < img->h; j++, video += W) {
        ys = (int)(j * inverse_factor + .5f) * factor;
        ys = img->w * (ys >= img->h ? img->h-1 : ys);
        fb = video;
        for(int i = 0; i < img->w; i++ ) {
            xs = (int)(i * inverse_factor + .5f) * factor;
            xs = (xs >= img->w) ? img->w-1 : xs;
            *(fb++) = img->pixels[xs + ys];
        }
    }
}

/* -------------------------------------------------------------------------
 * Image loading functions
 * ------------------------------------------------------------------------- */

#  if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
#   define BMP_SIGNATURE 0x4d42
#  else
#   define BMP_SIGNATURE 0x424d
#  endif

#define BMP_HEADER_SIZE 14 // size of bmp_header_t
typedef struct bmp_header_t {
	u16 signature;      // 'BM'
	u32 file_size;
	u32 reserved; 
	u32 data_offset;    // bitmap data offset from the beginning of the file
} bmp_header_t;

// Old version Window NT & 3.1x
typedef struct bmp_info_header_t {
	u32 info_size;          // depends on the file format version
	s32 width;
	s32 height;
	u16 planes;             // should be =1 
	u16 bits_per_pixel;
	u32 compression;        // 0=none, 1=RLE8, 2=RLE4
	u32 image_size;         // W * H * BPP or 0 if not compressed
	s32 x_pixels_per_meter;
	s32 y_pixels_per_meter;
	u32 color_used;         // first N color really used in the palette
	u32 important_colors;   // number of important colors. 0 = all 
} bmp_info_header_t; 

enum {
    BI_RAW = 0,
    BI_RLE8 = 1,
    BI_RLE4 = 2
};

void LoadBMP(image_t * img, palette_t * pal, const u8 * bmp_data, int bmp_size) {
    byte_reader_t reader, * f = &reader;
    bmp_header_t header;
    bmp_info_header_t info;

    InitBufferReader(&reader, bmp_data, bmp_size);

    /* parse bmp file header */
    header.signature = ReadU16(f);

    if(header.signature != BMP_SIGNATURE)
        FatalError("LoadBMP: invalid BMP signature = %d (expected %d)",
            header.signature, BMP_SIGNATURE);

    header.file_size = ReadU32(f);
    LogDebug("BMP: File size = %d (%Xh)\n", header.file_size, header.file_size);
    header.reserved =  ReadU32(f);
    header.data_offset = ReadU32(f);
    LogDebug("BMP: Data offset = %Xh\n", header.data_offset);
    
    /* parse bmp info header */
    info.info_size = ReadU32(f);

    info.width = ReadU32(f);
    info.height = ReadU32(f);
    info.planes = ReadU16(f);
    info.bits_per_pixel = ReadU16(f);
    info.compression = ReadU32(f);
    info.image_size = ReadU32(f);
    info.x_pixels_per_meter = ReadU32(f);
    info.y_pixels_per_meter = ReadU32(f);
    info.color_used = ReadU32(f);
    info.important_colors = ReadU32(f);
    if (info.important_colors == 0)
        info.important_colors = info.color_used; // all...

    char * comp = "(unknown compression)";
    switch (info.compression) {
        case BI_RAW: comp = "(RAW)"; break;
        case BI_RLE8: comp = "(RLE8)"; break;
        case BI_RLE4: comp = "(RLE4)"; break;
    }
    LogDebug("BMP: [%X] %dx%d %d-BPP %s", BMP_SIGNATURE, info.width, info.height, info.bits_per_pixel, comp);
    LogDebug("BMP: data size = %d, x_ppm = %d, y_ppm = %d, colors = %d, imp.cols = %d",
        info.image_size, info.x_pixels_per_meter, info.y_pixels_per_meter,
        info.color_used, info.important_colors);

    if (info.bits_per_pixel > 8 || info.bits_per_pixel < 4
        || (info.compression == BI_RLE8 && info.bits_per_pixel != 8)
        || (info.compression == BI_RLE4 && info.bits_per_pixel != 4)) {
        LogError("LoadBMP: BPP = %d", info.bits_per_pixel);
        LogError("LoadBMP: compression = %d", info.compression);
        FatalError("LoadBMP: unsupported format");
    }

    AllocImagePixels(img, info.width, info.height);
    img->color_count = info.color_used;

    SeekSet(f, header.data_offset);

    u8 v, c, r;
    u8 * dst = NULL;

    int row_size = ((info.bits_per_pixel * info.width + 31) >> 5) << 2;
    LogDebug("BMP: row_size = %d - color_used = %d", row_size, info.color_used);

    if (info.compression == BI_RAW) {
        for (int j = img->h; j > 0; j--) {
            dst = img->pixels + (j-1) * img->w; /* bottom to top */
            r = row_size;
            for (int i = 0; i < img->w; i++, r--) {
                v = ReadU8(f);
                if (info.bits_per_pixel == 4) {                       
                    *(dst++) = (v>>4)&0xF;
                    *(dst++) = v&0xF;
                    i++;
                } else { /* 8 BPP */
                    *(dst++) = v;
                }
            }
            SeekCur(f, r); // skip remaining row_size bytes (word aligned...)
        }
    } else if (info.compression == BI_RLE8 || info.compression == BI_RLE4) {
        int data_count = info.image_size;
        int row = img->h - 1;
        int col = 0;
        int position;

        dst = img->pixels + row*img->w; // bottom to top

        while (data_count > 0) {
            if (dst > (img->pixels + img->w * img->h)) {
                FatalError("LoadBMP: out of bound: %X (%X) - %d,%d\n", dst, img->pixels + img->w * img->h, col,row);
            }
            position = f->current - f->start;
            v = ReadU8(f);
            c = ReadU8(f);
            data_count -= 2;
            if (v > 0) {        // copy the next byte 'v' times
                // LogDebug("%08X  %3d,%3d,%6d: Expand Color %2X for %d bytes\n", position, col, row, data_count, c, v);
                col += v;
                if (info.compression == BI_RLE8) {
                    while (v--) {
                        *(dst++) = c;
                    }
                } else {
                    u8 hi = (c>>4) & 0xF;
                    u8 lo = c & 0xF;
                    bool pick_high = true; 
                    while (v--) {
                        *(dst++) = pick_high ? hi : lo;
                        pick_high = !pick_high;
                    }
                }
            } else {
                if (c == 0) { // end of line, switch to next row
                    // LogDebug("%08X  %3d,%3d,%6d: End of line\n", position, col, row, data_count);
                    col = 0;
                    row--;
                    dst = img->pixels + row*img->w;
                } else if (c == 1) { // end of image
                    // LogDebug("%08X  %3d,%3d,%6d: End of image\n", position, col, row, data_count);
                    break;
                } else if (c == 2) { // skip X columns and Y rows
                    u8 x = ReadU8(f);
                    u8 y = ReadU8(f);
                    // LogDebug("%08X  %3d,%3d,%6d: Skip X=%d and Y=%d\n", position, col, row, data_count, x, y);
                    data_count -= 2;
                    col += x;
                    row -= y;
                    dst = img->pixels + col + row*img->w;
                } else {
                    u8 byte;
                    int extra_byte;
                    if (info.compression == BI_RLE8)
                        extra_byte = c & 0x01;
                    else
                        extra_byte = !(((c-1)>>1)&0x1);
                    // LogDebug("%08X  %3d,%3d,%6d: Raw mode for %d bytes (+alignment=%d)\n", position, col, row, data_count, c, extra_byte);
                    col += c;
                    while (c && data_count > 0) {
                        byte = ReadU8(f);
                        data_count--;

                        if (info.compression == BI_RLE8) {
                            *(dst++) = byte;
                            c--;
                        } else {
                            *(dst++) = (byte>>4)&0xF;
                            *(dst++) = byte&0xF;
                            c -= 2;
                        }
                    }
                    if (extra_byte) {
                        ReadU8(f);
                        data_count--;
                    }
                }
            }
        }
    }

    /* decode the palette */
    int num_colors = 0;
    if (info.bits_per_pixel == 4)
        num_colors = 16;
    else if (info.bits_per_pixel == 8)
        num_colors = 256;

    LogDebug("BMP: loading %d colors at %Xh\n", num_colors, BMP_HEADER_SIZE + info.info_size);
    if (pal != NULL && info.bits_per_pixel <= 8) {
        SeekSet(f, BMP_HEADER_SIZE + info.info_size);
        for(int i = 0; i < num_colors; i++) {
            pal->colors[i].b = ReadU8(f);
            pal->colors[i].g = ReadU8(f);
            pal->colors[i].r = ReadU8(f);
            ReadU8(f); // skip reserved;
            pal->colors[i].a = 0xFF;
        }
    }
}

/* -------------------------------------------------------------------------
 * Sprite functions
 * ------------------------------------------------------------------------- */

/* Masked sprite works only if sprite background is ZERO (0) */
void GenerateSpriteMask(image_t * img) {
    if(img->masks) return;
    img->masks = (u8 *) MemAlloc(img->w * img->h * 2);
    u8 * msk = img->masks;
    u8 * src = img->pixels;
    for(int j = 0; j < img->h; j++) {
        for(int i = 0; i < img->w; i++) {
            *(msk++) = *(src++) ? 0x00 : 0xFF;
        }
    }
}

/* could be optimized using 64 bits and 128 bits data... */
void DrawSprite_c(const image_t * sprite, int x, int y) {
    if(x < -sprite->w || x > W || y < -sprite->h || y > H)
        return;

    const int clip_left = (x < 0) ? -x : 0;
    const int clip_top = (y < 0) ? -y : 0;
    const int clip_right = M_MIN(W - x, sprite->w);
    const int clip_bottom = M_MIN(H - y, sprite->h);

    const int width = clip_right - clip_left;
    const int words_count = width >> 2;

    /* good chance that if it is clipped on the left, it will not be clipped on the right ... */
    /* TODO : false asumption */
    const int remaining_bytes = clip_left&3 ? 0 : width - (width & 0xFFFFFC);

    u32 src_ofs = clip_left + clip_top * sprite->w;

    for(int j = clip_top; j < clip_bottom; j++, src_ofs += sprite->w)
    {
        u8 * src = sprite->pixels + src_ofs;
        u8 * msk = sprite->masks  + src_ofs;
        u8 * dst = GetBackbuffer() + x + clip_left + (y+j) * W;

        switch(clip_left&3) {
            case 1: *dst = *(src++) | (*dst & *(msk++)); dst++;
            case 2: *dst = *(src++) | (*dst & *(msk++)); dst++;
            case 3: *dst = *(src++) | (*dst & *(msk++)); dst++;
        }

        u32 * src32 = (u32 *) src;
        u32 * msk32 = (u32 *) msk;
        u32 * dst32 = (u32 *) dst;

        for(int i = words_count; i > 0; i--, src32++, dst32++, msk32++)
            *dst32 = (*dst32 & *msk32) | *src32 ;

        src = (u8 *) src32;
        msk = (u8 *) msk32;
        dst = (u8 *) dst32;

        switch(remaining_bytes) {
            case 3: *dst = *(src++) | (*dst & *(msk++)); dst++;
            case 2: *dst = *(src++) | (*dst & *(msk++)); dst++;
            case 1: *dst = *(src++) | (*dst & *(msk++)); dst++;
        }        
    }
}

void DrawSpriteRect_c(const image_t * sprite, int x, int y,  const rect_t * area)
{
    if(x < -area->w || x > W || y < -area->h || y > H)
        return;

    const int clip_left = (x < 0) ? -x : 0;
    const int clip_top = (y < 0) ? -y : 0;
    const int clip_right = M_MIN(W - x, area->w);
    const int clip_bottom = M_MIN(H - y, area->h);

    const int width = clip_right - clip_left;
    const int words_count = width >> 2;

    /* good chance that if it is clipped on the left, it will not be clipped on the right ... */
    const int remaining_bytes = clip_left&3 ? 0 : width - (width & 0xFFFFFC);

    u32 src_ofs = clip_left + area->x + (clip_top + area->y) * sprite->w;

    for(int j = clip_top; j < clip_bottom; j++, src_ofs += sprite->w) {
        u8 * src = sprite->pixels + src_ofs;
        u8 * msk = sprite->masks  + src_ofs;
        u8 * dst = GetBackbuffer() + x + clip_left + ( y + j) * W;

        switch(clip_left&3) {
            case 1: *dst = *(src++) | (*dst & *(msk++)); dst++;
            case 2: *dst = *(src++) | (*dst & *(msk++)); dst++;
            case 3: *dst = *(src++) | (*dst & *(msk++)); dst++;
        }

        u32 * src32 = (u32 *) src;
        u32 * msk32 = (u32 *) msk;
        u32 * dst32 = (u32 *) dst;

        for (int i = words_count; i > 0; i--, src32++, dst32++, msk32++)
            *dst32 = (*dst32 & *msk32) | *src32 ;

        src = (u8 *) src32;
        msk = (u8 *) msk32;
        dst = (u8 *) dst32;

        switch(remaining_bytes) {
            case 3: *dst = *(src++) | (*dst & *(msk++)); dst++;
            case 2: *dst = *(src++) | (*dst & *(msk++)); dst++;
            case 1: *dst = *(src++) | (*dst & *(msk++)); dst++;
        }        
    }
}

/* vertical and horizontal radius are deduced from the image size */
void GenerateQuadraticSprite(u8 * pixels, int w, int h, int color_range, int spot_radius, int smoothing_size) {
    float a = (float) w * .5f;
    float b = (float) h * .5f;

    float radius_max = fmin(a, b);

    for(int j = 0; j < h; j++) {
        
        float y = (float) j - b;
        float yy = y * y;

        for(int i = 0; i < w; i++) {
            float x = (float) i - a;;
            float xx = x * x;

            float radius = sqrt(xx + yy);

            int c = color_range;
            
            if( radius >= spot_radius + smoothing_size) {
                c = color_range * (1 - (radius / radius_max));
            }
            else if( radius >= spot_radius ) {               
                float smoothing_radius =
                    IntLerp(0, spot_radius + smoothing_size,
                        (radius - spot_radius) / smoothing_size);

                c = color_range * (1 - smoothing_radius / radius_max);
            }

            if(c > color_range)
                c = color_range;
            else if( c < 0 )
                c = 0;

            *(pixels++) = c;
        }
    }
}

# endif /* X_IMPLEMENTATION */

#endif /* X86_IMAGE_H */
