/**
 * samples/bobs/main.c
 * 
 * Copyleft (c) Pierre-Jean Turpeau 12/2021
 * <pierrejean AT turpeau DOT net>
 */
#define X_IMPLEMENTATION 1

#define TITLE "-[ bobs ]-"

#include <x86/x.h>
#include <x86/image.h>
#include <x86/math.h> 
#include <x86/extras.h>

static image_t ball;

#define W4 (W/4 - 8)
#define H4 (H/4 - 8)

#define NMAX 4096
s32 sine_lut_x[NMAX];
s32 sine_lut_y[NMAX];

static int var = 0;
static int count = 1;
static s32 x1inc;
static s32 y1inc;

static void InitBobs() {
    ConfigureIntSineLUT(sine_lut_x, NMAX, W4, W4);
    ConfigureIntSineLUT(sine_lut_y, NMAX, H4, H4);

    x1inc = GetIntRandomValue(16, 32);
    y1inc = GetIntRandomValue(16, 32);
    count = 1;
}

static void UpdateBobs(float elapsed_time) {
    ClearScreen(0);

    elapsed_time *= 0.002;

    /* wait a little bit before displaying something */
    if( var++ > 50 ) {

        /* start_time display bobs */
        if(count++ > 1024)
            count = 1024; /* limit the number of bobs */

        int x1 = var * x1inc;
        int y1 = var * y1inc;
        int x2 = var * 22;        
        int y2 = var * 38;

        for (int i = 0; i < count; i++) {
            DrawSprite_c(&ball,
                sine_lut_x[x1 % NMAX] + sine_lut_x[x2 % NMAX],
                sine_lut_y[y1 % NMAX] + sine_lut_y[y2 % NMAX]);

            x1 += 57;
            y1 += 29;

            y2 += 43;
            x2 += 38;
        }
    } else
        count = 1;
}

int main(int argc, char **argv) {
    InitPlatform(argc, (const char **) argv);
    ParseDefaultExeOptions();
    Console("\tpress [SPACE] to reset the bobs\n");
    Pause(2000);
    SetVGAMode();

    AllocImagePixels(&ball, 32, 32);
    GenerateQuadraticSprite(ball.pixels, 32, 32, 128, 3, 3);
    GenerateSpriteMask(&ball);

    SetColorPalette(0, 0x008E8E);

    v3f ambiant_color = {{0.3, 0.1, 0.1}};
	v3f diffuse_color = {{1., 0, 0}};
	v3f specular_color = {{1., 0.9, 0.9}};
    LightingGradientPalette(1, 128, ambiant_color, diffuse_color, specular_color, 10);

    InitBobs();

    while (! IsPlatformExit()) {
        BeginFrameLoop();
        ExecuteDefaultInteractions();

        if (GetLastEvent() == ' ')
            InitBobs(); /* reset parameters */

        if (!IsPaused())
            UpdateBobs(GetTime());

        EndFrameLoop();
    };

    ClosePlatform();

    Console("fps = %d", x86.Time.fps);
    return EXIT_SUCCESS;
}
