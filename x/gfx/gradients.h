/**
 * x/gfx/palette.h
 * 
 * Copyleft (c) Pierre-Jean Turpeau 11/2019
 * <pierrejean AT turpeau DOT net>
 */
#ifndef X_GFX_PALETTE_H
#define X_GFX_PALETTE_H

# include <x/x.h>
# include <x/math/v3f.h>

/* ---[ DEFINITIONS ]------------------------------------------------------- */

/* ---[ DECLARATIONS ]------------------------------------------------------ */
  
/**
 * from https://www.iquilezles.org/www/articles/palettes/palettes.htm 
 * 
 * params[0] = bias
 * params[1] = scale
 * params[2] = oscillations
 * params[3] = phase
 * 
 * Original operation:
 * a, b, c, & d are rgb vectors, t is time in 0..1 range
 * return a + b*cosf( 6.28318*(c*t+d) );
 */
extern void trigonometric_gradient(int from_index, int to_index, v3f params[4]);

/**
 * inspired by
 * http://www.c-jump.com/bcc/common/Talk3/OpenGL/Wk06_light/Wk06_light.html
 */
extern void lighting_gradient(int from_index, int to_index, v3f ambiant_color, v3f diffuse_color, v3f specular_color, float shininess);

/**
 * take the 'count' colors from 'start' and darken them to black according
 * to the requested 'levels'.
 */
extern void darken_palette_to_black(int start, int count, int levels);

/* ---[ IMPLEMENTATION ]---------------------------------------------------- */

# ifdef X_IMPLEMENTATION

#  include <math.h>

void trigonometric_gradient(int from_index, int to_index, v3f params[4])
{
    pix32 color;
    for(int i = from_index; i <= to_index; i++)
    {
        float t = (float) (i - from_index) / (float) (to_index - from_index);

        color.r = 255 * (params[0].r + params[1].r * cosf( M_TAU * (params[2].r * t + params[3].r)));
        color.g = 255 * (params[0].g + params[1].g * cosf( M_TAU * (params[2].g * t + params[3].g)));
        color.b = 255 * (params[0].b + params[1].b * cosf( M_TAU * (params[2].b * t + params[3].b)));

        set_color(i, color.v);
    }
}

/**
 * inspired by
 * http://www.c-jump.com/bcc/common/Talk3/OpenGL/Wk06_light/Wk06_light.html
 *
 * The ambiant_color is the base color on which diffuse_color and specular_color
 * are added relative to the color index into the given from-to range.
 * 
 * On the given range:
 *  - t = color index normalized to [0.0 - 1.0]
 *  - d = diffuse_color * sine(c)
 *  - s = specular_color * ( sine(c) ^ shininess )
 *  - output color = ambiant_color + d + s
 */
void lighting_gradient(int from_index, int to_index, v3f ambiant_color, v3f diffuse_color, v3f specular_color, float shininess)
{
    for(int i = from_index; i <= to_index; i++) {
        v3f color, specular;

        /* linear intensity as [0.0 - 1.0] in the given palette range */
        float t = (float) (i - from_index) / (float) (to_index - from_index);
        
        /* diffuse intensity level thanks to a sine curve */
        float diffuse_intensity = sinf(t * M_PI_2);
        Vector3fMul(&color, &diffuse_color, diffuse_intensity);

        /* simplified specular intensity based on the diffuse intensity */
        float specular_intensity = pow(diffuse_intensity, shininess);
        Vector3fMul(&specular, &specular_color, specular_intensity);
        Vector3fAdd(&color, &color, &specular);

        /* and now add the ambiant color */
        Vector3fAdd(&color, &color, &ambiant_color);

        /* clamp as needed */
        pix32 p;
        p.r = M_MIN(color.r, 1.f) * 255.f;
        p.g = M_MIN(color.g, 1.f) * 255.f;
        p.b = M_MIN(color.b, 1.f) * 255.f;

        set_color(i, p.v);
    }
}

void darken_palette_to_black(int start, int count, int levels) {
    assert((start + count - 1) < MAX_COLORS);
    assert((levels * count - 1) < MAX_COLORS);

    pix32 new_color;
    for(int j = 1; j < levels; j++) {
        float factor = ((float)(levels - j)) / (float) levels;
        for( int i = start; i < start + count; i++) {
            new_color.r = video.palette->colors[i].r * factor;
            new_color.g = video.palette->colors[i].g * factor;
            new_color.b = video.palette->colors[i].b * factor;
            video.palette->colors[i + count * j].v = new_color.v;
        }
    }
}

# endif /* X_IMPLEMENTATION */

#endif /* X_GFX_PALETTE_H */
