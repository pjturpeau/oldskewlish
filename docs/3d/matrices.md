# Matrices

## Association & commutation

Matrix multiplication is associative, which means: `ABC = (AB)C = A(BC)`

Matrix multiplication is generally anticommutative, which means: `ABC <> CBA` (in particular for rotations: *move forward 2 steps, then turn left* __is different from__ *turn left, then move forward 2 steps*)

[<img src="not-commutative-rotations.png" width="200"/>](not-commutative-rotation.png)

[Picture source: Benjamin Crowell, General Relativity, p. 256.](http://www.lightandmatter.com/genrel/)

## Post or pre-multiply for vectors

You have to decide wether you'll use pre or post multiply when tranforming a vector. This has an impact on the way you'll define the matrices.

Writing `p = Matrix * Vector` means `Vector` is pre-multiplied by `Matrix`.

## Language representation

When using two dimensional arrays, it's important to know if the programming language is in row-major or column-major mode.

The C language is row major, which mean the first dimension is for rows and the second dimension is for columns, eg. `float matrix[ROWS][COLS]`.

```
    float m[3][3];

    is equivalent to this following matrix:

    | m[0][0]  m[0][1]  m[0][2] | /* row 0 */
    | m[1][0]  m[1][1]  m[1][2] | /* row 1 */
    | m[2][0]  m[2][1]  m[2][2] | /* row 2 */
```

## Rotations order

Probable: yaw -> pitch -> roll (to be confirmed)

## Transformations order of a given object

Scale, then Rotate, then Translate : `Trasnform = T * R * S`
