
set(CMAKE_C_FLAGS "-Wall -pedantic") # -march=native -ffast-math -fno-math-errno")
set(CMAKE_C_FLAGS_DEBUG "-O0 -g -fverbose-asm")
set(CMAKE_C_FLAGS_RELEASE "-O3 -fomit-frame-pointer")

# initialize debug flag
string( TOLOWER "${CMAKE_BUILD_TYPE}" CMK_DEBUG_TYPE_LOWER )
if (CMK_DEBUG_TYPE_LOWER STREQUAL "debug")
    set(DEBUG_ACTIVATED true)
endif ()

# to avoid problems with paths containing spaces under MS Windows
set(CMAKE_RC_COMPILER "windres")

# construct the name of the executable based on the compilation platform
# math( EXPR BITS "8*${CMAKE_SIZEOF_VOID_P}" )
math( EXPR BITS "8*8" )
set( EXECUTABLE_NAME ${PROJECT_NAME}-x${BITS} )

add_executable( ${EXECUTABLE_NAME} ${SOURCES} ${HEADERS} ${RESOURCES} )

# could be replaced with target independent include_directories()
target_include_directories( ${EXECUTABLE_NAME}
    PUBLIC .
    PUBLIC .. 
    PUBLIC ../externals
)

# Math Library
if(NOT WIN32)
    find_library(MATH_LIBRARY m)
    if (NOT MATH_LIBRARY)
        set(MATH_LIBRARY "")
    endif()
    target_link_libraries(${EXECUTABLE_NAME} ${MATH_LIBRARY})
endif()

# SDL2 Library
find_package(SDL2 REQUIRED)
string(REPLACE "include/SDL2" "include" SDL2_INCLUDE_DIRS ${SDL2_INCLUDE_DIRS})
include_directories("${SDL2_INCLUDE_DIRS}")
target_link_libraries(${EXECUTABLE_NAME} ${SDL2_LIBRARIES})

# Additional WIN32 settings
if(WIN32)
    if(DEBUG_ACTIVATED)
        # message("++ Output console enabled!")
    else()
        # message("++ Output console disabled!")
        # this disable cout/cerr in Release mode
        set_property(TARGET ${EXECUTABLE_NAME} APPEND_STRING PROPERTY LINK_FLAGS "-mwindows")
    endif()

    target_compile_definitions( ${EXECUTABLE_NAME} PUBLIC WIN32)
	
	## may need to add ole32, oleaut32, imm32, winmm and version (for SDL2 static linking..)
    # target_link_libraries(${EXECUTABLE_NAME} mingw32 ws2_32)
endif()

# Additional MINGW settings
if(MINGW)
    target_link_libraries(${EXECUTABLE_NAME} mingw32 ws2_32)
endif()
