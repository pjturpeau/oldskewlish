/**
 * Pierre-Jean Turpeau 02/02/2020
 * 
 * gcc -o bin2c bin2c.C
 * 
 * bin2c name < file.bin > header.h
 *
 * Derived from one of my grist https://gitlab.com/-/snippets/2092200
 * Updated to make it compile using Open Watcom 2.0
 */

#include <stdlib.h>
#include <stdio.h>

#ifdef _WIN32
# include <io.h>
# include <fcntl.h>
# define SET_BINARY_MODE(handle) setmode(handle, O_BINARY)
#else
# define SET_BINARY_MODE(handle) ((void)0)
#endif

int main(int argc, char *argv[])
{
    if(argc < 2) {
        fprintf(stderr, "missing name parameter");
        return EXIT_FAILURE;
    }

    /* Prepare the standard input for reading binary data. */
    freopen(NULL, "rb", stdin);
    SET_BINARY_MODE(STDIN_FILENO);

    for(int i=0; argv[1][i] != 0; i++) {
        switch(argv[1][i]) {
            case '-':
                argv[1][i] = '_';
        }
    }

    printf("/*\n");
    printf("extern unsigned char %s_data[];\n", argv[1]);
    // printf("extern unsigned int  %s_end;\n", argv[1]);
    printf("extern unsigned int  %s_size;\n", argv[1]);
    printf("*/\n\n");

    printf("const unsigned char %s_data[] = {\n", argv[1]);

    int c = getchar();
    int bytes = 1;

    do {
        printf("    ");
        do {
            if( c != EOF)
                printf("0x%02x", c);
            c = getchar();
            if( c != EOF)
                printf(",");
            if ((bytes++) % 15 == 0)
                break;
        }
        while(c != EOF);
        printf("\n");
    } while (c != EOF);

    printf("    };\n\n");
    // the following line is not compatible with x64 architectures
    // printf("const unsigned int %s_end = (unsigned int)(%s_data + %d);\n", argv[1], argv[1], bytes);
    printf("const unsigned int %s_size = %d;\n", argv[1], bytes);
    return EXIT_SUCCESS;
}
