/**
 * samples/noise/white_noise.c
 * 
 * Copyleft (c) Pierre-Jean Turpeau 11/2019
 * <pierrejean AT turpeau DOT net>
 * 
 * Work best with monochrome palette
 */
#define X_IMPLEMENTATION 1

#define TITLE ".%%( white noise )%%."

#include <x86/x.h>
#include <x86/image.h>

extern unsigned char top_left_data[];
extern unsigned int  top_left_size;

image_t top_left_bmp;
image_t top_right_bmp;
image_t bottom_left_bmp;
image_t bottom_right_bmp;

/** Fill the image width random points */
void InitWhiteNoise() {
    SeedRandomUInt8Buffer(GetBackbuffer(), W*H, 0, 255);
}

/* ripped from TinyPTC by Gaffer */
void draw_whitenoise2() {
    int noise, carry;

    static int seed = 0x1234;

    u8 * video = GetBackbuffer();

    for(int i = 0; i < W*H; i++) {
        noise = seed;
        noise >>= 3;
        noise ^= seed;
        carry = noise & 1;
        noise >>= 1;
        seed >>= 1;
        seed |= (carry << 30);
        noise &= 0xFF;
        *(video++)= (noise<<16) | (noise<<8) | noise;        
    }
}

void draw_whitenoise() {
    u8 * video = GetBackbuffer();
    for (int j = 0; j < H; j++) {
        u8 *row1;

        if ( j == 0 )
            row1 = video + W * (H - 1);
        else
            row1 = video + W * (j - 1);
            
        u8 *row2 = video + W * j;
        u8 *row3 = video + W * (j + 1) % (W *H);

        *row1++ = 0;
        *row2++ = 0;
        *row3++ = 0;

        for (int i = 1; i < W - 1; i++)
        {
            int Color = (*row1 + *row3 + *(row2 - 1) + *(row1 + 1)) >> 1;

            if (Color < 0) Color = 0;

            *row2 = Color;

            row1++;
            row2++;
            row3++;
        }

        *row1++ = 0;
        *row2++ = 0;
        *row3++ = 0;
    }
}

void rendering_func() {
    static int one_or_two = 0;
    
    !one_or_two ? draw_whitenoise() : draw_whitenoise2();
    
    if(GetLastEvent() == ' ') one_or_two ^= 1;

    DrawSprite_c(&top_left_bmp, 0, 0);
    DrawSprite_c(&top_right_bmp, W - top_right_bmp.w, 0);
    DrawSprite_c(&bottom_left_bmp, 0, H - bottom_left_bmp.h);
    DrawSprite_c(&bottom_right_bmp, W - top_right_bmp.w, H - bottom_left_bmp.h);
}

int main(int argc, char ** argv) {
    InitPlatform(argc, (const char **) argv);
    ParseDefaultExeOptions();

    Console("\tpress [SPACE] to switch noise algorithm\n");
    Pause(2000);
    SetVGAMode();

    InitWhiteNoise();
    LinearGradientPalette(0, 0, 255, 0xFFFFFF);

    LoadBMP(&top_left_bmp, NULL, top_left_data, top_left_size);
    DuplicateImage(&top_right_bmp, &top_left_bmp);
    VerticalMirrorImage(&top_right_bmp);

    DuplicateImage(&bottom_left_bmp, &top_left_bmp);
    HorizontalMirrorImage(&bottom_left_bmp);

    DuplicateImage(&bottom_right_bmp, &top_right_bmp);
    HorizontalMirrorImage(&bottom_right_bmp);

    GenerateSpriteMask(&top_left_bmp);
    GenerateSpriteMask(&top_right_bmp);
    GenerateSpriteMask(&bottom_left_bmp);
    GenerateSpriteMask(&bottom_right_bmp);

    while(!IsPlatformExit()) {
        BeginFrameLoop();
        ExecuteDefaultInteractions();

        if (! IsPaused()) {
            rendering_func();
        }

        EndFrameLoop();
    };

    FreeImageData(&top_left_bmp);
    FreeImageData(&top_right_bmp);
    FreeImageData(&bottom_left_bmp);
    FreeImageData(&bottom_right_bmp);

    ClosePlatform();

    Console("fps = %d", x86.Time.fps);
    return 0;
}
