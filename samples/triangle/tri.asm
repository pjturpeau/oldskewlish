; Triangle Fill Routine for 80486 CPU with Fixed-Point Arithmetic and Loop Unrolling
; Input:
;   esi = pointer to framebuffer
;   eax = color (8-bit)
;   (x0, y0), (x1, y1), (x2, y2) = triangle vertices
;   ebx = framebuffer width (in pixels)

section .data
    ; Temporary variables for storing triangle vertices
    x0 dd 0
    y0 dd 0
    x1 dd 0
    y1 dd 0
    x2 dd 0
    y2 dd 0

    ; Temporary variables for storing slopes (fixed-point)
    dx1 dd 0
    dx2 dd 0
    dx3 dd 0

    ; Temporary variables for scanline drawing (fixed-point)
    x_start dd 0
    x_end dd 0

    ; Fixed-point scaling factor (16.16 format)
    FP_SCALE equ 1 << 16

section .text
    global _start

_start:
    ; Load triangle vertices into memory
    mov [x0], x0_value  ; Store x0
    mov [y0], y0_value  ; Store y0
    mov [x1], x1_value  ; Store x1
    mov [y1], y1_value  ; Store y1
    mov [x2], x2_value  ; Store x2
    mov [y2], y2_value  ; Store y2

    ; Sort vertices by y-coordinate (y0 <= y1 <= y2)
    ; This ensures the triangle is drawn from top to bottom
    mov ecx, [y0]
    cmp ecx, [y1]
    jle .skip_swap1      ; If y0 <= y1, skip swap
    xchg [x0], [x1]      ; Swap x0 and x1
    xchg [y0], [y1]      ; Swap y0 and y1
.skip_swap1:
    mov ecx, [y1]
    cmp ecx, [y2]
    jle .skip_swap2      ; If y1 <= y2, skip swap
    xchg [x1], [x2]      ; Swap x1 and x2
    xchg [y1], [y2]      ; Swap y1 and y2
.skip_swap2:
    mov ecx, [y0]
    cmp ecx, [y1]
    jle .skip_swap3      ; If y0 <= y1, skip swap
    xchg [x0], [x1]      ; Swap x0 and x1
    xchg [y0], [y1]      ; Swap y0 and y1
.skip_swap3:

    ; Calculate slopes using fixed-point arithmetic
    ; dx1 = ((x1 - x0) << FP_SCALE) / (y1 - y0)
    ; dx2 = ((x2 - x0) << FP_SCALE) / (y2 - y0)
    ; dx3 = ((x2 - x1) << FP_SCALE) / (y2 - y1)

    ; Calculate dx1
    mov ecx, [y1]
    sub ecx, [y0]        ; ecx = y1 - y0
    jz .skip_dx1         ; If y1 == y0, skip (avoid division by zero)
    mov eax, [x1]
    sub eax, [x0]        ; eax = x1 - x0
    shl eax, 16          ; Convert to fixed-point (eax = (x1 - x0) << 16)
    cdq                  ; Sign-extend eax into edx for division
    idiv ecx             ; eax = ((x1 - x0) << 16) / (y1 - y0)
    mov [dx1], eax       ; Store dx1
.skip_dx1:

    ; Calculate dx2
    mov ecx, [y2]
    sub ecx, [y0]        ; ecx = y2 - y0
    jz .skip_dx2         ; If y2 == y0, skip (avoid division by zero)
    mov eax, [x2]
    sub eax, [x0]        ; eax = x2 - x0
    shl eax, 16          ; Convert to fixed-point (eax = (x2 - x0) << 16)
    cdq                  ; Sign-extend eax into edx for division
    idiv ecx             ; eax = ((x2 - x0) << 16) / (y2 - y0)
    mov [dx2], eax       ; Store dx2
.skip_dx2:

    ; Calculate dx3
    mov ecx, [y2]
    sub ecx, [y1]        ; ecx = y2 - y1
    jz .skip_dx3         ; If y2 == y1, skip (avoid division by zero)
    mov eax, [x2]
    sub eax, [x1]        ; eax = x2 - x1
    shl eax, 16          ; Convert to fixed-point (eax = (x2 - x1) << 16)
    cdq                  ; Sign-extend eax into edx for division
    idiv ecx             ; eax = ((x2 - x1) << 16) / (y2 - y1)
    mov [dx3], eax       ; Store dx3
.skip_dx3:

    ; Initialize x_start and x_end with x0 (in fixed-point)
    mov eax, [x0]
    shl eax, 16          ; Convert x0 to fixed-point
    mov [x_start], eax   ; x_start = x0 << 16
    mov [x_end], eax     ; x_end = x0 << 16

    ; Loop over scanlines from y0 to y2
    mov ecx, [y0]        ; ecx = current y-coordinate (starts at y0)
.scanline_loop:
    cmp ecx, [y1]
    jge .second_half     ; If current y >= y1, switch to second half of the triangle

    ; First half of the triangle (y0 to y1)
    mov eax, [x_start]
    add eax, [dx1]       ; x_start += dx1 (fixed-point addition)
    mov [x_start], eax

    mov eax, [x_end]
    add eax, [dx2]       ; x_end += dx2 (fixed-point addition)
    mov [x_end], eax

    jmp .draw_scanline

.second_half:
    ; Second half of the triangle (y1 to y2)
    mov eax, [x_start]
    add eax, [dx3]       ; x_start += dx3 (fixed-point addition)
    mov [x_start], eax

    mov eax, [x_end]
    add eax, [dx2]       ; x_end += dx2 (fixed-point addition)
    mov [x_end], eax

.draw_scanline:
    ; Draw the current scanline
    mov edi, ecx         ; edi = current y-coordinate

    ; replace imul edi, ebx     furthermore, ebx is destroyed by the 32bits color pattern later on
    shl edi, 8
    shl ecx, 6
    add edi, ecx         ; edi = y * framebuffer width (to get the row offset)
    add edi, esi         ; edi = pointer to the start of the current scanline in the framebuffer

    ; Convert fixed-point x_start and x_end back to integers
    mov eax, [x_start]
    shr eax, 16          ; eax = x_start >> 16 (convert to integer)
    mov edx, [x_end]
    shr edx, 16          ; edx = x_end >> 16 (convert to integer)

    ; Ensure x_start <= x_end
    cmp eax, edx
    jg .swap_x           ; If x_start > x_end, swap them
    jmp .draw_pixels
.swap_x:
    xchg eax, edx        ; Swap x_start and x_end
.draw_pixels:
    mov ecx, edx
    sub ecx, eax         ; ecx = number of pixels to draw (x_end - x_start)
    jz .next_scanline    ; If no pixels to draw, skip to the next scanline
    add edi, eax         ; edi = pointer to the start of the pixel row

    ; TODO: compute the 32bit color pattern outside the scanline loop...

    ; Unrolled loop: Process 4 pixels at a time
    mov al, color        ; al = color to fill
    mov ah, al           ; ah = color (for faster copying)
    mov bx, ax           ; bx = color (16-bit)
    shl eax, 16          ; eax = color (32-bit)
    mov ax, bx           ; eax = color (32-bit, duplicated)

.unrolled_loop:
    cmp ecx, 4           ; Check if there are at least 4 pixels left
    jl .cleanup          ; If fewer than 4 pixels, jump to cleanup

    ; Write 4 pixels at once
    mov [edi], eax       ; Write 4 pixels
    add edi, 4           ; Move pointer forward by 4 pixels
    sub ecx, 4           ; Decrement pixel count by 4
    jmp .unrolled_loop   ; Repeat

.cleanup:
    ; Handle remaining pixels (less than 4)
    rep stosb            ; Fill remaining pixels with color

.next_scanline:
    inc ecx              ; Move to the next scanline (y++)
    cmp ecx, [y2]
    jle .scanline_loop   ; If y <= y2, continue looping

    ; Exit
    ret