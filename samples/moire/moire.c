/**
 * samples/moire/moire.c
 * 
 * Copyleft (c) Pierre-Jean Turpeau 11/2019
 * <pierrejean AT turpeau DOT net>
 */
#define X_IMPLEMENTATION 1

#define TITLE "-[ moire' ]-"

#include <x86/x.h>

#include <math.h>

/* ---[ DEFINITIONS ]------------------------------------------------------- */

int cx1, cy1;
int cx2, cy2;

static bool flat = true;

/* ---[ IMPLEMENTATION ]---------------------------------------------------- */

void UpdateMoire(float elapsed_time) {
    elapsed_time *= 0.002;
    cx1 = sin(elapsed_time / 2) * W / 3 + W / 2;
    cy1 = sin(elapsed_time / 4) * H / 3 + H / 2;
    cx2 = cos(elapsed_time / 2) * W / 3 + W / 2;
    cy2 = cos(elapsed_time / 1) * H / 3 + H / 2;
}

void DrawMoire() {
    u8 * video = GetBackbuffer();

    for( int j = 0; j < H; j++) {

        int dy1 = (j - cy1);
        dy1 *= dy1;

        int dy2 = (j - cy2);
        dy2 *= dy2;

        for(int i = 0; i < W; i++) {

            int dx1 = (i - cx1);
            dx1 *= dx1;

            int dx2 = (i - cx2);
            dx2 *= dx2;

            int value;

            if( flat ) {
                /* TODO: optimize with integer square roots */
                dx1 = (int)sqrt(dx1 + dy1);
                dx2 = (int)sqrt(dx2 + dy2);
                value = ((dx1 ^ dx2) >> 4 ) & 1;
            } else {        
                /* interesting results */
                float one = sqrt(dx1 + dy1);
                float two = sqrt(dx2 + dy2);
                value = 8 + 8 * (sin(one / 4) + sin(two / 3)) / 2;
            }
            *(video++) = value;
        }
    }
}

int main(int argc, char **argv) {
    InitPlatform(argc, (const char **) argv);
    ParseDefaultExeOptions();

    Console("\tpress [SPACE] to display gradient version\n");
    Pause(2000);

    SetVGAMode();

    SetColorPalette(0, 0);
    SetColorPalette(1, 0xFFFFFF);

    while (! IsPlatformExit()) {
        BeginFrameLoop();
        ExecuteDefaultInteractions();
        if (GetLastEvent() == ' ') {
            flat = ! flat;
            if (flat) {
                SetColorPalette(0, 0);
                SetColorPalette(1, 0xFFFFFF);
            }  else
                LinearGradientPalette(0, 0x000000, 16, 0xFFFFFF);
        }

        if (!IsPaused()) {
            UpdateMoire(GetTime());
            DrawMoire();
        }
        EndFrameLoop();
    };

    ClosePlatform();

    Console("fps = %d", x86.Time.fps);
    return EXIT_SUCCESS;
}
