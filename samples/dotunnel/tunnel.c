/**
 * samples/dots-tunnel/tunnel.c
 * 
 * Copyleft (c) Pierre-Jean Turpeau 11/2019
 * <pierrejean AT turpeau DOT net>
 */
#define X_IMPLEMENTATION 1

#define TITLE "-[ dot tunnel ]-"

#include <x86/x.h>

#include <math.h>

#define CIRCLE_COUNT 64

#define CIRCLE_DISPLAY_DOTS_COUNT 48
#define CIRCLE_RESOLUTION_FACTOR 8 /* used to simulate rotation */ 
#define CIRCLE_DOTS_COUNT (CIRCLE_DISPLAY_DOTS_COUNT*CIRCLE_RESOLUTION_FACTOR)

#define CIRCLE_RADIUS 128
#define TOTAL_DEPTH 512 /* Z value for the far most circle */

/* x_proj = x * fd / z   with   fd = 1 / tan(fov/2) */
#define FOCAL_DISTANCE 64.0f 

/* tables of points for one circle */
int circle_px[CIRCLE_DOTS_COUNT];
int circle_py[CIRCLE_DOTS_COUNT];

float animation_angle;

/* center points for each circle */
v3s center_array[CIRCLE_COUNT]; 

/* the index of the far most circle in the tunnel array
 * the one from which to start_time drawing (painter algorithm) */
int far_most_circle;


void DrawCircleOutline(int x, int y, int z, int circle_index)
{   
    int color;

    /* reminder:
     *   x_proj = x * FOCAL_DISTANCE / z
     *   y_proj = y * FOCAL_DISTANCE / z
     */
    float fdz = FOCAL_DISTANCE / z; /* do the divide once */

    int advance_angle = animation_angle * 64;

    for(int i = 0; i < CIRCLE_DISPLAY_DOTS_COUNT; i++)
    {
        /* compute the point index according to the angle advance to
         * simulate a rotation of the circle */
        int idx = (advance_angle + i * CIRCLE_RESOLUTION_FACTOR)
                    /* modulo the circle */
                    % (CIRCLE_DOTS_COUNT);

        if( (i + circle_index) % 4 > 1 )
            color = 256 - z * 128 / TOTAL_DEPTH;
        else
            color = 128 - z * 128 / TOTAL_DEPTH;

        /* projection of the circle point (cx,cy) relative to its
         * (x,y,z) center in the 3D space and translate the result
         * according to the screen center */
        int px = (x + circle_px[idx])*fdz + W/2;
        int py = (y + circle_py[idx])*fdz + H/2;

        /* skip out of bound points */
        if(px+1 >= W || px < 0 || py < 0 || py+1 >= H)
            continue;

        
        SetPixel( px,   py,   color );
        SetPixel( px+1, py,   color );
        SetPixel( px,   py+1, color );
        SetPixel( px+1, py+1, color );
    }
}

void InitTunnel()
{
    animation_angle = 0.0f;
    far_most_circle = 0;
 
    /* compute points relative positions for one circle */
    for(int i = 0; i < CIRCLE_DOTS_COUNT; i++)
    {
        /* compute radian angle */
        double angle = 2.0 * M_PI / (CIRCLE_DOTS_COUNT) * i; 
        
        circle_px[i] = CIRCLE_RADIUS * cos(angle);
        circle_py[i] = CIRCLE_RADIUS * sin(angle);
    }

    /* compute Z positions for each circle */
    for(int j = 0; j < CIRCLE_COUNT; j++)
    {
        center_array[j].x = 0;

        /* put initial circles out of the field of view to make them appear gracefully */
        center_array[j].y = TOTAL_DEPTH * FOCAL_DISTANCE;
        center_array[j].z = TOTAL_DEPTH * j / CIRCLE_COUNT;
    }
}

void UpdateTunnel(float elapsed_time)
{
    animation_angle += 0.04;

    for (int i = 0; i < CIRCLE_COUNT; i++)
    {
        center_array[i].z -= 4;

        /* throw the near most circle to the far most Z */
        if( center_array[i].z < 4 )
        {
            center_array[i].z += TOTAL_DEPTH ;

            center_array[i].x = W * 96/128 * cos(animation_angle-0.25) +
                W * 32/128 * sin(animation_angle*2);

            center_array[i].y = H * 48/128  * sin(animation_angle+0.25) -
                H * 64/128 * cos(animation_angle+0.75);

            /* once a circle has been thrown to the back, special action
             * is needed to correctly manage the tunnel array limit. */
            far_most_circle++;
            if( far_most_circle >= CIRCLE_COUNT )
            {
                far_most_circle = 0;
            }
        }
    }
}

void DrawTunnel()
{
    ClearScreen(0);

    /* need to be fine tuned according to: depth, radius, etc. */
    float b = animation_angle+1.25;

    int x = W *96/128*cos(b-0.25) + W *32/128*sin(b *2);
    int y = H *48/128*sin(b+0.25) - H *64/128*cos(b+0.75);

    /* painter algorithm: start_time by the far most circle */
    for(int i = far_most_circle-1; i >= 0; i--)
        DrawCircleOutline(center_array[i].x-x, center_array[i].y-y, center_array[i].z, i);

    for(int i = CIRCLE_COUNT-1; i > far_most_circle - 1 ; i--)
        DrawCircleOutline(center_array[i].x-x, center_array[i].y-y, center_array[i].z, i);
}

int main(int argc, char ** argv) {
    InitPlatform(argc, (const char **) argv);
    ParseDefaultExeOptions();

    SetVGAMode();

    InitTunnel();

    LinearGradientPalette(0, 0x0, 127, 0x00FFFF00);
    LinearGradientPalette(128, 0x0, 255, 0x000020FF);

    while(!IsPlatformExit()) {
        BeginFrameLoop();
        ExecuteDefaultInteractions();

        if (! IsPaused()) {
            UpdateTunnel(GetTime());
            DrawTunnel();
        }

        EndFrameLoop();
    };

    ClosePlatform();
    Console("fps = %d", x86.Time.fps);
    return 0;
}
