/**
 * ansi.h
 * Copyleft (c) Pierre-Jean Turpeau 05/2018
 * <pierrejean AT turpeau DOT net>
 * 
 * first step, incomplete though...
 * 
 * Known limitations:
 * 
 *  - does not fully support extended SAUCE infos as described in
 *    http://www.acid.org/info/sauce/sauce.htm
 * 
 *  - as such, it doesn't support wide (> 80 cols) ansi files
 *    (see arts/pmt-impakuma.ans)
 * 
 *  - doesn't support all graphical rendition attributes (e.g., blinking)
 * 
 *  ...
 */
#ifndef ANSI_H
#define ANSI_H

# include <x/x.h>
# include <x/gfx/fixed_font.h>

/* ---[ PUBLIC DEFINTIONS ]------------------------------------------------- */

# define COLS 80
# define ROWS 5000 /* to support Rad Man very long .ANS... ;) */

# define SAUCE_BLOCK_SIZE 128
# define SAUCE_ID_LEN 5
# define SAUCE_VERSION_LEN 2
# define SAUCE_TITLE_LEN 35
# define SAUCE_AUTHOR_LEN 20
# define SAUCE_GROUP_LEN 20
# define SAUCE_DATE_LEN 8
# define SAUCE_COMNT_ID_LEN 5
# define SAUCE_COMNT_BLOCK_LEN 64
# define SAUCE_MAX_COMMENT_COUNT 8 /* need more? */

typedef struct {
    char id[SAUCE_ID_LEN];
    char version[SAUCE_VERSION_LEN];
    char title[SAUCE_TITLE_LEN];
    char author[SAUCE_AUTHOR_LEN];
    char group[SAUCE_GROUP_LEN];
    char date[SAUCE_DATE_LEN];
    u32 file_size;
    u8 data_type;
    u8 file_type;
    u16 info1;
    u16 info2;
    u16 info3;
    u16 info4;
    u8 comments_count;
} sauce_data_t;

typedef union {
    struct {
        u8 flags; /* NORMAL/HIGH INTENSITY only for now */
        u8 fg_color;
        u8 bg_color;
        u8 char_value;
    };
    u32 ansi_value;
} ansi_char_t;

/* decoded ANSI file data */
typedef struct {
    int rows_count;
    ansi_char_t data[COLS*ROWS];
    sauce_data_t sauce;
    char comments[SAUCE_MAX_COMMENT_COUNT][SAUCE_COMNT_BLOCK_LEN];
} ansi_buffer_t;

ansi_buffer_t ansi_buffer;

/* ---[ DECLARATIONS ]------------------------------------------------------ */

/** parse ANSI file and prepare ANSI drawing data into ansi_buffer */
void parse_ansi(const u8 *data, int size);

/** returns true of previously parse ANSI file has SAUCE infos */
bool has_sauce_infos();

/** draw the content of ansi_buffer */
void draw_ansi(int start_line, const fixed_font_t * font);

/* ---[ IMPLEMENTATIONS ]--------------------------------------------------- */

# ifdef X_IMPLEMENTATION

/* ANSI attributes */
#  define ANSI_RESET 0
#  define ANSI_HIGH_INTENSITY   1
#  define ANSI_NORMAL_INTENSITY 22
#  define ANSI_ITALIC 3
#  define ANSI_UNDERLINE        4
#  define ANSI_DOUBLE_UNDERLINE 21
#  define ANSI_NO_UNDERLINE     24
#  define ANSI_BLINK      5
#  define ANSI_FAST_BLINK 6
#  define ANSI_NO_BLINK   25
#  define ANSI_REVERSE    7
#  define ANSI_NO_REVERSE 27
#  define ANSI_HIDE  8
#  define ANSI_SHOW  28

/* ANSI colors */
#  define ANSI_BLACK   0
#  define ANSI_RED     1
#  define ANSI_GREEN   2
#  define ANSI_YELLOW  3
#  define ANSI_BLUE    4
#  define ANSI_MAGENTA 5
#  define ANSI_CYAN    6
#  define ANSI_WHITE   7

/* Parsing/rendering flags */
# define ANS_FLAG_INTENSITY (1<<0)
# define ANS_FLAG_REVERSE   (1<<1)

/* contextual data used by parsing functions */
typedef struct {
    int x, y;
    int saved_x, saved_y;
    int fg_color, bg_color;
    int attributes;
} ansi_reader_t;

static ansi_reader_t _reader;

static void init_ansi_reader() {
    _reader.x = 1;
    _reader.y = 1;
    _reader.saved_x = 1;
    _reader.attributes = 0;
    _reader.fg_color = ANSI_WHITE;
    _reader.bg_color = ANSI_BLACK;

    ansi_buffer.rows_count = 1;
    memset(ansi_buffer.data, 0, sizeof(ansi_buffer));
}

static void update_rows_count() {
    if(_reader.y > ROWS)
        _reader.y = ROWS;
    if(_reader.y > ansi_buffer.rows_count)
        ansi_buffer.rows_count = _reader.y;
}

static void set_ansi_cursor(int x, int y) {
    _reader.x = x;
    _reader.y = y;
    update_rows_count();
}

static void move_ansi_cursor_x(int count) {
    _reader.x += count;
    if(_reader.x > COLS)
        _reader.x = COLS;
    else if(_reader.x < 1)
        _reader.x = 1;
}

static void move_ansi_cursor_y(int count) {
    _reader.y += count;
    if(_reader.y > ROWS)
        _reader.y = ROWS;
    else if(_reader.y < 1)
        _reader.y = 1;
    update_rows_count();
}

static void append_ansi_char(u8 c) {
    ansi_char_t * v;

    switch(c)
    {
    case 0xA: /* line feed */
        _reader.x = 1;
        _reader.y++;
        break;

    case 0xD: /* carriage return */
        _reader.x = 1;
        break;

    default:
        v = &ansi_buffer.data[(_reader.y-1) * COLS + _reader.x-1];

        v->char_value = c;
        v->fg_color = _reader.fg_color;
        v->bg_color = _reader.bg_color;
        v->flags = _reader.attributes;

        _reader.x++;
        if(_reader.x > COLS) {
            /* TODO: check if this is what we really want... */
            _reader.x = 1;
            _reader.y ++;
        }         
    }
    update_rows_count();
}

static bool is_ansi_command(int value) {
    switch(value) {
    case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G':
    case 'H': case 'f': case 'J': case 'K': case 'S': case 'T':	case 'm':
    case 's': case 'u': case 'n':
        return true;
    }
    return false;
}

# define MAX_ESC_PARAMS 16
static int esc_params[MAX_ESC_PARAMS]; /* store params of active escape seq. */
static int esc_params_count;

/* return parameter value defaulted to 'default_value' if not defined */
static int get_esc_param(int param_number, int default_value) {
    if(param_number > esc_params_count)
        return default_value;
    int value = esc_params[param_number - 1];
    if( value == - 1)
        return default_value;
    return value;
}

/** Select Graphic Rendition is CSI n [;k] m  */
static void select_graphic_rendition() {
    int i, value;
    for( i = 0; i < esc_params_count; i++ ) {
        value = esc_params[i];
        
        switch( value )
        {
        case ANSI_RESET:
            _reader.attributes = 0;
            _reader.fg_color = ANSI_WHITE;
            _reader.bg_color = ANSI_BLACK;
            break;
            
        case ANSI_HIGH_INTENSITY:
            _reader.attributes |= ANS_FLAG_INTENSITY;
            break;

        case ANSI_NORMAL_INTENSITY:
            _reader.attributes &= ~ANS_FLAG_INTENSITY;
            break;

        case ANSI_REVERSE:
            if((_reader.attributes & ANS_FLAG_REVERSE) == 0) {
                int tmp = _reader.fg_color;
                _reader.fg_color = _reader.bg_color;
                _reader.bg_color = tmp;
                _reader.attributes |= ANS_FLAG_REVERSE;
            }
            break;

        case ANSI_NO_REVERSE:
            if( (_reader.attributes & ANS_FLAG_REVERSE) == ANS_FLAG_REVERSE) {
                int tmp = _reader.fg_color;
                _reader.fg_color = _reader.bg_color;
                _reader.bg_color = tmp;
                _reader.attributes &= ~ANS_FLAG_REVERSE;
            }
            break;
            
        /* set foreground color */
        case 30: case 31: case 32: case 33: case 34: case 35: case 36: case 37:
            _reader.fg_color = value-30;
            break;
        case 39: /* default foreground color */
            _reader.fg_color = ANSI_WHITE; 
            break;

        /* set background color */
        case 40: case 41: case 42: case 43: case 44: case 45: case 46: case 47:
            _reader.bg_color = value-40;
            break;			
        case 49: /* default background color */
            _reader.bg_color = ANSI_BLACK;
            break;
            
        case ANSI_BLINK:
        case ANSI_FAST_BLINK:
        case ANSI_UNDERLINE:
        case ANSI_DOUBLE_UNDERLINE:
        case ANSI_NO_UNDERLINE:
        case ANSI_HIDE:
        case ANSI_SHOW:
        case ANSI_ITALIC:
            console_error("unsupported SGR: %d", value);
            /* NOT SUPPORTED, yet... :P */
            break;
        }
    }		
}

/* return the size of end-of-file SAUCE data */
static int read_sauce_infos(const u8 *data, int size) {
    int offset = size - SAUCE_BLOCK_SIZE;

    memcpy(&ansi_buffer.sauce, &data[offset], sizeof(sauce_data_t));

    if( strncmp(ansi_buffer.sauce.id, "SAUCE", SAUCE_ID_LEN) ) {
        ansi_buffer.sauce.file_size = 0;
        return 0;
    }
    
    offset += SAUCE_ID_LEN + SAUCE_VERSION_LEN + SAUCE_TITLE_LEN
        + SAUCE_AUTHOR_LEN + SAUCE_GROUP_LEN + SAUCE_DATE_LEN;

    ansi_buffer.sauce.file_size = *((u32 *)(data + offset));
    offset += 4;

    ansi_buffer.sauce.data_type = data[offset++];
    ansi_buffer.sauce.file_type = data[offset++];
    ansi_buffer.sauce.info1 = *(u16 *)(data + offset);
    offset += 2;
    ansi_buffer.sauce.info2 = *(u16 *)(data + offset); 
    offset += 2;
    ansi_buffer.sauce.info3 = *(u16 *)(data + offset);
    offset += 2;
    ansi_buffer.sauce.info4 = *(u16 *)(data + offset);
    offset += 2;
    ansi_buffer.sauce.comments_count = data[offset++];

    offset = size - SAUCE_BLOCK_SIZE - SAUCE_COMNT_BLOCK_LEN * ansi_buffer.sauce.comments_count;

    for(int k = 0; k < ansi_buffer.sauce.comments_count; k++) {
        memcpy(ansi_buffer.comments[k], (char *)&data[offset], 64);
        offset += SAUCE_COMNT_BLOCK_LEN;
    }

    return (SAUCE_BLOCK_SIZE + SAUCE_COMNT_ID_LEN
        + SAUCE_COMNT_BLOCK_LEN * ansi_buffer.sauce.comments_count);
}

bool has_sauce_infos() {
    return ansi_buffer.sauce.file_size != 0;
}

void parse_ansi(const u8 *data, int size) {
    bool escaped = false; /* true if escape sequence opened */
    bool extended = false; /* true if extended escape sequence ESC[ ? ## */
    
    int param_value = -1;
    
    init_ansi_reader();

    size -= read_sauce_infos(data, size);

    int offset = 0;

    while(offset < size) {
        u8 value = data[offset++];
        if( escaped ) {
				/* compute current parameter value */
				if( value >= '0' && value <= '9' ) {
					if( param_value == -1 ) {
						param_value = 0;
					}
					param_value *= 10;
					param_value += (value - '0'); 
				} else {
                    if( is_ansi_command(value) == true ) {
						/* store computed value */
                        if(esc_params_count < MAX_ESC_PARAMS)
						    esc_params[esc_params_count++] = param_value;
                        else
                            console_error("too many params");
						escaped = false; /* end of escaped sequence */
					}
					
					/* update ANSI buffer according to the decoded command */
					switch( value )
                    {
					case 'A':   /* move cursor up */
						move_ansi_cursor_y(-get_esc_param(1, 1));
						break;
					case 'B':   /* move cursor down */
						move_ansi_cursor_y(get_esc_param(1, 1));
						break;
					case 'C':   /* move cursor forward */
						move_ansi_cursor_x(get_esc_param(1, 1));
						break;
					case 'D':   /* move cursor back */
						move_ansi_cursor_x(-get_esc_param(1, 1));
						break;
					case 'E':   /* move cursor to the beginning of next line */
						_reader.x = 1;						
						move_ansi_cursor_y(get_esc_param(1, 1));
						break;
					case 'F':   /* move cursor to the beginning of prev. line */
						_reader.x = 1;						
						move_ansi_cursor_y(-get_esc_param(1, 1));
						break;
					case 'G':   /* set cursor horizontal position */
						_reader.x = get_esc_param(1, 1);
						break;
					case 'H':
					case 'f':   /* set cursor position */
						set_ansi_cursor( get_esc_param(1, 1), 
                            get_esc_param(2, 1));
						break;
					case 'J':   /* erase_screen */
                        console_error("ignored erase_screen: %d",
                            get_esc_param(1, 0));
						break;
					case 'K':   /* erase line */
                        console_error("ignored erase_line: %d",
                            get_esc_param(1, 0));
						break;
					case 'S':   /* scroll up */
                        console_error("ignored scroll_up: %d",
                            get_esc_param(1, 1));
						break;
					case 'T':   /* scroll down */
                        console_error("ignored scroll_down: %d",
                            get_esc_param(1, 1));
						break;
					case 'm':   /* update graphic rendition */
						select_graphic_rendition();
						break;
					case 's':   /* save cursor position */
                        _reader.saved_x = _reader.x;
                        _reader.saved_y = _reader.y;
						break;
					case 'u':   /* restore cursor position */
                        _reader.x = _reader.saved_x;
                        _reader.y = _reader.saved_y;
						break;
                    case '?': /* non standard extensions */
                        extended = true;
                        break;
                    case ';':   /* parameter separator */
						/* store computed value */
                        if(esc_params_count < MAX_ESC_PARAMS)
						    esc_params[esc_params_count++] = param_value;
                        else
                            console_error("too many params");
						param_value = -1;
                        break;
                    case 'h':
                    case 'l': 
                        /* ESC[7h => enable line warp */
                        /* ESC[7l => disable line warp */
                        /* TODO: enable iCE Colors on ESC[?33h */
                        /* TODO: enable iCE Colors on ESC[?33l */
					case 'n':
                    case 'i':
                    default:
                        if(value > 32) {
                            char param_str[255];
                            char buf[32];
                            strcpy(param_str, "ESC[ ");
                            if(extended) strcat(param_str, "? ");
    						esc_params[esc_params_count++] = param_value;
                            for(int i = 0; i < esc_params_count; i++) {
                                if(i) strncat(param_str, "; ", 254);
                                snprintf(buf, 32, "%d", esc_params[i]);
                                strncat(param_str, buf, 254);
                            }
                            snprintf(buf, 32, "%c", value);
                            strncat(param_str, buf, 254);
                            console_error("unsupported command @ %d - %s", offset-1, param_str);
                        }
                        else
                            console_error("unsupported command: %c (0x%X) @ %d", value, value, offset-1);
                        escaped = false;
                        break;
					}
				}

        } else if( value == 0x1B )  { /* start of escape sequence */
            offset++; /* skip bracket */
            escaped = true;
            extended = false;
            param_value = -1;
            esc_params_count = 0;
        } else
            append_ansi_char(value);
    }
}

void draw_ansi(int start_line, const fixed_font_t * font) {
    int h = H/font->cell_h;
    if(h > ansi_buffer.rows_count)
        h = ansi_buffer.rows_count;

    if(W/font->cell_w < 80) {
        console_error("font is incompatible with current resolution");
        return;
    } 

    int start_row = start_line / font->cell_h;
    int start_y = start_line % font->cell_h;

    int start_x = (W - 80 * font->cell_w) / 2; 

    ansi_char_t * ptr = ansi_buffer.data + (start_row * COLS);

    for(int j = 0, y = -start_y; j <= h; j++, y += font->cell_h) {
        for(int i = 0, x = start_x; i < COLS; i++, x += font->cell_w) {

            if(ptr->char_value > 31) {
                int fg = (ptr->flags & ANS_FLAG_INTENSITY) == ANS_FLAG_INTENSITY
                    ? ptr->fg_color + 8 : ptr->fg_color;
                fixed_char(font, x, y, ptr->char_value, fg, ptr->bg_color);
            }

            ptr++;
        }
    }
}

# endif /* X_IMPLEMENTATION */

#endif /* ANSI_H */
