/**
 * samples/ansiviewdr/main.c
 * 
 * Copyleft (c) Pierre-Jean Turpeau 01/2020
 * <pierrejean AT turpeau DOT net>
 * 
 */
#define X_IMPLEMENTATION 1

#define TITLE "-[ ANSi Viewer ]-"
#define W (320*2)
#define H (240*2)

#include <x/gfx/bmp.h>
#include <x/gfx/sprite.h>
#include <x/gfx/fixed_font.h>

#include "ansi.h"

#define INCBIN_PREFIX
#define INCBIN_STYLE INCBIN_STYLE_SNAKE
#include <incbin/incbin.h>

/* ---[ FORWARD DECLARATIONS ]---------------------------------------------- */

void reset_scroll();

/* -------------------------------------------------------------------------
 * Input files: can have more than one input files and switch between them
 * ------------------------------------------------------------------------- */

/* data needed for files management */
#define MAX_INPUT_FILES 1024 /* arbitrary... */
int files_count = 0; // total files 
int current_file_id = 0; // current file index
char *input_files[MAX_INPUT_FILES];

u8 * ansi_source_buffer = NULL;
int ansi_source_size = 0;

void init_input_files() {
    for(int i = 0; i < cli_argc; i++) {
        if( ! strcmp("-f", cli_argv[i]) ) {
            i++;
            while(i < cli_argc) {
                if(files_count < MAX_INPUT_FILES)
                    input_files[files_count++] = cli_argv[i++];
            }
        }
    }
    if(files_count == 0) {
        fatal_error("init_input_files: missing valid option -f");
    } else
        console("%d files found", files_count);
}

/* open and parse the current input file */
void parse_input_file() {
    console("Parsing %s", input_files[current_file_id]);

    FILE * f = fopen(input_files[current_file_id], "rb");

    if(!f)
        fatal_error("failed to open file");

    fseek(f, 0, SEEK_END);
    ansi_source_size = ftell(f);

    ansi_source_buffer = (u8 *) malloc(ansi_source_size);
    assert(ansi_source_buffer);

    fseek(f, 0, SEEK_SET);
    fread(ansi_source_buffer, sizeof(u8), ansi_source_size, f);

    fclose(f);

    parse_ansi(ansi_source_buffer, ansi_source_size);
    free(ansi_source_buffer);

    console("ANSI has %d rows", ansi_buffer.rows_count);

    reset_scroll();

    if(has_sauce_infos()) {
        console("SAUCE version  : %.2s", ansi_buffer.sauce.version);
        console("SAUCE title    : %.35s", ansi_buffer.sauce.title);
        console("SAUCE author   : %.20s", ansi_buffer.sauce.author);
        console("SAUCE group    : %.20s", ansi_buffer.sauce.group);
        console("SAUCE date     : %.8s", ansi_buffer.sauce.date);
        for(int k = 0; k < ansi_buffer.sauce.comments_count; k++)
            console("SAUCE comment #%02d : %.64s", k, ansi_buffer.comments[k]);
    }
} 

/* switch to next input file in the list and parse it */
void parse_next_input_file() {
    current_file_id = (current_file_id + 1) % files_count;
    parse_input_file(); 
}

/* -------------------------------------------------------------------------
 * Fonts section
 * ------------------------------------------------------------------------- */

# define DECLARE_FIXED_FONT(FONT) \
    INCBIN_EXTERN(FONT);    \
    image_t * FONT##_image; \
    fixed_font_t FONT

DECLARE_FIXED_FONT(dos8x8);
DECLARE_FIXED_FONT(dos4x8);
DECLARE_FIXED_FONT(dos8x16);
DECLARE_FIXED_FONT(amiga8x16);

#define MAX_FONTS 10
int fonts_count = 0;
int current_font_id;
fixed_font_t * fonts[MAX_FONTS];
fixed_font_t * font;

# define INIT_FIXED_FONT(FONT) { \
        FONT##_image = create_bmp(FONT##_data, FONT##_size); \
        init_fixed_font(&FONT, FONT##_image); \
        generate_sprite_mask(FONT##_image); \
        if(fonts_count < MAX_FONTS) \
            fonts[fonts_count++] = &FONT; \
    }

void init_fonts() {
    INIT_FIXED_FONT(dos8x16);
    INIT_FIXED_FONT(dos8x8);
    INIT_FIXED_FONT(dos4x8);
    INIT_FIXED_FONT(amiga8x16);

    /* init default font */
    font = fonts[0];
}

/* switch to the next available font */
void switch_font() {
    current_font_id = (current_font_id + 1) % fonts_count;
    font = fonts[current_font_id];

    reset_scroll();
}

/* -------------------------------------------------------------------------
 * Smooth scrolling
 * ------------------------------------------------------------------------- */

/* data needed for smooth scrolling */
static int start_line = 0;
static int current_line = 0;
static int scroll_line = 0;
static float elapsed_time = 0.f;
static float duration = 150.f;

void reset_scroll() {
    start_line = current_line = scroll_line = 0;
    elapsed_time = duration = 1.f;
}

/* jump to the beginning or to the end of the document */
void scroll_home_or_end(bool home) {
    scroll_line = home ? 0 : ansi_buffer.rows_count * font->cell_h - H;
    start_line = current_line;
    duration = 500;
    elapsed_time = 0;
}

/* relative scroll */
void scroll_ansi(int lines_count) {
    elapsed_time = 0;
    start_line = current_line;
    duration = 150;

    scroll_line = current_line + lines_count;
    if(scroll_line > ansi_buffer.rows_count * font->cell_h - H)
        scroll_line = ansi_buffer.rows_count * font->cell_h - H;
    else if(scroll_line < 0)
        scroll_line = 0;
}

/* slow auto scoll to the end of the document */ 
void enable_auto_scroll() {
    scroll_home_or_end(false);
    duration = abs(scroll_line - current_line)*3;
}

/* ensure smooth scrolling */
void animate_ansi(float delta_time) {
    elapsed_time += delta_time;

    if(elapsed_time < duration)
        current_line = lerp_s32(start_line, scroll_line, elapsed_time/duration);
    else
        current_line = scroll_line;
}

/* -------------------------------------------------------------------------
 * Colormap section
 * ------------------------------------------------------------------------- */

u32 vga_color_map[] = {
    0x000000, /* black */
    0xAA0000, /* red */
    0x00AA00, /* green */
    0xAA5500, /* yellow */
    0x0000AA, /* blue */
    0xAA00AA, /* magenta */
    0x00AAAA, /* cyan */
    0xAAAAAA, /* white */
    0x555555, /* bright black */
    0xFF5555, /* bright red */
    0x55FF55, /* bright green */
    0xFFFF55, /* bright yellow */
    0x5555FF, /* bright blue */
    0xFF55FF, /* bright magenta */
    0x55FFFF, /* bright cyan */
    0xFFFFFF, /* bright white */
};

void init_colors() {
    /* initialize color map */
    for(int k = 0; k < 16; k++)
        set_color(k, vga_color_map[k]);

    set_color(255, 0xFFFFFF);
}

/* -------------------------------------------------------------------------
 * Rendering loop and startup functions
 * ------------------------------------------------------------------------- */

void rendering_func(float dt) {
    ClearScreen(0);

    draw_ansi(current_line, font);
    animate_ansi(dt);

    int font_height = font->cell_h;
    
    switch( last_event() )
    {
    case KEY_ENTER: parse_next_input_file(); break;
    case 'f': case 'F': switch_font(); break;
    case KEY_HOME: scroll_home_or_end(true); break;
    case KEY_END: scroll_home_or_end(false); break;
    case KEY_UP: scroll_ansi(-font_height*3); break;
    case KEY_DOWN: scroll_ansi(font_height*3); break;
    case KEY_LEFT: scroll_ansi(-H/2); break;
    case KEY_RIGHT: scroll_ansi(H/2); break;
    case KEY_PAGEUP: scroll_ansi(-H); break;
    case KEY_PAGEDOWN: scroll_ansi(H); break;
    case ' ': enable_auto_scroll(); break;
    }
}

void start() {
    console("use [f] to switch font");
    console("use [ENTER] to switch to next file");
    console("use [SPACE] for auto-scroll");
    console("use [UP/DOWN, LEFT/RIGHT, PAGE UP/PAGE DOWN, HOME/END] to scroll");
    console("use -f option to specify one or multiple files");

    init_fonts();
    init_colors();

    init_input_files();
    parse_input_file();

    main_loop(rendering_func);
}

INCBIN(dos4x8, "x/res/font-dos-4x8.bmp");
INCBIN(dos8x8, "x/res/font-dos-8x8.bmp");
INCBIN(dos8x16, "x/res/font-dos-8x16.bmp");
INCBIN(amiga8x16, "x/res/font-amiga-8x16.bmp");
