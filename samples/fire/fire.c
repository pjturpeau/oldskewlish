/*
 * samples/fire/fire.c
 * 
 * Copyleft (c) Pierre-Jean Turpeau 02/2019
 * <pierrejean AT turpeau DOT net>
 */
#define X_IMPLEMENTATION 1

#define TITLE "-[ fire ]-"

#include <x86/x.h>

/**
 * max_color_index allows to generate the fire using less colors and keep room
 * for few others to display something else.
 * Value 64 works with a smaller fire result.
 */
const u8 max_color_index = 255;

/* interpolate index according to usable colors */
#define _INTERP(X) (X * max_color_index / (MAX_COLORS-1))

void InitFirePalette() {
    LinearGradientPalette(_INTERP(  0), 0x000000, _INTERP( 16), 0x021B3B);
    LinearGradientPalette(_INTERP( 16), 0x021B3B, _INTERP( 32), 0xFF1C1C);
    LinearGradientPalette(_INTERP( 32), 0xFF1C1C, _INTERP( 96), 0xFF8C00);
    LinearGradientPalette(_INTERP( 96), 0xFF8C00, _INTERP(128), 0xFFFFA0);
    LinearGradientPalette(_INTERP(128), 0xFFFFA0, _INTERP(255), 0xFFFFA0);
}

/* play with thresholds for different results */

#define FIRE_SEED_THRESHOLD 6
#define FIRE_CONVECTION_THRESHOLD 128

#define MAX_RAND_LOOKUP 4096
u8 rand_lookup[MAX_RAND_LOOKUP];

u32 y_offset[H];

void InitFireRandom() {
    for(int i = 0; i < MAX_RAND_LOOKUP; i++) {
        rand_lookup[i] = GetIntRandomValue(0,15);
    }

    for(int i = 0; i < H; i++) {
        y_offset[i] = W*i;
    }
}

/* Direct rendering of the fire instead of using pixel functions */
void DrawFireDirect() {
    static int rand_index = 0;

    u8 * video = GetBackbuffer() + ((H-1)*W) + 1;

    /* seed the fire - outside the visible area would be better */
    for(int i = 1; i < W - 1; i++ ) {
        if( rand_lookup[rand_index] < FIRE_SEED_THRESHOLD )
            *(video++) = max_color_index;
        else
            *(video++) = 0;// max_color_index / 24;
        rand_index = (rand_index + 1) % MAX_RAND_LOOKUP;
    }

    u32 value;

    /* start at H/4 - no need to start from y=0 */
    for (int j = 0; j < H; j++) {
        video = GetBackbuffer() + (j*W) + 1;

        for (int i = 1; i < W - 1; i++) {
            value  = *(video-1); // i-1
            value += *(video); // i
            value += *(video+1);   // i+1
            value += *(video+W); // j+1
            value >>= 2;

            if(value > 0 && value < 32)
                value--;

            *(video - W) = value;

            video++;
        }
    }

    /* to make it taller */
    for(int j = H/4; j < H-1; j++) {
        video = GetBackbuffer() + y_offset[j] + 1;
        for(int i = 1; i < W - 1; i++) {
            value = *(video + W);
            if(value > 0 && value < FIRE_CONVECTION_THRESHOLD)
                value--; /* make it more dynamic at the top */
            *(video) = value;
            video++;
        }       
    }
}

/* Draws the fire using GetPixel/SetPixel functions. Simply more readable... */
void DrawFire() {
    static int rand_index = 0;

    /* seed the fire - outside the visible area would be better */
    for(int i = 1; i < W - 1; i++ ) {
        if( rand_lookup[rand_index] < FIRE_SEED_THRESHOLD )
            SetPixel(i, H-1, max_color_index);
        else
            SetPixel(i, H-1, max_color_index / 8);
        rand_index = (rand_index + 1) % MAX_RAND_LOOKUP;
    }

    int value;

    for( int j = 1; j < H - 1; j++) {
        for(int i = 1; i < W - 1; i++) {
            value  = GetPixel(i-1, j);
            value += GetPixel(i, j);
            value += GetPixel(i+1, j);
            value += GetPixel(i, j+1);
            value >>= 2;
            SetPixel(i, j - 1, value);
        }
    }

    /* to make it taller */
    for(int j = 0; j < H - 1; j++) {
        for(int i = 1; i < W - 1; i++) {
            value = GetPixel(i, j + 1);
            if(value > 0 && value < FIRE_CONVECTION_THRESHOLD)
                value--; /* make it more dynamic at the top */
            SetPixel(i, j, value);
        }       
    }
}

void set_video_mode(int mode)
{
	union REGS regs;
	regs.w.ax = mode;
	int386(0x10, &regs, &regs);
}

void SetVGA(int mode) {
    set_video_mode(0x13);

    if(mode == 0) // keep 70hz
        return;

	_disable();
	outpw(0x3D4, 0x0011); // Turn off write protect to CRTC registers
	outpw(0x3D4, 0x0B06); // New vertical total=525 lines, bits 0-7
	outpw(0x3D4, 0x3E07); // New vertical total=525 lines, bits 8-9

    if (mode == 1) {
        /* Standard 320x200 pixel configuration */
        outpw(0x3D4, 0x0B16); // Adjust vblank end position=scanline 524
        outpw(0x3D4, 0xD710); // Vsync start=scanline 215
        outpw(0x3D4, 0x8911); // Vsync length=2 lines + turn write protect back on
    } else {
        /* Square pixels */
        outpw(0x3D4, 0xB910); // Vsync start=scanline 185
        outpw(0x3D4, 0x8F12); // Vertical display end=scanline 399, bits 0-7
        outpw(0x3D4, 0xB815); // Vertical blanking start=scanline 440, bits 0-7
        outpw(0x3D4, 0xE216); // Adjust vblank end position
        outpw(0x3D4, 0x8511); // Vsync length=2 lines + turn write protect back on
    }
	_enable();
}

int main(int argc, char **argv) {
    InitPlatform(argc, (const char **) argv);
    ParseDefaultExeOptions();
    Console("press [SPACE] to switch between SetPixel() and direct backbuffer algorithm");
    Console("press [ENTER] to switch between 70Hz and 60Hz video mode\n");
    Pause(2000);
    SetVGAMode();

    InitFireRandom();
    InitFirePalette();

    bool optimized = false;
    bool vga60hz = false; 
    int mode = 0;

    optimized = true;

    while(!IsPlatformExit()) {
        BeginFrameLoop();
        ExecuteDefaultInteractions();

            if (GetLastEvent() == ' ') {
                optimized = !optimized;
                InitFirePalette();
            }

            if (GetLastEvent() == KEY_ENTER) {
                mode = (mode + 1) % 3;
                SetVGA(mode);
                InitFirePalette();
            }

            if (! IsPaused()) {
                if (optimized)
                    DrawFireDirect();
                else
                    DrawFire();
            }

        EndFrameLoop();
    };

    ClosePlatform();

    Console("fps = %d", x86.Time.fps);
    return EXIT_SUCCESS;
}
