/**
 * samples/metaball/metaball.c
 * Copyleft (c) Pierre-Jean Turpeau 01/2020
 * <pierrejean AT turpeau DOT net>
 */
#define X_IMPLEMENTATION 1

#define TITLE "-[ metaballs ]-"

#include <time.h>
#include <x86/x.h>
#include <x86/extras.h>
#include <x86/image.h>

typedef struct
{
    int x, y;
    int mass;

    /* for movements */
    float speed_x, speed_y;
    float shift_x, shift_y;
    int amplitude_x, amplitude_y;
} metaball_t;

typedef enum
{
    E_STYLE_BASIC = 0,
    E_STYLE_BRUT = 1,
    E_STYLE_BLACK = 2,
    E_STYLE_OUTLINE = 3,
    E_STYLE_ELEVATIONS = 4,
    E_STYLE_LINES = 5,

    E_STYLE_LAST
} metaball_style;

#define NB_METABALLS 8

#define M_RANDF(m) (m * (float) rand() / (float) RAND_MAX)

static u32 * field;
static metaball_t metaballs[NB_METABALLS];

void InitOneMetaball(metaball_t * metaball)
{
    metaball->mass = 150 + rand() % 450;

    metaball->x = GetIntRandomValue(0, W);
    metaball->y = GetIntRandomValue(0, H);

    // for the metaball movement
    metaball->speed_x = GetFloatRandomValue(0, 2.0f);
    metaball->speed_y = GetFloatRandomValue(0, 2.0f);
    metaball->shift_x = GetFloatRandomValue(0, M_PI);
    metaball->shift_y = GetFloatRandomValue(0, M_PI);
    metaball->amplitude_x = W/2 - GetIntRandomValue(0, 50);
    metaball->amplitude_y = H/2 - GetIntRandomValue(0, 50);

}

void InitMetaballs() {
    for(int i = 0; i < NB_METABALLS; i++)
        InitOneMetaball(&metaballs[i]);
    field = MemAlloc(W*H*sizeof(field[0]));
    memset(field, 0, W * H * sizeof(field[0]));
}

u32 GetMetaballPotential(metaball_t * metaball, int x, int y) {
    int xx = x - metaball->x;
    int yy = y - metaball->y;

    if( xx == 0 && yy == 0) return 255;

    return
        (((metaball->mass * metaball->mass)<<12) / (xx * xx + yy * yy)) >> 12;
}

void MoveMetaballs(float elapsed_time) {
    elapsed_time *= 0.001f;

    for(int i = 0; i < NB_METABALLS; i++) {
        metaballs[i].x
            = W/2 + metaballs[i].amplitude_x
                * cos(metaballs[i].speed_x * elapsed_time + metaballs[i].shift_x);
        metaballs[i].y
            = H/2 + metaballs[i].amplitude_y
                * sin(metaballs[i].speed_y * elapsed_time + metaballs[i].shift_y);
    }
}

void DrawMetaballs(metaball_style style)
{
    memset(field, 0, W*H*sizeof(field[0]));

    u32 * pf = field;
    
    u8 * video = GetBackbuffer();

    for(int j = 0; j < H; j++) {
        for(int i = 0; i < W; i++) {

            for(int k = 0; k < NB_METABALLS; k++)
                *pf += GetMetaballPotential(&metaballs[k], i, j);

            switch(style) {
                case E_STYLE_BASIC:
                    if( *pf > 240) *pf = 255;
                    break;

                case E_STYLE_BRUT:
                    if( *pf > 225) *pf = 255; else *pf = 0;
                    break;

                case E_STYLE_BLACK:
                    if( *pf > 225) *pf = 0;
                    break;

                case E_STYLE_OUTLINE:
                    if( *pf > 255) *pf = 0; else if( *pf > 220) *pf = 255;
                    break;

                case E_STYLE_ELEVATIONS:
                    if( *pf > 50 && *pf < 90) *pf = 50;
                        else if( *pf > 90 && *pf < 140) *pf = 90;
                        else if (*pf > 140 && * pf < 200) *pf = 140;
                        else if(*pf > 200 && *pf < 255) *pf = 200;
                        else if(*pf > 255) *pf = 255;
                    break;

                case E_STYLE_LINES:
                    if( *pf > 50 && *pf < 60) *pf = 255;
                        else if( *pf > 90 && *pf < 100) *pf = 255;
                        else if (*pf > 140 && * pf < 150) *pf = 255;
                        else if(*pf > 180 && *pf < 200) *pf = 255;
                        else *pf = 0;
                    break;

                default:
                    break;
            }

            *(video++) = *(pf++);
        }
    }
}


int mb_style = 0;
float shininess = 100;

void InitPalette() {
    v3f ambiant_color = {{0, 0, 0.05}};
	v3f diffuse_color = {{0.8, 0.2, 0.8}};
	v3f specular_color = {{0.8, 0.8, 0.9}};
    LightingGradientPalette(0, 255, ambiant_color, diffuse_color, specular_color, shininess);
}

int main(int argc, char **argv) {
    InitPlatform(argc, (const char **) argv);
    ParseDefaultExeOptions();

    Console("press [SPACE] to test threshold options");
    Console("press [L]     to test shininess levels\n");
    Pause(2000);

    InitMetaballs();

    SetVGAMode();

    v3f ambiant_color = {{0, 0, 0.05}};
	v3f diffuse_color = {{0.8, 0.2, 0.8}};
	v3f specular_color = {{0.8, 0.8, 0.9}};
    LightingGradientPalette(0, 255, ambiant_color, diffuse_color, specular_color, shininess);

    while(!IsPlatformExit()) {
        BeginFrameLoop();
        ExecuteDefaultInteractions();

            if (! IsPaused()) {
                switch (GetLastEvent()) {
                    case ' ':
                        mb_style++;
                        mb_style %= E_STYLE_LAST;
                        break;

                    case 'l':
                    case 'L':
                        if (shininess == 100)
                            shininess = 10;
                        else if (shininess == 10)
                            shininess = 1;
                        else
                            shininess = 100;
                        InitPalette();                }

                MoveMetaballs(GetTime());
                DrawMetaballs(mb_style);
            }

        EndFrameLoop();
    };

    ClosePlatform();

    Console("fps = %d", x86.Time.fps);
    return EXIT_SUCCESS;
}
