/**
 * samples/bump2d/bump2d.h
 * 
 * Copyleft (c) Pierre-Jean Turpeau 12/2019
 * <pierrejean AT turpeau DOT net>
 */
#define X_IMPLEMENTATION 1

#define TITLE "-[ bump ]-"

#include <x86/x.h>
#include <x86/image.h>
#include <x86/math.h>
#include <x86/extras.h>

#define BUMP_LIGHT_SIZE 256
#define BUMP_LIGHT_CENTER (BUMP_LIGHT_SIZE / 2)  

typedef struct {
    int light_x; /* light x center */
    int light_y; /* light y center */

    const image_t * bg;
    image_t * light;
} bump_t;

#define SINE_PERIOD (1<<8)
#define SIN_X(angle) sine_lut_x[(angle)&(SINE_PERIOD-1)]
#define SIN_Y(angle) sine_lut_y[(angle)&(SINE_PERIOD-1)]

s32 sine_lut_x[SINE_PERIOD];
s32 sine_lut_y[SINE_PERIOD];

bool display_map = false;

extern unsigned char stone_data[];
extern unsigned int  stone_size;

void InitBump(bump_t * bump, const image_t * background) {
    bump->bg = background;
    bump->light_x = background->w / 2;
    bump->light_y = background->h / 2;

    bump->light = CreateImage(BUMP_LIGHT_SIZE, BUMP_LIGHT_SIZE);
    GenerateQuadraticSprite(bump->light->pixels, BUMP_LIGHT_SIZE, BUMP_LIGHT_SIZE, 255, 40, 5);

    v3f cos_conf = {{.75f, 1.f, -.25f}};
    v3f sin_conf = {{.25f, 2.f, .0f}};
    ConfigureIntDualSineLUT(
        sine_lut_x, SINE_PERIOD, W/2, W/2, cos_conf, sin_conf);

    ConfigureIntOscillatorLUT(
        sine_lut_y, SINE_PERIOD, H/2, H/2, .5);
}

void DrawBump(bump_t * bump, float dt) {

    static float elapsed_time_x = 0;
    elapsed_time_x = dt * 0.07;
    bump->light_x = SIN_X((u32) elapsed_time_x);

    static float elapsed_time_y = 0;
    elapsed_time_y = dt * 0.03;
    bump->light_y = SIN_Y((u32) elapsed_time_y);

    /* light top left coordinate */
    int topleft_x = bump->light_x - BUMP_LIGHT_CENTER; 
    int topleft_y = bump->light_y - BUMP_LIGHT_CENTER;

    u8 * video = GetBackbuffer() + 1 + W;

    for(int j = 1; j < H-1; j++ ) {
        int bump_y = j - topleft_y;

        for( int i = 1; i < W-1; i++ ) {
            int bump_x = i - topleft_x;

            int colx = bump_x
                + bump->bg->pixels[i - 1 + j * bump->bg->w]
                - bump->bg->pixels[i + 1 + j * bump->bg->w];

            int coly = bump_y
                + bump->bg->pixels[i + (j-1) * bump->bg->w]
                - bump->bg->pixels[i + (j+1) * bump->bg->w];

            if( colx >= 0 && colx < BUMP_LIGHT_SIZE && coly>= 0 && coly < BUMP_LIGHT_SIZE ) {
                *(video++) = bump->light->pixels[colx + (coly<<8)];
            } else
                *(video++) = 0;
        }
        video += 2;
    }
}


int main(int argc, char **argv) {
    InitPlatform(argc, (const char **) argv);
    ParseDefaultExeOptions();

    Console("use [SPACE] to display light map\n");
    Pause(2000);

    image_t texture;
    bump_t bump;

    LoadBMP(&texture, NULL, stone_data, stone_size);
    InitBump(&bump, &texture);

    SetVGAMode();

    v3f ambiant_color = {{0, 0, 0}};
	v3f diffuse_color = {{0.9, 0.9, 0.1}};
	v3f specular_color = {{0.9, 0.9, 0.9}};
    LightingGradientPalette(0, 255, ambiant_color, diffuse_color, specular_color, 10);

    u32 start = GetTime();
    while(!IsPlatformExit()) {
        BeginFrameLoop();
        ExecuteDefaultInteractions();

            if (GetLastEvent() == ' ')
                display_map = !display_map;

            if (!IsPaused()) {
                u32 elapsed_time = GetTime() - start;

                if(display_map) {
                    DrawImage_c(bump.light, 0, 0);
                } else {
                    DrawBump(&bump, elapsed_time);
                }
            } else {
                start = GetTime();
            }

        EndFrameLoop();
    };

    ClosePlatform();

    Console("fps = %d\n", x86.Time.fps);
    return EXIT_SUCCESS;    
}
