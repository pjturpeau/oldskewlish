/**
 * x/gfx/procs.h
 * 
 * Copyleft (c) Pierre-Jean Turpeau 02/2020
 * <pierrejean AT turpeau DOT net>
 * 
 * Kind of procedural generators
 */
#ifndef X_GFX_PROCS_H
#define X_GFX_PROCS_H

# include <x/x.h>

/* ---[ DECLARATIONS ]------------------------------------------------------ */

extern void generate_quadratic_sprite( u8 * pixels, int w, int h, int color_range, int spot_radius, int smoothing_size);

/* ---[ IMPLEMENTATION ]---------------------------------------------------- */

# ifdef X_IMPLEMENTATION

#  include <math.h>

/** vertical and horizontal radius are deduced from the image size */
extern void generate_quadratic_sprite(u8 * pixels, int w, int h, int color_range, int spot_radius, int smoothing_size) {
    float a = (float) w * .5f;
    float b = (float) h * .5f;

    float radius_max = fminf(a, b);

    for(int j = 0; j < h; j++) {
        
        float y = (float) j - b;
        float yy = y * y;

        for(int i = 0; i < w; i++) {
            float x = (float) i - a;;
            float xx = x * x;

            float radius = sqrtf(xx + yy);

            int c = color_range;
            
            if( radius >= spot_radius + smoothing_size) {
                c = color_range * (1 - (radius / radius_max));
            }
            else if( radius >= spot_radius ) {               
                float smoothing_radius =
                    lerp_f32(0, spot_radius + smoothing_size,
                        (radius - spot_radius) / smoothing_size);

                c = color_range * (1 - smoothing_radius / radius_max);
            }

            if(c > color_range)
                c = color_range;
            else if( c < 0 )
                c = 0;

            *(pixels++) = c;
        }
    }
}

# endif /* X_IMPLEMENTATION */

#endif /* X_GFX_PROCS_H */
