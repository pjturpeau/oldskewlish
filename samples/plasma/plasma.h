/**
 * samples/plasma/plasma.h
 * 
 * Copyleft (c) Pierre-Jean Turpeau 11/2019
 * <pierrejean AT turpeau DOT net>
 */
#ifndef PLASMA_H
#define PLASMA_H

# include <math.h>
# include <x86/x.h>
# include <x86/fp16.h>

# define SINE_PERIOD_BITS 9
# define SINE_PERIOD (1<<SINE_PERIOD_BITS)
# define SINE_PI   (SINE_PERIOD/2)
# define SINE_PI_2 (SINE_PERIOD/4)
# define SINE(ANGLE) sine_lut[(ANGLE)&((1<<SINE_PERIOD_BITS)-1)]

# define SINE_VALUE_BITS 12
# define SINE_VALUE_RANGE (1<<SINE_VALUE_BITS)

static fp16 sine_lut[SINE_PERIOD+1];
static float _elapsed_time = 0;

void InitPlasma() {
    for(int i = 0; i < SINE_PERIOD+1; i++)
        sine_lut[i] =
            SINE_VALUE_RANGE * (sin(i * M_TAU / SINE_PERIOD));
}

void UpdatePlasma(float elapsed_time) {
    _elapsed_time = elapsed_time * 0.001f;
}

void DrawComplexePlasma() {

    u8 * video = GetBackbuffer();

    for(int j = 0; j < H; j++) {
        float y = -0.5f + 1.0f * j / H;

        for(int i = 0; i < W; i++) {
            float x = -0.5f + 1.0f * i / W;

            float cx = x + 0.5f * sin(_elapsed_time * (1/5.f));
            float cy = y + 0.5f * cos(_elapsed_time * (1/8.f));

            float c =
                /* moving vertical stripes */
                sin(x * 10 + _elapsed_time) +

                    /* rotating and zooming stripes */
                    sin(10 * (x * sin(_elapsed_time * 0.5f) +
                        y * cos(_elapsed_time * 0.33f) ) + _elapsed_time) +

                    /* moving circular stripes */
                    sin( sqrt(100 * (cx * cx + cy * cy) + 1) + _elapsed_time);

            c *= 0.3f;

            *(video++) = (int)(128 + 128 * c) & 0xFF;
        }
    }
}

/** a simplified sine only version, no divide, no sqrt */
void DrawSinePlasma() {
    float t1 = 10.f * sin(_elapsed_time * .5f);
    float t2 = 15.f * sin(_elapsed_time * .25f);
    float t3 =  5.f * sin(_elapsed_time);
    float t4 =  1.f * sin(_elapsed_time * 1.5f);

    u8 * video = GetBackbuffer();

    for(int j = 0; j < H; j++)
    {
        float p3 = t3;
        float p4 = t4;

        for(int i = 0; i < W; i++)
        {
            float c  = sin(t1);            
            c += sin(t2 + p4 * 0.5f);
            c += sin(p3 + p4 * 0.25f);
            c += sin(p4);
            c *= 0.5f;

            *(video++) = (int)(128 + 128 * c) & 0xFF;

            p3 += 0.01f;
            p4 += 0.02f;
        }
        t1 += 0.02f;
        t2 += 0.03f;
    }
}

/* -------------------------------------------------------------------------
 * Fixed point versions
 * ------------------------------------------------------------------------- */

/**
 * run smoothly only when SINE_PERIOD_BITS = 13
 */
void DrawFixedPointPlasma() {
    u32 tm = FloatToFP16(_elapsed_time / 100) >> (16 - SINE_PERIOD_BITS);

    int t1 = 10 * SINE(tm >> 1)         >> (SINE_VALUE_BITS - SINE_PERIOD_BITS);
    int t2 = 15 * SINE(tm >> 2)         >> (SINE_VALUE_BITS - SINE_PERIOD_BITS);
    int t3 =  5 * SINE(tm)              >> (SINE_VALUE_BITS - SINE_PERIOD_BITS);
    int t4 =  1 * SINE(tm + (tm >> 1))  >> (SINE_VALUE_BITS - SINE_PERIOD_BITS);

    u8 * video = GetBackbuffer();

    for(int j = 0; j < H; j++) {
        int p3 = t3;
        int p4 = t4;

        for(int i = 0; i < W; i++) {
            u32 c = SINE(t1);
            c += SINE(t2 + (p4>>1));
            c += SINE(p3 + (p4>>2));
            c += SINE(p4);
            c >>= (1 - 7 + SINE_VALUE_BITS);

            *(video++) = (128 + c) & 0x0FF;

            p3 += (SINE_PI / M_PI * 0.01);
            p4 += (SINE_PI / M_PI * 0.02);
        }

        t1 += (SINE_PI / M_PI * 0.02);
        t2 += (SINE_PI / M_PI * 0.03);
    }
}

/**
 * Use linear interpolation of the sine LUT to improve sine resolution
 * and overall smoothness.
 */
void DrawFixedPointLerpPlasma() {

    /* use a 16.16 resolution fixed point timestamp as angle */
    u32 tm = FloatToFP16(_elapsed_time / 100);

    /* to lerp the sine_lut with an angle having 16 bits fractional part */ 
# define LERP_SINE(ANGLE) \
    IntLUTFixedPointLerp(sine_lut, (ANGLE)&((1<<16)-1), 16 - SINE_PERIOD_BITS)

    /* lerp LUT values: works when angle has more bits than the LUT period */

    int t1 = 10 * LERP_SINE(tm >> 1);
    int t2 = 15 * LERP_SINE(tm >> 2);
    int t3 =  5 * LERP_SINE(tm);
    int t4 =  1 * LERP_SINE(tm + (tm >> 1));

    /* adjust result so that sine_lut can be used without lerping */

    t1 >>= (SINE_VALUE_BITS - SINE_PERIOD_BITS);
    t2 >>= (SINE_VALUE_BITS - SINE_PERIOD_BITS);
    t3 >>= (SINE_VALUE_BITS - SINE_PERIOD_BITS);
    t4 >>= (SINE_VALUE_BITS - SINE_PERIOD_BITS);

    u8 * video = GetBackbuffer();

    for(int j = 0; j < H; j++) {
        int p3 = t3;
        int p4 = t4;

        for(int i = 0; i < W; i++) {
            u32 c = SINE(t1);
            c += SINE(t2 + (p4>>1));
            c += SINE(p3 + (p4>>2));
            c += SINE(p4);
            c >>= (1 - 7 + SINE_VALUE_BITS);

            *(video++) = (128 + c) & 0x0FF;

            p3 += (SINE_PI / M_PI * 0.01);
            p4 += (SINE_PI / M_PI * 0.02);
        }

        t1 += (SINE_PI / M_PI * 0.02);
        t2 += (SINE_PI / M_PI * 0.03);
    }
}

#endif /* PLASMA_H */
