/**
 * samples/checkbrd/checkbrd.c
 * 
 * Copyleft (c) Pierre-Jean Turpeau 02/2020
 * <pierrejean AT turpeau DOT net>
 */
#define X_IMPLEMENTATION 1

#define TITLE "-[ checker board ]-"

#include <math.h>
#include <x86/x.h>

#include "rotarycb.h"

#define MAP_W (W)
#define MAP_VAL(X,Y) map[(X)+(Y)*MAP_W]

u8 * map;

typedef enum {
    E_NORMAL,
    E_HSINE,
    E_ROTOZOOM
}
fx_type;

fx_type current_fx = E_NORMAL;

void InitMap() {
    int plot_size = 1;

    for(int j = 0; j < W; j++, plot_size++) {
        bool white = false;
        for(int i = 0; i < MAP_W; i++) {
            if((i+1)%plot_size == 0) white = !white;
            map[i + j * MAP_W] = white ? 255 : 0;
        }
    }
}

#define NB_LAYERS 4

/* need to be reworked to support smooth zooming */
void DrawCheckerBoard(float elapsed_time)
{
    v2s pos[NB_LAYERS];
    int map_index[NB_LAYERS];
    bool invert_pattern[NB_LAYERS];
    int pattern_size[NB_LAYERS];

    int x = W + W * sin(elapsed_time);
    int y = H + H * sin(elapsed_time * .5f);

    for(int i = 0; i < NB_LAYERS; i++) {
        float zoom_factor = pow(.5f, i);

        pos[i].x = x * zoom_factor;
        pos[i].y = y * zoom_factor;

        map_index[i] = 120 * zoom_factor;
        invert_pattern[i] = false;
        pattern_size[i] = map_index[i] * 2; 
    }

    for(int j = 0; j < H; j++)
    {
        int dx = 0;

        if(current_fx == E_HSINE)
            dx = 20 + 20 *  sin(elapsed_time + j *0.05f);

        for(int k = 0; k < NB_LAYERS; k++)
            invert_pattern[k]
                = (((j + pos[k].y)/map_index[k]) & 1) ? true : false;

        for(int i = 0; i < W; i++) {
            uint8_t color;

            for(int k = 0; k < NB_LAYERS; k++) {
                color =
                    MAP_VAL((i+pos[k].x + dx) % pattern_size[k], map_index[k]);
                if(invert_pattern[k]) color = color ? 0 : 255;
                color = color ? k+1: 0;
                if(color) break;
            }

            SetPixel(i, j, color);
        }
    }
}

void Animate(float elapsed_time)  {
    elapsed_time *= 0.001f;

    if(GetLastEvent() == ' ') {
        switch(current_fx) {
            case E_NORMAL:
                current_fx = E_HSINE;
                break;

            case E_HSINE:
                current_fx = E_ROTOZOOM;
                break;

            case E_ROTOZOOM:
                current_fx = E_NORMAL;
                break;
        }
    }

    // x_draw_image(backbuffer, &rcb.texture, 0, 0);
    switch( current_fx ) {
        case E_NORMAL:
        case E_HSINE:
            DrawCheckerBoard(elapsed_time);
            break;

        case E_ROTOZOOM:
            UpdateRotaryCheckerBoard(elapsed_time);
            DrawRotaryCheckerBoard();
            break;
    }
}

int main(int argc, char ** argv) {
    InitPlatform(argc, (const char **) argv);
    ParseDefaultExeOptions();
    Console("press [SPACE] to switch\n");
    Pause(2000);
    SetVGAMode();

    SetColorPalette(0, 0);
    SetColorPalette(1, 0xFFB2A7);
    SetColorPalette(2, 0xE6739F);
    SetColorPalette(3, 0xCC0E74);
    SetColorPalette(4, 0x790C5A);

    map = (u8 *) malloc(2 * W * W); /* (2*W, W) */
    InitMap();

    InitRotaryCheckerBoard();

    while(!IsPlatformExit()) {
        BeginFrameLoop();
        ExecuteDefaultInteractions();

        if (! IsPaused())
            Animate(GetTime());

        EndFrameLoop();
    };

    ClosePlatform();
    Console("fps = %d", x86.Time.fps);
    return 0;
}
