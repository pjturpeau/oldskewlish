#ifndef TEST_H
#define TEST_H

#define W 320
#define H 200

#include <x86/x.h>

/* from http://git.videolan.org/?p=x264.git;a=blob;f=tools/checkasm.c;hb=HEAD */
static inline uint32_t read_time(void)
{
    uint32_t a = 0;
# if defined(__GNUC__) && (defined(ARCH_X86) || defined(ARCH_X86_64) || defined(_X86_) || defined(__x86_64__))
    asm volatile(
        "cpuid          \n\t" /* every instruction issued is guaranteed to execute before the CPUID instruction. */
        "rdtsc" :"=a"(a) ::"edx" );
# elif defined(ARCH_PPC)
    asm volatile( "mftb %0" : "=r" (a) );
# elif defined(ARCH_ARM)     // ARMv7 only
    asm volatile( "mrc p15, 0, %0, c9, c13, 0" : "=r"(a) );
# endif
    return a;
}

#endif /* TEST_H */