/**
 * raycast-tunnel/rt_not_optimized.c
 * Copyleft (c) Pierre-Jean Turpeau 01/2020
 * <pierrejean AT turpeau DOT net>
 */
#include <x/3d/m3f.h>
#include <math.h>

extern image_t * g_texture;

static m3f _rotation_matrix;
static v3f _origin;

static float _radius = 256.0f;

static void _get_uv(int x, int y, int *u, int *v)
{
    v3f initial_direction = {{ x - W/2, y - H/2, 256.0f }};
    v3f direction;

    m3f_mul_vec(&direction, &_rotation_matrix, &initial_direction);
    Vector3fNormalize(&direction);

    /* 
     * It's a conic problem. We have to calculate the intersection between
     * a cylinder centered on (0,0) along the Z axis with the following
     * equation:   x^2 + y^2 = r^2 (equation of a circle for all z)
     * 
     * We need to find the intersection between this cylinder and a ray that
     * has an origin and a direction vectors: ray = origin + t * direction
     * 
     * Here we know the origin, we know the direction, and we know the radius
     * of our cylinder, thus we need to find t such as if we substitute (x,y)
     * by the ray coordinates in the cylinder equation, the result is equal
     * to radius^2
     * 
     * The full expanded equation is:
     * 
     *     t^2*(direction.x^2 + direction.y^2)
     *     + t*2*(origin.x*direction.x + origin.y*direction.y)
     *     + origin.x^2 + origin.y^2 - r^2 = 0;
     * 
     * it's a quadric that we can resolve using standard maths.
     */

    float a = direction.x * direction.x + direction.y * direction.y;
    float b = 2*(_origin.x * direction.x + _origin.y * direction.y);
    float c = _origin.x * _origin.x + _origin.y * _origin.y - _radius * _radius;

    /* calculate discriminent delta */
    float delta = b*b - 4*a*c;

    float t;

    /* if there's no real solution */
    if (delta < 0) {
        *u = 128;
        *v = 128;
        return;
    } else if ( delta == 0.0f ) {
        t  = -b/(2*a);
    } else {
        /* there are 2 solutions, get the nearest
         * in our case we can choose either one... */
        delta = sqrt(delta);
        float t1 = -b + delta;
        float t2 = -b - delta;
        t = t1 <= t2 ? t1 : t2;
        t /= 2*a;
    }

    /* finally compute the intersection
     *      intersection = direction * t + origin */
    Vector3fMul(&direction, &direction, t);
    Vector3fAdd(&direction, &direction, &_origin);

    /* do the mapping within the texture */
    *u = (int)(fabs(direction.z) * 0.15f);
    *v = (int)(fabs(atan2(direction.y, direction.x) * 256 / M_PI));
}

void rt_not_optimized(float delta_time)
{
    static float posangle = 0;

    const u8 * pixs = g_texture->pixels;

    int u = 0;
    int v = 0;

    posangle += delta_time * 0.0005f;

    _origin.x = 30 * cosf(posangle-0.25f) +  90 * sinf(posangle*2.f);
	_origin.y = 60 * sinf(posangle+0.25f) - 110 * cosf(posangle+0.75f);
    _origin.z -= delta_time * 0.5f; /* run along the tunnel */

    float xrotangle = sinf(posangle * 2) * 0.5f;
    float zrotangle = cosf(posangle);
    float yrotangle = xrotangle * zrotangle * 1.25f;

    m3f_rotation(&_rotation_matrix, xrotangle, yrotangle, -zrotangle);

    for(int j = 0; j < H; j++)
    {
        for(int i = 0; i < W; i++)
        {
            _get_uv(i, j, &u, &v);
            set_pixel(i, j, pixs[(u + 256 * v) & 0xFFFF]);
        }
    }
}
