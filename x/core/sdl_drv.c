/**
 * x/core/sdl_driver.h
 * 
 * Copyleft (c) Pierre-Jean Turpeau 04/2020
 * <pierrejean AT turpeau DOT net>
 * 
 * SDL2 platform specific operations.
 */
#ifndef X_CORE_SDL_DRV_H
#define X_CORE_SDL_DRV_H

# include <x/x.h>
# include <SDL2/SDL.h>

typedef struct
{
    SDL_Window * window;
    SDL_Renderer * renderer;
    SDL_Texture * screen_texture;
}
sdl_platform_t;

/* ---[ IMPLEMENTATION ]---------------------------------------------------- */

# ifdef X_IMPLEMENTATION

#  include <stdarg.h>

static sdl_platform_t sdl;

/* -------------------------------------------------------------------------
 * Initialize and terminate platform driver
 * ------------------------------------------------------------------------- */

static void init_platform() {
    if( SDL_Init(SDL_INIT_VIDEO /*| SDL_INIT_TIMER | SDL_INIT_EVENTS*/
        | SDL_INIT_AUDIO) != 0)
    {
        console_error("video: Unable to initialize SDL: %s", SDL_GetError());
        SDL_Quit();
        exit(1);
    }

    sdl.window = NULL;
    sdl.renderer = NULL;
    sdl.screen_texture = NULL;

    SDL_LogSetPriority(SDL_LOG_CATEGORY_APPLICATION, SDL_LOG_PRIORITY_INFO);
    s32 v = SDL_SetThreadPriority(SDL_THREAD_PRIORITY_HIGH);
    if(v)
        console_error("thread priority");
}

static void terminate_platform() {
    SDL_DestroyTexture(sdl.screen_texture);
    SDL_DestroyRenderer(sdl.renderer);
    SDL_DestroyWindow(sdl.window);
    SDL_Quit();
}

/* TODO: should output to an errors.txt file when console is disabled */
extern void fatal_error(const char * fmt, ...) {
    va_list args;
    va_start(args, fmt);
    SDL_LogMessageV(SDL_LOG_CATEGORY_APPLICATION, SDL_LOG_PRIORITY_ERROR, fmt, args);
    va_end(args);
    terminate_platform();
    exit(EXIT_FAILURE);
}

static int cpu_L1_cache_line_size() {
    return SDL_GetCPUCacheLineSize();
}

#  ifdef X86
struct features_t {
    const char *name;
    SDL_bool (*test)();
} _x86_feats[] = {
    {"RDTSC", SDL_HasRDTSC},
    {"MMX", SDL_HasMMX},
    {"SSE", SDL_HasSSE},
    {"SSE2", SDL_HasSSE2},
    {"SSE3", SDL_HasSSE3},
    {"SSE41", SDL_HasSSE41},
    {"SSE42", SDL_HasSSE42},
    {"AVX", SDL_HasAVX},
    {"AVX2", SDL_HasAVX2},
    {"AVX512F", SDL_HasAVX512F}
};
#  endif /* X86 */
   
static void print_system_infos() {
    int mode_count = SDL_GetNumDisplayModes(0);

    console("Available system RAM %d Mb", SDL_GetSystemRAM());

    console("CPU cache line size: %d bytes", cpu_L1_cache_line_size());

#  ifdef X86
    console("The CPU has the following extensions:");
    for(int i = 0; i < sizeof(_x86_feats)/sizeof(_x86_feats[0]); i++)
        console(" - %s: %s",
            _x86_feats[i].name, _x86_feats[i].test() ? "yes" : "no");
#  endif

    console("SDL2 platform has %d video modes", mode_count);

    for(int i = 0; i < mode_count; i++)
    {
        SDL_DisplayMode dm;
        if( SDL_GetDisplayMode(0, i, &dm) < 0) {
            console_error("console_infos: SDL_GetDisplayMode failed (%d)", SDL_GetError());
            continue;
        }
        console("video mode #%02d : %dx%d %s", i, dm.w, dm.h,
            SDL_GetPixelFormatName(dm.format));
    }
}

/* -------------------------------------------------------------------------
 * Interactions
 * ------------------------------------------------------------------------- */

void update_platform_buttons() {
    const Uint8 * state = SDL_GetKeyboardState(NULL);
    button_state[BTN_UP] = state[SDL_SCANCODE_UP] != 0;
    button_state[BTN_DOWN] = state[SDL_SCANCODE_DOWN] != 0;
    button_state[BTN_LEFT] = state[SDL_SCANCODE_LEFT] != 0;
    button_state[BTN_RIGHT] = state[SDL_SCANCODE_RIGHT] != 0;
}

static int last_platform_event() {
    SDL_Event event;
    if (SDL_PollEvent(&event)) {
        switch (event.type) {
            case SDL_QUIT:
                return EVT_QUIT;

            case SDL_KEYDOWN:
                switch( event.key.keysym.sym )
                {
                case SDLK_RETURN:
                    if( event.key.keysym.mod & KMOD_CTRL )
                        return KEY_CTRL_ENTER;
                    else if( event.key.keysym.mod & KMOD_ALT )
                        return KEY_ALT_ENTER;
                    break;

                case SDLK_UP: return KEY_UP;
                case SDLK_DOWN: return KEY_DOWN;
                case SDLK_LEFT: return KEY_LEFT;
                case SDLK_RIGHT: return KEY_RIGHT;
                case SDLK_PAGEDOWN: return KEY_PAGEDOWN;
                case SDLK_PAGEUP: return KEY_PAGEUP;
                case SDLK_END: return KEY_END;
                case SDLK_HOME: return KEY_HOME;
                } 
                return event.key.keysym.sym;

        }
    }
    return 0;
}

/* -------------------------------------------------------------------------
 * Time functions
 * ------------------------------------------------------------------------- */

static u64 init_time() {
    return SDL_GetPerformanceCounter();
}

static float get_elapsed_time_and_restart(u64 * time) {
    u64 NOW = SDL_GetPerformanceCounter();
    u64 elapsed = NOW - (u64)(*time);
    float result = (float) elapsed * 1000.0f / SDL_GetPerformanceFrequency();
    *time = NOW;
    return result;
}

static float get_elapsed_time(const u64 * time) {
    u64 NOW = SDL_GetPerformanceCounter();
    u64 elapsed = NOW - (u64)(*time);
    float result = (float) elapsed * 1000.0f / SDL_GetPerformanceFrequency();
    return result;
}

/* -------------------------------------------------------------------------
 * Platform specific screen rendering and windowing management
 * ------------------------------------------------------------------------- */

/** blit the video 8-bit chunky frontbuffer to a 32-bit texture */
static inline void blit_video_to_screen32(int x, int y, u32 * destination) {
    u32 * dst = destination;
    u8  * src = video.frontbuffer;

    if(video.warping) {
        static float itime = 0;
        itime += .3f;

        pix32 c;
        v2s * uv  = video.warping;
        int r, g, b;
        for(int j = 0; j < H; j++) {
            for(int i = 0; i < W; i++) {

                if(uv->u < 0  || uv->u >= W || uv->v < 0 || uv->v >= H) {
                    *(dst++) = 0;
                }
                else
                {
                    int offset = uv->u + uv->v * W;
                    c.v = video.palette->colors[src[offset]].v;

                    r = c.r + sin(uv->v + itime) * 10;
                    g = c.g + sin(uv->v + itime) * 10; 
                    b = c.b + sin(uv->v + itime) * 10;

                    if(r < 0) r = 0; else if(r > 255) r = 255;
                    if(g < 0) g = 0; else if(g > 255) g = 255;
                    if(b < 0) b = 0; else if(b > 255) b = 255;

                    c.r = r;
                    c.g = g;
                    c.b = b;

                    *(dst++) = c.v; 
                }
                uv++;
            }
        }
    } else if( x || y) {
        const int clip_left = (x < 0) ? -x : 0;
        const int clip_top = (y < 0) ? -y : 0;
        const int clip_right = M_MIN(W - x, W);
        const int clip_bottom = M_MIN(H - y, H);

        u32 * d = destination;
        memset(d, 0, W * H * 4);

        for(int j = clip_top; j < clip_bottom; j++) {
            u8 * s = src + clip_left + j * W;
            u32 * d = dst + x + clip_left + (y+j) * W;
            for(int i = clip_right - clip_left; i > 0; i--)
                *(d++) = video.palette->colors[*(s++)].v;
        }
    } else {
        for(int j = W*H; j > 0; j--) 
            *(dst++) = video.palette->colors[*(src++)].v;
    }

#  if !defined(DISABLE_LOAD_TRACKING)
    if(!video.show_fps) return;

    int index = video.frame_count;
    for(int k = 0; k < FRAME_MAX_COUNT; k++) {
        
        u32 x = 60 * video.frame_load_history[index] / video.frame_time_history[index];

        index = (index + 1) & (FRAME_MAX_COUNT-1);

        x = (x >= W-4) ? W-4 : x;
        
        dst = destination + 2 + (k+2) * W;
        for(int i = 0; i < x; i++)
            *(dst++) = 0xFF00006D;
        *(dst++) = 0xFFA0A0FF;
        for(int i = x ; i < 60; i++) 
            *(dst++) = 0xFF00002D;
        destination[62 + (k+2) * W] = 0xFF00FF00;
    }
#  endif
}

/** gather the platform video texture and blit the framebuffer */
static void update_video() {
    u8 * screen_pixels;
    int pitch;
    void ** pixels = (void **)&screen_pixels;

    if( ! SDL_LockTexture(sdl.screen_texture, NULL, pixels, &pitch) )
    {
        switch( pitch ) {
            case (W*4):
                blit_video_to_screen32(video.x_offset, video.y_offset,
                    (u32 *)screen_pixels);
                break;

            default:
                console_error("video: Unsupported pitch %d (width %d)", pitch, W);
                SDL_Quit();
                exit(1);
        }

        SDL_UnlockTexture(sdl.screen_texture);
        SDL_RenderCopy(sdl.renderer, sdl.screen_texture, NULL, NULL);
        SDL_RenderPresent(sdl.renderer);
    }
    else
        console_error("video: Unable to lock screen texture");
}

void screenshot() {
    u32 * screen_pixels;
    int pitch;
    void ** pixels = (void **)&screen_pixels;

    if( ! SDL_LockTexture(sdl.screen_texture, NULL, pixels, &pitch) )
    {
        switch( pitch ) {
            case (W*4):
                save_tga(screen_pixels, W, H, 24);
                break;

            default:
                console_error("screenshot: Unsupported pitch %d (width %d)", pitch, W);
        }

        SDL_UnlockTexture(sdl.screen_texture);
    }
    else
        console_error("screenshot: Unable to lock screen texture");
}

void toggle_windowing_mode(windowing_mode_t mode)
{
    #define WINDOW_TOGGLE_DELAY  100

    switch( mode ) {
        case DISPLAY_WINDOW:
            SDL_Delay(WINDOW_TOGGLE_DELAY); /* wait for current callback to finish */
            SDL_SetWindowFullscreen(sdl.window, 0);
            SDL_SetWindowSize(sdl.window, W * video.pixel_scale, H * video.pixel_scale);
            SDL_ShowCursor(SDL_ENABLE);
            break;

        case DISPLAY_FULLSCREEN_DESKTOP:
            SDL_SetWindowFullscreen(sdl.window, SDL_WINDOW_FULLSCREEN_DESKTOP);
            SDL_ShowCursor(SDL_DISABLE);
            break;
        
        case DISPLAY_FULLSCREEN_NATIVE:
            SDL_Delay(WINDOW_TOGGLE_DELAY); /* wait for current callback to finish */
            SDL_SetWindowSize(sdl.window, W, H); /* to let SDL select the closest resolution */
            SDL_SetWindowFullscreen(sdl.window, SDL_WINDOW_FULLSCREEN);
            SDL_ShowCursor(SDL_DISABLE);
            break;
    }

    video.display_mode = mode;

    int display_index = SDL_GetWindowDisplayIndex(sdl.window);
    SDL_DisplayMode dm;
    if (SDL_GetCurrentDisplayMode(display_index, &dm) != 0) {
        console_debug("SDL_GetCurrentDisplayMode failed: %s", SDL_GetError());
        return;
    }
    console_debug("video: resolution on display #%02d is %dx%d @ %d Hz", display_index, dm.w, dm.h, dm.refresh_rate);
}

static void open_video()
{
    console_debug("video: framebuffer resolution is %dx%d", W, H);

    uint32_t flags = SDL_WINDOW_SHOWN;

    sdl.window = SDL_CreateWindow(TITLE,
        SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
        W * video.pixel_scale, H * video.pixel_scale, flags);

    SDL_ShowCursor(SDL_ENABLE);

    /* create renderer and texture */

    flags = SDL_RENDERER_ACCELERATED;
    if( video.with_vsync ) flags |= SDL_RENDERER_PRESENTVSYNC;

    SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "nearest");

    u32 pixel_format = SDL_GetWindowPixelFormat(sdl.window);
    console_debug("video: pixel format is '%s'",
        SDL_GetPixelFormatName(pixel_format));

    if(pixel_format == SDL_PIXELFORMAT_RGB888)
        video.pitch = 4;
    else
        fatal_error("video: unknown or not supported pixel format: %s",
            SDL_GetPixelFormatName(pixel_format));
    
    sdl.renderer = SDL_CreateRenderer(sdl.window, -1, flags);
    sdl.screen_texture = SDL_CreateTexture(sdl.renderer, pixel_format,
        SDL_TEXTUREACCESS_STREAMING, W, H);

    SDL_RenderSetLogicalSize(sdl.renderer, W, H);

    toggle_windowing_mode(video.display_mode);
}

/* -------------------------------------------------------------------------
 * Print functions
 * ------------------------------------------------------------------------- */

void enable_console_debug() {
    SDL_LogSetPriority(SDL_LOG_CATEGORY_APPLICATION, SDL_LOG_PRIORITY_DEBUG);
}

void console(const char * fmt, ...) {
    va_list args;
    va_start(args, fmt);
    SDL_LogMessageV(SDL_LOG_CATEGORY_APPLICATION, SDL_LOG_PRIORITY_INFO, fmt, args);
    va_end(args);
}

void console_debug(const char * fmt, ...) {
    va_list args;
    va_start(args, fmt);
    SDL_LogMessageV(SDL_LOG_CATEGORY_APPLICATION, SDL_LOG_PRIORITY_DEBUG, fmt, args);
    va_end(args);
}

void console_error(const char * fmt, ...) {
    va_list args;
    va_start(args, fmt);
    SDL_LogMessageV(SDL_LOG_CATEGORY_APPLICATION, SDL_LOG_PRIORITY_ERROR, fmt, args);
    va_end(args);
}

/* -------------------------------------------------------------------------
 * Memory
 * ------------------------------------------------------------------------- */

/* TODO: move in win_drv.c later.... */

void * mem_alloc_align(int size, int align) {
    return _mm_malloc(size, align);
}

void * mem_alloc_cache_align(int size) {
    return _mm_malloc(size, cpu_L1_cache_line_size());
}

void mem_free(void * area) {
    if(area) _mm_free(area);
}

# endif /* X_IMPLEMENTATION */

#endif /* X_CORE_SDL_DRV_H */
