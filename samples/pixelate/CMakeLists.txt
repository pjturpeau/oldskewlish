cmake_minimum_required(VERSION 3.0)

project(pixelate VERSION 0.0 LANGUAGES C)

set(SOURCES
    pixelate.c
    minitel.c
)

if(WIN32)
    set(RESOURCES resources.rc)
endif()

list(APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_SOURCE_DIR}/..")
include(samples)
