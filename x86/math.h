/**
 * x86/math.h
 * 
 * Copyleft (c) Pierre-Jean Turpeau 12/2019
 * <pierrejean AT turpeau DOT net>
 * 
 * mainly a merge of:
 * x/math/v2f.h
 * x/math/v3f.h
 * x/math/numbers.h
 * x/math/trig_s32.h
 */
#ifndef X86_MATH_H
#define X86_MATH_H

/* -------------------------------------------------------------------------
 * Fast float functions
 * ------------------------------------------------------------------------- */

extern int FastSmallIntSqrt(unsigned int x); // good approximation for values < 289
extern float QuakeSqrt(float x); // quake sqrt
extern float FastFloatAtan2(float x, float y); // http://pubs.opengroup.org/onlinepubs/009695399/functions/atan2.html
extern float FastFloatInverseSqrt( float number ); // https://en.wikipedia.org/wiki/Fast_inverse_square_root 

/* -------------------------------------------------------------------------
 * 2D vector functions
 * ------------------------------------------------------------------------- */

extern void Vector2fMul(v2f * product, const v2f * multiplier, float multiplicand);
extern void Vector2fAdd(v2f * sum, const v2f * augend, const v2f * addend );
extern void Vector2fSub(v2f * difference, const v2f * minuend, const v2f * subtrahend);
extern void Vector2fNormalize(v2f * v);
extern float Vector2fDotProduct(const v2f * a, const v2f * b);
extern void Vector2fCopy(v2f * to, const v2f * from);

/* -------------------------------------------------------------------------
 * 3D vector functions
 * ------------------------------------------------------------------------- */

extern void Vector3fMul(v3f * product, const v3f * multiplier, float multiplicand);
extern void Vector3fAdd(v3f * sum, const v3f * augend, const v3f * addend );
extern void Vector3fSub(v3f * difference, const v3f * minuend, const v3f * subtrahend);
extern void Vector3fNormalize(v3f * v);
extern void Vector3fCrossProduct(v3f * result, const v3f * a, const v3f * b);
extern float Vector3fDotProduct(const v3f * a, const v3f * b);
extern void Vector3fCopy(v3f * to, const v3f * from);

/* -------------------------------------------------------------------------
 * Sine look-up-tables generators
 * ------------------------------------------------------------------------- */

extern void ConfigureIntSineLUT(s32 lut[], int period, int bias, int amplitude); // v = bias + amplitude * sin(angle)
extern void ConfigureIntOscillatorLUT(s32 lut[], int bias, int center, int amplitude, float divider); //v = bias + amplitude * sin(angle / divider) * sin(angle)
extern void ConfigureIntDualSineLUT(s32 lut[], int period, int bias, int amplitude, v3f cos_conf, v3f sin_conf); // v = bias + cos_amplitude * (cos.amplitude * cos(cos.shift + angle * cos.speed) + sin.amplitude * sin(sin.shift + angle * sin.speed))

/* ---[ IMPLEMENTATION ]---------------------------------------------------- */
# ifdef X_IMPLEMENTATION

/* -------------------------------------------------------------------------
 * Fast float functions
 * ------------------------------------------------------------------------- */

int FastSmallIntSqrt(unsigned int x) {
    /* adjust bias */
    x  += 127 << 23;
    /* approximation of square root */
    x >>= 1;
    return x;
}

#  define SQRT_MAGIC_F 0x5f3759df

/** quake sqrt */
float QuakeSqrt(float x) {
    const float xhalf = 0.5f*x;

    union { /* get bits for floating value */
        float x;
        int i;
    } u;

    u.x = x;
    u.i = SQRT_MAGIC_F - (u.i >> 1);     /* gives initial guess y0 */
    return x*u.x*(1.5f - xhalf*u.x*u.x); /* Newton step, repeating increases accuracy */
}

/**
 * http://pubs.opengroup.org/onlinepubs/009695399/functions/atan2.html
 * Volkan SALMA */
float FastFloatAtan2(float x, float y) {
    const float ONEQTR_PI = M_PI / 4.0;
    const float THRQTR_PI = 3.0 * M_PI / 4.0;
    float r, angle;
    float abs_y = fabs(y) + 1e-10f; /* kludge to prevent 0/0 condition */
    if ( x < 0.0f )
    {
        r = (x + abs_y) / (abs_y - x);
        angle = THRQTR_PI;
    }
    else
    {
        r = (x - abs_y) / (x + abs_y);
        angle = ONEQTR_PI;
    }
    angle += (0.1963f * r * r - 0.9817f) * r;
    if ( y < 0.0f )
        return( -angle );     /* negate if in quad III or IV */
    else
        return( angle );
}

/** https://en.wikipedia.org/wiki/Fast_inverse_square_root */
float FastFloatInverseSqrt( float number ) {	
	const float x2 = number * 0.5F;
	const float threehalfs = 1.5F;

	union {
		float f;
		uint32_t i;
	} conv; 
    
    conv.f = number; /* member 'f' set to value of 'number'. */
	conv.i  = 0x5f3759df - ( conv.i >> 1 );
	conv.f  *= ( threehalfs - ( x2 * conv.f * conv.f ) );
	return conv.f;
}

/* -------------------------------------------------------------------------
 * 2D vector functions
 * ------------------------------------------------------------------------- */

void Vector2fMul(v2f * product, const v2f * multiplier, float multiplicand) {
    product->x = multiplier->x * multiplicand;
    product->y = multiplier->y * multiplicand;
}

void Vector2fAdd(v2f * sum, const v2f * augend, const v2f * addend ) {
    sum->x = augend->x + addend->x;
    sum->y = augend->y + addend->y;
}

/** difference = minuend - subtrahend */
void Vector2fSub(v2f * difference, const v2f * minuend, const v2f * subtrahend) {
    difference->x = minuend->x - subtrahend->x;
    difference->y = minuend->y - subtrahend->y;
}

void Vector2fNormalize(v2f * v) {
    float inverse_length = FastFloatInverseSqrt(v->x*v->x + v->y*v->y);

    v->x *= inverse_length;
    v->y *= inverse_length;
}

/** @return a.b */
float Vector2fDotProduct(const v2f * a, const v2f * b) {
    return a->x * b->x + a->y * a->y;
}

void Vector2fCopy(v2f * to, const v2f * from) {
    memcpy(to, from, sizeof(v2f));
}

/* -------------------------------------------------------------------------
 * 3D Vector functions
 * ------------------------------------------------------------------------- */

void Vector3fMul(v3f * product, const v3f * multiplier, float multiplicand) {
    product->x = multiplier->x * multiplicand;
    product->y = multiplier->y * multiplicand;
    product->z = multiplier->z * multiplicand;
}

void Vector3fAdd(v3f * sum, const v3f * augend, const v3f * addend ) {
    sum->x = augend->x + addend->x;
    sum->y = augend->y + addend->y;
    sum->z = augend->z + addend->z;
}

/** difference = minuend - subtrahend */
void Vector3fSub(v3f * difference, const v3f * minuend, const v3f * subtrahend) {
    difference->x = minuend->x - subtrahend->x;
    difference->y = minuend->y - subtrahend->y;
    difference->z = minuend->z - subtrahend->z;
}

void Vector3fNormalize(v3f * v) {
    float inverse_length = FastFloatInverseSqrt(v->x*v->x + v->y*v->y + v->z*v->z);

    v->x *= inverse_length;
    v->y *= inverse_length;
    v->z *= inverse_length;
}

/** result = a x b */
void Vector3fCrossProduct(v3f * result, const v3f * a, const v3f * b) {
    assert(result != a && result != b);
    result->x = a->y * b->z - a->z * b->y;
    result->y = a->z * b->x - a->x * b->z;
    result->z = a->x * b->y - a->y * b->x;
}

/** @return a.b */
float Vector3fDotProduct(const v3f * a, const v3f * b) {
    return a->x * b->x + a->y * b->y + a->z * b->z;
}

void Vector3fCopy(v3f * to, const v3f * from) {
    memcpy(to, from, sizeof(v3f));
}

/* -------------------------------------------------------------------------
 * Sine look-up-tables generators
 * ------------------------------------------------------------------------- */

/** simple equation: v = bias + amplitude * sin(angle) */
void ConfigureIntSineLUT(s32 lut[], int period, int bias, int amplitude) {
    for(int i = 0; i < period; i++) {
        lut[i] = bias + amplitude * sin((float) i * M_TAU / (float) period);
    }
}

/**
 * configure with amplitude oscillations:
 * v = bias + amplitude * sin(angle / divider) * sin(angle)
 */
void ConfigureIntOscillatorLUT(s32 lut[], int period, int bias, int amplitude, float divider) {
    for (int i = 0; i < period; i++) {
        float radian = (float) i * M_TAU / (float) period;
        float h = sin( radian * divider );
        lut[i] = (int)(bias + amplitude * h * sin(radian));
    }
}

/**
 * configure with both sin and cos:
 * v = bias + cos_amplitude * (cos_amplitude * cos(cos_shift+angle*cos_speed)
 *     + sin_amplitude * sin(sin_shift+angle*sin_speed))
 */
void ConfigureIntDualSineLUT(s32 lut[], int period, int bias, int amplitude, v3f cos_conf, v3f sin_conf) {
    for (int i = 0; i < period; i++) {
        float radian = i * M_TAU / period;
        lut[i] = (int)(bias + amplitude *
            (cos_conf.amplitude * cos(cos_conf.speed*radian+cos_conf.shift) +
                sin_conf.amplitude * sin(sin_conf.speed*radian+sin_conf.shift)));
    }
}

# endif /* X_IMPLEMENTATION */

#endif /* X86_MATH_H */
