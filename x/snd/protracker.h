/**
 * x/snd/protracker.h
 * Copyleft (c) Pierre-Jean Turpeau 12/2023
 * <pierrejean AT turpeau DOT net>
 *
 * Device independent data structures for protracker modules.
 *
 * Inspired by:
 *  - http://manual.freeshell.org/tracker/Html/The_Protracker_file_format.html
 *  - https://greg-kennedy.com/tracker/modformat.html
 *  - http://coppershade.org/articles/More!/Topics/Protracker_File_Format/
 */
#ifndef X_SND_PROTRACKER_H
#define X_SND_PROTRACKER_H

#include <x/x.h>

/* ---[ DEFINITIONS ]------------------------------------------------------- */

/* signatures to detect 31 samples (instead of 15 for older files) */
typedef enum
{
    MOD_SIGNATURE_MK1  = 0x4D2E4B2E,  /* M.K. => 31 samples */
    MOD_SIGNATURE_MK2  = 0x4D214B21,  /* M!K! => 31 samples, +64 patterns */
    MOD_SIGNATURE_FLT4 = 0x464C5434,  /* FLT4 => 31 samples, 4 channels */
    MOD_SIGNATURE_FLT8 = 0x464C5438,  /* FLT8 => 31 samples, 8 channels */
    MOD_SIGNATURE_4CHN = 0x3443484E,  /* 4CHN => 31 samples, 4 channels */
    MOD_SIGNATURE_6CHN = 0x3643484E,  /* 6CHN => 31 samples, 6 channels */
    MOD_SIGNATURE_8CHN = 0x3843484E   /* 8CHN => 31 samples, 8 channels */
}
mod_signature_t;

/* offset of signature to check for the number of samples */
#define MOD_SIGNATURE_OFFSET    1080

/* offset for samples data */
#define MOD_SAMPLE_OFFSET       20
#define MOD_SAMPLE_SIZE         30
#define MOD_SAMPLE_NAME_LEN     22
#define MOD_SAMPLES_MAXCOUNT    31

#define MOD_PATTERNS_MAXCOUNT   128

/* patterns are divided into rows which contains notes and effects */
#define MOD_PATTERN_DIVISIONS   64

#define MOD_SONG_TITLE_LEN      20

#define MOD_CHANNELS_MAXCOUNT   8

#define MOD_DEFAULT_SPEED       6
#define MOD_DEFAULT_BPM         125

/* AMIGA PAULA chip:
 * - 4 channels with independent volume (vol. in [0-64]) 
 * - 8 bits samples
 * - Maximum playback rate of 28Khz
 * - PAL clock is 3.546895 MHz
 * - PAL interrupt rate is 8287 Hz
 * - NTSC interrupt rate is 8363 Hz
 */
#define MOD_AMIGA_PAUL_PAL_FREQ     3546895 /* PAULA clock rate in Hz */
#define MOD_AMIGA_C2_NOTE_PERIOD    428     /* in cm - standard is 527.47cm / 65.41 Hz */

/* in most */
// #define MOD_AMIGA_PAL_FREQ      7093789
// #define MOD_AMIGA_C2_PERIOD     428
// #define MOD_AMIGA_C2_FREQ       (MOD_AMIGA_PAL_FREQ / (MOD_AMIGA_C2_PERIOD * 2))

/* Sample position of each channel has to be scaled to scan the MOD samples
 * audio chunks at the right pace compared to a higher replay frequency
 * (e.g. 44 KHz). As a recall, the Amiga PAL C-2 note frequency is 8287.14 Hz */
#define MOD_SAMPLE_FP_SHIFT     10

/* effects */
typedef enum
{
    MOD_FX_SET_VOLUME    = 0x0C,
    MOD_FX_PATTERN_BREAK = 0x0D,
    MOD_FX_SET_SPEED     = 0x0F
}
mod_fx_t;

typedef enum
{
    /*    /\  /\
        \/  \/    */
    MOD_WAVEFORM_SINE     = 0,

    /* |\ |\ |\
         \| \| \| */
    MOD_WAVEFORM_RAMPDOWN = 1,

    /* _   _
        |_| |_| */
    MOD_WAVEFORM_SQUARE   = 2,

    MOD_WAVEFORM_RANDOM   = 3
}
mod_waveform_t;

/* Periods of notes according to the octave for finetune = 0
 * Values aren't strictly respected by module trackers, and note effects
 * also alter the final value => search the one with the closest match. */
static const u16 mod_period_table[] =
{
/*    C     C#    D     D#    E     F    F#     G    G#     A    A#   B */
    1712, 1616, 1524, 1440, 1356, 1280, 1209, 1141, 1076, 1016, 960, 906, /* Octave 0 */
	 856,  808,  762,  720,  678,  640,  604,  570,  538,  508, 480, 453, /* Octave 1 */
	 428,  404,  381,  360,  339,  320,  302,  285,  269,  254, 240, 226, /* Octave 2 */
	 214,  202,  190,  180,  170,  160,  151,  143,  135,  127, 120, 113, /* Octave 3 */
	 107,  101,   95,   90,   85,   80,   76,   71,   67,   63,  60,  57  /* Octave 4 */
};

/* for display only: to match with the index found according to the period table
 * (mod_period_table) */
static const char *mod_note_names[] =
{
    "C-0", "C#0", "D-0", "D#0", "E-0", "F-0", "F#0", "G-0", "G#0", "A-0", "A#0", "B-0",
    "C-1", "C#1", "D-1", "D#1", "E-1", "F-1", "F#1", "G-1", "G#1", "A-1", "A#1", "B-1",
    "C-2", "C#2", "D-2", "D#2", "E-2", "F-2", "F#2", "G-2", "G#2", "A-2", "A#2", "B-2",
    "C-3", "C#3", "D-3", "D#3", "E-3", "F-3", "F#3", "G-3", "G#3", "A-3", "A#3", "B-3",
    "C-4", "C#4", "D-4", "D#4", "E-4", "F-4", "F#4", "G-4", "G#4", "A-4", "A#4", "B-4"
};

typedef struct
{
    u32 length;        /* 65535 words max and 2-bytes words */
    u32 loop_offset;
    u32 loop_length;
    u8  finetune;
    u8  volume;        /* 0-64 */

    char name[MOD_SAMPLE_NAME_LEN];

    s8 *data; /* MOD samples are 8 bits signed */
}
mod_sample_t;

typedef struct {
    const char * name; /* make it optional, or replace it with a 8-bit index to name table... */
    u16 period;
    u8  sample_id;  /* 1 - 31 (0 means no sample) */
    u8  fx;         /* standard FX id [0x0-0xF] - 0xE is extended fx */
    u8  fx_param;
}
mod_note_t;

/* used for the replay routine */
typedef struct
{
    int8_t * sample_data;
    u32 sample_pos;
    u32 sample_length;
    u32 sample_loop_pos;
    u32 sample_loop_len;

    /* last read note for that channel - mainly for dataviz */
    mod_note_t *replay_last_note;

    mod_waveform_t vibrato_waveform;

    u16 period;
    u8  volume;
    u8  finetune;

    // u8 last_sample; /* TODO/TBD/TBC */
}
mod_channel_t;

/* TODO: organize data for better alignment/padding */
typedef struct
{
    u32 signature;

    u8  number_of_samples;  /* 15 or 31 */
    u8  number_of_channels; /* 4, 6 or 8 */
    u8  number_of_patterns;
    u8  song_sequence_length; /* 1-128: number of patterns played in the song */

    mod_sample_t    samples[MOD_SAMPLES_MAXCOUNT];

    /* TODO: organize memory to have adjacent notes for all channels of a given division*/
    mod_note_t      patterns_notes[MOD_PATTERNS_MAXCOUNT][MOD_PATTERN_DIVISIONS][MOD_CHANNELS_MAXCOUNT];

    /* the sequence of patterns id that defines the layout of the song */
    u8     song_patterns[MOD_PATTERNS_MAXCOUNT]; /* TODO: rename song_sequence? */

    char        title[MOD_SONG_TITLE_LEN];

    /* Replay data */
    /* ----------- */

    /* TODO: work on renaming for better understanding... */
    u8 replay_sequence_index;       /* index in song_patterns[] */
    u8 replay_pattern_id;          /* index in patterns_notes[] => TODO: rename replay_pattern_index  */
    u8 replay_position_in_pattern; /* current position in the active pattern (i.e., division index) */

    /* SPEED = number of ticks to wait before processing the next row in pattern */
    u8  speed;

    /* BPM = tempo used to calculate the interrupt handler rate
     *      Hz = 2 * BPM / 5        milliseconds = 2500 / BPM
     * This may not be used that way with generic audio device API. */
    u8  bpm;

    /* Fixed point increment used to scan the sample buffers at the right pace.
     * It is equal to AMIGA_C2_FREQUENCY / REPLAY_FREQUENCY
     * In case of stereo replay, it has to be divided by 2 for the same
     * mixing buffer size. */
    u32 sample_pos_incr;

    /* The rate at which the main mixing operation is called by the audio
     * device. It is needed to scale everything compared to a legacy replay
     * routine which would call the interrupt handler according to the BPM */
    u16 mixing_frequency;

    /* Because of high frequency mixing, it's the total number of mixing chunks
     * to wait before next MOD tick (BPM dependent).
     * It is used to reset mixing_count_remaining after each MOD tick. */
    u32 mixing_count_before_tick;

    /* The current remaining number of mixing chunks to wait before next tick */
    u32 mixing_count_remaining;

    /* number of output audio channels: 1 = mono, 2 = stereo */
    u8  mixing_channels;

    s16  last_left_chunk;
    s16  last_right_chunk;

    mod_channel_t channels[MOD_CHANNELS_MAXCOUNT];

    /* true if a jump or break has been performed in the row */
    bool jump_fx;

    /* for debug */
    u16 unknown_fx[16];
}
mod_song_t;

/* ---[ DECLARATIONS ]------------------------------------------------------ */

extern void mod_set_tempo(mod_song_t* mod, u32 bpm);
extern void mod_set_speed(mod_song_t* mod, u32 speed);
extern const char * mod_note_name(u32 period);

/* ---[ IMPLEMENTATION ]---------------------------------------------------- */

# ifdef X_IMPLEMENTATION

void mod_set_tempo(mod_song_t* mod, u32 bpm) {
    /* Hz = BPM * 2 / 5
     *
     * => mixing_count_tick = (song_frequency * 5 / 2) / song_BPM
     */
    mod->mixing_count_before_tick  = (mod->mixing_frequency * 5 >> 1) / bpm;
    mod->bpm = bpm;
}

void mod_set_speed(mod_song_t* mod, u32 speed) {
    mod->speed = speed;
}

static u32 _mod_note_index(u32 period);

/**
 * Returns the closest name of the note that matches the given period in the
 * mod_period_table.
 */
const char * mod_note_name(u32 period) {
    return mod_note_names[_mod_note_index(period)]; 
}

/**
 * Returns the closest index of the note that matches the given period according
 * to the mod_period_table.
 *
 * This is a very trivial search, but it is mainly used to retrieve a note name
 * during the loading phase, so we don't care of the speed here.
 */
static u32 _mod_note_index(u32 period) {
    u32 num_periods = sizeof(mod_period_table) / sizeof(mod_period_table[0]);

    for(u32 i = 0; i < num_periods; i++) {
        if(period == mod_period_table[i]) {
            return i;
        }
        else if(period > mod_period_table[i]) {
            if(0 == i)
                return i;
            else
                if (mod_period_table[i] - period < period - mod_period_table[i-1])
                    return i-1;
                else
                    return i;
        }
    }
    return num_periods-1;
}
# endif /* X_IMPLEMENTATION */

#endif /* X_SND_PROTRACKER_H */
