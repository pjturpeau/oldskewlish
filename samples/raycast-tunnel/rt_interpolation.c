/**
 * raycast-tunnel/rt_interpolation.c
 * Copyleft (c) Pierre-Jean Turpeau 01/2020
 * <pierrejean AT turpeau DOT net>
 *
 * The one using interpolation over a grid
 */
#include <x/x.h>
#include <x/3d/m3f.h>
#include <math.h>

#define GRID_SIZE   8
#define FP_SIZE     16 /* fixed point precision */

extern image_t * g_texture;

static m3f _rotation_matrix;
static v3f _origin;

static float _radius = 256.0f;

static v2s _grid[H/GRID_SIZE+1][W/GRID_SIZE+1];

static void _get_uv(int x,int y, int *u, int *v)
{
    v3f initial_direction = {{x - W/2, y - H/2, 256.f}};
    v3f direction;

    m3f_mul_vec(&direction, &_rotation_matrix, &initial_direction);
    Vector3fNormalize(&direction);

    /* 
     * It's a conic problem. We have to calculate the intersection between
     * a cylinder centered on (0,0) along the Z axis with the following
     * equation:   x^2 + y^2 = r^2 (equation of a circle for all z)
     * 
     * We need to find the intersection between this cylinder and a ray that
     * has an origin and a direction vectors: ray = origin + t * direction
     * 
     * Here we know the origin, we know the direction, and we know the radius
     * of our cylinder, thus we need to find t such as if we substitute (x,y)
     * by the ray coordinates in the cylinder equation, the result is equal
     * to radius^2
     * 
     * The full expanded equation is:
     * 
     *     t^2*(direction.x^2 + direction.y^2)
     *     + t*2*(origin.x*direction.x + origin.y*direction.y)
     *     + origin.x^2 + origin.y^2 - r^2 = 0;
     * 
     * it's a quadric that we can resolve using standard maths.
     */

    float a = direction.x * direction.x + direction.y * direction.y;
    float b = 2*(_origin.x * direction.x + _origin.y * direction.y);
    float c = _origin.x * _origin.x + _origin.y * _origin.y - _radius * _radius;

    /* calculate discriminent delta */
    float delta = b*b - 4*a*c;

    float t;

    /* if there's no real solution */
    if (delta < 0) {
        *u = 128;
        *v = 128;
        return;
    }
    else if ( delta == 0.0f ) {
        t  = -b/(2*a);
    }
    else {
        /* there are 2 solutions, get the nearest */
        delta = sqrtf(delta);
        float t1 = -b + delta;
        float t2 = -b - delta;
        t = t1 <= t2 ? t1 : t2;
        t /= 2*a;
    }

    /* finally compute the intersection
     *      intersection = direction * t + origin */
    Vector3fMul(&direction, &direction, t);
    Vector3fAdd(&direction, &direction, &_origin);

    /* do the mapping within the texture */
    *u = (int)(fabs(direction.z) * 0.15f);
    *v = (int)(fabs(atan2f(direction.y, direction.x) * 256 / M_PI));
}

/* Could certainly be faster if all floating point numbers are changed for fixed point ones */
void rt_interpolation(float delta_time)
{
    static float posangle = 0;

    const u8* pixs = g_texture->pixels;

    int u = 0;
    int v = 0;

    posangle += delta_time * 0.0005f;

    _origin.x = 30 * cosf(posangle-0.25f) +  90 * sinf(posangle*2.f);
	_origin.y = 60 * sinf(posangle+0.25f) - 110 * cosf(posangle+0.75f);
    _origin.z -= delta_time * 0.5f; /* run along the tunnel */

    float xrotangle = sinf(posangle * 2) * 0.5f;
    float zrotangle = cosf(posangle);
    float yrotangle = xrotangle * zrotangle * 1.25f;

    m3f_rotation(&_rotation_matrix, xrotangle, yrotangle, -zrotangle);

    for(int j = 0; j <= H/GRID_SIZE; j++) {
        for(int i = 0; i <= W/GRID_SIZE; i++) {
            _get_uv(i * GRID_SIZE, j * GRID_SIZE, &u, &v);
            _grid[j][i].u = u;
            _grid[j][i].v = v;
        }
    }

   /*
    * now computing left and right du/dv to interpolate within the texture
    * along the y axis on the screen, so that we're able to compute dx/dx and
    * interpolate onto each horizontal scanline to draw the pixels.
    * Very naive and simple version, there's room for optimization everywhere
    */

    long left_u, left_v, left_du, left_dv;
    long right_u, right_v, right_du, right_dv;

    long scanline_u, scanline_v, scanline_du, scanline_dv;

    for (int j = 0; j < H/GRID_SIZE; j++) {
        for (int i = 0; i < W/GRID_SIZE; i++) {
            left_du = ((_grid[j+1][i].u - _grid[j][i].u) << FP_SIZE) / GRID_SIZE;
            left_dv = ((_grid[j+1][i].v - _grid[j][i].v) << FP_SIZE) / GRID_SIZE;

            right_du = ((_grid[j+1][i+1].u - _grid[j][i+1].u) << FP_SIZE) / GRID_SIZE;
            right_dv = ((_grid[j+1][i+1].v - _grid[j][i+1].v) << FP_SIZE) / GRID_SIZE;

            left_u = _grid[j][i].u << FP_SIZE;
            left_v = _grid[j][i].v << FP_SIZE;

            right_u = _grid[j][i+1].u << FP_SIZE;
            right_v = _grid[j][i+1].v << FP_SIZE;

            u8 * video = backbuffer() + i * GRID_SIZE + W * j * GRID_SIZE;

            for (int n = 0; n < GRID_SIZE; n++) {
                scanline_u = left_u;
                scanline_v = left_v;

                scanline_du = (right_u - left_u) / GRID_SIZE;
                scanline_dv = (right_v - left_v) / GRID_SIZE;

                for (int m = 0; m < GRID_SIZE; m++ ) {
                    *(video++) = pixs[((scanline_u >> FP_SIZE) + ((scanline_v >> FP_SIZE) <<8))  & 0xFFFF];

                    scanline_u += scanline_du;
                    scanline_v += scanline_dv;
                }
                video += W - GRID_SIZE;

                left_u += left_du;
                left_v += left_dv;

                right_u += right_du;
                right_v += right_dv;
            }
        }
    }
}
