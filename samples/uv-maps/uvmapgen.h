/**
 * samples/uv-maps/uvmapgen.h
 * 
 * Copyleft (c) Pierre-Jean Turpeau 01/2020
 * <pierrejean AT turpeau DOT net>
 * 
 * I try to use meaningful names...
 */
#ifndef UV_MAP_GEN_H
#define UV_MAP_GEN_H

#include <x86/math.h>

#include "uvmap.h"

v3u8 uv_map_gen_tunnel(uv_mapping * fx, float x, float y, float angle, float radius)
{
    v3u8 uvw;
    uvw.u = 0.4f * fx->texture->w / radius;
    uvw.v = fx->texture->h * angle / M_PI;

    if( radius <= 0.15f )
        uvw.w = fx->shadow_levels - 1;
    else
        uvw.w = fx->shadow_levels * 0.15f / radius;
    return uvw;
}

v3u8 uv_map_gen_concentric_waves(uv_mapping * fx, float x, float y, float angle, float radius)
{
    v3u8 uvw;
    uvw.u = fx->texture->w * angle / M_TAU;
    uvw.v = fx->texture->h * (radius + radius * sin(radius * 4)) / M_PI;
    uvw.w = 0;
    return uvw;
}

v3u8 uv_map_gen_warping(uv_mapping * fx, float x, float y, float angle, float radius)
{
    v3u8 uvw;
    radius = x *x + y *y + 2.0f;
    uvw.u = fx->texture->w * x * 2.0f / radius;
    uvw.v = fx->texture->h * y * 2.0f / radius;
    uvw.w = 0;
    return uvw;
}


v3u8 uv_map_gen_tv_warping(uv_mapping * fx, float x, float y, float angle, float radius)
{
# define WARP 3.5f
    v3u8 uvw;

    y /= 2.f;
    x /= (2.f * W / (float) H);

    float dx = x * x;
    float dy = y * y;

    x = x * (1.f + (dy * (.3f * WARP)));
    y = y * (1.f + (dx * (.4f * WARP)));
    
    /* square radius */
    radius = pow( pow(x*x,4.0) + pow(y*y,4.0), 1.0/10.0 );
    for(int k = 0; k < 6; k++)
        radius = M_SMOOTHSTEP(radius);

    x += .5f;
    y += .5f;

    uvw.u = fx->texture->w * x;
    uvw.v = fx->texture->h * y;
    uvw.w = 0;

    if(x < .0f || x > 1.f || y < .0f || y > 1.f)
        uvw.w = fx->shadow_levels-1;
    else
        uvw.w = fx->shadow_levels * radius;

    return uvw;
}

v3u8 uv_map_gen_warping_near(uv_mapping * fx, float x, float y, float angle, float radius)
{
    v3u8 uvw;
    float r = x *x + y *y;
    uvw.u = fx->texture->w * x * 0.3f / r;
    uvw.v = fx->texture->h * y * 0.3f / r;
    if(radius <= 0.15f)
        uvw.w = fx->shadow_levels-1;
    else
        uvw.w = fx->shadow_levels * 0.15f / radius;
    return uvw;
}


v3u8 uv_map_gen_warping_far(uv_mapping * fx, float x, float y, float angle, float radius)
{
    v3u8 uvw;
    float r = x *x + y *y;
    uvw.u = fx->texture->w * x * 1.2f / r;
    uvw.v = fx->texture->h * y * 1.2f / r;
    if(radius <= 0.3f )
        uvw.w = fx->shadow_levels-1;
    else
        uvw.w = fx->shadow_levels * 0.3f / radius;
    return uvw;


}


v3u8 uv_map_gen_swirl_tunnel(uv_mapping * fx, float x, float y, float angle, float radius) {
    v3u8 uvw;

    v2f p;
    p.x = x;
    p.y = y;

    float r = sqrt(Vector2fDotProduct(&p, &p));
    float a = angle + .1f * sin(r * 10.f);
    float s = .8 * cos(7.f * a);
    float w = (2.f + 1.5f * s) * r;

    uvw.u = fx->texture->w * .9 / (r + .2 * s);
    uvw.v = fx->texture->h * 3.f  * a / M_PI;

    if( w < 0.001f )
        uvw.w = fx->shadow_levels-1;
    else
        uvw.w = fx->shadow_levels * (1.f - w * (1.f/4.87f));

    return uvw;
}

/* --------------------------------------------------------------------------
 * below are transformations inspired by
 * http://www.iquilezles.org/www/articles/deform/deform.htm
 * --------------------------------------------------------------------------*/

v3u8 uv_map_gen_symetric_planes(uv_mapping * fx, float x, float y, float angle, float radius)
{
    v3u8 uvw;
    uvw.u = 0.5 * fx->texture->w * x / fabs(y);
    uvw.v = 0.5 * fx->texture->h / fabs(y);

    if(fabs(y) < 0.15f)
        uvw.w = fx->shadow_levels - 1;
    else
        uvw.w = fx->shadow_levels * 0.15 / fabs(y);

    return uvw;
}

v3u8 uv_map_gen_star_tunnel(uv_mapping * fx, float x, float y, float angle, float radius)
{    
    v3u8 uvw;

    float h = 0.5 + 0.5*cos(10.0 * angle);
    float s = M_SMOOTHSTEP(h);
    
    uvw.u = fx->texture->w / (radius + 0.15 * s);
    uvw.v = fx->texture->h * 2.0f * angle / M_PI;

    if(radius + 0.15 * s < 0.2)
        uvw.w = fx->shadow_levels - 1;
    else
        uvw.w = fx->shadow_levels * 0.2 / (radius + 0.15 * s);

    return uvw;
}

v3u8 uv_map_gen_flat_swirl(uv_mapping * fx, float x, float y, float angle, float radius)
{
    v3u8 uvw;
    uvw.u = fx->texture->w *(x * cos(2 * radius) - y * sin(2 * radius));
    uvw.v = fx->texture->h *(y * cos(2 * radius) + x * sin(2 * radius));
    uvw.w = 0;
    return uvw;
}

v3u8 uv_map_gen_flower(uv_mapping * fx, float x, float y, float angle, float radius)
{
    v3u8 uvw;

    /* swirl it... */
    /* angle += sin(radius); */
    uvw.u = fx->texture->w / (radius + 0.5f + 0.5f * sin(5 * angle));
    uvw.v = fx->texture->h * angle * 2.0f / M_PI;
    uvw.w = 0;
    return uvw;
}

v3u8 uv_map_gen_big_flower(uv_mapping * fx, float x, float y, float angle, float radius)
{
    v3u8 uvw;
    uvw.u = fx->texture->w * (0.05 * y + 0.1 * cos(angle * 2) / radius);
    uvw.v = fx->texture->h * (0.05 * x + 0.1 * cos(angle * 2) / radius);
    uvw.w = 0;
    return uvw;
}

v3u8 uv_map_gen_square_tunnel(uv_mapping * fx, float x, float y, float angle, float radius)
{
    v3u8 uvw;
    float r = pow( pow(x*x,4.0) + pow(y*y,4.0), 1.0/8.0 );
    uvw.u = fx->texture->w * 0.3 / r;
    uvw.v = fx->texture->h * angle / M_PI;
    if( r <= 0.1f)
        uvw.w = fx->shadow_levels - 1;
    else
        uvw.w = fx->shadow_levels * 0.1f / r;
    return uvw;
}

v3u8 uv_map_gen_bubble(uv_mapping * fx, float x, float y, float angle, float radius)
{
    v3u8 uvw;
    float mx = 0.65;
    float my = 0.55;

    float sx = x - mx;
    float sy = y - my;
    float a1 = atan2(sy, sx);
    float r1 = sqrt(sx*sx + sy*sy);

    sx = x + mx;
    sy = y + my;
    float a2 = atan2(sy, sx);
    float r2 = sqrt(sx*sx + sy*sy);

    uvw.u = fx->texture->w * (r1 - r2 ) * 0.25;
    uvw.v = fx->texture->h * asin(sin(a1-a2)) / M_PI;
    uvw.w = 0;
    return uvw;
}

v3u8 uv_map_gen_noname(uv_mapping * fx, float x, float y, float angle, float radius)
{
    v3u8 uvw;
    uvw.u = fx->texture->w * 0.5 * (x + 0.5 * cos(angle * 2.0f)/radius);
    uvw.v = fx->texture->h * 0.5 * (y + 0.5 * sin(angle * 2.0f)/radius);
    uvw.w = 0;
    return uvw;
}

v3u8 uv_map_gen_excentric_tunnel(uv_mapping * fx, float x, float y, float angle, float radius)
{
    v3u8 uvw;
    uvw.u = fx->texture->w * 0.3 / (radius + 0.5 * x);
    uvw.v = fx->texture->h * 3.0 * angle / M_PI;
    uvw.w = 0;
    if( (radius + 0.5 * x) <= 0.1f)
        uvw.w = fx->shadow_levels - 1;
    else
        uvw.w = fx->shadow_levels * 0.1 / (radius + 0.5 * x);    
    return uvw;
}    

v3u8 uv_map_gen_swirl(uv_mapping * fx, float x, float y, float angle, float radius)
{
    v3u8 uvw;
    angle += sin(2.5f * radius);
    uvw.u = fx->texture->w * 0.25f * cos(angle) / radius;
    uvw.v = fx->texture->h * 0.25f * sin(angle) / radius;
    uvw.w = 0;

    return uvw;
}

#endif /* UV_MAP_GEN_H */
