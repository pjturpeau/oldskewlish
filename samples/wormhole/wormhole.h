/**
 * samples/wormhole/wormhole.h
 * 
 * Copyleft (c) Pierre-Jean Turpeau 12/2019
 * <pierrejean AT turpeau DOT net>
 *
 * Code inspired by: Calogiuri Enzo Antonio [Insolit Dust] 
 */
#ifndef X_WORMHOLE_H
#define X_WORMHOLE_H

#include <x86/x.h>
#include <x86/image.h>

#include <stdio.h>
#include <math.h>

#define WH_TEXTURE_WIDTH 16

typedef struct {
    image_t * map;
    image_t * texture;

    u32 startu;
    u32 startv;
}
wormhole_t;

void InitWormhole(wormhole_t * wh, image_t * map, image_t * texture) {
    wh->map = map;
    wh->texture = texture;
    wh->startu = 0;
    wh->startv = 0;
}

void UpdateWormhole(wormhole_t * wh, float elapsed_time) {
    static float previous_time = 0;

    elapsed_time *= 0.8;

    float dt = elapsed_time - previous_time;

    wh->startu += dt * 30 * cos(elapsed_time/3024);
    wh->startv += WH_TEXTURE_WIDTH * dt * 2 * sin(elapsed_time/4024);

    previous_time = elapsed_time;
}

void DrawWormhole(wormhole_t * wh) {
    u8 * texture_pixels = (u8 *) (wh->texture->pixels);
    u8 * map_data = (u8 *) (wh->map->pixels);
    u8 * video = GetBackbuffer();
    for(int i = 0; i < W * H; i++) {
        /* for 16x16 texture sized only */
        video[i] = texture_pixels[
            ((wh->startu >> 8) + (wh->startv >> 8)  * wh->texture->w +
            map_data[i]) & 0xFF];

        /* for greater texture size, could be replaced by the following
         * code, _wormhole shall be of type u32[] though: */
        /* image.pixels[i] = texture_pixels[
             (u32(startu) + u32(startv * texture->w) +
              _wormhole[i]) % (texture->w * texture->h)];
        */
    }
}

// generate the wormhole map and save it to a bmp file.
// divs & spokes should be multiple of divisors, once divided, the result
// should be multiple of the texture size
void GenerateWormholeMap(
        const char * filename, 
        int width,      int height,
        int centerx,    int centery,
        int spokes,     int spokes_divisor,
        int divs,       int divs_divisor,
        int stretch,    bool vertical)
{
    Console("Gen: %dx%d (%d,%d) spokes=%d,%d divs=%d,%d strech=%d %s",
        width, height, centerx, centery,
        spokes, spokes_divisor, divs, divs_divisor, stretch,
        vertical ? "vertical" : "horizontal");

    u8 * map = (u8 *) malloc(width * height);
    assert(map);

    for(int j = 1; j < divs + 1; j++) {
        float z = -0.5f + (log(5.0f * j / divs)); /* TODO: use optimized log */

        float div_calc_x = (float) width * j / (float) divs;
        float div_calc_y = (float) height * j / (float) divs;

        printf(" [%2d%%]\r", j * 100 / divs);

        for (int i = 0; i < spokes; i++) {
            float angle = 2.0f * M_PI * i / spokes;
            
            /* todo: use precalc sin */
            float x = div_calc_x * cos(angle);
            float y = div_calc_y * sin(angle);
            
            /* this creates the downward curve in the center */
            if (vertical) 
                y -= stretch * z;
            else
                x -= stretch * z;

            x += centerx;
            y += centery;
            
            if ((x >= 0) && (x < width) && (y >= 0) && (y < height)) {
                uint8_t color =
                    (uint8_t)( ((i/spokes_divisor) % WH_TEXTURE_WIDTH) + 
                        WH_TEXTURE_WIDTH * ( (j/divs_divisor) % WH_TEXTURE_WIDTH) );

                map[(int) x + (int) y * width] = color;
            }
        }
    }

    palette_t bw;
    LinearGradientPalette(0, 0, 255, 0xFFFFFF);
    SavePalette(&bw);

    SaveTGA(filename, map, width, height, 8, &bw);
    free(map);
    Console("%s built!", filename);
}

#endif /* X_WORMHOLE_H */
