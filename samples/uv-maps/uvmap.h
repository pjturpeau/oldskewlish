/**
 * samples/uv-maps/uvmap.h
 * 
 * Copyleft (c) Pierre-Jean Turpeau 12/2019
 * <pierrejean AT turpeau DOT net>
 * 
 * Generalized UV 2D mapping.
 */
#ifndef UV_MAP_H
#define UV_MAP_H

#include <x86/image.h>

#include <math.h>

#define UV_MAP_WIDTH 256

typedef struct {
    u8 u, v, w;
    u8 padding;
} v3u8;

typedef struct {
    float start_u;
    float start_v;
    int useful_colors; /* number of usefull colors in the texture palette */
    int shadow_levels; /* number of possible gradient levels in the palette */
    image_t * texture;
    v3u8 * uv_map;
}
uv_mapping;


typedef v3u8 (*uv_map_gen_func)(uv_mapping * fx, float x, float y, float angle, float radius);

extern void uv_map_init(uv_mapping * fx, image_t * texture);
extern void uv_map_set_useful_colors(uv_mapping * fx, int nb_colors);
extern void uv_map_generate(uv_mapping * fx, uv_map_gen_func gen_func);

extern void uv_map_draw(const uv_mapping * fx);

extern void uv_map_draw_u_map(uv_mapping * fx);
extern void uv_map_draw_v_map(uv_mapping * fx);
extern void uv_map_draw_w_map(uv_mapping * fx);
extern void uv_map_draw_texture(uv_mapping * fx);

/* ---[ IMPLEMENTATION ]---------------------------------------------------- */

#ifdef X_IMPLEMENTATION

void uv_map_init(uv_mapping * fx, image_t * texture) {
    assert(texture->w == UV_MAP_WIDTH && texture->h == UV_MAP_WIDTH);
    fx->start_u = 0;
    fx->start_v = 0;
    fx->useful_colors = 0;
    fx->shadow_levels = 0;
    fx->texture = texture;
    fx->uv_map = (v3u8 *) malloc(W*H*sizeof(v3u8));
    assert(fx->uv_map);
}

void uv_map_set_useful_colors(uv_mapping * fx, int nb_colors) {
    assert(nb_colors > 0); /* bad useful_colors parameter */
    fx->useful_colors = nb_colors;
    fx->shadow_levels = MAX_COLORS / fx->useful_colors;
}

void uv_map_generate(uv_mapping * fx, uv_map_gen_func gen_func) {
    for(int j = 0; j < H; j++) {
        float y = 2.f * j / (float) H - 1.f; /* range -1..1 */
        for(int i = 0; i < W; i++) {

            float x = (i - W * .5f) / (H * .5f);
            
            float angle = atan2(y, x);
            float radius = sqrt(x * x + y * y);

            fx->uv_map[i + j * W] = gen_func(fx, x, y, angle, radius);
        }
    }
}

void uv_map_draw(const uv_mapping * fx) {
    u8 * video = GetBackbuffer();
    v3u8 * uvw = fx->uv_map;
    for( int j = 0; j < W * H; j++, uvw++) {
        int s_u = (int) fx->start_u;
        int s_v = (int) fx->start_v;

        u32 texture_offset = (s_u + uvw->u + ((uvw->v + s_v) << 8)) & 0xFFFF;

        *(video++) = fx->texture->pixels[texture_offset] + uvw->w * fx->useful_colors;
    }
}

void uv_map_draw_u_map(uv_mapping * fx) {
    for( int j = 0; j < W * H; j++)
        GetBackbuffer()[j] = fx->uv_map[j].u;
}

void uv_map_draw_v_map(uv_mapping * fx) {
    for( int j = 0; j < W * H; j++)
        GetBackbuffer()[j] = fx->uv_map[j].v;
}       
void uv_map_draw_w_map(uv_mapping * fx) {
    for( int j = 0; j < W * H; j++)
        GetBackbuffer()[j] = fx->uv_map[j].w;
}

void uv_map_draw_texture(uv_mapping * fx) {
    DrawImage_c(fx->texture, (W - UV_MAP_WIDTH)>>1, 0);
}

#endif /* X_IMPLEMENTATION */

#endif /* UV_MAP_H */
