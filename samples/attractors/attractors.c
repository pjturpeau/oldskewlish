/**
 * samples/attractors/attractors.c
 * 
 * Copyleft (c) Pierre-Jean Turpeau 01/2020
 * <pierrejean AT turpeau DOT net>
 * 
 * TODO: preliminary experiment - highly unfinished
 */
#define X_IMPLEMENTATION 1

#define TITLE "-[ attractors ]-"

#include <math.h>

#include <x86/image.h>
#include <x86/draw.h>
#include <x86/extras.h>

#define DOTS_RESOLUTION_FACTOR 1
#define DOTS_DISPLAY_COUNT 10000
#define DOTS_COUNT (DOTS_DISPLAY_COUNT * DOTS_RESOLUTION_FACTOR)

bool pause_time = false;

v3f dots[DOTS_COUNT];
int visible_points_count = 0;
int start_index = 0;

void draw_dots(int eq_num, float dt) {
    static float elapsed_time = 0;

    if(!pause_time)
        elapsed_time = dt * 0.001;

    /* simulate moving points */
    static int advance = 0;
    advance += 50;
    
    /* progressive appearance */
    if(visible_points_count < DOTS_DISPLAY_COUNT) {
        s32 n = 100;
        while(n && visible_points_count < DOTS_DISPLAY_COUNT) {
            v3f d = dots[visible_points_count-1];

            // Lorenz
            // dots[visible_points_count].x = d.x + 0.01 * (10 * (d.y - d.x));
            // dots[visible_points_count].y = d.y + 0.01 * (d.x * (20 - d.z) - d.y);
            // dots[visible_points_count].z = d.z + 0.01 * (d.x * d.y - 2.667 * d.z);

# define F 0.01

            // dots[visible_points_count].x = d.x + F * (10.0 * (d.y - d.x));
            // dots[visible_points_count].y = d.y + F * (d.x * (20.0 - d.z) - d.y);
            // dots[visible_points_count].z = d.z + F * (d.x * d.y - 2.667 * d.z);


            // Pickover 3D 
            dots[visible_points_count].x = sin(2.24 * d.y) - d.z * cos(.43*d.x);
            dots[visible_points_count].y = d.z * sin(-.65*d.x) - cos(-2.1*d.y);
            dots[visible_points_count].z = sin(d.x);

            n--;
            visible_points_count++;
        }

        // visible_points_count += dt;
        if(visible_points_count >= DOTS_DISPLAY_COUNT)
            visible_points_count = DOTS_DISPLAY_COUNT;
    } else {
        /* progressive evolution */
        // s32 n = dt * 100;
        // s32 k = start_index;
        // dots[0] = dots[100];
    }
    
    u8 * video = GetBackbuffer();

    for(int k = 0; k < visible_points_count; k++) {
        int i = (advance + k * DOTS_RESOLUTION_FACTOR) % DOTS_COUNT;

        float t = M_TAU * (float) i / DOTS_COUNT;

        if(k) {
            v3f d = dots[k-1];

            float ddt = 0.01f;

            // dots[k].x = d.x + ddt * (10.0 * (d.y - d.x)) + cos(elapsed_time);
            // dots[k].y = d.y + ddt * (d.x * (20.0 - d.z) - d.y);
            // dots[k].z = d.z + ddt * (d.x * d.y - 2.667 * d.z);

            dots[k].x = sin(2.24 * d.y) - d.z * cos(.43*d.x) + cos(elapsed_time*0.1);;
            dots[k].y = d.z * sin(-.65*d.x) - cos(-2.1*d.y);
            dots[k].z = sin(d.x);

        }

        float dz = 16.0f / (15.0f + dots[k].z);

        s32 u = W/2 + W/4 * dots[k].x * dz;
        s32 v = H/2 + H/4 * dots[k].y * dz;

        s32 c,o;
# define PUT(DU,DV,C) \
    { o=u+DU+(v+DV)*W; c = video[o] + C; if(c>255) c=255; video[o]=c;}

        if( u >= 1 && v >= 1 && u < W-1 && v < H-1) {
            PUT(-1,-1, 8); PUT(0,-1,16); PUT(1,-1, 8);
            PUT(-1, 0,16); PUT(0, 0,32); PUT(1, 0,16);
            PUT(-1, 1, 8); PUT(0, 1,16); PUT(1, 1, 8);
        }        

        // if( u >= 0 && v >= 0 && u < W && v < H) {
        //     s32 c = 16;
        //     c += video[u + v * W];
        //     if(c > 255) c = 255;
        //     video[u + v * W] = c; 
        // }
    }
}

void AttenuateBackbuffer() {
# define ATTENUATION_FACTOR (7 * (1<<16) / 10) /* 0.7 in 16.16 fixed point */
    u8 * video = GetBackbuffer();
    for(int i = W * H; i > 0; i--, video++)
        *video = (*video * ATTENUATION_FACTOR) >> 16;
}

void Animate(float dt) {
    static int eq_num = 0;
    static bool attenuate = true;

    int e = GetLastEvent();
    if (e == KEY_ENTER) {
        pause_time = ! pause_time;
    } else if (e == 'a' || e == 'A') {
        attenuate = ! attenuate;
    }

    if(attenuate)
        AttenuateBackbuffer();
    else
        ClearScreen(0);

    draw_dots(0, dt);
}

int main(int argc, char ** argv) {
    InitPlatform(argc, (const char **) argv);
    ParseDefaultExeOptions();

    //  Console("press [SPACE] to change curve parameters");
    Console("press [ENTER] to pause movement");
    Console("press [A] to disable/enable backbuffer attenuation");
    Pause(2000);
 
    SetVGAMode();

    v3f ambiant_color = {{0, 0.05, 0.1}};
	v3f diffuse_color = {{0.2, 0.2, 0.8}};
	v3f specular_color = {{0.7, 0.8, 0.9}};
    LightingGradientPalette(0, 255, ambiant_color, diffuse_color, specular_color, 1);

    for(int i = 0; i < DOTS_COUNT; i++)
        dots[i].x = dots[i].y = dots[i].z = 1.0f;
    
    visible_points_count = 1;

    while(!IsPlatformExit()) {
        BeginFrameLoop();
        ExecuteDefaultInteractions();
    
        if (! IsPaused())
            Animate(GetTime());

        EndFrameLoop();
    };

    ClosePlatform();
    Console("fps = %d", x86.Time.fps);
    return 0;
}