/**
 * samples/wormhole/main.c
 * 
 * Copyleft (c) Pierre-Jean Turpeau 12/2018
 * <pierrejean AT turpeau DOT net>
 */
#define X_IMPLEMENTATION
#define TITLE "-[ wormhole ]-"

#include <x86/x.h>
#include <x86/image.h>
#include <x86/timeline.h>

#include "wormhole.h"

extern unsigned char blue_data[];
extern unsigned int  blue_size;
extern unsigned char green_data[];
extern unsigned int  green_size;
extern unsigned char pink_data[];
extern unsigned int  pink_size;
extern unsigned char map1_data[];
extern unsigned int  map1_size;
extern unsigned char map2_data[];
extern unsigned int  map2_size;

image_t map1;
image_t map2;
image_t tex1;
image_t tex2;
image_t tex3;

palette_t tex1_palette;
palette_t tex2_palette;
palette_t tex3_palette;
palette_t * texture_palette;
palette_t black_palette;

wormhole_t wormhole;
    
void switch_map_and_texture() {
    static int map_num = 0;
    static int tex_num = 0;

    map_num = (map_num + 1) % 2;
    tex_num = (tex_num + 1) % 3;

    if (map_num)
        wormhole.map = &map2;
    else
        wormhole.map = &map1;

    switch( tex_num ) {
        case 0: wormhole.texture = &tex1; texture_palette = &tex1_palette; break;
        case 1: wormhole.texture = &tex2; texture_palette = &tex2_palette; break;
        case 2: wormhole.texture = &tex3; texture_palette = &tex3_palette; break;
    }
}

void AnimateTimeline(u32 system_time) {
    UpdateTimelineTime(system_time);
    FadePaletteTime(3000, 250, texture_palette, &black_palette);
    ExecuteActionOnTime(3250, switch_map_and_texture);
    FadePaletteTime(3250, 250, &black_palette, texture_palette);
    JumpTimelineTo(3000, 3000+2500*4);
}

int main(int argc, char ** argv) {
    InitPlatform(argc, (const char **) argv);
    ParseDefaultExeOptions();

    if (CheckExeOption("-genmaps")) {
        Console("Generating maps");

        // 2304 % 8 = 0
        // (2304 / 8) % WH_TEXTURE_WIDTH must be 0
        GenerateWormholeMap( "map1.tga",
            W, H, W/2, H/2, 2304, 8, 2304, 8, 25, true);

        GenerateWormholeMap( "map2.tga",
            W, H, W/2, H/2, 2304, 8, 2304, 8, 30, false);
        return EXIT_SUCCESS;
    }

    Console("Use '-genmaps' CLI option to generate wormhole maps");
    Console("USe [SPACE] key to display the current map");
    Pause(2000);

    SetVGAMode();

    ClearPalette(0);
    SavePalette(&black_palette);

    LoadTGA(&map1, NULL, map1_data, map1_size);
    LoadTGA(&map2, NULL, map2_data, map2_size);
    LoadBMP(&tex1, &tex1_palette, pink_data, pink_size);
    LoadBMP(&tex2, &tex2_palette, green_data, green_size);
    LoadBMP(&tex3, &tex3_palette, blue_data, blue_size);

    InitTimeline();
    InitWormhole(&wormhole, &map1, &tex1);
    RestorePalette(&tex1_palette);

    texture_palette = &tex1_palette;

    bool display_map = false;

    while (!IsPlatformExit()) {

        BeginFrameLoop();
        ExecuteDefaultInteractions();

            if(GetLastEvent() == ' ') {
                display_map = ! display_map;
                if (display_map)
                    LinearGradientPalette(0, 0, 255, 0xFFFFFF);
                else
                    RestorePalette(texture_palette);
            }

            if (!IsPaused()) {
                UpdateWormhole(&wormhole, GetTime());
            }

            if (display_map)
                DrawImage_c(wormhole.map, 0, 0);
            else {
                AnimateTimeline(GetTime());
                DrawWormhole(&wormhole);
            }

        EndFrameLoop();
    }

    ClosePlatform();

    Console("fps = %d\n", x86.Time.fps);
    return EXIT_SUCCESS;
}
