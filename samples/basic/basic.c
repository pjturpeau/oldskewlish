/*
 * samples/fire/fire.c
 * 
 * Copyleft (c) Pierre-Jean Turpeau 02/2019
 * <pierrejean AT turpeau DOT net>
 */
#define X_IMPLEMENTATION 1

#define TITLE "-[ basic example ]-"

#include <x86/x.h>

int main(int argc, char **argv) {
    InitPlatform(argc, (const char **) argv);
    ParseDefaultExeOptions();

    Console("The most simple example");
    Pause(2000);

    SetVGAMode();

    while (! IsPlatformExit()) {
        BeginFrameLoop();
        ExecuteDefaultInteractions();

            SetPixel(160,  99, 15);
            SetPixel(159, 100, 15);
            SetPixel(160, 100, 15);
            SetPixel(161, 100, 15);
            SetPixel(160, 101, 15);

        EndFrameLoop();
    };

    ClosePlatform();

    Console("fps = %d", x86.Time.fps);
    return EXIT_SUCCESS;
}
