/**
 * x86/fsm.h
 * 
 * Copyleft (c) Pierre-Jean Turpeau 03/2020
 * <pierrejean AT turpeau DOT net>
 * 
 * Very trivial finite state machine framework:
 *  - it uses function state pattern (one function per state)
 *  - each state function manages entry/run/exit actions
 * 
 * Possible future evolutions:
 *  - hierarchical state machine
 *  - external event injection 
 */
#ifndef X86_FSM_H
#define X86_FSM_H

# include <x86/x.h>

/* ---[ DEFINITIONS ]------------------------------------------------------- */

typedef enum {
    E_ENTRY,
    E_RUN,
    E_EXIT
} fsm_action_t;

struct fsm_t;

typedef void (*fsm_state_func)( struct fsm_t *, fsm_action_t ); // prototype for all state functions

/* ---[ DECLARATIONS ]------------------------------------------------------ */

/* =================( functions to control a fsm )========================== */

extern void InitStateMachine(struct fsm_t * fsm, fsm_state_func initial_state); // initialize the initial state
extern void ExecuteStateMachine(struct fsm_t * fsm); // execute the current state of the fsm (E_RUN action)
extern bool IsStateMachineRunning(struct fsm_t * fsm); // return True is the state machine is not in the sunk state

/* ============( functions that can be called from states )================ */

extern void SwitchStateMachineState(struct fsm_t * fsm, fsm_state_func to_state); // switch to a new state and call E_EXIT/E_ENTRY actions accordingly
extern void TerminateStateMachine(struct fsm_t * fsm); // Exit the current state and switch to an internal sunk state


/* ---[ IMPLEMENTATION ]---------------------------------------------------- */

# ifdef X_IMPLEMENTATION

typedef struct fsm_t {
    fsm_state_func current;
} fsm_t;


void InitStateMachine(fsm_t * fsm, fsm_state_func initial_state) {
    fsm->current = initial_state;
    fsm->current(fsm, E_ENTRY);
}

void ExecuteStateMachine(fsm_t * fsm) {
    fsm->current(fsm, E_RUN);
}

void SwitchStateMachineState(fsm_t * fsm, fsm_state_func to_state) {
    fsm->current(fsm, E_EXIT);
    fsm->current = to_state;
    fsm->current(fsm, E_ENTRY);
}

void _StateMachineSinkState(fsm_t * fsm, fsm_action_t action) {
    LogDebug("_StateMachineSinkState");
    (void)(fsm);
    (void)(action);
    ; /* nothing to do */
}

void TerminateStateMachine(fsm_t * fsm) {
    SwitchStateMachineState(fsm, _StateMachineSinkState);
}

bool IsStateMachineRunning(fsm_t * fsm) {
    return (fsm->current != _StateMachineSinkState) ? true : false;
}

# endif /* X_IMPLEMENTATION */

#endif /* X86_FSM_H */
