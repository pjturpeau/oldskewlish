/**
 * x/gfx/sprite.h
 * 
 * Copyleft (c) Pierre-Jean Turpeau 05/2020
 * <pierrejean AT turpeau DOT net>
 * 
 * Transparent sprite display using mask data.
 */
#ifndef X_GFX_SPRITE_H
#define X_GFX_SPRITE_H

# include <x/x.h>

/* ---[ DEFINITIONS ]------------------------------------------------------- */


/* ---[ DECLARATIONS ]------------------------------------------------------ */

/** Generate mask data for transparency. The color index 0 is used as the
 * transparent color.
 * Mask is usefull in bitwise operation like :
 *     pixel = sprite_pixel_value | (current_pixel_value | sprite_mask_value) */
extern void generate_sprite_mask(image_t * image);

/** draw the clipped sprite to the backbuffer according to its mask */ 
extern void draw_sprite(const image_t * sprite, int x, int y);

/** draw the specified 'area' from the sprite 'image' */
extern void draw_sprite_area(const image_t * img, int x, int y,  const rect_t * area);

/* ---[ IMPLEMENTATION ]---------------------------------------------------- */

# ifdef X_IMPLEMENTATION

void generate_sprite_mask(image_t * image) {
    if(image->masks) return;
    image->masks = (u8 *) mem_alloc_cache_align(image->w * image->h);
    u8 * msk = image->masks;
    u8 * src = image->pixels;
    for(int j = 0; j < image->h; j++) {
        for(int i = 0; i < image->w; i++) {
            *(msk++) = *(src++) ? 0x00 : 0xFF;
        }
    }
}

/* could be optimized using 64 bits and 128 bits data... */
void draw_sprite(const image_t * sprite, int x, int y) {
    if(x < -sprite->w || x > W || y < -sprite->h || y > H)
        return;

    const int clip_left = (x < 0) ? -x : 0;
    const int clip_top = (y < 0) ? -y : 0;
    const int clip_right = M_MIN(W - x, sprite->w);
    const int clip_bottom = M_MIN(H - y, sprite->h);

    const int width = clip_right - clip_left;
    const int words_count = width >> 2;

    /* good chance that if it is clipped on the left, it will not be clipped on the right ... */
    /* TODO : false asumption */
    const int remaining_bytes = clip_left&3 ? 0 : width - (width & 0xFFFFFC);

    u32 src_ofs = clip_left + clip_top * sprite->w;

    for(int j = clip_top; j < clip_bottom; j++, src_ofs += sprite->w)
    {
        u8 * src = sprite->pixels + src_ofs;
        u8 * msk = sprite->masks  + src_ofs;
        u8 * dst = backbuffer() + x + clip_left + (y+j) * W;

        switch(clip_left&3) {
            case 1: *dst = *(src++) | (*dst & *(msk++)); dst++;
            case 2: *dst = *(src++) | (*dst & *(msk++)); dst++;
            case 3: *dst = *(src++) | (*dst & *(msk++)); dst++;
        }

        u32 * src32 = (u32 *) src;
        u32 * msk32 = (u32 *) msk;
        u32 * dst32 = (u32 *) dst;

        for(int i = words_count; i > 0; i--, dst32++)
            *dst32 = *(src32++) | (*dst32 & *(msk32++));

        src = (u8 *) src32;
        msk = (u8 *) msk32;
        dst = (u8 *) dst32;

        switch(remaining_bytes) {
            case 3: *dst = *(src++) | (*dst & *(msk++)); dst++;
            case 2: *dst = *(src++) | (*dst & *(msk++)); dst++;
            case 1: *dst = *(src++) | (*dst & *(msk++)); dst++;
        }        
    }
}

void draw_sprite_area(const image_t * sprite, int x, int y,  const rect_t * area)
{
    if(x < -area->w || x > W || y < -area->h || y > H)
        return;

    const int clip_left = (x < 0) ? -x : 0;
    const int clip_top = (y < 0) ? -y : 0;
    const int clip_right = M_MIN(W - x, area->w);
    const int clip_bottom = M_MIN(H - y, area->h);

    const int width = clip_right - clip_left;
    const int words_count = width >> 2;

    /* good chance that if it is clipped on the left, it will not be clipped on the right ... */
    const int remaining_bytes = clip_left&3 ? 0 : width - (width & 0xFFFFFC);

    u32 src_ofs = clip_left + area->x + (clip_top + area->y) * sprite->w;

    for(int j = clip_top; j < clip_bottom; j++, src_ofs += sprite->w) {
        u8 * src = sprite->pixels + src_ofs;
        u8 * msk = sprite->masks  + src_ofs;
        u8 * dst = backbuffer() + x + clip_left + ( y + j) * W;

        switch(clip_left&3) {
            case 1: *dst = *(src++) | (*dst & *(msk++)); dst++;
            case 2: *dst = *(src++) | (*dst & *(msk++)); dst++;
            case 3: *dst = *(src++) | (*dst & *(msk++)); dst++;
        }

        u32 * src32 = (u32 *) src;
        u32 * msk32 = (u32 *) msk;
        u32 * dst32 = (u32 *) dst;

        for(int i = words_count; i > 0; i--, dst32++)
            *dst32 = *(src32++) | (*dst32 & *(msk32++));

        src = (u8 *) src32;
        msk = (u8 *) msk32;
        dst = (u8 *) dst32;

        switch(remaining_bytes) {
            case 3: *dst = *(src++) | (*dst & *(msk++)); dst++;
            case 2: *dst = *(src++) | (*dst & *(msk++)); dst++;
            case 1: *dst = *(src++) | (*dst & *(msk++)); dst++;
        }        
    }
}

# endif /* X_IMPLEMENTATION */

#endif /* X_GFX_SPRITE_H */
