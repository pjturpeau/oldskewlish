# 3D basics

Right-handed cartesian reference.

need illustration.

## vectors

x,y,z,w vectors.

For a position vector 'w = 1' so that translation multiply makes sense
For a direction vector 'w = 0' so that translation multiply has not impact.


## other

TBD : row major mode vs column major mode
TBD : premultiply vs postmultiply

https://www.davrous.com/2013/06/13/tutorial-series-learning-how-to-write-a-3d-soft-engine-from-scratch-in-c-typescript-or-javascript/

https://www.3dgep.com/understanding-the-view-matrix/

https://www.slideshare.net/Mark_Kilgard/cs-354-object-viewing-and-representation

https://www.ntu.edu.sg/home/ehchua/programming/opengl/CG_BasicsTheory.html

http://www.codinglabs.net/article_world_view_projection_matrix.aspx
http://www.ecere.com/3dbhole/3d_transformations.html
https://www.codeproject.com/Articles/1247960/Learning-Basic-Math-Used-In-3D-Graphics-Engines

## Quaternions

https://www.3dgep.com/understanding-quaternions/

## other


- https://github.com/rlk/util3d/blob/master/math3d.md
- https://github.com/rlk/util3d/blob/master/cube.md
- https://github.com/rlk/util3d/blob/master/plane.md
- https://csc.lsu.edu/~kooima/misc/cs594/proj3/index.html
