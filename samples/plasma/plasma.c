/**
 * samples/plasma/plasma.c
 * 
 * Copyleft (c) Pierre-Jean Turpeau 12/2019
 * <pierrejean AT turpeau DOT net>
 */
#define X_IMPLEMENTATION

#define TITLE "-[ plasma ]-"

#include <x86/x.h>
#include <x86/timeline.h>

#include "plasma.h"


palette_t p1, p2, p3, p4;

void AnimateTimeline(u32 system_time) {
    UpdateTimelineTime(system_time);
    FadePaletteTime(3000, 500, &p1, &p2);
    FadePaletteTime(3000+2500*1, 500, &p2, &p3);
    FadePaletteTime(3000+2500*2, 500, &p3, &p4);
    FadePaletteTime(3000+2500*3, 500, &p4, &p1);
    JumpTimelineTo(3000, 3000+2500*4);
}

void rendering_func(f32 dt) {
}

int main(int argc, char **argv) {
    InitPlatform(argc, (const char **) argv);
    ParseDefaultExeOptions();

    Console("press [SPACE] to test plasma implementations\n");
    Pause(2000);

    SetVGAMode();

    v3f params2[4] = {
        {{0.5, 0.5, 0.5}},
        {{0.5, 0.5, 0.5}},
        {{1.0, 1.0, 0.5}},
        {{0.8, 0.9, 0.3}}
    };
    TrigonometricGradientPalette(0, 255, params2);
    SavePalette(&p2);

    LinearGradientPalette(  0, 0x40e0d0,  85, 0xff8c00);
    LinearGradientPalette( 85, 0xff8c00, 170, 0xff0080);
    LinearGradientPalette(171, 0xff0080, 255, 0x40e0d0);
    SavePalette(&p3);

    LinearGradientPalette(  0, 0xff00ff, 127, 0x02FDFF);
    LinearGradientPalette(128, 0x02FDFF, 255, 0xFF00FF);
    SavePalette(&p4);

    LinearGradientPalette(  0, 0xFF007B,  80, 0xFFC2F6);
    LinearGradientPalette( 81, 0xFFC2F6, 127, 0xFFFA5A);
    LinearGradientPalette(128, 0xFFFA5A, 180, 0xFFCA00);
    LinearGradientPalette(181, 0xFFCA00, 255, 0xFF007B);
    SavePalette(&p1);
    
    InitTimeline();
    InitPlasma();

    int plasma_version = 0;

    while(!IsPlatformExit()) {
        BeginFrameLoop();
        ExecuteDefaultInteractions();

            if(GetLastEvent() == ' ')
                plasma_version = (plasma_version + 1) % 4;

            if (!IsPaused()) {
                u32 elapsed_time = GetTime();
                AnimateTimeline(elapsed_time);
                UpdatePlasma(elapsed_time);

                switch( plasma_version ) {
                    case 0: DrawFixedPointLerpPlasma(); break;
                    case 1: DrawFixedPointPlasma(); break;
                    case 2: DrawSinePlasma(); break;
                    case 3: DrawComplexePlasma(); break;
                }
            }

        EndFrameLoop();
    };

    ClosePlatform();

    Console("fps = %d\n", x86.Time.fps);
    return EXIT_SUCCESS;        
}
