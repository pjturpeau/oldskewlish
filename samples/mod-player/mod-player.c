/**
 * samples/mod-player/mod-player.c
 * 
 * Copyleft (c) Pierre-Jean Turpeau 12/2023
 * <pierrejean AT turpeau DOT net>
 * 
 */
#define X_IMPLEMENTATION 1

#define TITLE "-[ mod player ]-"

#include <x/x.h>
#include <x/snd/player.h>
#include <SDL2/SDL_audio.h>

mod_player_t player;

void usage() {
    console("Use -f <music.mod>");
}

void print_replay_state() {

    static u8 prev_seq_index = -1;
    static u8 prev_position = -1;

    /* ensure to output something only when needed... */
    if( prev_seq_index == player.mod.replay_sequence_index
        && prev_position == player.mod.replay_position_in_pattern)
        return;

    if(prev_seq_index != player.mod.replay_sequence_index) {
        printf("\n");
    }

    prev_seq_index = player.mod.replay_sequence_index;
    prev_position = player.mod.replay_position_in_pattern;

    static char text_buf[256];
    char * text_ptr = text_buf;
    u32 count = sprintf(text_ptr, "%02d/%02d - %02d.%02d",
        player.mod.replay_sequence_index,
        player.mod.song_sequence_length,
        player.mod.replay_pattern_id,
        player.mod.replay_position_in_pattern);
    text_ptr += count;
    for(int i = 0; i < player.mod.number_of_channels; i++) {
        mod_channel_t * channel = &(player.mod.channels[i]);
        mod_note_t * note = channel->replay_last_note;
        count = sprintf(text_ptr, " | %s", note->period ? note->name : "...");
        text_ptr += count;
        if(note->sample_id > 0) {
            count = sprintf(text_ptr, " %02d v%02X", note->sample_id, channel->volume);
            text_ptr += count;
        } else {
            strcpy(text_ptr, " .. ...");
            text_ptr += 7;
        }

        if(note->fx || note->fx_param) {
            count = sprintf(text_ptr, " %1X%02X", note->fx, note->fx_param);
            text_ptr += count;
        } else {
            strcpy(text_ptr, " ....");
            text_ptr += 4;
        }

        if(channel->finetune) {
            count = sprintf(text_ptr, " %1X", channel->finetune);
            text_ptr += count;
        } else  {
            strcpy(text_ptr, " .");
            text_ptr += 2;
        }

        if(channel->period) {
            count = sprintf(text_ptr, " %05d", channel->period);
            text_ptr += count;
        } else  {
            strcpy(text_ptr, " .....");
            text_ptr += 6;
        }
    }

    strcpy(text_ptr, "\n");
    printf(text_buf);
}

void print_mod_infos() {
    char * sig = (char *)&(player.mod.signature);
    printf("Signature: %X (%c%c%c%c)\n", player.mod.signature, sig[0], sig[1], sig[2], sig[3]);
    printf("Title: %s\n", player.mod.title);
    printf("Song length: %d\n", player.mod.song_sequence_length);
    printf("Number of patterns: %d\n", player.mod.number_of_patterns);
    printf("Samples: %d\n", player.mod.number_of_samples);

    for(u32 i = 0; i < player.mod.number_of_samples; i++) {
        mod_sample_t *sample = &(player.mod.samples[i]);
        printf("   Sample %2u - len %5u - finetune %x - volume %2d - repeat(%4x,%4x) - name='%s'\n",
            i,
            sample->length,
            sample->finetune,
            sample->volume,
            sample->loop_offset,
            sample->loop_length,
            sample->name);
    }
}

void rendering_func(float dt) {
    ClearScreen(0);

    s32  y;
    u32 channel_display_height = H>>2;

    /* color index to use for each channel in the default palette */
    u8 channel_color_index[4] = {4, 7, 13, 14};

    for(u32 i = 0; i < W; i++)
    {
        u32 audio_chunk_index = (i * player.mixing_audio_chunks ) / W;

        for( u32 cc = 0; cc < 4; cc++)
        {
            y = -player.channel_audio_buf[cc][audio_chunk_index];
            y *= (channel_display_height>>1);
            y /= (127*64);
            y += channel_display_height*cc;
            y += (channel_display_height>>1);
            set_pixel_clipped(i, y, channel_color_index[cc]);
        }
        // channel_display_height = H;
        // u32 audio_chunk_index = (i * 2 * player.mixing_audio_chunks ) / W
        // y = -player.audio_stream[audio_chunk_index];
        // y *= (channel_display_height>>1);
        // y /= (127*64);
        // y += channel_display_height/2;m
        // // y += (channel_display_height>>1);
        // set_pixel_clipped(i, y, 4);
    }

    print_replay_state();
}

u8 * load_file_buf(const char * filename, u32 *size_out) {
    FILE *f = fopen(filename, "rb");
    if(f == NULL)
        fatal_error("unable to open file %s", filename);

    /* obtain file size */
    fseek(f, 0, SEEK_END);
    *size_out = ftell(f);
    rewind(f);
    assert(*size_out > 0);

    u8 * data = mem_alloc_cache_align(*size_out);
    assert(data);

    size_t result = fread(data, 1, *size_out, f);
    assert(result == *size_out);

    fclose(f);
    console("file: %s - %u bytes", filename, *size_out);
    return data;
}

/* open SDL audio device and start the audio playback */
void start_audio_playback(mod_player_t* userdata, u32 mixing_frequency, u32 mixing_chunk_count, u8 mixing_channels) {
    SDL_AudioSpec audio_config;
    SDL_memset(&audio_config, 0, sizeof(audio_config));

    audio_config.freq     = mixing_frequency;   /* number of sample frames per second */
    audio_config.format   = AUDIO_S16;          /* each channel is 16 bits */
    audio_config.channels = mixing_channels;    /* 1 = mono, 2 = stereo */

    /* samples is the number of audio chunks. According to the SDL doc the size
     * in bytes of the buffer sent to the callback depends on both the format
     * and the number of channels */
    audio_config.samples  = mixing_chunk_count;
    audio_config.userdata = userdata;
    audio_config.callback = (SDL_AudioCallback)mod_player_mix_audio_callback;

    /* open SDL audio device and */
    SDL_AudioDeviceID audio_device = SDL_OpenAudioDevice(NULL, 0, &audio_config, NULL, 0);
    if(audio_device == 0)
        fatal_error("Failed to open audio device: %s", SDL_GetError());
    else
        SDL_PauseAudioDevice(audio_device, 0);
}

#define REPLAY_FREQ     28000
#define REPLAY_CHUNKS   2048
#define REPLAY_CHANNELS 2

void start() {
    usage();

    const char * filename = check_args("-f");
    if(filename == NULL)
        fatal_error("missing filename");

    console("Loading %s", filename);

    u8* mod_data;
    u32 mod_data_size;

    mod_data = load_file_buf(filename, &mod_data_size);

    mod_player_load(    &player,
                        REPLAY_FREQ,
                        REPLAY_CHUNKS,
                        REPLAY_CHANNELS,
                        mod_data,
                        mod_data_size);


    print_mod_infos();

    start_audio_playback(&player, REPLAY_FREQ, REPLAY_CHUNKS, REPLAY_CHANNELS);

    // fclose(stdout); /* to prevent printf() disturb the replay routine */

    main_loop(rendering_func);


    for(int i = 0; i < 16; i++) {
        u16 v = player.mod.unknown_fx[i];
        if(v != 0)
            printf("fx = %X : %X %X\n", i, (v>>8)&0xF, v&0xFF);
    }

    free(mod_data);
}
