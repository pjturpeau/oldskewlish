/**
 * samples/dwitter/dwitter.c
 * 
 * Copyleft (c) Pierre-Jean Turpeau 03/2020
 * <pierrejean AT turpeau DOT net>
 * 
 * ripped from demos by various authors shown @ http://www.dwitter.net
 * (demo length <= 140 chars)
 * 
 * Dwitter shortcuts are:
 * u(t) is called 60 times per second.
 * t: elapsed time in seconds.
 * c: A 1920x1080 canvas.
 * x: A 2D context for that canvas.
 * S: Math.sin => sinf() here
 * C: Math.cos => cosf() here
 * T: Math.tan => tanf() here
 * R: Generates rgba-strings, ex.: R(255, 255, 255, 0.5)
 * 
 * In javascript, bitwise operations on a float number are done onto its integer part.
 * 
 * personal dwitter addendum:
 * A: fabsf()
 * M: fmodf() => replace % for floats
 * N: truncf() => replace ~~ which is supposed to be faster than Math.trunc
 * P: powf() => replace **
 * 
 * For warning removals:
 * nop(): for construct like expr?fillRect(...):0; <= replace 0 by nop();
 */
#define X_IMPLEMENTATION 1

#include <stdlib.h>
#include <time.h>

#define TITLE "-[ dwitter ]-"

#include <x86/x.h>
#include <x86/draw.h>

#include <math.h>
// #include <x/math/numbers.h>

typedef double (*f_f_func)(double);
typedef double (*f_fxf_func)(double, double);
typedef u8 (*ColorIndex_func)(int);
typedef void (*dwitter_func)(float);
typedef void (*fillRect_func)(float x, float y, float w, float h);

typedef struct {
    int width;
    int height;
} canvas;

typedef struct {
    u8 clear_color;
    u8 fillStyle;
    fillRect_func fillRect;
}
context;

/* dwitter like globals */
f_f_func A, C, N, S, T;
f_fxf_func M, P;
ColorIndex_func R;
float a,b,d,D,e,f,g,h,i,j,k,l,m,n,o,q,r,s,t,u,v,w,X,Y,y,z;
canvas c;
context x;
float elapsed_time;
float arf; /* aspect ratio factor between original resolution and video res */

void nop() {
    ;/* nothing to do */
}

void fillRect(float xx, float y, float w, float h) {
    float s = (float) W / (float) c.width;

    xx *= s;
    y *= s;
    w *= s;
    h *= s;
    if(w < 1 && h < 1)
        SetPixel_c(xx, y, x.fillStyle);
    else {
        w = w < 1 ? 1 : w;
        h = h < 1 ? 1 : h;
        DrawRectangle_c(xx, y, xx + w, y + h, x.fillStyle);
    }
}

u8 colorIndex(int t) {
    return (u8)(t&0xFF);
}

void initialize_dwitter() {
    A = fabs;
    C = cos;
    S = sin;
    T = tan;
    M = fmod;
    N = trunc;
    P = pow;
    R = colorIndex;
    
    c.width = 1920;
    c.height = 1080;
    a=b=d=e=f=g=h=i=j=k=l=m=n=o=q=r=s=t=u=v=w=y=z=0;
    X=Y=D=0;
    elapsed_time = 0;

    x.clear_color = 0;
    x.fillStyle = 255;
    x.fillRect = fillRect;

    arf = (1920.f/1080.f) / ((float) W / (float) H);    
}

void u00(float t) {
    for(k=2e3;(j=(int)--k>>6);s=g*g/200,x.fillRect((i-j+4)*g,(8+C(i/4)*C((t*30-N(j))/4)*3)*g,s,s))i=M(k,64),g=960/(35-j);//x.fillStyle=R(g=960/(35-j));
}

/* from https://www.dwitter.net/d/15798 */
void u01(float t) {
    c.width=1920;
    for(i=0;i<2000;i+=20){for(j=0;j<1200*arf; j+=20){x.fillStyle=R(255*(.5+.5*S(.0002*t*(i-960)*(j-540))));x.fillRect(i,j,20,20);}}
}

/* https://www.dwitter.net/d/9427 */
void u02(float t) {
    if(!t){l=0;r=-1;}while(l<22*arf){x.fillStyle=l*l+C(t*2*l/25*r/25)*350+r*r;x.fillRect(++r*50,l*50,50,50);if(r>38){l++;r=-1;}}l=0;
}

/* https://www.dwitter.net/d/1494 */
void u03(float t) {
    l=c.width+=i=0;h=400;for(;++i<200;)for(j=a=M(t+C(i),2);j++<50;){w=a*h-h;x.fillRect(l*C(i*i)+~~(int)a*w*C(j),(~~(int)a?h:a*h)*(C(i)*C(i)+2)+w*S(j)/4,3,3);}
}

/* from https://www.dwitter.net/u/smtsjhr */
void u04(float t) {
    c.width=1920;n=40;d=50;
    for(i=0;i<n*n;i++){
    k=M(i,n);j=(i-k)/n;f=(j*k+n*n)*t/1000;
    x.fillRect(d*j,d*k,d*S(f),d*C(f));}
}

void u05(float t) {
    for(a=c.width|=0;l=C(t),m=S(t),d=C(a)*S(b=a*1.2502),e=S(a--)*S(b);i=h/40-5,x.fillRect((d*l+e*m)*h+960,C(b)*h+540*arf,i,i))h=900/(2+(e*l-d*m));
}

/* from https://www.dwitter.net/u/katkip */
void u06(float t) {
    c.width=200;for(j=4;j--;)for(i=899;i--;)x.fillRect(99+C(i+t)*(40+S(i*2+999/j)*15*j),50*arf+S(i*2+t-j)*(30+S(100/i-j*1 )*5*j-i/99),1,S(j+t+i/2));
}

void u07(float t) {
    for(c.width^=0,i=(int)(1296*arf);i--;X=M(i,48),Y=N(i/48),d=1-((X-24)*(X-24)+(Y-13)*(Y-13))/300,s=M(200*S(t*C(d))*d*d,45),x.fillRect(X*40-s/2,Y*40-s/2,s,s));
}

void u08(float t) {
    c.width=1920;for(i=0;i<200;i++){y=i*8-100; a=900+C(t*5)*200 + C(i+t*2)*400;x.fillRect(a,y+C(t*5)*100,20,20);}
}

void u09(float t) {
    i=q=c.width=2e3;for(;i--;z*=(z/2),a=i/310-t,x.fillRect(o*C(a)*z+q/2,(r*S(b)+190)*z,z,z))z=q/((o=2e2+(r=25+9*C(6*S(a)))*C(b=M(i,50)/8))*S(a)+710),a+=2*t;
}

/* https://www.dwitter.net/d/135 */
void u10(float t) {
    c.width=192;Y=A(M(t*99,176)-88);X=A(M(t*139,376)-188);x.fillRect(X,Y+7,3,3);x.fillRect(187,Y+C(X/39)*29,5,20);x.fillRect(0,Y-S(X/29)*22,5,20);
}

void u11(float t) {
    c.width=1920;g=540;for(i=0;i<g;a=i-t,l=S(a)*i,z=g-l*C(t),s=g*9/z,x.fillRect(960+18/(z*18)*l*S(t)*g,g+C(a)*i/z*g,s,s),++i);
}

void u12(float t) {
    r=500;c.width^=0;for(i=j=2;j>-2;j-=1/2e3)l=j*r-t,D=(M(20,(S(i)*50)*C(.1+i/1000)*.7))*C(l/2),i+=.25032,x.fillRect(C(j)*S(l)*r+999,S(j)*r+520,D,D);
}

void u13(float t) {
    c.width=1920;for(i=-2;i<2;i+=.05)for(j=-4;j<4;j+=.05)1/(j*j+i*i)+1/((j-M(t,12)+6)*(j-(int)t%12+6)+i*i)>1?x.fillRect(j*240+960,i*240+540,12,12):nop(); 
}

void u14(float t) {
    float a[3];
    c.width^=0;d=0.15;r=4;for(i=-r;i<r;i+=d)for(j=-r;j<r;j+=d){a[0]=i;a[1]=j;a[2]=C(i+t)+S(j+t);x.fillRect(960+150*(C(0)*a[0]+S(t)*a[1]),540-150*a[2],7,7);}
}

void u15(float t) {
    c.width=999;for(a=b=z=0,w=140;z<400;z+=.1)a=S(a*2.4-t)*S(t*2)+C(a+b+t)+C(z*2.34+t),b=C(z*1.56),x.fillRect(a*w+500,b*w+w+w,2,2);
}

void uu16(float t) {
    c.width=1920;for(i=0;i<100;i++){a = M(i,10);b=floor(i/10);x.fillRect(600+a*80, 140 + b*80, 60*C((a+b+1)*t), 60*S((a-b)*t));}
}

void u17(float t) {
    c.width=2e3;n=50;v=25;for(i=0;i<n;i++){o=M(-t,1)*n;q=i*v+o;x.fillRect(q,q,M(i,2)?n:v,M(i,2)?v:n);x.fillRect(620,590+fmin(S(t*6)*99,M(-t*n,n)),n,n);}
}

/* https://www.dwitter.net/d/113 */
void u18(float t) {
    w=c.width=299;for(i=2691;i--;)x.fillStyle=R(2e5/i),k=2e5+i+t/(0|(int)(i/w))*99,y=49+C(k/19)*5+S(k)+C(k/23)*9+S(k/7)*7,x.fillRect((int)i%(int)w,y*2e3/i,1,w);
}

void u19(float t) {
    for(l=c.width=512,a=C(t),b=S(t);l--;d=.2*z/a,x.fillRect((k-a*a*i-b*a*j+b/a)*z+300,(a*i+b*j+a*k)*z+99,d,d))i=(int)l>>6,j=(int)l>>3&7,k=(int)l%8-4,z=20+b*i-a*j;
}

void u20(float t) {
    c.width|=1;X=960;Y=700;for(i=320;i--;x.fillRect(X,Y,7,7))X+=6*C(a=i/50-9*S(t/4)*S(i/10)),Y+=6*S(a);
}

void u21(float t) {
    c.width=640; for(i=0; i < 900; i++) {
        a=M(i,30);y=i/30;x.fillRect(a*10+y*10,180-a*5+y*5-S(((15-a)*(15-a)+(15-y)*(15-y))*0.02+t*5)*16,2,2);
    }
}

/* https://www.dwitter.net/d/2556 */
void u22(float t) {
    w=960,v=340;for(i=99;i--;)for(j=59;j--;)X=i/2-25-C(t),Y=2+S(i/5)*S(j/5+t*3),s=99/j,x.fillRect(w+1/j*X*w,v+Y/j*w,s,s);
}

/* https://www.dwitter.net/d/193 */
void u23(float t) {
    c.width=199;f=99;d=M(t,4);for(i=210;i--;)r=d>.5?f*S(i*f)*(d-.5):0,x.fillStyle=R(i),x.fillRect(f+29*S((int)(t/4)^9)*d+C(i)*r,f-d*f+d*d*49+S(i)*r,1,2);
}

/* https://www.dwitter.net/d/7275 */
void u24(float t) {
    d=700;for(c.width+=i=0;i<d;){X=S(99*i);
    z=C(55*i);
    b=(4-z*C(t)+X*S(t))/d;
    x.fillRect(d+(X*C(t)+z*S(t))/b,d+C(99*i++)/b,.03/b,.03/b);}    
}

/* https://www.dwitter.net/d/1978 */
void u25(float t) {
    for(d=256;--d;)for(X=-32;++X<32;x.fillRect(960+X*8e3/d,100+7e4/d-h,120,9))a=d/9+t*6,h=(C(a)+C(X/9+a/3))*35,g=h+C((int)X^(int)d)*6+90,x.fillStyle=R(g/.8);
}

/* https://www.dwitter.net/d/11886 */
void u26(float t) {
    const int seed=4;

    for(i=(int)(43*arf);i--;)for(j=71;j--;)
        x.fillRect(j*30+S(i/seed+t)*100-100,i*30+C(j/seed+t)*100-100,30,30);
}

dwitter_func dwitter_list[] = {
    u26, u00, u01, u02, u03, u04, u05, u06, u07, u08, u09, u10, u11, u12, u13, u14, u15, uu16, u17, u18, u19, u20, u21, u22, u23, u24, u25
};

const int u_count = sizeof(dwitter_list)/sizeof(dwitter_list[0]);

int current = 0;

int main(int argc, char ** argv) {
    InitPlatform(argc, (const char **) argv);
    ParseDefaultExeOptions();
    Console("press [SPACE] to switch\n");
    Pause(2000);
    SetVGAMode();

    LinearGradientPalette(0, 0, 255, 0xFFFFFF);
    initialize_dwitter();

    while(!IsPlatformExit()) {
        BeginFrameLoop();
        ExecuteDefaultInteractions();

        if(GetLastEvent() == ' ') {
            current++;
            current %= u_count;
            initialize_dwitter();
        }
    
        if (! IsPaused()) {
            ClearScreen(0);
            dwitter_list[current](GetTime()*0.001);
        }

        EndFrameLoop();
    };

    ClosePlatform();
    Console("fps = %d", x86.Time.fps);
    return 0;
}
