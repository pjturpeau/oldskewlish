/**
 * x86/fp16.h
 * 
 * Copyleft (c) Pierre-Jean Turpeau 04/2020
 * <pierrejean AT turpeau DOT net>
 * 
 * preliminary fixed point 16.16
 */
#ifndef X86_FP16_H
#define X86_FP16_H

#include <x86/x.h>

/* ---[ DEFINITIONS ]------------------------------------------------------- */

typedef s32 fp16;

#define FP16_SHIFT 16 /* fractional bits Q16.16 */
#define FP16_SCALE (1<<FP16_SHIFT)
#define FP16_MASK (FP16_SCALE-1);

/* ---[ DECLARATIONS ]------------------------------------------------------ */

static inline f32 FP16ToFloat(fp16 v);
static inline s32 FP16ToInt(fp16 v);
static inline u32 FP16ToUnsignedInt(fp16 v);
static inline fp16 FloatToFP16(f32 v);
static inline fp16 IntToFP16(s32 v);
static inline fp16 UnsignedIntToFP16(u32 v);

static inline fp16 MulUnsignedFP16(fp16 a, fp16 b);

/** because shifting negative values is implementation dependent */
static inline fp16 MulSignedFP16(fp16 a, fp16 b);

/* for division: look about "Division by Invariant Multiplication" */

/* ---[ IMPLEMENTATION ]---------------------------------------------------- */

static inline f32 FP16ToFloat(fp16 v) {
    return (f32) v / (f32) FP16_SCALE;
}

static inline s32 FP16ToInt(fp16 v) {
    return v / FP16_SCALE;
}

static inline u32 FP16ToUnsignedInt(fp16 v) {
    return ((u32) v) >> FP16_SHIFT;
}

static inline fp16 FloatToFP16(f32 v) {
    return v * FP16_SCALE;
}

static inline fp16 IntToFP16(s32 v) {
    /* left shifting negative value is implementation dependent */
    return v * FP16_SCALE; 
}

static inline fp16 UnsignedIntToFP16(u32 v) {
    return v << FP16_SHIFT; 
}

static inline fp16 MulUnsignedFP16(fp16 a, fp16 b) {
    return ((u32)a * (u32)b) >> FP16_SHIFT;
}

static inline fp16 MulSignedFP16(fp16 a, fp16 b) {
    return (a * b) / FP16_SCALE;

}

# ifdef X_IMPLEMENTATION
# endif /* X_IMPLEMENTATION */

#endif /* X86_FP16_H */
