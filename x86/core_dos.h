/**
 * x86/core_core.h
 * 
 * Copyleft (c) Pierre-Jean Turpeau 10/2024
 * <pierrejean AT turpeau DOT net>
 */
#ifndef X86_CORE_DOS_H
#define X86_CORE_DOS_H

# include <x86/x.h>

/* ---[ IMPLEMENTATION ]---------------------------------------------------- */
# ifdef X_IMPLEMENTATION

#  include <i86.h>
#  include <dos.h>      // _dos_gettime()
#  include <stdarg.h>   // va_list
#  include <conio.h>    // kbhit()

/* ---[ PlATFORM DEFINITIONS ]---------------------------------------------- */

struct MSDOS_t {
    FILE * log_file;
    u32 end_frame_time;
    u8 saved_video_mode;
    bool show_vbl;
};

static struct MSDOS_t msdos;

/* ---[ FORWARD DECLARATION OF PRIVATE FUNCTIONS ]-------------------------- */

static void _OpenLogFile();
static void _CloseLogFile();
static void _Console(const char * prefix, const char * fmt, va_list args);
static void _Log(const char * prefix, const char * fmt, va_list args);

static u8   _GetVideoMode();
static void _SetVideoMode(int mode);
static void _SetVGARGB(int color, int r, int g, int b);
static void _WaitVGASync();
static inline void _UpdateVGAVideo();

static void _InitCPUInfos();
static void _PrintSystemInfos();

extern u32 x86_read_bios_rtc();

/* -------------------------------------------------------------------------
 * Execution functions
 * ------------------------------------------------------------------------- */

void InitPlatform(int argc, const char** argv) {
    memset(&x86, 0, sizeof(X86_t));
    memset(&msdos, 0, sizeof(struct MSDOS_t));

    x86.State.argc = argc;
    x86.State.argv = (char**) argv;
    x86.Video.backbuffer = 0;
    x86.Video.frontbuffer = 0;

    msdos.saved_video_mode = _GetVideoMode();

    Console(""); // separate from dos extender message

    _OpenLogFile();
    _InitCPUInfos();
}

void ClosePlatform() {
    if(_GetVideoMode() != msdos.saved_video_mode) {
        _SetVideoMode(msdos.saved_video_mode);
    }
    _CloseLogFile();
}

void FatalError(const char * fmt, ...) {
    ClosePlatform();
    va_list args;
    va_start(args, fmt);
    _Console("FATAL: ", fmt, args);
    va_end(args);
    exit(EXIT_FAILURE);
}

void ParseDefaultExeOptions() {
    if (CheckExeOption("-debug")) {
        // enable_console_debug();
    }
    if (CheckExeOption("-novsync")) {
        LogDebug("CLI: no vsync");
        x86.Video.with_vsync = true;
    }
    if (CheckExeOption("-infos")) {
        _PrintSystemInfos();
        ClosePlatform();
        exit(EXIT_SUCCESS);
    }
    if (CheckExeOption("-h") || CheckExeOption("-?")
        || CheckExeOption("/?") || CheckExeOption("/help")
        || CheckExeOption("-help") || CheckExeOption("--help")) {
        Console("%s options:", x86.State.argv[0]);
        Console("    -debug         force debug level infos");
        Console("    -infos         print platform infos on console");
        Console("    -novsync       disable synchronization to vertical retrace");
        Console(" ");
        ClosePlatform();
        exit(EXIT_SUCCESS);
    }
}

/* -------------------------------------------------------------------------
 * Time functions
 * ------------------------------------------------------------------------- */

// TODO: replace with a 1ms timer....
u32 GetTime() {
    static u32 start_tick = 0;
    if (start_tick == 0)
        start_tick = x86_read_bios_rtc();

    u32 cur_tick = x86_read_bios_rtc();
    return (cur_tick - start_tick) * 54;
}

void Pause(int ms) {
    int end_time = GetTime() + ms;
    while (GetTime() < end_time) {
        sleep(0);
    }
}

/* -------------------------------------------------------------------------
 * Video functions
 * ------------------------------------------------------------------------- */

void SetVGAMode() {
    _SetVideoMode(0x13);
    x86.Video.frontbuffer = (u8*)0x0A0000;
    x86.Video.backbuffer = (u8 *) MemAlloc(W*H);
    memset(x86.Video.frontbuffer, 0, W*H);
    memset(x86.Video.backbuffer, 0, W*H);
}

void BeginFrameLoop() {
    assert(x86.Video.backbuffer != 0);
    UpdatePlatformInteractions();
}

void EndFrameLoop() {
    if (msdos.show_vbl)
        _SetVGARGB(0, 0, 0, 0);
    DrawPalette();
    _WaitVGASync();
    if (msdos.show_vbl)
        _SetVGARGB(0, 0, 0, 63);
    SwapScreenBuffer();
}

static inline void SwapScreenBuffer() {
    _UpdateVGAVideo(x86.Video.backbuffer);
}

/* -------------------------------------------------------------------------
 * Pixels
 * ------------------------------------------------------------------------- */

/* -------------------------------------------------------------------------
 * Palette
 * ------------------------------------------------------------------------- */

/* rgb as 0xRRGGBB value */
void SetColorPalette(int index, u32 rgb888) {
    assert(index >= 0 && index < MAX_COLORS );
    pix32_t pix;
    pix.v = rgb888;
    pix.r = pix.r >> 2;
    pix.g = pix.g >> 2;
    pix.b = pix.b >> 2;
    _SetVGARGB(index, pix.r, pix.g, pix.b);
}

void RestorePalette(const palette_t * palette) {
    for (int i = 0; i < MAX_COLORS; i++)
        SetColorPalette(i, palette->colors[i].v);
}

u32 GetColorPalette(int index) {
    assert(index >= 0 && index < MAX_COLORS );
    pix32_t pix;
    outp(0x3C7, index);
    pix.r = inp(0x3C9) << 2;
    pix.g = inp(0x3C9) << 2;
    pix.b = inp(0x3C9) << 2;
    return pix.v;
}

void SavePalette(palette_t * palette) {
    for (int i = 0; i < MAX_COLORS; i++)
        palette->colors[i].v = GetColorPalette(i);
}

/* -------------------------------------------------------------------------
 * Interactivity
 * ------------------------------------------------------------------------- */

void UpdatePlatformInteractions() {
    if (kbhit()) {
        char a = getch();
        switch (a) {
            case 27:
                x86.Input.exit_event = true;
                x86.Input.last_event = KEY_ESC;
                break;

            default:
                x86.Input.last_event = a;
                break;
        }
    } else
        x86.Input.last_event = EVT_NONE;
}

void ExecuteDefaultInteractions() {
    int event = GetLastEvent();
    switch( event )
    {
        // case KEY_ESC:
        // case EVT_QUIT:
        //     return;

        case 'c': /* show color palette */
        case 'C':
            x86.State.show_palette = (x86.State.show_palette + 1) % 3;
            if (!x86.State.show_palette) ClearScreen(0);
            break;

        case 'p': /* pause */
        case 'P':
            x86.State.pause = !x86.State.pause;
            // if( !pause )
            //     inner_time = init_time();
            break;

        case 's': /* screenshot */
        case 'S':
            TakeScreenshot();
            break;

        case 'v':
        case 'V':
            msdos.show_vbl = ! msdos.show_vbl;
            if (! msdos.show_vbl)
                _SetVGARGB(0, 0, 0, 0);

        case KEY_TAB:
            x86.State.show_fps = ! x86.State.show_fps;
            break;

        default: /* delegate */
            x86.Input.last_event = event;
            break;
    }
}

/* -------------------------------------------------------------------------
 * Numbers
 * ------------------------------------------------------------------------- */

u8 GetUInt8RandomValue();
#pragma aux GetUInt8RandomValue = \
    "mov    ah,02Ch" \
    "int    21h" \
    "mov    al, dl" \
    "shl    al, 4" \
    "shr    dl, 1" \
    "shl    cl, 2" \
    "xor    al, dl" \
    "xor    al, cl" \
    "xor    al, dh" \
    value   [al]    \
    modify exact [eax edx ecx];

/* -------------------------------------------------------------------------
 * Console
 * ------------------------------------------------------------------------- */

void Console(const char * fmt, ...) {
    va_list args;
    va_start(args, fmt);
    _Console(NULL, fmt, args);
    va_end(args);
}

void LogDebug(const char * fmt, ...) {
    va_list args;
    va_start(args, fmt);
    _Log("DEBUG: ", fmt, args);
    va_end(args);
}

void LogError(const char * fmt, ...) {
    va_list args;
    va_start(args, fmt);
    _Log("ERROR: ", fmt, args);
    va_end(args);
}

/* -------------------------------------------------------------------------
 * Memory
 * ------------------------------------------------------------------------- */
void * MemAllocAlign(int size, int align) {
    assert(IsPowerOfTwo(align)); // align must be a power of 2
    uintptr_t mask = ~(uintptr_t)(align - 1); // TODO: check align is a power of two
    void *mem = malloc(size+align-1);
    void *ptr = (void *)(((uintptr_t)mem+align-1) & mask);
    LogDebug("alloc(%d, %d) = %X / %X", size, align, (uintptr_t)mem, (uintptr_t)ptr);
    return ptr;
}

void * MemAlloc(int size) {
    return MemAllocAlign(size, x86.Cpu.L1_cache_line_bytes);
}

void MemFree(void * area) {
    if(area) free(area);
}

/* -------------------------------------------------------------------------
 * Private LOG functions
 * ------------------------------------------------------------------------- */
static void _OpenLogFile() {
    msdos.log_file = fopen("console.log", "w");
    if(msdos.log_file == NULL) {
        Console("ERROR: unable to open console.log file");
        Pause(1000);
        return;
    }

    fseek(msdos.log_file, 0, SEEK_END);

    struct dostime_t t;
    _dos_gettime(&t);
    fprintf(msdos.log_file, "--[%02d:%02d:%02d.%02d]-------------------------\n",
        t.hour, t.minute, t.second, t.hsecond);
}

static void _CloseLogFile() {
    if(msdos.log_file != NULL) {
        fclose(msdos.log_file);
        msdos.log_file = NULL;
    }
}

static void _LogToFile(const char *msg) {
    if(msdos.log_file)
        fwrite(msg, 1, strlen(msg), msdos.log_file);
}

static void _Log(const char * prefix, const char * fmt, va_list args) {
    char str[256];
    /* OWC 2.0 under DOS: vprintf() hangs and vsnprintf() is not working properly */
    vsprintf((char *) str, fmt, args);
    _LogToFile(prefix);
    _LogToFile(str);
    _LogToFile("\n");
}

static void _Console(const char * prefix, const char * fmt, va_list args) {
    char str[256];
    /* OWC 2.0 under DOS: vprintf() hangs and vsnprintf() is not working properly */
    vsprintf((char *) str, fmt, args);
    if (prefix) printf(prefix);
    printf((char const *) str);
    printf("\n");
    
    if (!prefix)
        prefix = " INFO: ";
    _LogToFile(prefix);
    _LogToFile(str);
    _LogToFile("\n");
}

/* -------------------------------------------------------------------------
 * Private VGA functions
 * ------------------------------------------------------------------------- */

static u8 _GetVideoMode();
#pragma aux _GetVideoMode = \
    "mov    ah,0Fh" \
    "int    10h" \
    value [al] \
    modify exact [eax];

static void _SetVideoMode(int mode);
#pragma aux _SetVideoMode = \
    "xor    ah,ah" \
    "int    10h" \
    parm [eax] \
    modify exact [eax];

static void _SetVGABorderColor(int color);
#pragma aux _SetVGABorderColor = \
    "mov    dx,03DAh" \
    "in     al,dx" \
    "mov    dx,03C0h" \
    "mov    al,31h" \
    "out    dx,al" \
    "mov    al,bl" \
    "out    dx,al" \
    parm [ebx] \
    modify exact [eax edx];

static inline void _UpdateVGAVideo(u8 *buffer);
#pragma aux _UpdateVGAVideo = \
    "cld" \
    "mov    edi, 0A0000h" \
    "mov    ecx,16000" \
    "rep    movsd" \
    parm [esi] \
    modify exact [ecx esi edi];

static void _ClearVGABuffer(u8 *buffer);
#pragma aux _ClearVGABuffer = \
    "cld" \
    "mov    ecx,16000" \
    "xor    eax,eax" \
    "rep    stosd" \
    parm [edi] \
    modify exact [eax ecx edi];

static void _CopyVGABuffer(u8 *buffer, u8 *dest);
#pragma aux _CopyVGABuffer = \
    "cld" \
    "mov    ecx,16000" \
    "rep    movsd" \
    parm [esi] [edi] \
    modify exact [ecx esi edi];

// clipped
static void _SetVGAPixel_c(u8 *buffer, int x, int y, int color);
#pragma aux _SetVGAPixel_c = \
    "cmp    eax,319" \
    "ja     @@clip" \
    "cmp    ebx,199" \
    "ja     @@clip" \
    "imul   ebx,320" \
    "add    ebx,eax" \
    "mov    [edi+ebx],cl" \
    "@@clip:" \
    parm [edi] [eax] [ebx] [ecx] \
    modify exact [ebx];

static void _WaitVGASync(void);
#pragma aux _WaitVGASync = \
    "mov    dx,03DAh" \
    "@@waitnovsync:" \
    "in     al,dx" \
    "test   al,8" \
    "jnz    @@waitnovsync" \
    "@@waitvsync:" \
    "in     al,dx" \
    "test   al,8" \
    "jz     @@waitvsync" \
    modify exact [eax edx];

// alternative simplified C code...
// static void _WaitVGASync() {
//     while (inp(0x03DA) & 0x08);
//     while ((inp(0x3DA) & 0x08) == 0);
// }

/* wait for Display Enable */
static void _WaitVGADisplayEnable(void);
#pragma aux _WaitVGADisplayEnable = \
    "mov    dx,03DAh" \
    "de:" \
    "in     al,dx" \
    "test   al,1" \
    "jnz    de" \
    modify exact [eax edx];

static void _SetVGARGB(int color, int r, int g, int b);
#pragma aux _SetVGARGB = \
    "mov    dx,03C8h" \
    "out    dx,al" \
    "inc    dx" \
    "mov    al,bl" \
    "out    dx,al" \
    "mov    al,cl" \
    "out    dx,al" \
    "mov    eax,esi" \
    "out    dx,al" \
    parm    [eax] [ebx] [ecx] [esi] \
    modify exact [eax edx];


/* -------------------------------------------------------------------------
 * Private CPU functions
 * ------------------------------------------------------------------------- */

/* CPUID 0 EBX signatures */
# define CPU_VENDOR_GENUINE_INTEL   0x756E6547  /* GenuineIntel */
# define CPU_VENDOR_AUTHENTIC_AMD   0x68747541  /* AuthenticAMD */
# define CPU_VENDOR_CYRIX_INSTEAD   0x69727943  /* CyrixInstead */

# define CPU_FLAG_HAS_CPUID     0x200000
# define CPU_FLAG_IS_486        0x40000

/* Cyrix ports and registers definitions */
# define CYRIX_WRTIE_IO_PORT    0x22
# define CYRIX_READ_IO_PORT     0x23
# define CYRIX_DIR0_REG_INDEX   0xFE
# define CYRIX_DIR1_REG_INDEX   0xFF

/* https://stanislavs.org/helppc/bios_data_area.html */
/* 0040h:006Ch is the IRQ0 TIMER TICKS SINCE MIDNIGHT updated 18.2 times per
 * second by the channel 0 of the 8254 system timer. */
# define SEG_BIOS_DATA_AREA     0x40
# define OFF_BIOS_TICK_COUNT    0x6C

typedef struct {
    u8 family;
    u8 model;
    char *name;
}
proc_def_t;

# define ARRAY_SIZE(SARRAY) (sizeof(SARRAY)/sizeof(SARRAY[0]))

const char * const intel_type_list[] = {
    [0] = "Original OEM",
    [1] = "Overdrive",
    [2] = "Dual-capable",
    [3] = "Reserved"
};

const proc_def_t intel_name_list[] = {
    3, 3, "i386",

    /* 486 have 16-bytes cache lines */
    /* https://www.ardent-tool.com/CPU/docs/Intel/486/datasheets/240440-001.pdf */
    4, 0, "i486 DX 25-33",
    4, 1, "i486 DX-50",
    4, 2, "i486 SX",
    4, 3, "i486 DX2",
    4, 4, "i486 SL",
    4, 4, "i486 SX2",
    4, 7, "i486 DX2 Write-Back",
    4, 8, "i486 DX4",
    4, 9, "i486 DX4 Write-Back",

    /* P5 have 32-bytes cache lines */
    5, 1, "Pentium 60-66 (P5)",
    5, 2, "Pentium 75-200 (P54C)",
    5, 3, "Pentium for 486 systems (P24T)",
    5, 4, "Pentium MMX (P55C)",

    /* from https://sandpile.org/x86/cpuid.htm#level_0000_0001h */
    5, 7, "P54C",   
    5, 8, "P55C (0.25µm)",

    /* 32-bytes cache lines */
    /* https://www.lpthe.jussieu.fr/~talon/pentiumII.pdf */
    6, 1, "Pentium Pro (P6)",
    6, 3, "Pentium II 233-450 (Model 3)",
    6, 5, "Pentium II (Model 5/Xeon/Celeron)",
    6, 6, "Celeron",
    6, 7, "Pentium III 450-600 (Katmai)",
    6, 8, "Pentium III 500-1133 (CopperMine)"
};

/* https://datasheets.chipdb.org/Cyrix/detect.pdf */
const proc_def_t cyrix_name_list[] = {
    /* Cyrix 486 has 4 bytes cache lines */
    /* https://www.ardent-tool.com/CPU/docs/Cyrix/486/94073.pdf */

    /* 16 bytes cache lines*/
    /* https://www.ardent-tool.com/CPU/docs/Cyrix/5x86/5X-DPAPR.pdf */
    4, 4, "MediaGX (5x86)",

    /* 32 bytes cache lines */
    /* https://www.ardent-tool.com/CPU/docs/Cyrix/6x86/6x-arch.pdf */
    5, 2, "M1 / 6x86 / 6x86L",
    5, 4, "MediaGX (6x86) MMX Enhanced",

    /* 32 bytes cache lines */
    /* https://www.ardent-tool.com/CPU/docs/Cyrix/MII/mpf96.pdf */
    6, 0, "6x86MX" 
};

/* https://a4lg.com/tech/x86/database/x86-families-and-models.en.html */
const proc_def_t amd_name_list[] = {
    4,  1, "Am486 DX",
    4,  3, "Am486 DX2",
    4,  7, "Am486 DX2 Write-Back",
    4,  8, "Am486 DX4",
    4,  9, "Am486 DX4 Write-Back",
    4, 10, "Elan SC4",
    4, 14, "Am5x86",
    4, 15, "Am5x86 Write-Back",

    /* K5/K6 have 32-bytes cache lines as per AMD datasheets */
    5,  0, "AMD-K5",
    5,  1, "AMD-K5 Model 1",
    5,  2, "AMD-K5 Model 2",
    5,  3, "AMD-K5 Model 3",
    5,  6, "AMD-K6",
    5,  7, "AMD-K6 Model 7",
    5,  8, "AMD-K6-2",
    5,  9, "AMD-K6-III",
    5, 13, "AMD-K6-2+/III+",

    6,  1, "AMD Athlon",
    6,  2, "AMD Athlon Model 2",
    6,  3, "AMD Duron",
    6,  4, "AMD Athlon Model 4",
    6,  6, "AMD Athlon/Duron MP/XP/4",
    6,  7, "AMD Duron Model 7",
    6,  8, "AMD Athlon XP/MP/XP-M",
    6, 10, "AMD Athlon XP/MP/XP-M/X-PM(LV)"
};

/* ---[ GLOBAL DEFINITIONS ]------------------------------------------------ */
static u32 cpu_performance_frequency = 0; /* Hz */

/* ---[ ASSEMBLY ]---------------------------------------------------------- */

/* inlined cpuid function: fills eax, ebx, ecx, and edx in *regs */
void x86_cpuid(int mode, struct DWORDREGS __far * regs);
#pragma aux x86_cpuid = \
    "cpuid" \
    "mov    DWORD PTR [edi], eax" \
    "mov    DWORD PTR [edi+4], ebx" \
    "mov    DWORD PTR [edi+8], ecx" \
    "mov    DWORD PTR [edi+12], edx" \
    parm [eax] [edi] \
    modify exact [eax ebx ecx edx edi];

/* Fills the Time Stamp Counter. It calls CPUID first which ensures that all
 * previously issued instructions are executed first. */
void x86_rdtsc(u64 __far * tsc);
#pragma aux x86_rdtsc = \
    "rdtsc" \
    "mov DWORD PTR [edi+4], edx" \
    "mov DWORD PTR [edi], eax" \
    parm [edi] \
    modify exact [eax ebx ecx edx edi];

/* method from https://datasheets.chipdb.org/Cyrix/detect.pdf */
u32 x86_check_cpu_flag(u32 flag_bit);
#pragma aux x86_check_cpu_flag = \
    "pushfd" \
    "pop    eax" \
    "mov    ecx, eax" \
    "xor    eax, ebx" \
    "push   eax" \
    "popfd" \
    "pushfd" \
    "pop    eax" \
    "cmp    eax, ecx" \
    "jz     not_supported" \
    "mov    eax, 1" \
    "jmp    exxit" \
    "not_supported:" \
    "xor    eax,eax" \
    "exxit:" \
    parm [ebx] \
    value [eax] \
    modify exact [eax ebx ecx];

u32 x86_is_cyrix486_cpu();
#pragma aux x86_is_cyrix486_cpu = \
    "xor    ax, ax"\
    "sahf"\
    "mov    ax, 5"\
    "mov    bx, 2"\
    "div    bl"\
    "lahf"\
    "cmp    ah, 2"\
    "jne    not_cyrix"\
    "mov    eax, 1"\
    "jmp    exxit"\
    "not_cyrix:"\
    "mov    eax, 0"\
    "exxit:"\
    value [eax] \
    modify exact [eax ebx];

u32 x86_read_bios_rtc();
#pragma aux x86_read_bios_rtc = \
    "mov    edi, 046Ch" \
    "mov    eax, [edi]" \
    value [eax] \
    modify exact [eax edi];

/* ---[ IMPLEMENTATION ]---------------------------------------------------- */

void _InitCPUInfos() {
    struct DWORDREGS regs;
    u32 cpu_family = 0;

    /* INITIALIZES CACHE LINE SIZE */

    if (x86_check_cpu_flag(CPU_FLAG_HAS_CPUID) == 1) {
        x86_cpuid(1, &regs);
        cpu_family = (regs.eax >> 8) & 0xf;

        switch (cpu_family) {
            case 4:
                x86.Cpu.L1_cache_line_bytes = 16; /* i486 family */
                break;

            case 5:
            case 6:
            default:
                x86.Cpu.L1_cache_line_bytes = 32; /* P5/P6 family */
                break;
        }
    } else {
        x86.Cpu.L1_cache_line_bytes = 16;
    }

    /* RDTSC instruction is not available in CPU prior to the pentium */
    if (cpu_family <= 4) {
        cpu_performance_frequency = 0;
        return;
    }

    /* INITIALIZES CPU FREQUENCY */

    u64 tsc0, tsc1;

    /* ensure we're not in the middle of a tick...*/
    u32 ticks = x86_read_bios_rtc();
    while (x86_read_bios_rtc() == ticks)
        ;

    /* read and save the initial TSC */
    x86_rdtsc(&tsc0);                   

    /* now wait for the delay */
    ticks += (5 + 1);
    while (x86_read_bios_rtc() != ticks)
        ;

    /* read and save the new TSC */
    x86_rdtsc(&tsc1);

    cpu_performance_frequency = (tsc1 - tsc0) * (18.2f / 5.0f);
}

static const char * get_cpu_type(u32 type, const char * const list[], u32 count) {
    if (type >= count) {
        return "unknown cpu type";
    } else {
        return list[type];
    }
}

static const char * get_cpu_family_model(u32 family, u32 model, const proc_def_t list[], u32 count) {
    for (int i = 0; i < count; i++) {
        const proc_def_t *cpu = &list[i];
        if (cpu->family < family)
            continue;
        if (cpu->family == family && cpu->model == model) {
            return cpu->name;
        }
    }
    return "unknown cpu model/family";
}


void _ConcatLog(char * s, size_t n, const char * fmt, va_list args) {
    char str[256];
    vsprintf((char*)str, fmt, args); // vsnprintf() is not working properly in WTC 2.0
    strncat(s, str, n);
}

static void _BuildLogMessage(char * s, int n, const char * fmt, ...) {
    va_list args;
    va_start(args, fmt);
    _ConcatLog(s, n, fmt, args);
    va_end(args);
}

#define RESET_LOG(message) message[0] = 0;

static void _PrintSystemInfos() {
    char log[256];

    RESET_LOG(log);
    if (x86_check_cpu_flag(CPU_FLAG_HAS_CPUID) == 0) {
        if (x86_check_cpu_flag(CPU_FLAG_IS_486) == 1) {

            _BuildLogMessage(log, 256, "CPU Type:i486+");
            
            if (x86_is_cyrix486_cpu()) {
                outp(CYRIX_WRTIE_IO_PORT, CYRIX_DIR0_REG_INDEX);
                u32 device_id = inp(CYRIX_READ_IO_PORT);

                _BuildLogMessage(log, 256, " (Cyrix:0x%02X) \"", device_id);

                outp(CYRIX_WRTIE_IO_PORT, CYRIX_DIR1_REG_INDEX);
                u32 dir1_infos = inp(CYRIX_READ_IO_PORT);

                u32 step_id = (dir1_infos >> 4) & 0x3;
                u32 revision = dir1_infos & 0x3;

                if (device_id <= 0x07)
                    _BuildLogMessage(log, 256, "Cx486SLC/DLC/SRx/DRx");
                else if (device_id <= 0x13)
                    _BuildLogMessage(log, 256, "Cx486S");
                else if (device_id <= 0x1F)
                    _BuildLogMessage(log, 256, "Cx486DX/DX2");
                else if (device_id <= 0x2F)
                    _BuildLogMessage(log, 256, "5x86");
                else
                    _BuildLogMessage(log, 256, "????");
                
                _BuildLogMessage(log, 256, "\" Step:%d Revision:%d", step_id, revision);
            }

        } else {
            _BuildLogMessage(log, 256, "CPU Type:i386");
        }
        Console(log);
    }

    struct DWORDREGS regs;

    x86_cpuid(0, &regs);
    Console("CPUID Max Level:%d", regs.eax);
    Console("CPUID 0: %08X %08X %08X %08X ", regs.eax, regs.ebx, regs.ecx, regs.edx);
    x86_cpuid(1, &regs);
    Console("CPUID 1: %08X %08X %08X %08X ", regs.eax, regs.ebx, regs.ecx, regs.edx);

    x86_cpuid(0, &regs);
    RESET_LOG(log);
    _BuildLogMessage(log, 256, "CPU Vendor:%.4s", &regs.ebx);
    _BuildLogMessage(log, 256, "%.4s", &regs.edx);
    _BuildLogMessage(log, 256, "%.4s", &regs.ecx);

    u32 vendor = regs.ebx;

    x86_cpuid(1, &regs);
    _BuildLogMessage(log, 256, " CoreType:0x%08X", regs.eax);

    u32 cpu_type = (regs.eax >> 12) & 0x3;
    u32 family = (regs.eax >> 8) & 0xf;
    u32 model = (regs.eax >> 4) & 0xf;
    u32 stepping = regs.eax & 0xf;
    Console(log);

    RESET_LOG(log);
    _BuildLogMessage(log, 256, "CPU Family:%d Model:%d Stepping:%d",
        family, model, stepping);
    Console(log);

    RESET_LOG(log);
    _BuildLogMessage(log, 256, "CPU Type:");
    _BuildLogMessage(log, 256, get_cpu_type(cpu_type, &intel_type_list, ARRAY_SIZE(intel_type_list)));
    _BuildLogMessage(log, 256, " \"");
    switch (vendor) {
        case CPU_VENDOR_CYRIX_INSTEAD:
            _BuildLogMessage(log, 256, get_cpu_family_model(family, model, &cyrix_name_list, ARRAY_SIZE(cyrix_name_list)));
            break;

        case CPU_VENDOR_AUTHENTIC_AMD:
            _BuildLogMessage(log, 256, get_cpu_family_model(family, model, &amd_name_list, ARRAY_SIZE(amd_name_list)));
            break;
                        
        case CPU_VENDOR_GENUINE_INTEL:
            _BuildLogMessage(log, 256, get_cpu_family_model(family, model, &intel_name_list, ARRAY_SIZE(intel_name_list)));
            break;

        default:
            switch (family) {
                case 4: _BuildLogMessage(log, 256, "Unknown 486"); break;
                case 5: _BuildLogMessage(log, 256, "Unknown 586"); break;
                case 6: _BuildLogMessage(log, 256, "Unknown 686"); break;
                default: _BuildLogMessage(log, 256, "????");
            }
    }
    _BuildLogMessage(log, 256, "\"");
    Console(log);

    RESET_LOG(log);
    _BuildLogMessage(log, 256, "CPU Features: ");
    x86_cpuid(1, &regs);
    bool has_fpu = (regs.edx & 0x01);
    bool has_tsc = ((regs.edx>>4) & 0x01);
    bool has_mmx = ((regs.edx>>23) & 0x01);
    if (has_fpu) _BuildLogMessage(log, 256, "FPU ");
    if (has_tsc) _BuildLogMessage(log, 256, "TSC ");
    if (has_mmx) _BuildLogMessage(log, 256, "MMX ");
    Console(log);

    Console("CPU Cache Line:%d-bytes", x86.Cpu.L1_cache_line_bytes);

    if (cpu_performance_frequency)
        Console("CPU Clock Frequency:%u Hz (%u Mhz)\n", cpu_performance_frequency, (cpu_performance_frequency + 500000) / 1000000);
}

# endif /* X_IMPLEMENTATION */

#endif /* X86_CORE_DOS_H */
