# UV Mapping

## 2D Tunnel

Based on precalc tables
([excellent explanation](http://lodev.org/cgtutor/tunnel.html) / [mirror](docs/Tunnel_Effect.zip))

![tunnel](docs/tunnel-2d-screenshot.png)

## Generated maps

Distance map  
![distance map](docs/distance_map.png)

Angle map  
![angle map](docs/angle_map.png)

## Inspirations

A bunch of transformations have been directly inspired by
[Inigo Quilez](http://www.iquilezles.org/www/articles/deform/deform.htm)

The texture used here might not be adequate for all transformations,
better results might be obtained with other carefuly chosen textures.

Also, mixing transformations with a dynamic plasma-like texture,
interferences or 2d bump mapping could give interesting results.
