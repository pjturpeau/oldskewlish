# Oldschool demo samples in C

A personal repository for old-school stuffs that I code as a hobby on my spare
time using simple C constructs.

I'm mostly focusing on low-res / CPU-only / framebuffer based / 8-bits per pixel
technics and old-school effects and tricks.

See [turpeau.net/pierrejean/demoscene/](https://turpeau.net/pierrejean/demoscene/) for past *productions*.

## Screenshots

For the impatient, you can jump to [screenshots](docs/screenshots.md).

## The X86 library part

It is my simple stupid zero setup framework to support both easy prototyping of
new effects and easy porting/fine-tuning under MS-DOS.

As such the `x86/` library is under high refactoring:
 - better separate the `core` part from extras functions
 - support MS-DOS target through the Open Watcom C 2.0 compiler
 - coding style moving from snake_case to CamelCase for the API
 - header files refactoring

In the process of moving everything to support the new API and architecture, I
keep two versions of the libraries to support the  incremental transition of
existing effects: `x/` is now freezed and `x86/` will be upgraded step-by-step.

See [history](#history) for details.

### Architecture and API

**REFACTORING IN PROGRESS __DO NOT USE__**  
**(legacy `x/` API redesigned into `x86/` with MSDOS support)**


While I use a [header-only](https://en.wikipedia.org/wiki/Header-only) approach
to simplify the build process, I'm not very strict on the pure _single header_
technique.

The core part definition is supported by one single header file:
[`x86/x.h`](x86/x.h)


## Samples

In [`samples/`](samples/) you'll find the repository of my
experiments developed based on [`x86/x.h`](x86/x.h) (and other plugins...)

Coding level is heterogeneous as well as optimization strategies (sometimes
using floating point arithmetics, sometimes using fixed point arithmetics), and
I doubt all of them would looked smooth back on my i386DX or even on my Pentium.

### Execution options

You can exit thanks to the `[ESC]` key.  
The `[c]` key toggles the display of the color palette.  
The `[p]` key pauses the animation. 
The `[s]` key generates a screenshot.tga file.
The `[TAB]` key shows CPU load infos.

Under any SDL2 platform you can:
 - Use `[ALT] + [ENTER]` to switch between fullscreen and windowing mode
 - Use `[CTRL] + [ENTER]` to switch to framebuffer resolution

For the CLI, use `-h` or `-?` to display available options.

## Coding style

- file names and directory names: `kebab-case`
- variables, attributes, fields: `snake_case`
- constants or macros: `SCREAMING_SNAKE_CASE`
- function names: `CamelCase`
- clipped graphical functions names are suffixed with `_c`

## Open topics

 - decent MOD player integration
 - small size 4K/64k approach and freestanding executables under MSDOS
 - more performance testing on core/innerloop functions

## Extra documentations

### About fx

- [UV mapping fx explanation](samples/uv-mapping/README.md)
- [Rotozom fx explanation](samples/rotozoom/README.md)
- [Raycasting tunnel explanation](samples/raycast-tunnel/README.md)

### Interesting technical references

- [Interpolation Tricks](http://sol.gfxile.net/int-erpolation/)
- [Timer steps](https://gafferongames.com/post/fix_your_timestep/)
- [FPS measurement technics](http://sdl.beuc.net/sdl.wiki/SDL_Average_FPS_Measurement)
- [Easing equations](http://robertpenner.com/easing/)
- [Curves](http://xahlee.info/SpecialPlaneCurves_dir/specialPlaneCurves.html) and [Surfaces](http://xahlee.info/surface/gallery.html)
- [Particles systems from the ground up](http://buildnewgames.com/particle-systems/)
- [GBA programming](https://www.coranac.com/tonc/)
- [Cubic & $eeN technical docs](https://www.cubic.org/docs/)

### Informational

- [Immediate 2d](https://github.com/npiegdon/immediate2d)
- [mini framebuffer](https://github.com/emoon/minifb)
- [raylib](https://www.raylib.com/)

## History

### 2024-11-01 - v3.0

First iteration of the new `x86` library to support x86/DOS compilation.  
Only `samples/fire` is working for now.

### 2024-10-28 - v2.1

v2.1 tagged in main branch.  

It is the last pure SDL2 version before complete refactoring to support msdos
compilation with Watcom C.

It will probably have impact on core video/platform API.

### 2020-04-10 - oldskewlish-reboot branch

v2.0 tag.  

`xlib` rebooted into `oldskewlish` which true goal is not the framework itself but the revival of old school code feeling (at least the one I have in mind).

Many aspects will be simplified to limit code verbosity and bloatedness:

- simple stupid zero-setup approach, more minimalistic and readable based on a self-sufficient core extended through independent plugins
- new naming convention using `<verb>_<function>()` schema
- `x_`prefixing removed (I don't care of collisions...)
- no more object approach when possible (E.g., video is a singleton, keyframing is a singleton, ...)
- scene-graph code removed: bloated OOP and not so much used. The [keyframe](x/gfx/keyframe.h) plugin does the same job in [plasma](samples/plasma/plasma.c) with less lines and a lot less code complexity (see the history)
- for now static display resolution (this could change however).
- true separation of SDL dependencies

### 2020-04-08 - xlib v1.0

xlib V1.0 tagged in main branch.

Mostly objects driven approach, using mainly stack-allocated objects.
Everything prefixed with `x_` and using `x_<object type>_<operation>()` naming convention. 

Including a preliminary object oriented scene-graph.

### 2019-11-30 - Reboot of demo fx in C

Didn't like that much C++ dev complexity nowadays.
Rebooted development in a pure C version.

### 2018-02-14 - starting C++11 Code Snippets

Started [c++ demoscene samples](https://gitlab.com/pjturpeau/demos-cpp) as a hobby during commuting.
