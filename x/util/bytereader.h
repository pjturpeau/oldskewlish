/**
 * x/util/bytereader.h
 * 
 * Copyleft (c) Pierre-Jean Turpeau 12/2023
 * <pierrejean AT turpeau DOT net>
 * 
 * Very trivial finite state machine framework:
 *  - it uses function state pattern (one function per state)
 *  - each state function manages entry/run/exit actions
 * 
 * Possible future evolutions:
 *  - hierarchical state machine
 *  - external event injection 
 */
#ifndef X_UTIL_BYTEREADER_H
#define X_UTIL_BYTEREADER_H

# include <x/x.h>
# include <SDL2/SDL_endian.h>

/* ---[ DEFINITIONS ]------------------------------------------------------- */

#define ERR_NONE            0x00
#define ERR_INVALID_OFFSET  0x10

typedef struct {
    u8 * data;
    u32 size;   /* in bytes */
    u32 offset; /* in bytes */
    u32 error;
    bool big_endian;
} bytereader_t;

/* ---[ DECLARATIONS ]------------------------------------------------------ */

/* if is_big_endian is set to true, the reader converts data to host byte order */
void bytereader_init(bytereader_t * reader, u8 * data, u32 size, bool is_big_endian);

void bytereader_seek(bytereader_t * reader, u32 offset_position);
void bytereader_skip(bytereader_t * reader, u32 skip_count);
u32  bytereader_tell(bytereader_t * reader);
u8 * bytereader_address(bytereader_t * reader);

/* fill str_out with a zero-terminal string */
void bytereader_read_string(bytereader_t * reader, char *str_out, u32 maxlen);

u32  bytereader_read_u32(bytereader_t * reader);
u16  bytereader_read_u16(bytereader_t * reader);
u8   bytereader_read_u8(bytereader_t * reader);

void bytereader_read_bytes(bytereader_t * reader, u8 *out, u32 size);

/* ---[ IMPLEMENTATION ]---------------------------------------------------- */

# ifdef X_IMPLEMENTATION

static inline u32 _dummy_ntohl(u32 data) { return data; }
static inline u16 _dummy_noths(u16 data) { return data; }

void bytereader_init(bytereader_t * reader, u8 * data, u32 size, bool big_endian) {
    reader->data = data;
    reader->size = size;
    reader->offset = 0;
    reader->error = ERR_NONE;
    reader->big_endian = big_endian;
}

void bytereader_seek(bytereader_t * reader, u32 offset_position) {
    reader->offset = offset_position;
}

void bytereader_skip(bytereader_t * reader, u32 skip_count) {
    reader->offset += skip_count;
}

u32  bytereader_tell(bytereader_t * reader) {
    return reader->offset;
}

u8 * bytereader_address(bytereader_t * reader) {
    return (u8 *)(reader->data + reader->offset);
}

void bytereader_read_string(bytereader_t * reader, char *str_out, u32 maxlen) {
    if( reader->offset + maxlen <= reader->size ) {
        memcpy(str_out, reader->data + reader->offset, maxlen);
        str_out[maxlen-1] = 0;
        reader->offset += maxlen;
    } else {
        reader->error = ERR_INVALID_OFFSET;
        str_out[0] = 0;
    }
}

u32 bytereader_read_u32(bytereader_t * reader) {
    if( reader->data && reader->offset<= (reader->size - 4)) {
        u32 *address = (u32 *)(reader->data + reader->offset);
        reader->offset += 4;
        if(reader->big_endian)
            return SDL_SwapBE32(*address);
        else
            return *address;
    } else {
        reader->error = ERR_INVALID_OFFSET;
        return 0;
    }
}

u16 bytereader_read_u16(bytereader_t * reader) {
    if( reader->data && reader->offset <= (reader->size - 2)) {
        u16 *address = (u16 *)(reader->data + reader->offset);
        reader->offset += 2;
        if(reader->big_endian)
            return SDL_SwapBE16(*address);
        else
            return *address;
    } else {
        reader->error = ERR_INVALID_OFFSET;
        return 0;
    }
}

u8 bytereader_read_u8(bytereader_t * reader) {
    if( reader->data && reader->offset <= (reader->size - 1)) {
        u8 *address = (u8 *) (reader->data + reader->offset);
        reader->offset += 1;
        return *address;
    } else {
        reader->error = ERR_INVALID_OFFSET;
        return 0;
    }
}

void bytereader_read_bytes(bytereader_t * reader, u8 *out, u32 size) {
    if( reader->data && reader->offset  <= (reader->size - size) ) {
        void *address = reader->data + reader->offset;
        memcpy(out, address, size);
        reader->offset += size;
    } else
        reader->error = ERR_INVALID_OFFSET;
}

# endif /* X_IMPLEMENTATION */

#endif /* X_UTIL_BYTEREADER_H */