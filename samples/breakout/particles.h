/**
 * samples/akranoying/particles.h
 * Copyleft (c) Pierre-Jean Turpeau 11/2020
 * <pierrejean AT turpeau DOT net>
 *
 * very naive configurable particle system
 */
#ifndef PARTICLES_H
#define PARTICLES_H

#include <x86/x.h>
#include <x86/draw.h>

typedef struct {
    v2f position;
    v2f velocity;

    /* remember that objects of different mass fall at the same rate */
    v2f accelerate;

    float mass;
    float growth;

    float lifetime;
    float lifespan;
    float deathrate;

    int colors_count;
    u8 * colors;
}
particle_t;

/* ---[ DECLARATIONS ]------------------------------------------------------ */

extern void AddOneParticle(int x, int y, float lifespan, float deahrate, float mass, float growth, float speed, float angle, v2f * accelerate, u8 * colors, int colors_count);

extern void UpdateParticles(float time_step);

extern void DrawParticles();

/* ---[ IMPLEMENTATION ]---------------------------------------------------- */

# ifdef X_IMPLEMENTATION

#define MAX_PARTICLES 4096 // to be refined
static int _particles_count = 0;
static particle_t _particles[MAX_PARTICLES]; /* particles to update and draw */

void AddOneParticle(int x, int y, float lifespan, float deathrate, float mass, float growth, float speed, float angle, v2f * accelerate, u8 * colors, int colors_count) {

    if(_particles_count >= MAX_PARTICLES-1) {
        LogError("AddOneParticle: too many particles");
        return;
    }

    particle_t * p = &_particles[_particles_count++];

    p->position.x = x;
    p->position.y = y;

    p->lifetime = 0.f;
    p->lifespan = lifespan;
    p->deathrate = deathrate;

    p->mass = mass;
    p->growth = growth;

    p->velocity.u = speed * cos(angle);
    p->velocity.v = -speed * sin(angle);

    p->accelerate.u = p->accelerate.v = 0;
    if(accelerate) {
        p->accelerate.u = accelerate->u;
        p->accelerate.v = accelerate->v;
    }

    p->colors = colors;
    p->colors_count = colors_count;
}

static int cycle = 0;

void UpdateParticles(float time_step) {
    cycle++;
    for(int i = 0; i < _particles_count; i++) {
        particle_t * p = &_particles[i];

        /* dead or alive ? */
        p->lifetime += p->deathrate * time_step;

        if(p->lifetime > p->lifespan) {
            _particles_count--;
            if(i < _particles_count) {
                memcpy(&_particles[i], &_particles[_particles_count], 
                    sizeof(particle_t));
                i--;
            }
            continue;
        }

        /* basic physics update */
        p->velocity.u += p->accelerate.u * time_step;
        p->velocity.v += p->accelerate.v * time_step;
        p->mass += p->growth * time_step;

        /* move the thing */
        p->position.x += p->velocity.u * time_step;
        p->position.y += p->velocity.v * time_step;
    }
}

void DrawParticles() {
    for(int i = 0; i <_particles_count; i++) {

        particle_t * p = &_particles[i];
        if(p->lifetime > p->lifespan)
            continue;

        int ci = p->colors_count * p->lifetime / p->lifespan;

        if(p->mass <= 1.f)
            SetPixel_c(p->position.x, p->position.y, p->colors[ci]);
        else
            DrawCircle(p->position.x, p->position.y, p->mass, p->colors[ci]);
    }
}

# endif /* X_IMPLEMENTATION */

#endif /* PARTICLES_H */