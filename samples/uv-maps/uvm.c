/**
 * samples/uv-maps/uvm.c
 * 
 * Copyleft (c) Pierre-Jean Turpeau 12/2019
 * <pierrejean AT turpeau DOT net>
 */
#define X_IMPLEMENTATION 1

#define TITLE "-[ uv 2d mapping ]-"

#include <x86/x.h>
#include <x86/image.h>

#include "uvmap.h"
#include "uvmapgen.h"

extern unsigned char texture_data[];
extern unsigned int  texture_size;

#define USEFUL_COLORS 16 /* the number of useful colors in the texture (for shadows) */

static int map_display = 0;
static uv_mapping uvmap_fx;
image_t texture;
palette_t palette;

static uv_map_gen_func gen_funcs[] = {
    uv_map_gen_swirl_tunnel,
    uv_map_gen_tunnel,
    uv_map_gen_concentric_waves,
    uv_map_gen_warping,
    uv_map_gen_tv_warping,
    uv_map_gen_warping_near,
    uv_map_gen_warping_far,
    uv_map_gen_symetric_planes,
    uv_map_gen_flat_swirl,
    uv_map_gen_star_tunnel,
    uv_map_gen_flower,
    uv_map_gen_big_flower,
    uv_map_gen_square_tunnel,
    uv_map_gen_bubble,
    uv_map_gen_noname,
    uv_map_gen_excentric_tunnel,
    uv_map_gen_swirl
};

void UpdateAndDrawMap(float dt) {
    /* not the best but it works for all the configurations... */
    uvmap_fx.start_u = dt * 0.05;
    uvmap_fx.start_v = dt * 0.05;

    switch(map_display) {
        case 0: uv_map_draw(&uvmap_fx); break;
        case 1: uv_map_draw_u_map(&uvmap_fx); break;
        case 2: uv_map_draw_v_map(&uvmap_fx); break;
        case 3: uv_map_draw_w_map(&uvmap_fx); break;
        case 4: ClearScreen(0); uv_map_draw_texture(&uvmap_fx); break;
    }

    int event = GetLastEvent();

    if( event == 'm' || event == 'M' ) { /* [M] key to display maps */
        map_display++;
        map_display %= 5;
        switch(map_display) {
            case 0: RestorePalette(&palette); break;
            case 1: LinearGradientPalette(0, 0, 255, 0xFFFFFF); break;
            case 3: LinearGradientPalette(0, 0, uvmap_fx.shadow_levels, 0xFFFFFF); break;
            case 4: RestorePalette(&palette); break;
        }
    }
    else if ( event == 'h' || event == 'H' ) {
        static bool show_shadows = true;
        show_shadows = !show_shadows;
        uvmap_fx.useful_colors = show_shadows ? texture.color_count : 0;
    } 
    else if( event == ' ' ) { /* [SPACE] to generate new maps */
        static int map_version = 0;
        map_version++;
        map_version %= sizeof(gen_funcs)/sizeof(uv_map_gen_func);
        uv_map_generate(&uvmap_fx, gen_funcs[map_version]);
    }
}

int main(int argc, char **argv) {
    InitPlatform(argc, (const char **) argv);
    ParseDefaultExeOptions();

    Console("Extra key actions");
    Console("\t[M]\tto display the U, V & W maps");
    Console("\t[H]\tto activate/deactivate the shadow map");
    Console("\t[SPACE]\tto generate new maps");

    SetVGAMode();

    LoadBMP(&texture, &palette, texture_data, texture_size);
    RestorePalette(&palette);
 
    uv_map_init(&uvmap_fx, &texture);
    uvmap_fx.useful_colors = texture.color_count;
    uvmap_fx.shadow_levels = MAX_COLORS/texture.color_count;

    DarkenPaletteToBlack(0, texture.color_count, uvmap_fx.shadow_levels);
    SavePalette(&palette); /* store darkening... */

    uv_map_generate(&uvmap_fx, gen_funcs[0]);

    while(!IsPlatformExit()) {
        BeginFrameLoop();
        ExecuteDefaultInteractions();

            if (! IsPaused())
                UpdateAndDrawMap(GetTime());

        EndFrameLoop();
    };

    ClosePlatform();

    Console("fps = %d", x86.Time.fps);
    return EXIT_SUCCESS;
}
