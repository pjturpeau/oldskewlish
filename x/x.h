/**
 * x/x.h
 * 
 * Copyleft (c) Pierre-Jean Turpeau 04/2020
 * <pierrejean AT turpeau DOT net>
 * 
 * use '#define X_IMPLEMENTATION = 1' before including this file in your
 * main cpp file so that the compiler has access to the source code
 * implementation.
 */
#ifndef X_H
#define X_H

#include <stdint.h> /* uint8_t, uint16_t, uint32_t */
#include <stdbool.h>
#include <assert.h>

# include <x/default_config.h>

/* ---[ COMMON DEFINITIONS ]------------------------------------------------ */

# if defined(ARCH_X86) || defined(ARCH_X86_64) || defined(_X86_) || defined(__x86_64__)
#  undef X86
#  define X86
# endif

# ifndef M_PI
#  define M_PI 3.14159265358979323846
# endif

# ifndef M_PI_2
#  define M_PI_2 1.57079632679489661923
# endif

# ifndef M_TAU
#  define M_TAU 6.28318530717958647692
# endif

/* from http://sol.gfxile.net/interpolation/ */
# define M_SMOOTHSTEP(x)   ((x) * (x) * (3 - 2 * (x)))
# define M_SMOOTHERSTEP(x) ((x) * (x) * (x) * ((x) * ((x) * 6 - 15) + 10))

# define M_MIN(x, y) (((x) < (y)) ? (x) : (y))
# define M_MAX(x, y) (((x) > (y)) ? (x) : (y))

typedef uint8_t  u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;
 
typedef int8_t   s8;
typedef int16_t  s16;
typedef int32_t  s32;
typedef int64_t  s64;

typedef float   f32;
typedef double  f64;

typedef union {
    struct { f32 x, y; };
    struct { f32 u, v; };
} v2f;

typedef union {
    struct { s32 x, y; };
    struct { s32 u, v; };
    struct { s32 w, h; };
} v2s;

typedef union {
    struct { s32 x, y, z; };
    struct { s32 u, v, w; };
    struct { s32 r, g, b; };
} v3s;

typedef union {
    struct { f32 x, y, z; };
    struct { f32 u, v, w; };
    struct { f32 r, g, b; };
} v3f;

typedef union {
    f32 data[4];
    struct { f32 x, y, z, w; };
    struct { f32 a, r, g, b; };
} v4f;

typedef struct {
    s32 x, y, w, h;
} rect_t;

/* -------------------------------------------------------------------------
 *  Execution functions
 * ------------------------------------------------------------------------- */

/** callback definition for the user-defined periodic update function */
typedef void (*update_func_t)(float time_step);

/**
 * Manage the main infinite loop.
 * 
 * Cyclic operations are:
 *  - process of managed interaction events
 *  - execute the update function with the time elapsed between two frames
 *  - track and display the cpu load (if enabled)
 *  - swap front and back buffers (if enabled)
 *  - update the platform video buffer
 * 
 * Elapsed time between each frame helps to:
 *  - integrate movements and physics on a time basis (time steps)
 *  - keep smooth movements even with stuttering frame rates
 *  - stable speed whatever the execution environment
 *  
 * By default, main_loop exists on KEY_ESC and platform's exit event.
 */
extern void main_loop(update_func_t callback);

/**
 * Process command-line arguments 
 * 
 * It returns NULL if no CLI option matches 'opt'.
 * If 'opt' matches an option in the args list, it returns:
 *  - the pointer to the next string argument if it is a valid value
 *    (i.e., not starting with a dash '-')
 *  - an empty string ("") otherwise.
 */
extern const char * check_args(const char * opt);

/** Print message onto the console, close resources and terminate */
extern void fatal_error(const char *fmt, ...);

/* -------------------------------------------------------------------------
 * Pixel drawing functions
 * ------------------------------------------------------------------------- */

/** set the backbuffer pixel with the 'color_index' (asserted but unclipped) */
static inline void set_pixel(int x, int y, u8 color_index);

/** clipped and unasserted version of set_pixel */
static inline void set_pixel_clipped(int x, int y, u8 color);

/** returns the backbuffer pixel found at (x,y) coordinates (assserted but
 * unclipped) */ 
static inline u8 get_pixel(int x, int y);

/** clear the backbuffer using 'clear_color' */
static inline void ClearScreen(u8 clear_color);

/** the one in which all drawing functions set pixels */
static inline u8 * backbuffer();

/**
 * In case of single buffering (which is the default), backbuffer() and
 * frontbuffer() return the same address.
 * 
 * I use backbuffer() everywhere since I think the frontbuffer() should return
 * the address of the true native 8 BPP video framebuffer (when available) in
 * which you rarely draw directly inside but rather wait for VBL sync and then
 * blit the content of your backbuffer to the video frontbuffer.
 *
 * When access to the previous frame is needed, on non-native 8BPP platforms
 * (e.g., SDL2 under Windows), the "real" frontbuffer color depth match the
 * desktop one which could be 16BPP or 32BPP, as such a "fake" front buffer has
 * to be emulated using the EMULATE_FRONT_BUFFER preprocessor definition.
 * Swapping between the two is automatic.
 */
static inline u8 * frontbuffer();

/* -------------------------------------------------------------------------
 * Miscellaneous video functions
 * ------------------------------------------------------------------------- */

/** freezes rendering resources and save screen pixels into screenshot.tga */
extern void screenshot();

/** enable TV like distortion when blitting to platform pixels */
extern void enable_warping();
extern void disable_warping();

/** save image 'data' into a .TGA file according to 'bits' (8, 16, 24, 32) */
extern void save_tga(void * data, int w, int h, int bits);

/** shake the video output by adding the given amount to the shake intensity */
extern void shake(int amount);

/* -------------------------------------------------------------------------
 * Color operations
 * ------------------------------------------------------------------------- */

extern void set_color(int index, u32 rgb888);

/** Set the video palette as a linear interpolation of RGB components */ 
extern void linear_gradient(int from_index, u32 from_rgb,
                            int to_index,   u32 to_rgb);

/* -------------------------------------------------------------------------
 * Palette operations
 * ------------------------------------------------------------------------- */

# define MAX_COLORS 256

typedef struct pix32 {
    union { /* endian dependant color components */
        struct {
#  if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
            u8 b:8, g:8, r:8, a:8;
#  else
            u8 a:8, r:8, g:8, b:8;
#  endif
        };
        u32 v; /* 32-bits color value */
    };
} pix32;

typedef struct {
    pix32 colors[MAX_COLORS];
} palette_t;

/** allocate a palette */
extern palette_t * create_palette();
extern void delete_palette();

/** reset the palette to the given color */
void clear_palette(u32 rgb);

/** Copy the current video palette into 'palette_out' */ 
extern void save_palette(palette_t * palette_out);

/** Configure the video palette using 'palette_int' */
extern void restore_palette(const palette_t * palette_in);

/** copy the content of 'src' palette to 'palette_in' */
extern void copy_palette(palette_t * palette_in, const palette_t * src);

/**
 * The video palette is a linear interpolation of RGB components between
 * 'from' and 'to' palettes according to the given 'factor'*
 */ 
extern void fade_to(const palette_t * from, const palette_t * to, float factor);

/* -------------------------------------------------------------------------
 * Images operations
 * ------------------------------------------------------------------------- */

typedef struct {
    u8 *pixels; 
    u8 *masks;
    palette_t * palette; /* optional */
    int w;
    int h;
    int transparent_color;
    int color_count;
} image_t;

/**
 * allocate an image and its pixels according to w and h
 * masks = NULL, palette = NULL, color_count = 0, transparent_color = -1
 */
extern image_t * create_image(int w, int h);

/** delete both pixels and palette */
extern void delete_image(image_t * image);

/* -------------------------------------------------------------------------
 * Interactivity
 * ------------------------------------------------------------------------- */

enum {
    EVT_NONE = 0,

    /* standard ASCII codes */
    KEY_TAB   = 9,
    KEY_ENTER = 13,
    KEY_ESC   = 27,

    /* non standard ASCII codes */
    KEY_UP       = 0x80,
    KEY_DOWN     = 0x81,
    KEY_LEFT     = 0x82,
    KEY_RIGHT    = 0x84,

    KEY_PAGEUP   = 0x90,
    KEY_PAGEDOWN = 0x91,
    KEY_END      = 0x92,
    KEY_HOME     = 0x93,

    /* non standard shortcuts */
    EVT_QUIT = -1,
    KEY_CTRL_ENTER = -2,
    KEY_ALT_ENTER  = -3,
};

/** returns the last interaction event or EVT_NONE otherwise */
extern int last_event();

typedef enum {
    BTN_LEFT    = 0,
    BTN_RIGHT   = 1,
    BTN_UP      = 2,
    BTN_DOWN    = 3,

    LAST_BUTTON
}
button_t;

/** returns true if the given button is in the press state */
extern bool button(button_t b);

/* -------------------------------------------------------------------------
 * Numbers - math functions are postfixed by s32, u32, f32, f64, s64, u64
 * ------------------------------------------------------------------------- */

/**
 * (max - min) shall be much much less than RAND_MAX
 * http://c-faq.com/lib/randrange.html
 */
static inline int random_s32(int min, int max);

/**
 * (max - min) shall be much much less than RAND_MAX
 * See: http://c-faq.com/lib/randrange.html
 */
static inline float GetFloatRandomValue(float min, float max);

/** seed the buffer with random values */
static inline void seed_buffer_u8(u8 * buffer, int size, int min, int max);

/**
 * float linear interpolation between two values
 * from: start value
 * to: end value
 * ratio: interpolation ratio in 0..1 interval
 */
static inline float lerp_f32(float from, float to, float ratio);

/**
 * signed integer linear interpolation between two values
 * from: start value
 * to: end value
 * ratio: interpolation ratio in 0..1 interval
 */
static inline s32 lerp_s32(s32 from, s32 to, float ratio);

/**
 * look-up table based linear interpolation.
 * 
 * It interpolates between lut(x) and lut(x+1) to smooth the result which is
 * usefull with low resolution LUTs.
 * 
 * IMPORTANT = The LUT needs to allocate N+1 elements.
 * 
 * bits = number of bits for the fractional port of 'x' used to lerp
 * 
 * see: https://www.coranac.com/tonc/text/fixed.htm
 */
extern s32 lerp_lut_s32(const s32 lut[], int x, const int bits);

/* -------------------------------------------------------------------------
 * Console output
 * ------------------------------------------------------------------------- */

extern void console(const char * fmt, ...);
extern void console_debug(const char * fmt, ...);
extern void console_error(const char * fmt, ...);
extern void enable_console_debug();

/* -------------------------------------------------------------------------
 * Memory
 * ------------------------------------------------------------------------- */

extern void * mem_alloc_align(int size, int align);
extern void * mem_alloc_cache_align(int size);
extern void mem_free(void * area);

/* -------------------------------------------------------------------------
 * Concrete implementations
 * ------------------------------------------------------------------------- */

# include <x/core/x.c> /* yes, don't be afraid */

#endif /* X_H */
