/**
 * samples/particles/main.c
 * Copyleft (c) Pierre-Jean Turpeau 02/2019
 * <pierrejean AT turpeau DOT net>
 */
#define X_IMPLEMENTATION

#define TITLE "-[ particles ]-"

#include <x86/x.h>
#include <x86/image.h>
#include <x86/extras.h>
// #include <x/gfx/gradients.h>
// #include <x/gfx/procs.h>

#include "particle.h"

particles_t fx;
image_t * brush;

void ConfigureLiquidFlowParticles(particles_t * particles) {
    InitParticles(particles);

    particles->angle_min = M_PI/2 - M_PI/8;
    particles->angle_max = M_PI/2 + M_PI/8;

    particles->speed_min = 2;
    particles->speed_max = 4;

    particles->mass_min = 4;
    particles->mass_max = 12;

    /* kind of gravity */
    particles->acceleration.u = 0;
    particles->acceleration.v = 0.03;

    particles->pos_min.x = W / 2;
    particles->pos_min.y = H - 10;

    particles->pos_max = particles->pos_min;

    particles->deathrate = 1.2f;
    particles->emitter_rate = 5.f;
}

void ConfigureSteadyWindParticles(particles_t * particles) {
    InitParticles(particles);

    particles->angle_min = M_PI/2 - M_PI/8;
    particles->angle_max = M_PI/2 + M_PI/8;

    particles->speed_min = 3;
    particles->speed_max = 5;

    particles->mass_min = 4;
    particles->mass_max = 12;

    /* steady lateral wind */
    particles->acceleration.u = .2f;
    particles->acceleration.v = 0;

    particles->pos_min.x = W / 4;
    particles->pos_min.y = H - 10;

    particles->pos_max = particles->pos_min;

    particles->deathrate = 3.0f;
    particles->emitter_rate = 10.0f;
}

void AnimateParticles(float elapsed_time) {
    static int conf = 0;

    int event = GetLastEvent();
    if( event == ' ') {
        conf = (conf + 1) % 2;
        if(conf)
            ConfigureSteadyWindParticles(&fx);
        else
            ConfigureLiquidFlowParticles(&fx);
    }
    else if (event == KEY_ENTER) {
        particle_rendering_mode = (particle_rendering_mode + 1) % 3;
    }

    static float last_time = 0;
    elapsed_time *= 0.05f;
    
    ClearScreen(0);
    UpdateParticles(&fx, elapsed_time-last_time);
    DrawParticles(&fx);
    last_time = elapsed_time;
}

int main(int argc, char **argv) {
    InitPlatform(argc, (const char **) argv);
    ParseDefaultExeOptions();

    Console("Key actions:");
    Console("\t[SPACE] \t change particles configuration");
    Console("\t[ENTER] \t change drawing configuration");
    Pause(2000);

    SetVGAMode();

    v3f ambiant_color = {{0.05, 0.05, 0.05}};
	v3f diffuse_color = {{0.8, 0.2, 0.2}};
	v3f specular_color = {{0.9, 0.9, 0.9}};
    LightingGradientPalette(0, 255, ambiant_color, diffuse_color, specular_color, 20);

    ConfigureLiquidFlowParticles(&fx);

#define BSIZE 24

    brush = CreateImage(BSIZE, BSIZE);
    GenerateQuadraticSprite(brush->pixels, brush->w, brush->h, 32, 0, 0);

    while(!IsPlatformExit()) {
        BeginFrameLoop();
        ExecuteDefaultInteractions();

            if (!IsPaused())
                AnimateParticles(GetTime());

        EndFrameLoop();
    };

    DeleteImage(brush);
    ClosePlatform();

    LogDebug("emitted particles: %d", fx.particles_count);

    Console("fps = %d\n", x86.Time.fps);
    return EXIT_SUCCESS;        

}
