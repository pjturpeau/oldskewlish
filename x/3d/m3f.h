/**
 * x/3d/m3f.h
 * 
 * Copyleft (c) Pierre-Jean Turpeau 12/2019
 * <pierrejean AT turpeau DOT net>
 */
#ifndef X_MATH_M3F_H
#define X_MATH_M3F_H

#include <x/math/v3f.h>

#include <math.h>

typedef union {
    float data[3][3]; /* rows, cols */

    struct {
        float m00, m01, m02; /* row 0 */
        float m10, m11, m12; /* row 1 */
        float m20, m21, m22; /* row 2 */
    };

    struct {
        v3f row0;
        v3f row1;
        v3f row2;
    };
} m3f;

extern void m3f_identity(m3f * m);
extern void m3f_mul_vec(v3f * result, const m3f * m, const v3f * v);
extern void m3f_rotation(m3f * m, float rad_x, float rad_y, float rad_z);

/* ---[ IMPLEMENTATION ]---------------------------------------------------- */

# ifdef X_IMPLEMENTATION

void m3f_identity(m3f * m) {
    m->m00 = 1; m->m01 = 0; m->m02 = 0;
    m->m10 = 0; m->m11 = 1; m->m12 = 0;
    m->m20 = 0; m->m21 = 0; m->m22 = 1;
}

void m3f_mul_vec(v3f * result, const m3f * m, const v3f * v) {
    result->x = m->m00 * v->x + m->m01 * v->y + m->m02 * v->z;
    result->y = m->m10 * v->x + m->m11 * v->y + m->m12 * v->z;
    result->z = m->m20 * v->x + m->m21 * v->y + m->m22 * v->z;
}

/**
 *          1        0        0
 * R(x) =   0      cosf(x)   -sinf(x)
 *          0      sinf(x)    cosf(x)
 *
 *
 *         cosf(y)    0      sinf(y)
 * R(y) =   0        1        0
 *        -sinf(y)    0      cosf(y)
 *
 *
 *        cosf(z)   -sinf(z)    0
 * R(z) = sinf(z)    cosf(z)    0
 *          0        0        1
 *
 *                       
 * ROT = R(y) * R(x) * R(z)  -  y = yaw, x = pitch, z = roll
 * 
 *        cosf(y)*cosf(z)+sinf(x)*sinf(y)*sinf(z)  -cosf(y)*sinf(z)+sinf(x)*sinf(y)*cosf(z)   cosf(x)*sinf(y)
 * ROT =  cosf(x)*sinf(z)                        cosf(x)*cosf(z)                       -sinf(x)
 *       -sinf(y)*cosf(z)+cosf(y)*sinf(x)*sinf(z)   sinf(y)*sinf(z)+cosf(y)*sinf(x)*cosf(z)   cosf(y)*cosf(x)
 */
void m3f_rotation(m3f * m, float rad_x, float rad_y, float rad_z)
{
    float csx = cosf(rad_x);
    float snx = sinf(rad_x);

    float csy = cosf(rad_y);
    float sny = sinf(rad_y);

    float csz = cosf(rad_z);
    float snz = sinf(rad_z);

    float snx_sny = snx * sny;
    float csy_snx = csy * snx;

    m->m00 = csy * csz + snx_sny * snz;   m->m01 = -csy*snz + snx_sny * csz;     m->m02 = csx * sny;
    m->m10 = csx * snz;                   m->m11 = csx * csz;                    m->m12 = -snx;
    m->m20 = -sny*csz + csy_snx * snz;    m->m21 = sny * snz + csy_snx * csz;    m->m22 =  csy * csx;
}

# endif /* X_IMPLEMENTATION */

#endif /* X_MATH_M3F_H */
