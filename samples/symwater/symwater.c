/**
 * samples/symbolic-water/symbolic-water.c
 * 
 * Copyleft (c) Pierre-Jean Turpeau 11/2020
 * <pierrejean AT turpeau DOT net>
 *
 * Inspired by https://twitter.com/K4rakaraC/status/1189627552366809089
 */
#define X_IMPLEMENTATION

#define TITLE "-[ symbolic water ]-"

#include <math.h>
#include <x86/x.h>

#define WAVE_COLOR 66
#define OCEAN_SPRAY_COLOR 65

#define PRIMARY_AMPLITUDE 16
#define SECONDARY_AMPLITUDE 16
#define TOTAL_AMPLITUDE (PRIMARY_AMPLITUDE+SECONDARY_AMPLITUDE)
#define VPOSITION H/2

u8 sky_gradient[H];
u8 water_gradient[H];
typedef struct {
    int x, y;
    int inc;
    int size, max_size;
} sparkle;

#define SPARKLES_COUNT 150
sparkle sparkles[SPARKLES_COUNT];

int prev_vpos[W];

static inline void DrawHorizontalLine(int x, int y, int len, u8 color) {
    assert(x >= 0 && (x + len <= W) && y >= 0 && y < H);
    memset(GetBackbuffer() + x + y * W, color, len);
}

void DrawHorizontalLine_c(int x, int y, int len, u8 color) {
    if(x < 0) { x = 0; len -= x; } else if(x >= W) return;
    int xr = x + len;
    if(xr >= W) { len = W - x; }
    if(y < 0 || y >= H) return;
    DrawHorizontalLine(x, y, len, color);
}

/* draw an horizontal sparkle and clip it according to screen & wave position */
void draw_sparkle_clipped(int x, int y, int len, u8 color) {
    if(x < 0) { x = 0; len -= x; } else if(x >= W) return;
    int xr = x + len;
    if(xr >= W) { len = W - x; }
    if(y < 0 || y >= H) return;

    u8 * b = GetBackbuffer() + x + y * W;
    for(int i = x; i < x+len; i++, b++) {
        if(prev_vpos[i] < y-5)
            *b = color;
    }
}

void UpdateWater(float elapsed_time) {
    for(int x = 0; x < W; x++) {
        /* compute the swell */
        int swell = VPOSITION
            + cos(sin(x/32.f)+elapsed_time/512.f) * PRIMARY_AMPLITUDE
            + sin(x/64.f + 8.f * cos(elapsed_time / 2048.f))
                * SECONDARY_AMPLITUDE;

        /* fill the pixel gap when the swell grow-up fast */
        int dy = prev_vpos[x] - swell;
        if(dy > 0) {
            for(int i = swell; i < swell+dy+1; i++)
                SetPixel(x, i, OCEAN_SPRAY_COLOR);
        }
        prev_vpos[x] = swell;

        /* random erasing of the ocean spray */
        for(int i = swell; i < swell + 5; i++) {
            if(GetFloatRandomValue(0, 1) > 0.9f)
                SetPixel(x, i, water_gradient[i]);
        }

        /* erase a little bit more to give a progressive feeling */
        for(int i = swell + 5; i < swell + 15; i++) {
            if(GetFloatRandomValue(0, 1) > 0.6f) {
                SetPixel(x, i, water_gradient[i]);
            }
        }

        /* draw the portion of the sky needed when the wave goes down */
        for(int y = VPOSITION - TOTAL_AMPLITUDE - 5; y < swell; y++)
            SetPixel(x, y, sky_gradient[y]);

        /* draw the portion of the water needed when the wave goes up
         * useful with high swell since previous erasing is random based */
        for(int y = swell + 15; y < VPOSITION+TOTAL_AMPLITUDE+1; y++)
            SetPixel(x, y, water_gradient[y]);

        SetPixel(x, swell, WAVE_COLOR);
        SetPixel(x, swell+1, OCEAN_SPRAY_COLOR);
    }

    /* redrow the lower part of the water */
    for(int y = VPOSITION+TOTAL_AMPLITUDE+1; y < H; y++)
        DrawHorizontalLine(0, y, W, water_gradient[y]);

    /* redraw the sparkles */
    for(int i = 0; i < SPARKLES_COUNT; i++) {
        sparkle* e = &sparkles[i];
        e->size += e->inc;
        if(e->size >= e->max_size)
            e->inc = -1;
        if(e->size == 0 && e->inc == -1) { /* update the sparkle position */
            e->x = GetIntRandomValue(0, W);
            e->y = GetIntRandomValue(VPOSITION - TOTAL_AMPLITUDE, H-5);
            e->inc = 1;
        }

        if(e->y > VPOSITION + TOTAL_AMPLITUDE)
            DrawHorizontalLine_c(e->x - e->size, e->y, 2 * e->size, water_gradient[e->y-TOTAL_AMPLITUDE]);
        else
            draw_sparkle_clipped(e->x - e->size, e->y, 2 * e->size, water_gradient[e->y-TOTAL_AMPLITUDE]);        
    }
}

int main(int argc, char **argv) {
    InitPlatform(argc, (const char **) argv);
    ParseDefaultExeOptions();

    SetVGAMode();

    LinearGradientPalette( 0, 0xEF7D57, 31, 0xFFCD75);
    LinearGradientPalette(32, 0x3b5dc9, 64, 0x29366f);
    SetColorPalette(65, 0xFFFFFF);
    SetColorPalette(66, 0x000000);
    // linear_gradient(128, 0x5b7de9, 192, 0x3b5dc9);
    LinearGradientPalette(128, 0x9bbdff, 160, 0x29366f);

    /* precalc vertical gradients */
    for (int i = 0; i < H; i++) {
        if (i < VPOSITION + TOTAL_AMPLITUDE) {
            f32 ratio = (f32)i/(f32)(VPOSITION+TOTAL_AMPLITUDE);
            sky_gradient[i] = IntLerp(0, 31, ratio);
        }
        
        if (i > VPOSITION - TOTAL_AMPLITUDE) {
            f32 ratio = (f32)(i - VPOSITION + TOTAL_AMPLITUDE)
                / (f32)(H - VPOSITION + TOTAL_AMPLITUDE);
            water_gradient[i] = IntLerp(32, 64, ratio);
        } else 
            water_gradient[i] = 32;
    }

    for(int y = 0; y < VPOSITION; y++)
        DrawHorizontalLine(0, y, W, sky_gradient[y]);

    for(int y = VPOSITION; y < H; y++)
        DrawHorizontalLine(0, y, W, water_gradient[y]);

    /* initialize sparkles */
    for(int i = 0; i < SPARKLES_COUNT; i++) {
        sparkles[i].x = GetIntRandomValue(0, W);
        sparkles[i].y = GetIntRandomValue(VPOSITION - TOTAL_AMPLITUDE, H-5);
        sparkles[i].max_size = GetIntRandomValue(5, 25);
        sparkles[i].inc = 1;
        sparkles[i].size = GetIntRandomValue(0, sparkles[i].max_size);
    }

    for(int i = 0; i < W; i++)
        prev_vpos[i] = 0;

    while(!IsPlatformExit()) {
        BeginFrameLoop();
        ExecuteDefaultInteractions();

        if (!IsPaused())
            UpdateWater(GetTime());

        EndFrameLoop();
    };

    ClosePlatform();

    Console("fps = %d", x86.Time.fps);
    return EXIT_SUCCESS;
}
