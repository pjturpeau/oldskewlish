#ifndef CONFIG_H
#define CONFIG_H

#define WIDTH  320
#define HEIGHT 240

#include <x/gfx/bitmap.h>

extern x_bitmap g_texture;

#endif
