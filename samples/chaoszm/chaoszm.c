/**
 * samples/chaoszm/chaoszm.c
 * 
 * Copyleft (c) Pierre-Jean Turpeau 04/2020
 * <pierrejean AT turpeau DOT net>
 */
#define X_IMPLEMENTATION 1

#define TITLE "-[ chaos zoom ]-"

#include <x86/x.h>
#include <x86/image.h>

# define NZ_SIZE 4
image_t * noize;

void SwapPalette()
{
    static bool alternate = false;
    if((alternate = !alternate)) {
        v3f params2[4] = {
            {{0.5, 0.5, 0.5}}, {{0.5, 0.5, 0.5}},
            {{1.0, 1.0, 0.5}}, {{0.0, 0.9, 0.3}}
        };    
        TrigonometricGradientPalette(0, 255, params2);    
    } else {
        v3f params3[4] = {
            {{0.5, 0.5, 0.5}}, {{0.5, 0.5, 0.5}},
            {{1.0, 1.0, 1.0}}, {{0.0, 0.1, 0.2}}
        };    
        TrigonometricGradientPalette(0, 255, params3);    
    }
    SetColorPalette(0, 0);
}

static void ChaosZoom(float elapsed_time) {
    elapsed_time *= 0.001;

    /* control the chaos using color segments and avoid background color */ 
    static int color_segment = 0;
    color_segment = (color_segment + 1) % 64;

    const int color1 = 1 + color_segment * 4;
    
    int color2 = color_segment * 4 + 4;
    color2 = color2 > 255 ? 255 : color2; /* clamp */

    /* insert random data in the center of the backbuffer() */
    SeedRandomUInt8Buffer(noize->pixels, NZ_SIZE*NZ_SIZE, color1, color2);
    DrawImage_c(noize, (W - noize->w)/2, (H - noize->h)/2 );

    /* play with non linear rotation angle and scaling factor */
    const float scale = .98f + .02f * sin(elapsed_time * .75f) + .01f * cos(elapsed_time * 1.5f); 
    const float rotation = M_PI + 0.1f * sin(elapsed_time * 0.6f) * cos(elapsed_time * 0.9f);
    const float scaled_cos = scale * cos(rotation);
    const float scaled_sin = scale * sin(rotation);
    
    const int center_x = W >> 1;
    const int center_y = H >> 1;

    for(int j = 0; j < H; j++) {
        
        const int py = j - center_y; /* [-h/2; +h/2] range */
        u8 * video = GetBackbuffer() + j * W;

        for(int i = 0; i < W; i++) {
            const int px = i - center_x; /* [-w/2; +w/2] range */

            /* centered 2d rotation */
            int x = center_x + px * scaled_cos - py * scaled_sin;
            int y = center_y + px * scaled_sin + py * scaled_cos;

            /* clamping is better than putting zero when outside the buffer */
            if(x < 0) x = 0; else if (x >= W) x=W-1;
            if(y < 0) y = 0; else if (y >= H) y=H-1;

            *(video++) = GetPixel(x,y);
        }
    }
}


int main(int argc, char ** argv) {
    InitPlatform(argc, (const char **) argv);
    ParseDefaultExeOptions();
    Console("press [SPACE] to switch the palette");
    Pause(2000);
    SetVGAMode();

    noize = CreateImage(NZ_SIZE, NZ_SIZE);
    SwapPalette();

    while(!IsPlatformExit()) {
        BeginFrameLoop();
        ExecuteDefaultInteractions();

        if( GetLastEvent() == ' ' )
            SwapPalette();

        if (! IsPaused())
            ChaosZoom(GetTime());

        EndFrameLoop();
    };

    ClosePlatform();
    Console("fps = %d", x86.Time.fps);
    return 0;
}
