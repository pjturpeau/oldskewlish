/**
 * samples/starfld/starfld.c
 * 
 * Copyleft (c) Pierre-Jean Turpeau 12/2019
 * <pierrejean AT turpeau DOT net>
s */
#define X_IMPLEMENTATION

#define TITLE "-[ starfield ]-"

#include <x86/x.h>
#include <x86/math.h>
#include <x86/draw.h>

/* ---[ DEFINITIONS ]------------------------------------------------------- */

/**
 * field of view = 90° => tan(90/2)=tan(45)=1
 * focal_length_x = (screen_width/2) / tan(field_of_view/2)
 * focal_length_y = (screen_height/2) / tan(field_of_view/2)
 * focal_length = (focal_length_x + focal_length_y) / 2
 */
static const float focal_length = (W/2 + H/2)/2; /* with fov = 90° */
#define FAR_LENGTH (focal_length * 4)

typedef struct {
    v2s pos_2d;
    v3f pos_3d;
    float z_speed;
    float proj; /* for debug */

    v2s pos_2d_prev;
    float z_prev;
} star_t;

#define STARS_COUNT 500
star_t stars[STARS_COUNT];

#define COLOR_SCALE (((float) MAX_COLORS) / (FAR_LENGTH))

/* ---[ IMPLEMENTATION ]---------------------------------------------------- */

static int draw_type = 0;

void InitStar(star_t * star) {
    star->pos_3d.x = GetFloatRandomValue(-W/2, W/2);
    star->pos_3d.y = GetFloatRandomValue(-H/2, H/2);
    star->pos_3d.z = GetFloatRandomValue(FAR_LENGTH, FAR_LENGTH * 2);// + rand() % ((int) FAR_LENGTH);
    star->z_speed = 2;

    star->proj = focal_length / star->pos_3d.z;

    star->pos_2d.x = star->pos_3d.x * star->proj;
    star->pos_2d.y = star->pos_3d.y * star->proj;
}

void MoveStar(star_t * star) {
    star->pos_3d.z -= star->z_speed;
    if( star->pos_3d.z <= 0) { /* -focal_length ) */
        star->pos_3d.z += FAR_LENGTH;

        star->proj = focal_length / star->pos_3d.z;

        star->pos_2d.x = star->pos_3d.x * star->proj;
        star->pos_2d.y = star->pos_3d.y * star->proj;
    }

    star->proj = focal_length / star->pos_3d.z;

    star->pos_2d_prev.x = star->pos_2d.x;
    star->pos_2d_prev.y = star->pos_2d.y;

    star->pos_2d.x = star->pos_3d.x * star->proj;
    star->pos_2d.y = star->pos_3d.y * star->proj;
}

void InitStars() {
    for(int i = 0; i < STARS_COUNT; i++)
        InitStar(&stars[i]);

    stars[0].pos_3d.x = 1;
    stars[0].pos_3d.y = 1;
}

void MoveStars() {
    for(int i = 0; i < STARS_COUNT; i++)
        MoveStar(&stars[i]);
}

void SpeedUpStars(float acceleration) {
    for(int i = 0; i < STARS_COUNT; i++)
        stars[i].z_speed += acceleration;
}

static inline void DrawLineNotClipped(int x1, int y1, int x2, int y2, u8 c) {
    if( x1 >= 0 && x1 < W && x2 >= 0 && x2 < W && y1 >= 0 && y1 < H && y2 >= 0 && y2 < H)
        DrawLine(x1, y1, x2, y2, c);
}

void DrawStars() {
    for(int i = 0; i < STARS_COUNT; i++)
        if(stars[i].pos_3d.z <= FAR_LENGTH) 
            DrawLineNotClipped(
                stars[i].pos_2d.x + W/2,
                stars[i].pos_2d.y + H/2,
                stars[i].pos_2d_prev.x + W/2,
                stars[i].pos_2d_prev.y + H/2,
                MAX_COLORS - COLOR_SCALE * stars[i].pos_3d.z);
}

void AttenuateBackbuffer() {
# define ATTENUATION_FACTOR (7 * (1<<16) / 10) /* 0.7 in 16.16 fixed point */
    u8 * video = GetBackbuffer();
    for(int i = W * H; i > 0; i--, video++)
        *video = (*video * ATTENUATION_FACTOR) >> 16;
}

void BlurBackbuffer() {
    DrawHorizontalLine(0, 0, W, 0);
    DrawHorizontalLine(0, H-1, W, 0);
    u8 * video = GetBackbuffer() + W;
    for(int i = W*H - W * 2; i > 0; i--, video++) {
        int color = *(video-1) + *(video - W) + *(video + W) + *(video + 1);
        color = (color >> 2) - 1; /* decrease a little bit */
        *(video) = color < 0 ? 0  : color;
    }
}


int main(int argc, char **argv) {
    InitPlatform(argc, (const char **) argv);
    ParseDefaultExeOptions();

    Console("press [SPACE] to test image clear options\n");
    Pause(2000);

    SetVGAMode();
    LinearGradientPalette(0, 0, 255, 0xFFFFFF);
    
    InitStars();
    u32 start = GetTime();
    u32 prev = start;

    while(!IsPlatformExit()) {
        BeginFrameLoop();
        ExecuteDefaultInteractions();

            if(GetLastEvent() == ' ')
                draw_type++;

            if (!IsPaused()) {
                u32 elapsed = GetTime() - start;
                u32 dt = GetTime() - prev;

                switch(draw_type) {
                    case 0: AttenuateBackbuffer(); break;
                    case 1: BlurBackbuffer(); break;
                    case 2: ClearScreen(0); break;
                    default: draw_type = 0;
                }

                MoveStars();
                DrawStars();

                if (elapsed < 5000)
                    SpeedUpStars(dt * 0.0005f);
            } else {
                start = GetTime();
            }
            prev = GetTime();

        EndFrameLoop();
    };

    ClosePlatform();

    Console("fps = %d", x86.Time.fps);
    return EXIT_SUCCESS;    
}
