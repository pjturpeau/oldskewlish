/**
 * bball/main.c
 * 
 * Copyleft (c) Pierre-Jean Turpeau 01/2020
 * <pierrejean AT turpeau DOT net>
 */
#define X_IMPLEMENTATION 1

#define TITLE "-[ bouncing ball ]-"

#include <stdlib.h>
#include <time.h>

#include <x86/x.h>
#include <x86/draw.h>

#include "bball.h"

bouncing_ball ball;

void AnimateBouncingBall(float elapsed_time) {
    // very simple flat background
    DrawRectangle(0, 0, W, H>>1, 0);
    DrawRectangle(0, H>>1, W, H, 1);

    UpdateBouncingBall(&ball, elapsed_time * 0.05);

    // the outlined ball
    DrawEllipse(ball.pos.x+10, ball.pos.y-3, ball.radius.x, ball.radius.y, 4);
    DrawEllipse(ball.pos.x, ball.pos.y, ball.radius.x, ball.radius.y, 2);
    DrawEllipseOutline(ball.pos.x, ball.pos.y, ball.radius.x, ball.radius.y, 3);
}


int main(int argc, char **argv) {
    InitPlatform(argc, (const char **) argv);
    ParseDefaultExeOptions();
    SetVGAMode();

    LinearGradientPalette(0, 0, 255, 0xFFFFFF);
    SetColorPalette(0, 0x200040);

    SetColorPalette(0, 0x206D7B);
    SetColorPalette(1, 0x015A6B);
    SetColorPalette(2, 0xFF7C00);
    SetColorPalette(3, 0xFFB773);
    SetColorPalette(4, 0x303030);

    InitBouncingBall(&ball);

    while(!IsPlatformExit()) {
        BeginFrameLoop();
        ExecuteDefaultInteractions();        

            if (!IsPaused())
                AnimateBouncingBall(GetTime());

        EndFrameLoop();
    };

    ClosePlatform();

    Console("fps = %d\n", x86.Time.fps);
    return EXIT_SUCCESS;        
}

