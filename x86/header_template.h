/**
 * x86/header_template.h
 * 
 * Copyleft (c) Pierre-Jean Turpeau 12/2023
 * <pierrejean AT turpeau DOT net>
 * 
 * This is for documentation purpose only.
 * Template header file to use for any new feature.
 *
 */
#ifndef X86_UTIL_HEADER_TEMPLATE_H
#define X86_UTIL_HEADER_TEMPLATE_H

# include <x86/x.h>
/* here comes additional includes */

/* ---[ DEFINITIONS ]------------------------------------------------------- */

/* here comes #define, typedef, enums, etc. */

/* ---[ DECLARATIONS ]------------------------------------------------------ */

/* here comes public functions declarations */

/* ---[ IMPLEMENTATION ]---------------------------------------------------- */

# ifdef X_IMPLEMENTATION

/* here comes public and private functions implementation */

# endif /* X_IMPLEMENTATION */

#endif /* X86_UTIL_HEADER_TEMPLATE_H */
