#define X_IMPLEMENTATION 1

#include "test.h"

#include <x86/image.h>

#define NRUNS 5
#define NLOOPS 100000 /* big number to ensure ramp-up of CPU throttle */

image_t naive_sprite;
image_t masked_sprite;

#define SPRITE_SIZE 32

/* generate a square image with half transparent pixels */
void generate_image(image_t * image, int w) {
    image->w = w;
    image->h = w;
    image->pixels = (u8 *) MemAlloc(w * w);
    u8 * p = image->pixels;
    for(int j = 0; j < w; j++)
        for(int i = 0; i < w; i++) {
            *(p++) = i<=j ? 2 : 0;
        }
}

/* test every pixel against color index 0 */
void draw_sprite_naive(int x, int y) {
    image_t * sprite = &naive_sprite;

    if(x < -sprite->w || x > W || y < -sprite->h || y > H)
        return;

    const int clip_left = (x < 0) ? -x : 0;
    const int clip_top = (y < 0) ? -y : 0;
    const int clip_right = M_MIN(W - x, sprite->w);
    const int clip_bottom = M_MIN(H - y, sprite->h);

    for(int j = clip_top; j < clip_bottom; j++) {
        u8 * src = sprite->pixels + clip_left + j * sprite->w;
        u8 * dst = GetBackbuffer() + x + clip_left + (y+j) * W;
        for(int i = clip_left; i < clip_right; i++) {
            const int c = *(src++);
            if(c) *(dst) = c;
            dst++;
        }
    }
}

void generate_sprite(image_t * image) {
    image->masks = (u8 *) MemAlloc(image->w * image->h);
    u8 * msk = image->masks;
    u8 * src = image->pixels;
    for(int j = 0; j < image->h; j++) {
        for(int i = 0; i < image->w; i++) {
            *(msk++) = *(src++) ? 0x00 : 0xFF;
        }
    }
}

/* use bit mask operations instead of testing each pixel */
void draw_sprite_u8(int x, int y) {
    image_t * sprite = &masked_sprite;

    if(x < -sprite->w || x > W || y < -sprite->h || y > H)
        return;

    const int clip_left = (x < 0) ? -x : 0;
    const int clip_top = (y < 0) ? -y : 0;
    const int clip_right = M_MIN(W - x, sprite->w);
    const int clip_bottom = M_MIN(H - y, sprite->h);

    u32  src_ofs = clip_left +  clip_top * sprite->w;
    // u8 * src_ref = sprite->pixels + clip_left +  clip_top * sprite->w;
    // u8 * msk_ref = sprite->masks + clip_left + clip_top * sprite->w;
    u8 * dst_ref = GetBackbuffer() + x + clip_left + (y + clip_top) * W;

    for(int j = clip_top; j < clip_bottom; j++) {
        u8 * src = sprite->pixels + src_ofs;
        u8 * msk = sprite->masks  + src_ofs;
        u8 * dst = dst_ref;

        for(int i = clip_left; i < clip_right; i++)
            *(dst++) = *(src++) | ((*dst) & *(msk++));

        dst_ref += W;
        src_ofs += sprite->w;
    }
}

/* perform bit mask operations with a group of 4 pixels */
void draw_sprite_u32(int x, int y) {
    image_t * sprite = &masked_sprite;

    if(x < -sprite->w || x > W || y < -sprite->h || y > H)
        return;

    const int clip_left = (x < 0) ? -x : 0;
    const int clip_top = (y < 0) ? -y : 0;
    const int clip_right = M_MIN(W - x, sprite->w);
    const int clip_bottom = M_MIN(H - y, sprite->h);

    const int width = clip_right - clip_left;
    const int words_count = width >> 2;

    /* good chance that if it is clipped on the left, it will not be clipped on the right ... */
    const int remaining_bytes = clip_left&3 ? 0 : width - (width & 0xFFFFFC);

    u32 src_ofs = clip_left + clip_top * sprite->w;

    for(int j = clip_top; j < clip_bottom; j++, src_ofs += sprite->w)
    {
        u8 * src = sprite->pixels + src_ofs;
        u8 * msk = sprite->masks  + src_ofs;
        u8 * dst = GetBackbuffer() + x + clip_left + (y+j) * W;

        switch(clip_left&3) {
            case 1: *dst = *(src++) | (*dst & *(msk++)); dst++;
            case 2: *dst = *(src++) | (*dst & *(msk++)); dst++;
            case 3: *dst = *(src++) | (*dst & *(msk++)); dst++;
        }

        u32 * src32 = (u32 *) src;
        u32 * msk32 = (u32 *) msk;
        u32 * dst32 = (u32 *) dst;

        for(int i = words_count; i > 0; i--, dst32++)
            *dst32 = *(src32++) | (*dst32 & *(msk32++));

        src = (u8 *) src32;
        msk = (u8 *) msk32;
        dst = (u8 *) dst32;

        switch(remaining_bytes) {
            case 3: *dst = *(src++) | (*dst & *(msk++)); dst++;
            case 2: *dst = *(src++) | (*dst & *(msk++)); dst++;
            case 1: *dst = *(src++) | (*dst & *(msk++)); dst++;
        }        
    }
}

/* perform bit mask operation with a group of 8 pixels */
void draw_sprite_u64(int x, int y) {
    image_t * sprite = &masked_sprite;

    if(x < -sprite->w || x > W || y < -sprite->h || y > H)
        return;

    const int clip_left = (x < 0) ? -x : 0;
    const int clip_top = (y < 0) ? -y : 0;
    const int clip_right = M_MIN(W - x, sprite->w);
    const int clip_bottom = M_MIN(H - y, sprite->h);

    const int width = clip_right - clip_left;
    const int words_count = width >> 3;

    /* good chance that if it is clipped on the left, it will not be clipped on the right ... */
    const int remaining_bytes = clip_left&7 ? 0 : width - (width & 0xFFFFF8);

    u32 src_ofs = clip_left + clip_top * sprite->w;

    for(int j = clip_top; j < clip_bottom; j++, src_ofs += sprite->w)
    {
        u8 * src = sprite->pixels + src_ofs;
        u8 * msk = sprite->masks  + src_ofs;
        u8 * dst = GetBackbuffer() + x + clip_left + (y+j) * W;

        switch(clip_left&7) {
            case 1: *(dst++) = *(src++) | (*dst & *(msk++));
            case 2: *(dst++) = *(src++) | (*dst & *(msk++));
            case 3: *(dst++) = *(src++) | (*dst & *(msk++));
            case 4: *(dst++) = *(src++) | (*dst & *(msk++));
            case 5: *(dst++) = *(src++) | (*dst & *(msk++));
            case 6: *(dst++) = *(src++) | (*dst & *(msk++));
            case 7: *(dst++) = *(src++) | (*dst & *(msk++));
        }

        u64 * src64 = (u64 *) src;
        u64 * msk64 = (u64 *) msk;
        u64 * dst64 = (u64 *) dst;

        for(int i = words_count; i > 0; i--)
            *(dst64++) = *(src64++) | (*dst64 & *(msk64++));

        src = (u8 *) src64;
        msk = (u8 *) msk64;
        dst = (u8 *) dst64;

        switch(remaining_bytes) {
            case 7: *(dst++) = *(src++) | (*dst & *(msk++));
            case 6: *(dst++) = *(src++) | (*dst & *(msk++));
            case 5: *(dst++) = *(src++) | (*dst & *(msk++));
            case 4: *(dst++) = *(src++) | (*dst & *(msk++));
            case 3: *(dst++) = *(src++) | (*dst & *(msk++));
            case 2: *(dst++) = *(src++) | (*dst & *(msk++));
            case 1: *(dst++) = *(src++) | (*dst & *(msk++));
        }        
    }
}

typedef void (*draw_sprite_func)(int, int);

v2s rand_lut[65536];

void test_sprite(char * name, draw_sprite_func f) {
    int ticks[NRUNS];

    /* run XX times to ensure CPU ramp-up */
    for(int k = 0; k < NRUNS; k++) {
        int start = GetTime();
        for(int i = 0; i < NLOOPS; i++) {
            v2s p = rand_lut[i&65535];
            f(p.x, p.y);
        }
        int end = GetTime();
        ticks[k] = end-start;
    }

    /* keep the best value */
    int best = ticks[0];
    for(int k = 1; k < NRUNS; k++)
        if(ticks[k] < best) best = ticks[k];
    
    LogDebug("%s : %u ticks/%u calls - %f ticks/call\n", name, best, NLOOPS, (float)best/(float)NLOOPS);
}


void UpdateDraw() {
    ClearScreen(1);

    DrawImage_c(&naive_sprite, 0, 0);

    int x = SPRITE_SIZE + 5;
    draw_sprite_naive(x, 0);

    x += SPRITE_SIZE + 5;
    draw_sprite_u8(x, 0);

    x += SPRITE_SIZE + 5;
    draw_sprite_u32(x, 0);

    x += SPRITE_SIZE + 5;
    draw_sprite_u64(x, 0);

    draw_sprite_u32(-1, SPRITE_SIZE+5);
    draw_sprite_u32(-2, (SPRITE_SIZE+5)*2);
    draw_sprite_u32(-3, (SPRITE_SIZE+5)*3);
    draw_sprite_u32(-4, (SPRITE_SIZE+5)*4);
    draw_sprite_u32(-8, (SPRITE_SIZE+5)*5);


    draw_sprite_u32(W-SPRITE_SIZE+1, SPRITE_SIZE+5);
    draw_sprite_u32(W-SPRITE_SIZE+2, (SPRITE_SIZE+5)*2);
    draw_sprite_u32(W-SPRITE_SIZE+3, (SPRITE_SIZE+5)*3);
    draw_sprite_u32(W-SPRITE_SIZE+4, (SPRITE_SIZE+5)*4);
    draw_sprite_u32(W-SPRITE_SIZE+8, (SPRITE_SIZE+5)*5);
}



int main(int argc, char **argv) {
    InitPlatform(argc, (const char **) argv);
    ParseDefaultExeOptions();
    LogDebug("coucou\n");
    Console("hey!!!!\n");
    SetVGAMode();

    uint32_t start, end;

    /* generate images */
    generate_image(&naive_sprite, SPRITE_SIZE);
    generate_image(&masked_sprite, SPRITE_SIZE);

    /* generate mask data */
    generate_sprite(&masked_sprite);


    int inside = 0;
    int outside = 0;

    for(int i = 0; i < 65536; i++) {
        rand_lut[i].x = GetIntRandomValue(-SPRITE_SIZE, W); //- SPRITE_SIZE/2);
        rand_lut[i].y = GetIntRandomValue(-SPRITE_SIZE, H); //- SPRITE_SIZE/2);
        if( rand_lut[i].x >= 0 && rand_lut[i].x < (W-SPRITE_SIZE)
            && rand_lut[i].y >= 0 && rand_lut[i].y < (H-SPRITE_SIZE))
            inside++; else outside++;
    }
    LogDebug("positions : %.1f inside, %.1f outside\n",
        100.f * inside / 65536.f, 100.f * outside / 65536.f);

    test_sprite("naive", draw_sprite_naive);
    test_sprite("mask u8", draw_sprite_u8);
    test_sprite("mask u32", draw_sprite_u32);
    test_sprite("mask u64", draw_sprite_u64);

    LogDebug("\n"); inside = outside = 0;

    for(int i = 0; i < 65536; i++) {
        rand_lut[i].x = GetIntRandomValue(0, W - SPRITE_SIZE - 5);
        rand_lut[i].y = GetIntRandomValue(0, H - SPRITE_SIZE - 5);
        if( rand_lut[i].x >= 0 && rand_lut[i].x < (W-SPRITE_SIZE)
            && rand_lut[i].y >= 0 && rand_lut[i].y < (H-SPRITE_SIZE) )
            inside++; else outside++;
    }
    LogDebug("positions : %.1f inside, %.1f outside\n",
        100.f * inside / 65536.f, 100.f * outside / 65536.f);

    test_sprite("naive", draw_sprite_naive);
    test_sprite("mask u8", draw_sprite_u8);
    test_sprite("mask u32", draw_sprite_u32);
    test_sprite("mask u64", draw_sprite_u64);

    SetColorPalette(0, 0x333333);
    SetColorPalette(1, 0x003388);
    SetColorPalette(2, 0xFF0000);
    SetColorPalette(255, 0xFFFFFF);

    ClearScreen(1);
    draw_sprite_u32(-1, 0);
    draw_sprite_u32(W-SPRITE_SIZE+2, (SPRITE_SIZE+5)*2);
 
    while (! IsPlatformExit()) {
        BeginFrameLoop();
        ExecuteDefaultInteractions();

        UpdateDraw();

        EndFrameLoop();
    };

    MemFree(naive_sprite.pixels);
    MemFree(masked_sprite.pixels);
    MemFree(masked_sprite.masks);

    ClosePlatform();

    Console("fps = %d", x86.Time.fps);
    return EXIT_SUCCESS;
}
