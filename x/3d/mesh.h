/**
 * x/3d/m4f.h
 * 
 * Copyleft (c) Pierre-Jean Turpeau 12/2019
 * <pierrejean AT turpeau DOT net>
 * 
 */
#include <x/math/v3f.h>
#include <x/3d/m4f.h>

typedef struct {
    v3f position;
    v3f direction;
} camera_t;

#define MESH_NAME_LEN 16
typedef struct {
    char name[MESH_NAME_LEN];

    v3f * vertices;
    int num_vertices;

    v3f position;
    v3f rotation;
} mest_t;

void init_mesh(mest_t * mesh, const char * name, int number_of_vertices) {
    strncpy(mesh->name, name, MESH_NAME_LEN-1);

    mesh->vertices = (v3f *) mem_alloc_cache_align(number_of_vertices * sizeof(v3f));
    mesh->num_vertices = number_of_vertices;
}

void delete_mesh(mest_t * mesh) {
    mem_free(mesh->vertices);
}

static v3f g_cube_mesh[8] = {
    {{-1.0f,  1.0f,  1.0f}},
    {{ 1.0f,  1.0f,  1.0f}},
    {{-1.0f, -1.0f,  1.0f}},
    {{-1.0f, -1.0f, -1.0f}},
    {{-1.0f,  1.0f, -1.0f}},
    {{ 1.0f,  1.0f, -1.0f}},
    {{ 1.0f, -1.0f,  1.0f}},
    {{ 1.0f, -1.0f, -1.0f}}
};

void create_cube_mesh(mest_t * mesh) {
    init_mesh(mesh, "cube", 8);
    memcpy(mesh->vertices, g_cube_mesh, 8 * sizeof(v3f));
}

/* incomplete / not working */
void draw_meshes(mest_t ** meshes, int num_meshes, camera_t * cam)  {
    v3f up_vector = {{0, 1.0f, 0}};
    m4f view_matrix, projection_matrix;

    /* the view matrix uses a left-handed matrix (points toward something) */
    m4f_look_at_left_handed(&view_matrix, &cam->position, &cam->direction, &up_vector);

    m4f_perspective_fov_right_handed(&projection_matrix, 0.78f, 320.0f/240.0f, 0.01f, 1.0f);

    for(int i = 0; i < num_meshes; i++)  {

        m4f model_matrix;
        // m4f_rotation_yaw_pitch_roll(&model_matrix, meshes[i]->rotation.y, meshes[i]->rotation.x, meshes[i]->rotation.z);

        m4f translation_matrix;
        m4f_translation(&translation_matrix, meshes[i]->position);

        m4f world_matrix;
        m4f_mul(&world_matrix, &model_matrix, &translation_matrix);

        /* transform_matrix = world_matrix * view_matrix * projection_matrix */
        m4f transform_matrix, tmp;
        m4f_mul(&tmp, &world_matrix, &view_matrix);
        m4f_mul(&transform_matrix, &tmp, &projection_matrix);

        for(int j = 0; j < meshes[i]->num_vertices; j++) {
            // x_point p;

            // x_mesh_project(&p, &meshes[i]->vertices[j], &transform_matrix);
        }
    }
}