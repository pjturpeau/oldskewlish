# xlib API cheatsheet

Draft approach for a single file API documentation.

## TODO

Reboot the API doc as a cheat-sheet like in raylib : https://www.raylib.com/cheatsheet/cheatsheet.html
Simplifications : https://www.lexaloffle.com/bbs/?tid=35330

## Core functions

- Execution functions
    - [void start()](#start)
    - [void main_loop(update_func_t call_back)](#main_loop)
    - [const char * check_args(const char *)](#check_args)
    - [void fatal_error(const char *fmt, ...)](#fatal_error)
 
- Pixel drawing functions
    - [void set_pixel(int, int, u8)](#set_pixel)
    - u8 get_pixel(int, int)
    - void ClearScreen(u8)
    - u8 * backbuffer()
    - [u8 * frontbuffer()](#frontbuffer)
 
- Image functions
    - image_t * create_image(int w, int h)
    - void delete_image(image_t * image)
 
- Color functions
    - [void set_color(int index, u32 rgb888)](#set_color)
    - void linear_gradient(int from_index, u32 from_rgb, int to_index, u32 to_rgb);
 
- Palette functions
    - void clear_palette(u32 rgb);
    - void save_palette(palette_t * palette_out);
    - void restore_palette(const palette_t * palette);
    - void copy_palette(palette_t * palette, const palette_t * src);
    - void fade_to(const palette_t * from, const palette_t * to, float factor);
 
- Miscellaneous video functions
    - screenshot
    - save_tga
    - enable_warping / disable_warping
 
- Interactivity functions
    - events
    - [int last_event()](#last_event)
 
- Console functions
    - console
    - console_debug
    - console_error
    - enable_debug
 
- Number functions
    - random_s32
    - GetFloatRandomValue
    - seed_buffer_u8
 
- Interpolation functions
    - lerp_s32
    - lerp_f32
    - lerp_lut_s32
 
- Memory functions
    - void * mem_alloc_align(int size, int align);
    - extern void * mem_alloc_cache_align(int size);
    - void mem_free(void * area);

## Graphic plugins

### Drawing functions

``` c
# include <x/gfx/draw.h
```

### 

## Execution functions

### start

``` c
void start();
```

It's the user-defined entry point used by the ``xlib`` framework. It replaces the ususal ``main()`` function, and ensure everything is correctly initialized such as:
 - seeding random numbers
 - initialize platform specific drivers
 - parse default CLI options
 - setup the default palette
 - create video buffers

### main_loop

``` c 
typedef void (*update_func_t)(float delta_time);

void main_loop(update_func_t callback);
```

It manages the main infinite loop and perform cyclic operations, which are:
 - process of managed interaction events
 - execute the update function with the time elapsed between two frames
 - track and display the cpu load (if enabled)
 - swap front and back buffers (if enabled)
 - update the platform video buffer
 
Elapsed time between each frame helps to:
 - integrate movements and physics on a time basis (time steps)
 - keep smooth movements even with stuttering frame rates
 - stable speed whatever the execution environment
  
By default, ``main_loop`` exists on ``KEY_ESC`` and platform's exit event.

### check_args

``` c
const char * check_args(const char * opt);
```

``check_args()`` parses command line options passed to the executable through the CLI. It supports none and single parameter options.

- parameter ``opt`` : string to look for in the argument list

- return value:
    - When ``opt`` is found, it returns:
        - the pointer to the next argument in the list if it's a valid value (i.e., not starting with a dash '-')
        - an empty string (``""``) otherwise.
    - When ``opt`` is not found, it returns ``NULL``.

See example in [samples/api_doc/check_args.c](/samples/api_doc/check_args.c)

### fatal_error

``` c
void fatal_error(const char *fmt, ...);
```

Output the error message to the console (if enabled), close/free platform resources and terminate execution.

## Drawing functions

``` c
/* Pixel functions */

inline void set_pixel(int x, int y, u8 color);
static inline void set_pixel_clipped(int x, int y, u8 color);
static inline u8 get_pixel(int x, int y);
```

``` c
static inline void ClearScreen(const u8 clear_color);      // clear the screen with the given color index
static inline u8 * backbuffer();                            // returns the point to the back buffer
static inline u8 * frontbuffer();
```


## Image functions

``` c
image_t * create_image(int w, int h);           // allocate a new image
void delete_image(image_t * image);             // delete previously allocated image
```
 
## Color functions

``` c
void set_color(int index, u32 rgb888);          // configure one color entry in the color palette

void linear_gradient(int from_index, u32 from_rgb, int to_index, u32 to_rgb);   // generate a linear gradient in the color palette
```
 
## Palette functions

``` c
void clear_palette(u32 rgb);                    // allocate a new palette
void save_palette(palette_t * palette_out);     // save the current color palette
void restore_palette(const palette_t * palette) // set the palette: TODO: rename in set_palette()
void copy_palette(palette_t * palette, const palette_t * src);
void fade_to(const palette_t * from, const palette_t * to, float factor);   // set the palette as a fade between two palettes
```


## Miscellaneous videos functions

### screenshot

``` c
void screenshot();
```

### enable_warping / disable_warping

``` c 
void enable_warping();

void disable_warping();
```

## Memory functions

## Interactivity functions

### Definition of events

``` c
    EVT_NONE = 0,

    /* standard ASCII codes */
    KEY_TAB   = 9,
    KEY_ENTER = 13,
    KEY_ESC   = 27,

    /* non standard ASCII codes */
    KEY_UP       = 0x80,
    KEY_DOWN     = 0x81,
    KEY_LEFT     = 0x82,
    KEY_RIGHT    = 0x84,

    KEY_PAGEUP   = 0x90,
    KEY_PAGEDOWN = 0x91,
    KEY_END      = 0x92,
    KEY_HOME     = 0x93,

    /* non standard shortcuts */
    KEY_CTRL_ENTER = -2,
    KEY_ALT_ENTER  = -3,
```

### function ``int last_event()``

Returns the last interaction event pulled from the event queue.

It returns ``NO_EVENT`` if the queue is empty.


By default, the following events are captured and used by the core, as such they're not put back through ``last_event()``:

    - KEY_TAB
    - KEY_ESC
    - EVT_QUIT
    - KEY_CTRL_ENTER
    - KEY_ALT_ENTER

See example in [samples/api_doc/last_event.c](/samples/api_doc/last_event.c)


## Number functions

### random_s32 / GetFloatRandomValue

``` c
s32 random_s32(s32 min, s32 max);
```

``` c
f32 GetFloatRandomValue(f32 min, f32 max);
```

### seed_buffer_u8

``` c
void seed_buffer_u8(u8 * buffer, int size, int min, int max);
```

### linear interpolations

``` c
f32 lerp_f32(float from, float to, float ratio);
```

``` c
s32 lerp_s32(s32 from, s32 to, float ratio);
```

``` c
s32 lerp_lut_s32(const s32 lut[], int x, const int bits);
```

## Console

``` c
void console(const char * fmt, ...);
```

``` c
void console_debug(const char * fmt, ...);
```

``` c
void console_error(const char * fmt, ...);
```

``` c
void enable_console_debug();
```

## Basic types

These are type shortcuts to favor concise/compact code syntax:

``` c
typedef uint8_t  u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;
 
typedef int8_t   s8;
typedef int16_t  s16;
typedef int32_t  s32;
typedef int64_t  s64;

typedef float   f32;
typedef double  f64;
```

## Arithmetic constants

``` c++
#define M_PI   3.14159265358979323846
#define M_PI_2 1.57079632679489661923
#define M_TAU  6.28318530717958647692
```

## Execution flow

```
   [ .exe ]   [ xlib ]                  [ user ]
      |          |                          |
      | main()   |                          |
      +--------->|                          |
      :          | start()                  |
      .          +------------------------->|
                 |                          |
                 |   main_loop(update_func) |
                 |<-------------------------+
                 |                          |
                 | update_func(dtime)       |
                 +------------------------->|
                 :                          :
                 .                          .
```

 .exe --> main() --> start() --> main

x.c :: main()
initialize external libraries
initialize video context
initialize random numbers
parse command line
call user-defined start() function

## Core dependencies

```
x/x.h 
  |
  +-> x/core/x.c
  |     | 
  |     +-> x/core/<platform>_drv.c
  |
  +-> x/default_config.h
        |
        +-> W : default video width
        |
        +-> H : default video height
        |
        +-> TITLE : default title
```
