/**
 * x86/easing.h
 * 
 * Copyleft (c) Pierre-Jean Turpeau 12/2019
 * <pierrejean AT turpeau DOT net>
 * 
 * Example usage:
 * 
 *  value =
 *      learp_f32( from_value, to_value,
 *          easing_curve( elapsed_time / duration_time ) )
 */
#ifndef X86_EASING_H
#define X86_EASING_H

# include <x86/x.h>
# include <math.h>

/* ---[ DEFINITIONS ]------------------------------------------------------- */

/* standard ease function type */
typedef float (*ease_f32_func)(float ratio);

/* ---[ DECLARATIONS ]------------------------------------------------------ */

static inline float ease_identity(float r);
static inline float ease_in_quad(float r);
static inline float ease_out_quad(float r);
static inline float ease_out_sine(float r);
static inline float ease_in_cubic(float r);
static inline float ease_in_quart(float r);
static inline float ease_out_quart(float r);
static inline float ease_pike_quad(float r);
static inline float ease_out_bounce(float r);

/* ---[ IMPLEMENTATION ]---------------------------------------------------- */

/* now the easing curves */

static inline float ease_identity(float r) {
    return r;
}

static inline float ease_in_quad(float r) {
    return r * r;
}

static inline float ease_out_quad(float r) {
    return -r * (r -2);
}

static inline float ease_out_sine(float r) {
    return sin(r * M_PI_2);
}

static inline float ease_in_cubic(float r) {
    return r * r * r;
}

static inline float ease_in_quart(float r) {
    return r * r * r * r;
}

static inline float ease_out_quart(float r) {
    r--;
    return - (r * r * r * r - 1);
}

static inline float ease_pike_quad(float r) {
    if(r <= .5f)
        return ease_in_quad(r / .5f);
    return ease_out_quad((1 - r) / .5f);
}

static inline float ease_out_bounce(float r) {
    if(r < (1/2.75))  {
        return 7.5625 * r * r;
    } else if(r < (2/2.75)) {
        r -= (1.5/2.75);
        return 7.5625 * r * r + 0.75;
    } else if(r < (2.5/2.75)) {
        r -= (2.25/2.75);
        return 7.5625 * r * r + 0.9375;
    } else {
        r -= (2.625/2.75);
        return 7.5625 * r * r + 0.984375;
    }
}

#endif /* X86_EASING_H */
