# Remaining ideas and things todo

There are infinite possibilities and I now try to stop adding new items in this
list, unless it has emerged from working through one existing item.s

## X86 API

 - ANSI Viewer: Better SDL text mode management to emulate 0xB800 addressing and
   consistent porting between SDL and MSDOS.
 - Video mode: add SetVGA60HzMode() and SetTextMode() (ansi viewer)
 - Timing/perf: finish porting the zen timer to both DOS/SDL
 - Sprites in x86/image.h: experiment compressed transparent sprites instead vs masked sprites
 - Masked sprite: try interleave mask bytes with pixels (better for bigger sprites!)
 - porting fx to x86: ANSI viewer, mod-player, raycast-tunnel ... 
 - breakout: proper keyboard management under DOS + shake + finish it one day...

### Done porting from x/ to x86/
 - wormhole: finish configuration generator w/ SaveTGA() function
 - timeline.h: need to manage one-shot actions to better manage the case were
   elapsed time is > duration and avoid the additional 'if' at the end of
   animations...
 - keyframe.h: refactored into a more simple & stupid timeline.h
 - moved to x86/image.h
   - x/gfx/bmp.h
   - x/gfx/image.h
   - x/gfx/sprite.h
   - x/gfx/procs.h
 - move to x86/core_math.h
   - x/math/trig_s32.h
   - x/math/numbers.h
   - x/math/v3f.h
   - x/math/v2f.h
 - move to x86/draw.h
   - x/gfx/draw.h
 - easing.h and fp16.h moved to x86/
 - font.h done

## Existing effects

 - checkbrd: clarify the code...
 - attractors: first try that should be better polished..
 
## New effects 

I filled the list according to:
- [A list of oldschool demo effects](https://www.pouet.net/topic.php?which=7523) (mirrored [demoeffects list.txt](docs/demoeffects_list.txt))

Without any specific order:

- Fx: cool feeling: camera shake, hit comics/particles, fake smokes,...
- Fx : uv-mapping => complete with H & V stripes texture
- ~~Fx: ansi viewer (good old .ans)~~
- ~~Fx: ansi viewer shall manage dynamic font selection~~
- ~~Fx: ansi viewer shall manage smooth scrolling~~
- Fx: basic 3d maths (w/ less muls than possible)
- Fx: basic sw 3d rendering (eg., for glenz, lamberts, fake phong...)
- Fx: bouncing dragon ball [Virutal Escape / Equinox](https://youtu.be/i_7ZQBkobY4?t=117)]
- Fx: 4 or more centered dots spheres of different size rotating in various directions and displayed w/ big round dots on FG & small on bg of diff colors
- Fx: dots/squares-based framebuffer display
- Fx: dots/squares sine (flag-like) animated texture
- Fx: fake caustic/god light rays
- Fx: flag-like animation on text
- Fx: filed vector tunnel with flares (as in timezone/eufrosyne)
- Fx: flocking simulation
- Fx: fluid dynamics through [vector fields](docs/GDC03.pdf)
- Fx: harmanograph and other curves (to complete lissajous like https://twitter.com/pixelvision8/status/1181936197863694337)
- Fx: lens 2d (as in my j2me version)
- Fx: metaballs using marching squares technics
- Fx: mini-game remake of Emulation Camp?
- Fx: Mode 7 3D porting of http://frankforce.com/?p=7427
- Fx: module player (4chan)
- Fx: moiré + bump mix (see: [Back to the roots / Haujobb](https://youtu.be/XzWNOV4W2ME?t=137))
- Fx: morphing backgrounds (under a vertcial scroll) like in https://tympanus.net/Development/MorphingBackgroundShapes/index3.html
- Fx: morphing text using blobs/metaballs
- Fx: particles sprites should be resized according to ageing
- Fx: particles w/ turbulences (through force grid/fields ?) (as in https://gamedevelopment.tutsplus.com/tutorials/adding-turbulence-to-a-particle-system--gamedev-13332)
- Fx: particles w/ repellers (https://natureofcode.com/book/chapter-4-particle-systems/)
- Fx: pencil line rendering
- Fx: raycasting floor
- Fx: raycasting tunnel with shadowing
- Fx: recursively animated cubes (and hypercube ? with antialias ?)
- Fx: signed distance functions to study in a 2D perspective
- Fx: sphere texture mapping
- Fx: starfield with 3d rotations
- Fx: starfield hyperspace w/ tunnel
- Fx: starfield w/ sprites (instead of lines/dots)
- Fx: text scrolls (straight, sinus, starwars) (http://www.stashofcode.fr/wp-content/uploads/figure1-3.png) and (https://youtu.be/x6MIOeHERac?t=192)
- Fx: text liners (circle, lissajous or rosace based movements)
- ~~Fx: tv noise improvement (2 or more pixel wide lines) as of https://m.youtube.com/watch?v=YMFBAOjuMWw~~
- Fx: vector balls
- Fx: voxel-like rendering
- ~~Fx: water effect old-fashioned~~
- ~~Fx: water ripple effect~~
- Fx: wobble effect on fullscreen image
- Fx: wormhole map generator explanations
- Fx: nice dithering things here https://twitter.com/TRASEVOL_DOG/status/946788526162661376
- Fx: other dithering 3D https://www.reddit.com/r/perfectloops/comments/cv7p9k/a_pico8_3d_engine_test_with_custom_dithering/
- Art: 1-bit palette https://lospec.com/palette-list/1bit-monitor-glow
- Lib: optimize TV warping mode (?)
- Lib: documenting xlib shows a mix with oldskewlish constraints such as default key management or default palette that shall be separated
- ~~Lib: frontbuffer TV warping during blitting~~
- Lib: shake frontbuffer during blitting (with a breakout style mini game)
- ~~Lib: introduce masked sprites : (sprite OR (mask AND background))~~
- ~~Lib: manage fixed size fonts~~
- ~~Lib: use _mm_malloc~~
- Lib: ellipse filled drawing doesn't work when transparent
- Lib: filled circle seems wrong on small sizes compared to unfilled ones
- ~~Lib: runtime FPS display (text or graphic)~~
- Lib: procedural texture using Perlin Noise => http://www.csee.umbc.edu/~olano/s2002c36/ch02.pdf
- Lib: test framework improvement (mainly result display in table)
- Lib: trigonometry optimizations as in http://www.ganssle.com/approx.htm ?
- ~~Doc: separate external references (PDFs) from internal docs~~
- Fx: curves w/ Lorenz and De Jong Attractors https://siva.dev/de-jong-attractors/
- Fx: curves w/ Spirographs https://siva.dev/spirographs/
- Fx: curves porting from https://www.dwitter.net/h/d3
- Lib: optimize math using SIMD SSE/AVX