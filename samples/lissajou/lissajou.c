/**
 * samples/lissajou/lissajou.c
 * 
 * Copyleft (c) Pierre-Jean Turpeau 01/2020
 * <pierrejean AT turpeau DOT net>
 * 
 * Nice to visualize and interesting to use for movements. Not all of them are
 * real Lissajous. They're all trigonometric though (and sometimes adapted to
 * fit very simple rendering constraints)
 *
 * See: http://www.mi.sanu.ac.rs/vismath/taylor2009/index.html
 */
#define X_IMPLEMENTATION 1

#define TITLE "-[ lissajous ]-"

#include <math.h>

#include <x86/x.h>
#include <x86/extras.h>

#define DOTS_RESOLUTION_FACTOR 8
#define DOTS_DISPLAY_COUNT 1024
#define DOTS_COUNT (DOTS_DISPLAY_COUNT * DOTS_RESOLUTION_FACTOR)

typedef enum {
    X_DIM,
    Y_DIM,
    Z_DIM
} eq_dimension_t;

float z_dots[DOTS_COUNT];
bool pause_time = false;
int visible_points_count = 0;


float square_knot(float t, float e, eq_dimension_t dim) {
    switch (dim) {
        case X_DIM: return sin(3.f * t + .7f + e);
        case Y_DIM: return sin(5.f * t + 1.f);
        case Z_DIM: return sin(7.f * t);
    }
    return 0;
}

float three_twist_knot(float t, float e, eq_dimension_t dim) {
    switch (dim) {
        case X_DIM: return sin(3.f * t + .7f + e);
        case Y_DIM: return sin(2.f * t + .2f);
        case Z_DIM: return sin(7.f * t);
    }
    return 0;
}

float compound_triple_knot(float t, float e, eq_dimension_t dim) {
    switch (dim) {
        case X_DIM: return .59*cos(t+e) + .3*cos(-2*t) - .45*cos(-5*t);
        case Y_DIM: return .59*sin(t) + .3*sin(-2*t) - .45*sin(-5*t);
        case Z_DIM: return 2*(.1*sin(9*t) + .25*sin(6*t));
    }
    return 0;    
}

float compound_quadruple_knot(float t, float e, eq_dimension_t dim) {
    switch (dim) {
        case X_DIM: return .6*cos(t+e) + .25*cos(t-3*t) - .26*cos(9*t);
        case Y_DIM: return .6*sin(t) + .25*sin(t-3*t) - .26*sin(9*t);
        case Z_DIM: return .12*sin(16*t) - .6*sin(4*t);
    }
    return 0;    
}

float inner_circulating_knot(float t, float e, eq_dimension_t dim) {
    switch (dim) {
        case X_DIM: return cos(4.f * t + e) + 0.875f * cos(7.f * t);
        case Y_DIM: return sin(4.f * t) + 0.875f * sin(7.f * t);
        case Z_DIM: return 2*(.35f * sin(12.f * t) - .15f * sin(3.f * t ));
    }
    return 0;    
}

float modified_2_9_knot(float t, float e, eq_dimension_t dim) {
    switch (dim) {
        case X_DIM: return cos(2*t+e) + .25 * cos(-7*t);
        case Y_DIM: return sin(4*t) + .25*sin(-17*t);
        case Z_DIM: return 2 * .35*sin(9*t) * cos(t*15);
    }
    return 0;    
}

float heart_knot(float t, float e, eq_dimension_t dim) {
    switch (dim) {
        case X_DIM: return 5.0 * (sin(t+e)*sin(t)*sin(t));
        case Y_DIM: return 4.0*cos(t) - 1.3*cos(2.0*t) - 0.6*cos(3.0*t) - 0.2*cos(4.0*t);
        case Z_DIM: return 3.0*cos(t);
    }
    return 0;    
}

typedef float (*eq_func)(float, float, eq_dimension_t);

eq_func eq_list[] = {
    heart_knot,
    modified_2_9_knot,
    inner_circulating_knot,
    compound_triple_knot,
    compound_quadruple_knot,
    three_twist_knot,
    square_knot
};

/* pre-calculate perspective projection divisions for all Z points of a given
 * curve */
void InitLissajouZLUT(int eq_num) {
    float zmin = 0;
    float xmax = 0;
    float ymax = 0;
    float v;
    for(int i = 0; i < DOTS_COUNT; i++) {
        float t = M_TAU * (float) i / DOTS_COUNT;
        z_dots[i] = eq_list[eq_num](t, 0, Z_DIM);

        zmin = M_MIN(zmin, z_dots[i]);

        v = eq_list[eq_num](t, 0, X_DIM);
        xmax = M_MAX(v, xmax);

        v = eq_list[eq_num](t, 0, Y_DIM);
        ymax = M_MAX(v, ymax);
    }

# define SCL 64.0f  /* scaling factor to adjust perspective angle */
# define ZNR 1.7f   /* Z_Near factor to adjust FOV */

    for(int i = 0; i < DOTS_COUNT; i++)
        z_dots[i] = SCL / (ZNR*zmin + z_dots[i]);

    visible_points_count = 0;

    LogDebug("eq #%d xmax=%.2f, ymax=%.2f, zmin=%.2f", eq_num, xmax, ymax, zmin);
}

void DrawLissajou(int eq_num, float elapsed_time) {
    static float time = 0;

    if(!pause_time)
        time = elapsed_time * 0.001;

    eq_func eq = eq_list[eq_num];

    /* simulate moving dotted points */
    static int advance = 0;
    advance ++;
    
    /* progressive appearance */
    if(visible_points_count < DOTS_DISPLAY_COUNT) {
        visible_points_count += 10;
        if(visible_points_count >= DOTS_DISPLAY_COUNT)
            visible_points_count = DOTS_DISPLAY_COUNT;
    }
    
    u8 * video = GetBackbuffer();

    for(int k = 0; k < visible_points_count; k++) {

        int i = (advance + k * DOTS_RESOLUTION_FACTOR) % DOTS_COUNT;

        float t = M_TAU * (float) i / DOTS_COUNT;

        /* evolving elapsed_time "simulate" rotation */
        s32 u = W/2 + eq(t, time, X_DIM) * z_dots[i];
        s32 v = H/2 + eq(t, 0, Y_DIM) * z_dots[i];
        s32 c, o;

# define PUT(DU,DV,C) \
    { o=u+DU+(v+DV)*W; c = video[o] + C; if(c>255) c=255; video[o]=c;}

        if( u >= 1 && v >= 1 && u < W-1 && v < H-1) {
            PUT(-1,-1, 8); PUT(0,-1, 8); PUT(1,-1, 8);
            PUT(-1, 0,16); PUT(0, 0,32); PUT(1, 0,16);
            PUT(-1, 1, 8); PUT(0, 1,16); PUT(1, 1, 8);
        }
    }
}

void AttenuateBackbuffer() {
# define ATTENUATION_FACTOR (7 * (1<<16) / 10) /* 0.7 in 16.16 fixed point */
    u8 * video = GetBackbuffer();
    for(int i = W * H; i > 0; i--, video++)
        *video = (*video * ATTENUATION_FACTOR) >> 16;
}

    
int main(int argc, char **argv) {
    InitPlatform(argc, (const char **) argv);
    ParseDefaultExeOptions();

    Console("press [SPACE] to change the curve parameters");
    Console("press [ENTER] to pause curve movement");
    Console("press [A] to disable/enable the backbuffer attenuation");
    Pause(2000);

    SetVGAMode();

    v3f ambiant_color = {{0.1, 0, 0.05}};
	v3f diffuse_color = {{0.8, 0.2, 0.2}};
	v3f specular_color = {{0.9, 0.8, 0.8}};
    LightingGradientPalette(0, 255, ambiant_color, diffuse_color, specular_color, 1);

    InitLissajouZLUT(0);

    int equation_id = 0;
    bool attenuate = true;

    while(!IsPlatformExit()) {
        BeginFrameLoop();
        ExecuteDefaultInteractions();

            if (! IsPaused()) {
                switch (GetLastEvent())
                {
                    case ' ':
                        equation_id = (equation_id + 1) % (sizeof(eq_list)/sizeof(eq_list[0]));
                        InitLissajouZLUT(equation_id);
                        break;

                    case KEY_ENTER:
                        pause_time = ! pause_time;
                        break;

                    case 'a':
                    case 'A':
                        attenuate = ! attenuate;
                }

                if(attenuate)
                    AttenuateBackbuffer();
                else
                    ClearScreen(0);

                DrawLissajou(equation_id, GetTime());
            }

        EndFrameLoop();
    };

    ClosePlatform();

    Console("fps = %d", x86.Time.fps);
    return EXIT_SUCCESS;
}
