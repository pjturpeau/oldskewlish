/**
 * x86/draw.h
 * 
 * Copyleft (c) Pierre-Jean Turpeau 01/2020
 * <pierrejean AT turpeau DOT net>
 */
#ifndef X86_DRAW_H
#define X86_DRAW_H

# include <x86/x.h>

/* ---[ DECLARATIONS ]----------------------------------------------------- */

/* vector drawing primitives */
/* ========================= */

static inline void DrawHorizontalLine(int x, int y, int len, u8 color);
extern void DrawHorizontalLine_c(int x, int y, int len, u8 color);

/** add an horizontal line to the backbuffer and clamp when needed */
extern void AddHorizontalLine(int x, int y, int len, u8 color, u8 clamp_color);

extern void DrawLine(int x1, int y1, int x2, int y2, u8 color);

extern void DrawRectangle(int x1, int y1, int x2, int y2, u8 fill_color);
extern void DrawRectangle_c(int x1, int y1, int x2, int y2, u8 fill_color);

/** draw a clipped filled circle */
extern void DrawCircle(int xc, int yc, int radius, u8 fill_color);

/** draw a clipped circle */
extern void DrawCircleOutline(int xc, int yc, int radius, u8 color);

/** add a clipped filled circle to the backbuffer and clamp when needed */
extern void AddCircle(int xc, int yc, int radius, u8 color, u8 clamp_color);

/** from http://dns.uls.cl/~ej/daa_08/Algoritmos/books/book10/9407l/9407l.htm */
extern void DrawEllipseOutline(int xOrigin, int yOrigin, int a, int b, u8 color);
extern void DrawEllipse(int xOrigin, int yOrigin, int a, int b, u8 color);


/* ---[ INLINED IMPLEMENTATION ]-------------------------------------------- */

static inline void DrawHorizontalLine(int x, int y, int len, u8 color) {
    assert(x >= 0 && (x + len <= W) && y >= 0 && y < H);
    memset(GetBackbuffer() + x + y * W, color, len);
}

/* ---[ IMPLEMENTATION ]---------------------------------------------------- */

# ifdef X_IMPLEMENTATION

void DrawHorizontalLine_c(int x, int y, int len, u8 color) {
    if(x < 0) { x = 0; len -= x; } else if(x >= W) return;
    int xr = x + len;
    if(xr >= W) { len = W - x - 1; }
    if(y < 0 || y >= H) return;
    DrawHorizontalLine(x, y, len, color);
}

void AddHorizontalLine(int x, int y, int len, u8 color, u8 clamp_color) {
    assert(x >= 0 && (x + len <= W) && y >= 0 && y < H);
    u8 * video = GetBackbuffer() + x + y * W;
    for(int i = len; i > 0; i--) {
        int c = *(video) + color;
        *(video++) = c > clamp_color ? clamp_color : c;
    }
}

void DrawLine(int x1, int y1, int x2, int y2, u8 color) {
    int dx =  abs (x2 - x1), sx = x1 < x2 ? 1 : -1;
    int dy = -abs (y2 - y1), sy = y1 < y2 ? 1 : -1;
    int err = dx + dy; /* error value e_xy */

    int syv = sy * W;
    u8 * video = GetBackbuffer() + x1 + y1 * W;

    for (;;) {  /* loop */
        *video = color;
        if (x1 == x2 && y1 == y2) break;
        int e2 = 2 * err;
        if (e2 >= dy) { err += dy; x1 += sx; video += sx;} /* e_xy+e_x > 0 */
        if (e2 <= dx) { err += dx; y1 += sy; video += syv;} /* e_xy+e_y < 0 */
    }
}

void DrawLine_c(int x1, int y1, int x2, int y2, u8 color) {
    int dx =  abs (x2 - x1), sx = x1 < x2 ? 1 : -1;
    int dy = -abs (y2 - y1), sy = y1 < y2 ? 1 : -1;
    int err = dx + dy; /* error value e_xy */

    int syv = sy * W;
    u8 * video = GetBackbuffer() + x1 + y1 * W;

    for (;;) {  /* loop */
        if (x1 >= 0 && x1 < W && y1 >= 0 && y1 < H)
            *video = color;
        else if ( (sx>0 && x1>=W) || (sx<0 && x1<0) || (sy>0 && y1>=H) || (sy<0 && y1<0))
            break;
        if (x1 == x2 && y1 == y2) break;
        int e2 = 2 * err;
        if (e2 >= dy) { err += dy; x1 += sx; video += sx;} /* e_xy+e_x > 0 */
        if (e2 <= dx) { err += dx; y1 += sy; video += syv;} /* e_xy+e_y < 0 */
    }
}

void DrawRectangle(int x1, int y1, int x2, int y2, u8 fill_color) {
    int s;
    if( x2 < x1 ) { s=x1; x1=x2; x2=s; }
    if( y2 < y1 ) { s=y1; y1=y2; y2=s; }
    for(int y = y1; y < y2; y++)
        DrawHorizontalLine(x1, y, x2 - x1, fill_color);
}

void DrawRectangle_c(int x1, int y1, int x2, int y2, u8 fill_color) {
    if( x2 < x1 ) { int s=x1; x1=x2; x2=s; }
    if( y2 < y1 ) { int s=y1; y1=y2; y2=s; }

    if(x1 >= W || x2 < 0) return;
    if(x1 < 0) x1 = 0;
    if(x2 >= W) x2 = W-1;
    if(y1 >= H || y2 < 0) return;
    if(y1 < 0) y1 = 0;
    if(y2 >= H) y2 = H-1;

    for(int y = y1; y < y2; y++)
        DrawHorizontalLine(x1, y, x2 - x1, fill_color);
}

/* generic fill primitive */
static void _draw_fill4(int xc, int yc, int x, int y, u8 color) {
    int xl = xc - x;
    if(xl < 0) xl = 0;

    int xr = xc + x;
    if(xr >= W) xr = W - 1;

    int len = xr - xl + 1;
    if(len <= 0) return;
    
    int yy1 = yc + y;
    if(yy1 >= 0 && yy1 < H)
        DrawHorizontalLine(xl, yy1, len, color);

    int yy2 = yc - y;
    if(yy2 >= 0 && yy2 < H)
        DrawHorizontalLine(xl, yy2, len, color);
}

/** see https://www.geeksforgeeks.org/bresenhams-circle-drawing-algorithm/ */
void DrawCircle(int xc, int yc, int radius, u8 fill_color)
{
    int x = 0;
	int y = radius;
	int decision = 3 - 2 * radius;

    decision += 6;
    x++;

    int xl = xc - radius;
    if(xl < 0) xl = 0;

    int xr = xc + radius;
    if(xr >= W) xr = W - 1;

    int len = xr - xl + 1;
    if(len > 0 && yc > 0 && yc < H)
        DrawHorizontalLine(xl, yc, len, fill_color);

    while(x < y) { 
        _draw_fill4(xc, yc, y ,x, fill_color);
		if(decision < 0)
			decision += 4 * x + 6;
        else {
            y--;
			decision += 4 * (x - y) + 10;
            if(y != x)
                _draw_fill4(xc, yc, x ,y, fill_color);
		}
        x++;
    }
}

static void _add_fill4(int xc, int yc, int x, int y, u8 color, u8 clamp_color) {
    int xl = xc - x;
    if(xl < 0) xl = 0;

    int xr = xc + x;
    if(xr >= W) xr = W - 1;

    int len = xr - xl + 1;
    if(len <= 0) return;
    
    int yy1 = yc + y;
    if(yy1 >= 0 && yy1 < H)
        AddHorizontalLine(xl, yy1, len, color, clamp_color);

    int yy2 = yc - y;
    if(yy2 >= 0 && yy2 < H)
        AddHorizontalLine(xl, yy2, len, color, clamp_color);
}

/** see https://www.geeksforgeeks.org/bresenhams-circle-drawing-algorithm/ */
void AddCircle(int xc, int yc, int radius, u8 color, u8 clamp_color)
{
    int x = 0;
	int y = radius;
	int decision = 3 - 2 * radius;

    decision += 6;
    x++;

    int xl = xc - radius;
    if(xl < 0) xl = 0;

    int xr = xc + radius;
    if(xr >= W) xr = W - 1;

    int len = xr - xl + 1;
    if(len > 0 && yc > 0 && yc < H)
        AddHorizontalLine(xl, yc, len, color, clamp_color);

    while(x < y) { 
        _add_fill4(xc, yc, y ,x, color, clamp_color);
		if(decision < 0)
			decision += 4 * x + 6;
        else {
            y--;
			decision += 4 * (x - y) + 10;
            if(y != x)
                _add_fill4(xc, yc, x ,y, color, clamp_color);
		}
        x++;
    }
}

static inline void _draw_plot4(int xc, int yc, int x, int y, u8 color) {
    SetPixel_c(xc - x, yc + y, color);
    SetPixel_c(xc + x, yc + y, color);
    SetPixel_c(xc - x, yc - y, color);
    SetPixel_c(xc + x, yc - y, color);
}

/** using Bresenham's algorithm */
void DrawCircleOutline(int xc, int yc, int radius, u8 color) {
    int x = 0;
	int y = radius;
	int decision = 3 - 2 * radius;

    _draw_plot4(xc, yc, x, y, color);
    _draw_plot4(xc, yc, y, x, color);

	while(x < y) { 
		if(decision < 0)
			decision += 4 * x + 6;
        else {
			y--;
			decision += 4 * (x - y) + 10;
		}
        x++;
        _draw_plot4(xc, yc, x, y, color);
        _draw_plot4(xc, yc, y, x, color);
    }
}


// /* forward private declarations */
// typedef void (*_draw_hline_func)(int x, int y, int len, u8 color);

void DrawEllipseOutline(int xOrigin, int yOrigin, int a, int b, u8 color)
{
    int aSquared = a*a;
    int bSquared = b*b;
    int twoASquared = 2 * aSquared;
    int twoBSquared = 2 * bSquared;

    {    /* Plot the octant from the top to the top-right */
        int x = 0;
        int y = b;
        int twoXTimesBSquared = 0;
        int twoYTimesASquared = y * twoASquared;

        int error = -y* aSquared;  /* b^2 x^2 + a^2 y^2 - a^2 b^2 - a^2y */
    
        while (twoXTimesBSquared <= twoYTimesASquared )
        {
            _draw_plot4( xOrigin, yOrigin, x, y, color);
            x += 1;
            twoXTimesBSquared += twoBSquared;
            error += twoXTimesBSquared - bSquared;
            if (error >= 0)
            {
                y -= 1;
                twoYTimesASquared -= twoASquared;
                error -= twoYTimesASquared;
            }
        }
    }
    {    /* Plot the octant from right to top-right */
        int x = a;
        int y = 0;
        int twoXTimesBSquared = x * twoBSquared;
        int twoYTimesASquared = 0;
        int error = -x* bSquared;  /* b^2 x^2 + a^2 y^2 - a^2 b^2 - b^2x */

        while (twoXTimesBSquared > twoYTimesASquared)
        {
            _draw_plot4( xOrigin, yOrigin, x, y, color);
            y += 1;
            twoYTimesASquared += twoASquared;
            error += twoYTimesASquared - aSquared;
            if (error >= 0)
            {
                x -= 1;
                twoXTimesBSquared -= twoBSquared;
                error -= twoXTimesBSquared;
            }
        }
    }
}

void DrawEllipse(int xOrigin, int yOrigin, int a, int b, u8 color) {
    int aSquared = a*a;
    int bSquared = b*b;
    int twoASquared = 2 * aSquared;
    int twoBSquared = 2 * bSquared;

    {    /* Plot the octant from the top to the top-right */
        int x = 0;
        int y = b;
        int twoXTimesBSquared = 0;
        int twoYTimesASquared = y * twoASquared;

        int error = -y * aSquared;  /* b^2 x^2 + a^2 y^2 - a^2 b^2 - a^2y */
    
        while (twoXTimesBSquared <= twoYTimesASquared )
        {
//            _x_draw_fill4( xOrigin, yOrigin, x, y, color, hline_func);
            x += 1;
            twoXTimesBSquared += twoBSquared;
            error += twoXTimesBSquared - bSquared;
            if (error >= 0)
            {
                y -= 1;
                _draw_fill4( xOrigin, yOrigin, x, y, color);
                twoYTimesASquared -= twoASquared;
                error -= twoYTimesASquared;
            }
        }
    }
    {    /* Plot the octant from right to top-right */
        int x = a;
        int y = 0;
        int twoXTimesBSquared = x * twoBSquared;
        int twoYTimesASquared = 0;
        int error = -x* bSquared;  /* b^2 x^2 + a^2 y^2 - a^2 b^2 - b^2x */

        while (twoXTimesBSquared > twoYTimesASquared)
        {
            _draw_fill4( xOrigin, yOrigin, x, y, color);
            y += 1;
            twoYTimesASquared += twoASquared;
            error += twoYTimesASquared - aSquared;
            if (error >= 0)
            {
                x -= 1;
                twoXTimesBSquared -= twoBSquared;
                error -= twoXTimesBSquared;
            }
        }
    }
}

#define SWAP(x,y) {tmp=x;x=y;y=tmp;}

void DrawFilledTriangle_c(int x1, int y1, int x2, int y2, int x3, int y3, u8 color) {
	uint8_t t1x,t2x,y,minx,maxx,t1xp,t2xp;
	bool changed1 = false;
	bool changed2 = false;
	int8_t signx1,signx2,dx1,dy1,dx2,dy2;
	uint8_t e1,e2;

    int tmp;

    // Sort vertices
	if (y1>y2) { SWAP(y1,y2); SWAP(x1,x2); }
	if (y1>y3) { SWAP(y1,y3); SWAP(x1,x3); }
	if (y2>y3) { SWAP(y2,y3); SWAP(x2,x3); }

	t1x=t2x=x1; y=y1;   // Starting points

	dx1 = (int8_t)(x2 - x1); if(dx1<0) { dx1=-dx1; signx1=-1; } else signx1=1;
	dy1 = (int8_t)(y2 - y1);

	dx2 = (int8_t)(x3 - x1); if(dx2<0) { dx2=-dx2; signx2=-1; } else signx2=1;
	dy2 = (int8_t)(y3 - y1);
	
	if (dy1 > dx1) {   // swap values
        SWAP(dx1,dy1);
		changed1 = true;
	}
	if (dy2 > dx2) {   // swap values
        SWAP(dy2,dx2);
		changed2 = true;
	}
	
	e2 = (uint8_t)(dx2>>1);
    // Flat top, just process the second half
    if(y1==y2) goto next;
    e1 = (uint8_t)(dx1>>1);
	
	for (uint8_t i = 0; i < dx1;) {
		t1xp=0; t2xp=0;
		if(t1x<t2x) { minx=t1x; maxx=t2x; }
		else		{ minx=t2x; maxx=t1x; }
        // process first line until y value is about to change
		while(i<dx1) {
			i++;			
			e1 += dy1;
            while (e1 >= dx1) {
				e1 -= dx1;
                if (changed1) t1xp=signx1;//t1x += signx1;
				else          goto next1;
			}
			if (changed1) break;
			else t1x += signx1;
		}
	// Move line
	next1:
        // process second line until y value is about to change
		while (1) {
			e2 += dy2;		
			while (e2 >= dx2) {
				e2 -= dx2;
				if (changed2) t2xp=signx2;//t2x += signx2;
				else          goto next2;
			}
			if (changed2)     break;
			else              t2x += signx2;
		}
	next2:
		if(minx>t1x) minx=t1x; if(minx>t2x) minx=t2x;
		if(maxx<t1x) maxx=t1x; if(maxx<t2x) maxx=t2x;
        DrawHorizontalLine_c(minx, y, (maxx-minx), color);
		// Now increase y
		if(!changed1) t1x += signx1;
		t1x+=t1xp;
		if(!changed2) t2x += signx2;
		t2x+=t2xp;
        y += 1;
		if(y==y2) break;
		
    }
	next:
	// Second half
	dx1 = (int8_t)(x3 - x2); if(dx1<0) { dx1=-dx1; signx1=-1; } else signx1=1;
	dy1 = (int8_t)(y3 - y2);
	t1x=x2;

	if (dy1 > dx1) {   // swap values
        SWAP(dy1,dx1);
		changed1 = true;
	} else changed1=false;
	
	e1 = (uint8_t)(dx1>>1);
	
	for (uint8_t i = 0; i<=dx1; i++) {
		t1xp=0; t2xp=0;
		if(t1x<t2x) { minx=t1x; maxx=t2x; }
		else		{ minx=t2x; maxx=t1x; }
	    // process first line until y value is about to change
		while(i<dx1) {
            e1 += dy1;
            while (e1 >= dx1) {
				e1 -= dx1;
                if (changed1) { t1xp=signx1; break; }//t1x += signx1;
				else          goto next3;
			}
			if (changed1) break;
			else   	   	  t1x += signx1;
			if(i<dx1) i++;
		}
	next3:
        // process second line until y value is about to change
		while (t2x!=x3) {
			e2 += dy2;
            while (e2 >= dx2) {
				e2 -= dx2;
				if(changed2) t2xp=signx2;
				else          goto next4;
			}
			if (changed2)     break;
			else              t2x += signx2;
		}
	next4:
		if(minx>t1x) minx=t1x; if(minx>t2x) minx=t2x;
		if(maxx<t1x) maxx=t1x; if(maxx<t2x) maxx=t2x;
        DrawHorizontalLine_c(minx, y, (maxx-minx), color);
		// Now increase y
		if(!changed1) t1x += signx1;
		t1x+=t1xp;
		if(!changed2) t2x += signx2;
		t2x+=t2xp;
        y += 1;
		if(y>y3) return;
	}
}

# endif /* X_IMPLEMENTATION */

#endif /* X86_DRAW_H */
