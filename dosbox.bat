@echo off

REM dosbox launcher

if "%~1"=="" goto :BLANK

if not exist "%DOSBOX%" goto :DOSBOXERR

set filepath="%1"

for /F "delims=" %%i in (%filepath%) do set dirname=%%~dpi
for /F "delims=" %%i in (%filepath%) do set filename=%%~nxi
for /F "delims=" %%i in (%filepath%) do set basename=%%~ni

"%DOSBOX%" -noconsole -conf "%~dp0\dosbox-x.Pentium120_VGA.conf" -c "mount Y '%dirname%'" -c "Y:" -c "%filename%" -c "pause"  -c "exit"
goto :END

:DOSBOXERR
echo[
echo dosbox.exe not found using the DOSBOX env var
echo[
goto :END

:BLANK
echo[
echo Missing DOS executable to launch
echo %0 "<path/to/file.exe>"
echo[

:END
