
#include <x86/x.h>
#include <x86/draw.h>
#include "particles.h"

typedef struct {
    v2f pos;
    v2f velocity;
    int radius;
} ball_t;

ball_t ball;

void InitBall() {
    ball.pos.x = W/2;
    ball.pos.y = H/2;
    ball.velocity.x = .1f;//GetFloatRandomValue(1, 2);
    ball.velocity.y = .1f;//GetFloatRandomValue(1, 2);
    ball.radius = 3;
}

void DrawBall() {
    DrawCircle(ball.pos.x, ball.pos.y, ball.radius, 5);
    DrawCircleOutline(ball.pos.x, ball.pos.y, ball.radius, 5);
}

void UpdateBall(float time_step) {
    ball.pos.x += ball.velocity.x * time_step;
    ball.pos.y += ball.velocity.y * time_step;

    int xmin = ball.pos.x - ball.radius;
    int xmax = ball.pos.x + ball.radius;
    int ymin = ball.pos.y - ball.radius;
    int ymax = ball.pos.y + ball.radius;

    /* hit the walls? */

    if(xmax > W) {
        ball.pos.x = W - ball.radius-1;
        ball.velocity.x = -ball.velocity.x;
        // shake(20);
    }
    else if(xmin < 0) {
        ball.pos.x = ball.radius;
        ball.velocity.x = -ball.velocity.x;
        // shake(20);
    }

    if(ymax > H) {
        ball.pos.y = H - ball.radius-1;
        ball.velocity.y = -ball.velocity.y;
        // shake(10);
    }
    else if(ymin < 0) {
        ball.pos.y = ball.radius; 
        ball.velocity.y = -ball.velocity.y;
        // shake(10);
    }
}
