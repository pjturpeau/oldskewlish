
#include <x86/x.h>

// Fixed-point scaling factor (16.16 format)
#define FP_SCALE (1 << 16)


inline long ceilfp(long x)
{
    x +=  0xffff;
    return (x >> 16);
}


// Function to fill a triangle
void DrawFilledTriangleQ16(int x0, int y0, int x1, int y1, int x2, int y2, u8 color) {
    // Sort vertices by y-coordinate (y0 <= y1 <= y2)
    if (y0 > y1) { int t = y0; y0 = y1; y1 = t; t = x0; x0 = x1; x1 = t; }
    if (y1 > y2) { int t = y1; y1 = y2; y2 = t; t = x1; x1 = x2; x2 = t; }
    if (y0 > y1) { int t = y0; y0 = y1; y1 = t; t = x0; x0 = x1; x1 = t; }

    // Calculate slopes (fixed-point)
    int dx1 = 0, dx2 = 0, dx3 = 0;

    if (y1 != y0) dx1 = ((x1 - x0) << 16) / (y1 - y0); // Slope of edge y0 -> y1
    if (y2 != y0) dx2 = ((x2 - x0) << 16) / (y2 - y0); // Slope of edge y0 -> y2
    if (y2 != y1) dx3 = ((x2 - x1) << 16) / (y2 - y1); // Slope of edge y1 -> y2

    // Initialize x_start and x_end (fixed-point)
    int x_start = x0 << 16;
    int x_end = x0 << 16;

    // Loop over scanlines from y0 to y2
    for (int y = y0; y < y2; y++) {
        // Switch slopes when crossing y1
        if (y < y1) {
            x_start += dx1; // Update x_start for first half
            x_end += dx2;   // Update x_end for first half
        } else {
            x_start += dx3; // Update x_start for second half
            x_end += dx2;   // Update x_end for second half
        }

        // Convert fixed-point x_start and x_end back to integers
        int x_start_int = x_start >> 16;
        int x_end_int = x_end >> 16;

        // Ensure x_start_int <= x_end_int
        if (x_start_int > x_end_int) {
            int t = x_start_int;
            x_start_int = x_end_int;
            x_end_int = t;
        }

        // Calculate the number of pixels to draw
        int pixel_count = x_end_int - x_start_int;
        if (pixel_count < 0) continue; // Skip if no pixels to draw

        // Calculate the starting offset in the framebuffer
        // u8 *fb_ptr = &framebuffer[y * FB_WIDTH + x_start_int];
        u8 * fb_ptr = &GetBackbuffer()[y * W + x_start_int];

        if (pixel_count == 0) {
            *fb_ptr = color;
            continue;
        }

        // Unrolled loop: Process 4 pixels at a time
        int i = 0;
        for (; i + 3 < pixel_count; i += 4) {
            fb_ptr[i] = color;     // Pixel 1
            fb_ptr[i + 1] = color; // Pixel 2
            fb_ptr[i + 2] = color; // Pixel 3
            fb_ptr[i + 3] = color; // Pixel 4
        }

        // Handle remaining pixels (less than 4)
        for (; i < pixel_count; i++) {
            fb_ptr[i] = color; // Fill remaining pixels
        }
    }
}


void DrawFilledTriangleFloat(int x0, int y0, int x1, int y1, int x2, int y2, u8 color) {
    // if (t0.y==t1.y && t0.y==t2.y) return; // I dont care about degenerate triangles 

    // sort the vertices, t0, t1, t2 lower−to−upper (bubblesort yay!) 
    if (y0 > y1) { int t = y0; y0 = y1; y1 = t; t = x0; x0 = x1; x1 = t; }
    if (y1 > y2) { int t = y1; y1 = y2; y2 = t; t = x1; x1 = x2; x2 = t; }
    if (y0 > y1) { int t = y0; y0 = y1; y1 = t; t = x0; x0 = x1; x1 = t; }

    int total_height = y2-y0; 
    for (int i=0; i<total_height; i++) { 
        bool second_half = i>y1-y0 || y1==y0; 
        int segment_height = second_half ? y2-y1 : y1-y0; 
        float alpha = (float)i/total_height; 
        float beta  = (float)(i-(second_half ? y1-y0 : 0))/segment_height; // be careful: with above conditions no division by zero here
        int xa = x0 + (x2-x0)*alpha; 
        // int ya = y0 + (y2-y0)*alpha; 
        int xb = second_half ? x1 + (x2-x1)*beta : x0 + (x1-x0)*beta; 
        // int yb = second_half ? y1 + (y2-y1)*beta : y0 + (y1-y0)*beta; 
        if (xa>xb) { int t = xa; xa=xb;xb=t; }
        for (int j=xa; j<=xb; j++) {
            SetPixel(j, y0+i, color); 
        } 
    } 
}