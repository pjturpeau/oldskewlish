/**
 * x/default_config.h
 * 
 * Copyleft (c) Pierre-Jean Turpeau 04/2020
 * <pierrejean AT turpeau DOT net>
 */
#ifndef X_DEFAULT_CONFIG_H
#define X_DEFAULT_CONFIG_H

# ifndef TITLE
#  define TITLE "-( oldskewlish )-"
# endif

# ifndef W
#  define W 320
# endif

# ifndef H
#  define H 200
# endif

/* to disable both load and fps tracking */
/* #define DISABLE_LOAD_TRACKING 1 */

#endif /* X_DEFAULT_CONFIG_H */
