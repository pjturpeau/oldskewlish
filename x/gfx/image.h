/**
 * x/gfx/image.h
 * 
 * Copyleft (c) Pierre-Jean Turpeau 11/2019
 * <pierrejean AT turpeau DOT net>
 * 
 * Functions in support of image_t defined in x/x.h
 * 
 * TODO:
 * From Image to Image:
 * SinX()
 * SinY()
 * NoiseX()
 * DotShowing() // replace pixels by dots/square/diamonds changing size
 */
#ifndef X_GFX_IMAGE_H
#define X_GFX_IMAGE_H

# include <x/x.h>

/* ---[ DEFINITIONS ]------------------------------------------------------- */


/* ---[ DECLARATIONS ]------------------------------------------------------ */

/** set image pixel - no clipping, no checks */
static inline void set_image(image_t * image, int x, int y, u8 color);

/** get image pixel - no clipping, no checks  */
static inline u8 get_image(const image_t * image, int x, int y);

static inline void clear_image(image_t * image, u8 color);

/** draw the clipped opaque image to the backbuffer */ 
extern void draw_image(const image_t * sprite, int x, int y);

/** add the clipped image to the backbuffer and clamp color when needed */
extern void add_image(const image_t * sprite, int x, int y, int clamp_color);

extern void vertical_mirror(image_t * image);

extern void horizontal_mirror(image_t * image);

/**
 * draw image with pixelate effect - the image is supposed to be smaller than
 * the framebuffer - hence no size check, no clipping...
 */
extern void draw_pixelate_naive(image_t * image, int x, int y, float factor);

/* ---[ IMPLEMENTATION ]---------------------------------------------------- */

# ifdef X_IMPLEMENTATION

static inline void set_image(image_t * image, int x, int y, u8 color) {
    assert( x >= 0 && y >= 0 && x < image->w && y < image->h);
    image->pixels[y * image->w + x] = color;
}

static inline u8 get_image(const image_t * image, int x, int y) {
    assert( x >= 0 && y >= 0 && x < image->w && y < image->h);
    return image->pixels[y * image->w + x];
}

static inline void clear_image(image_t * image, u8 color) {
    memset( image->pixels, color, image->w * image->h);
}

void draw_image(const image_t * image, int x, int y) {
    if(x < -image->w || x > W || y < -image->h || y > H)
        return;

    const int clip_left = (x < 0) ? -x : 0;
    const int clip_top = (y < 0) ? -y : 0;
    const int clip_right = M_MIN(W - x, image->w);
    const int clip_bottom = M_MIN(H - y, image->h);

    for(int j = clip_top; j < clip_bottom; j++) {
        u8 * src = image->pixels + clip_left + j * image->w;
        u8 * dst = backbuffer() + x + clip_left + (y+j) * W;
        memcpy(dst, src, clip_right - clip_left);
    }
}

void add_image(const image_t * sprite, int x, int y, int clamp_color) {
    if(x < -sprite->w || x > W || y < -sprite->h || y > H)
        return;

    const int clip_left = (x < 0) ? -x : 0;
    const int clip_top = (y < 0) ? -y : 0;
    const int clip_right = M_MIN(W - x, sprite->w);
    const int clip_bottom = M_MIN(H - y, sprite->h);

    for(int j = clip_top; j < clip_bottom; j++) {
        u8 * src = sprite->pixels + clip_left + j * sprite->w;
        u8 * dst = backbuffer() + x + clip_left + (y+j) * W;
        for(int i = clip_left; i < clip_right; i++) {
            const int c = *(src++) + *(dst);
            *(dst++) = c > clamp_color ? clamp_color : c;
        }
    }
}

void vertical_mirror(image_t * image) {
    if( image->pixels ) {
        u8 * pix = image->pixels;
        for(int j = 0; j <image->h; j++) {
            for(int i = 0; i < image->w/2; i++) {
                u8 tmp = pix[image->w - 1 - i];
                pix[image->w - 1 - i] = pix[i];
                pix[i] = tmp; 
            }
            pix += image->w;
        }
    }
}

void horizontal_mirror(image_t * image) {
    if( image->pixels ) {
        u8 * pix = image->pixels;
        u8 * pix2 = image->pixels + (image->h -1) * image->w;
        for(int j = 0; j < image->h/2; j++) {
            for(int i = 0; i < image->w; i++) {
                u8 tmp = pix2[i];
                pix2[i] = pix[i];
                pix[i] = tmp; 
            }
            pix += image->w;
            pix2 -= image->w;
        }
    }
}

void draw_pixelate_naive(image_t * image, int x, int y, float factor) {
    float inverse_factor = 1.0f / factor;
    u8 * fb;
    u8 * video = backbuffer() + x + y * W;
    int ys, xs; /* source coordinates */
    for(int j = 0; j < image->h; j++, video += W) {
        ys = (int)(j * inverse_factor + .5f) * factor;
        ys = image->w * (ys >= image->h ? image->h-1 : ys);
        fb = video;
        for(int i = 0; i < image->w; i++ ) {
            xs = (int)(i * inverse_factor + .5f) * factor;
            xs = (xs >= image->w) ? image->w-1 : xs;
            *(fb++) = image->pixels[xs + ys];
        }
    }
}

# endif /* X_IMPLEMENTATION */

#endif /* X_GFX_IMAGE_H */
