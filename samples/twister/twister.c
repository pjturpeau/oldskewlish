/**
 * samples/twister/main.c
 * 
 * Copyleft (c) Pierre-Jean Turpeau 12/2019
 * <pierrejean AT turpeau DOT net>
 */
#define X_IMPLEMENTATION 1

#define TITLE "-[ twister ]-"

#include <x86/x.h>
#include <x86/draw.h>

#include <math.h>

#define TWISTER_WIDTH 90
#define TWISTER_HALF_WIDTH (90/2)

#define GRADIENT_SIZE TWISTER_HALF_WIDTH
#define GRADIENT_FIRST_INDEX 3

#define OUTLINE_SIZE 2
#define SHADOW_SIZE 15

int rendering_type = 0;

typedef struct {
    float x;
    float y;
    float intensity;
    u8 color;
} twister_vertex_t;

void InitTwister(u32 rgb_colors[4]) {
    SetColorPalette(0, 0x888888); /* background */
    SetColorPalette(1, 0x666666); /* shadow */
    SetColorPalette(2, 0); /* outline */

    for(int i = 0; i < 4; i++) {
        int i1 = GRADIENT_FIRST_INDEX + GRADIENT_SIZE * i;
        int i2 = GRADIENT_FIRST_INDEX + GRADIENT_SIZE * (i + 1) - 1;
        LinearGradientPalette(i1, rgb_colors[i], i2, 0);
    }
}

void DrawTwisterHorizontalLine(twister_vertex_t * v1, twister_vertex_t * v2, int y) {
    switch(rendering_type) {
        case 1:
            DrawHorizontalLine(v1->x, y, v2->x - v1->x, v1->color + GRADIENT_SIZE * v2->y);
            break;
        case 2:
            DrawHorizontalLine(v1->x, y, v2->x - v1->x, v1->color);
            break;
        case 0: {
            int dx = v2->x - v1->x;
            if( !dx ) return;

            int delta_lum = GRADIENT_SIZE * v2->y - GRADIENT_SIZE * v1->y;
            delta_lum <<= 16;
            delta_lum /= dx;

            int color = (int)(v1->color + GRADIENT_SIZE * v1->y) << 16;

            for( int i = 0; i < dx; i++, color += delta_lum)
                SetPixel(v1->x + i, y, color >> 16);
        }
        break;
    }
}

void DrawTwister(float elapsed_time) {
    ClearScreen(0);

    elapsed_time *= 0.002;

    twister_vertex_t vertex[4];

    const int CENTER_POS_X = W >> 1;
    const float HEIGHT_INV = 1.0f / H;

    /* setup vertexes colors */
    for(int i = 0; i < 4; i++)
        vertex[i].color = GRADIENT_FIRST_INDEX + GRADIENT_SIZE *i;

    /* for each scanline */
    for(int y = 0; y < H; y++) {

        float vertical_step = (float) y * HEIGHT_INV;

        int x_position = CENTER_POS_X - TWISTER_HALF_WIDTH * sin(elapsed_time * 0.5f + vertical_step) + TWISTER_HALF_WIDTH * cos(elapsed_time + vertical_step);

        float torsion_angle = M_TAU * sin(elapsed_time + vertical_step) * cos(elapsed_time * 0.25f);

        /* vertexes position in one plane */
        for(int i = 0; i < 4; i++) {
             /* every 90 degree */
            vertex[i].x = cos(torsion_angle + i * M_PI_2); /* in [-1..1] range */
            vertex[i].y = 0.5 + 0.5 * sin(torsion_angle + i * M_PI_2); /* in [0..1] range for intensity */
        }

        /* display coordinates */
        for(int i = 0; i < 4; i++) {
            vertex[i].x *= TWISTER_HALF_WIDTH;
            vertex[i].x += x_position;
        }

        int outline_xmin = W;
        int outline_xmax = 0;
        int outline_xmed;

        for(int i = 0; i < 4; i++) {
            if( vertex[i].x < vertex[(i+1)&3].x) {
                DrawTwisterHorizontalLine(&vertex[i], &vertex[(i+1)&3], y);

                /* update min, max, median position for the outline */
                if( vertex[(i+1)&3].x > outline_xmax ) {
                    outline_xmax = vertex[(i+1)&3].x;
                    outline_xmed = vertex[i].x;
                }
                if( vertex[i].x < outline_xmin )
                    outline_xmin = vertex[i].x;
            }
        }       

        /* outline */
        DrawHorizontalLine(outline_xmin-OUTLINE_SIZE, y, OUTLINE_SIZE, 2);
        DrawHorizontalLine(outline_xmax-1, y, OUTLINE_SIZE, 2);
        DrawHorizontalLine(outline_xmed-OUTLINE_SIZE/2, y, OUTLINE_SIZE, 2);

        /* shadow */
        DrawHorizontalLine(outline_xmax-1 + OUTLINE_SIZE, y, SHADOW_SIZE, 1);
    }
}

int main(int argc, char **argv) {
    InitPlatform(argc, (const char **) argv);
    ParseDefaultExeOptions();

    Console("\tpress [SPACE] to switch rendering style\n");
    Pause(2000);

    SetVGAMode();

    u32 colors[4] = { 0xFF0000, 0x00FF00, 0xFF00FF, 0x0000FF };
    InitTwister(colors);

    while(!IsPlatformExit()) {
        BeginFrameLoop();
        ExecuteDefaultInteractions();

            if( GetLastEvent() ==  ' ' ) {
                rendering_type++;
                rendering_type %= 3;
            }   
            
            if (!IsPaused())
                DrawTwister(GetTime());

        EndFrameLoop();
    };

    ClosePlatform();

    Console("fps = %d", x86.Time.fps);
    return EXIT_SUCCESS;    
}
