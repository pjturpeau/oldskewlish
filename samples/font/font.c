/**
 * samples/font/font.c
 * 
 * Copyleft (c) Pierre-Jean Turpeau 12/2019
 * <pierrejean AT turpeau DOT net>
 */
#define X_IMPLEMENTATION

#define TITLE "-[ font ]-"

#include <x86/x.h>
#include <x86/font.h>

extern unsigned char f32x32f6_data[];
extern unsigned int  f32x32f6_size;
extern unsigned char bbl_8x10_data[];
extern unsigned int  bbl_8x10_size;
extern unsigned char ami_8x16_data[];
extern unsigned int  ami_8x16_size;

image_t amifont_image;
image_t bigfont_image;
image_t funfont_image;

font_t amifont;
font_t bigfont;
font_t funfont;

void InitFonts() {
    palette_t palette;
    LoadProportionalFont(&funfont, NULL, bbl_8x10_data, bbl_8x10_size);
    LoadProportionalFont(&amifont, NULL, ami_8x16_data, ami_8x16_size);
    LoadProportionalFont(&bigfont, &palette, f32x32f6_data, f32x32f6_size);
    RestorePalette(&palette);
}

int main(int argc, char **argv) {
    InitPlatform(argc, (const char **) argv);
    ParseDefaultExeOptions();
    SetVGAMode();

    InitFonts();

    while(!IsPlatformExit()) {
        BeginFrameLoop();
        ExecuteDefaultInteractions();

        ClearScreen(16);
        DrawProportionalText(&funfont, 10, 10, "& FONT JUST FOR FUN &");
        DrawProportionalText(&amifont, 240, 10, "amiga font\nstill RULE\nin 202x!");
        DrawProportionalText(&bigfont, 10, 50, "BIG FONT\n32X32\nFOR BIG\nSCROLLERS");

        EndFrameLoop();
    };

    ClosePlatform();
    Console("fps = %d", x86.Time.fps);
    return EXIT_SUCCESS;
}
