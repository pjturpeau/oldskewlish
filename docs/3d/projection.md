# 3d basics

## Screen normalization

```
               |
-1 ,-----------------------. -1
   |           |           | 
   |                       | h
 - - - - - - - + - - - - - - - -
   |            (0,0)      |
   |           |           |
+1 `-----------------------' +1
        w      |          

                             h
Screen aspect ratio (SAR) = ---
                             w
```

## Projection

### What we think we see

```
        |  .      |
        |     []  |
        |  o     [|]
        |         |
        |  O      |
    -1 -+---------+- +1  Screen
            eye
```
Far objects are smallers: O o .
Objets outside the boundaries are hidden.
You can have more objects are the far distance.

### The field of view


```
      -1                +1
         \             /
          \   theta   /
           \  ,-~-.  /
            \/     \/
        -1  --------- +1  Screen
              \   /
               \ /
                v
               eye
```
theta = field of view angle
small theta = zooming in (view fewer objects)
big theta angle is zooming out (view more objects)

### Scaling factor

The scaling factor is used to relate the objects to the Field Of View angle.


```
   left .-------+-------. right
         \      |far   /
          \     |     /
           \  ,-|-.  /
            \/  |  \/
             \  |  /
              \ | /
               \|/
                v
               eye

tan(theta/2) = right/far

                           1
Scaling factor (SF) = ------------
                      tan(theta/2)

if we don't take the inverse tangent, objects will be pushed outside the FOV as
soon as Theta increase.
```


See also ![img](images/G923g.jpg)

### Scaling Z

```
+Z A    .---------------.   Z Far
   |     \             /      A
   |      \           /       |
   |       \         /        |
   |        \       /         |
   V         -------  Z Near  |
              \   /      A    |
               \ /       |    |
                v        |    |
               eye       V    V

                            ZFar
Z Scale Factor (ZSF) = --------------
                       (ZFar - ZNear)

Eye Distance Offset = -ZNear * ZSF (once scaled of course...)
```


### 3D projection

```
[x, y, z] -> [SAR*SF*x, SF*y, z*ZSF - ZNear*ZSF]

     SAR * SF * x          SF * y
x' = ------------     Y' = ------
           z                 z

 | SAR * SF       0        0         0 |
 |    0          SF        0         0 |
 |    0           0       ZSF        1 |
 |    0           0    -Znear * ZSF  0 |

```

