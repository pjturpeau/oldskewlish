/**
 * samples/triangle/main.c
 * 
 * Copyleft (c) Pierre-Jean Turpeau 12/2019
 * <pierrejean AT turpeau DOT net>
s */
#define X_IMPLEMENTATION

#define TITLE "-[ starfield ]-"

#include <x86/x.h>
#include <x86/math.h>
#include <x86/draw.h>

#include "tri.h"

/* ---[ DEFINITIONS ]------------------------------------------------------- */


int main(int argc, char **argv) {
    InitPlatform(argc, (const char **) argv);
    ParseDefaultExeOptions();

    Console("press [SPACE] to test image clear options\n");
    Pause(2000);

    SetVGAMode();

    while(!IsPlatformExit()) {
        BeginFrameLoop();
        ExecuteDefaultInteractions();

            // if(GetLastEvent() == ' ')
            //     draw_type++;

        // Define triangle vertices
        int x0 = 100, y0 = 100;
        int x1 = 199, y1 = 50;
        int x2 = 300, y2 = 190;

        DrawFilledTriangleQ16(x0, y0, x1, y1, x2, y2, 15);

        SetPixel(x0, y0, 2);
        SetPixel(x1, y1, 2);
        SetPixel(x2, y2, 2);

        // x0 -= 2;
        // x1 -= 2;
        // x2 -= 2;

        // // triangle(x0, y0, x1, y1, x2, y2, 1);
        // DrawFilledTriangle_c(x0, y0, x1, y1, x2, y2, 1);
        // SetPixel(x0, y0, 3);
        // SetPixel(x1, y1, 3);
        // SetPixel(x2, y2, 3);

        EndFrameLoop();
    };

    ClosePlatform();

    Console("fps = %d", x86.Time.fps);
    return EXIT_SUCCESS;    
}
