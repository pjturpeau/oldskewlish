/**
 * samples/rotozoom/rotoz.c
 * 
 * Copyleft (c) Pierre-Jean Turpeau 12/2019
 * <pierrejean AT turpeau DOT net>
 */
#define X_IMPLEMENTATION 1

#define TITLE "-[ rotozoom ]-"

#include <x86/x.h>
#include <x86/image.h>
#include "rotozoom.h"

extern unsigned char bomberman0_data[];
extern unsigned int  bomberman0_size;
extern unsigned char bomberman1_data[];
extern unsigned int  bomberman1_size;
extern unsigned char bomberman2_data[];
extern unsigned int  bomberman2_size;


static image_t tex0;
static image_t tex1;
static image_t tex2;

rotozoom_t fx;

static void AnimateRotozoom(float elapsed_time) {
    static bool checker_board = false;
    static int tex_num = 0;
    static int start_time = -1;

    if(GetLastEvent() == ' ')
        checker_board = !checker_board;

    if(start_time == -1)
        start_time = elapsed_time;

    if( (elapsed_time - start_time) > 500) {       /* change the texture every 500ms */
        start_time = elapsed_time;
        tex_num = (tex_num + 1) & 3;
        switch(tex_num) {
            case 0: fx.texture = &tex0; break;
            case 1:
            case 3: fx.texture = &tex1; break;
            case 2: fx.texture = &tex2; break;
        }
    }

    UpdateRotozoom(&fx, elapsed_time);
    DrawRotozoom(&fx, checker_board);
}

int main(int argc, char **argv) {
    InitPlatform(argc, (const char **) argv);
    ParseDefaultExeOptions();

    Console("press [SPACE] to activate checker-board");
    Pause(2000);
    
    SetVGAMode();

    palette_t palette;
    LoadBMP(&tex0, &palette, bomberman0_data, bomberman0_size);
    LoadBMP(&tex1, NULL, bomberman1_data, bomberman1_size);
    LoadBMP(&tex2, NULL, bomberman2_data, bomberman2_size);

    RestorePalette(&palette);

    SetColorPalette(128, 0xFFFFFF); /* for the checker-board version */

    init_rotozoom(&fx, &tex0);
    
    while(!IsPlatformExit()) {
        BeginFrameLoop();
        ExecuteDefaultInteractions();

            if (! IsPaused())
                AnimateRotozoom(GetTime());

        EndFrameLoop();
    };

    ClosePlatform();

    Console("fps = %d", x86.Time.fps);
    return EXIT_SUCCESS;
}
