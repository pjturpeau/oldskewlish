/**
 * x86/extras.h
 * 
 * Copyleft (c) Pierre-Jean Turpeau 10/2024
 * <pierrejean AT turpeau DOT net>
 */
#ifndef X86_EXTRAS_H
#define X86_EXTRAS_H

extern void LightingGradientPalette(int from_index, int to_index, v3f ambiant_color, v3f diffuse_color, v3f specular_color, float shininess);

/* ---[ IMPLEMENTATION ]---------------------------------------------------- */
# ifdef X_IMPLEMENTATION

#  include <x86/math.h>

/**
 * inspired by
 * http://www.c-jump.com/bcc/common/Talk3/OpenGL/Wk06_light/Wk06_light.html
 *
 * The ambiant_color is the base color on which diffuse_color and specular_color
 * are added relative to the color index into the given from-to range.
 * 
 * On the given range:
 *  - t = color index normalized to [0.0 - 1.0]
 *  - d = diffuse_color * sine(c)
 *  - s = specular_color * ( sine(c) ^ shininess )
 *  - output color = ambiant_color + d + s
 */
void LightingGradientPalette(int from_index, int to_index, v3f ambiant_color, v3f diffuse_color, v3f specular_color, float shininess) {
    for (int i = from_index; i <= to_index; i++) {
        v3f color, specular;

        /* linear intensity as [0.0 - 1.0] in the given palette range */
        float t = (float) (i - from_index) / (float) (to_index - from_index);
        
        /* diffuse intensity level thanks to a sine curve */
        float diffuse_intensity = sin(t * M_PI_2);
        Vector3fMul(&color, &diffuse_color, diffuse_intensity);

        /* simplified specular intensity based on the diffuse intensity */
        float specular_intensity = pow(diffuse_intensity, shininess);
        Vector3fMul(&specular, &specular_color, specular_intensity);
        Vector3fAdd(&color, &color, &specular);

        /* and now add the ambiant color */
        Vector3fAdd(&color, &color, &ambiant_color);

        /* clamp as needed */
        pix32_t p;
        p.r = M_MIN(color.r, 1.f) * 255.f;
        p.g = M_MIN(color.g, 1.f) * 255.f;
        p.b = M_MIN(color.b, 1.f) * 255.f;
        SetColorPalette(i, p.v);
    }
}


# endif /* X_IMPLEMENTATION */

#endif /* X86_EXTRAS_H */
