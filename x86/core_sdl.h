/**
 * x86/sdl_core.h
 * 
 * Copyleft (c) Pierre-Jean Turpeau 10/2024
 * <pierrejean AT turpeau DOT net>
 */
#ifndef X86_SDL_CORE_H
#define X86_SDL_CORE_H

# include <x86/x.h>

/* ---[ INLINED IMPLEMENTATIONS ]------------------------------------------- */

/* ---[ IMPLEMENTATION ]---------------------------------------------------- */
# ifdef X_IMPLEMENTATION

#  include <SDL2/SDL.h>
typedef enum {
    DISPLAY_WINDOW,
    DISPLAY_FULLSCREEN_DESKTOP, /* keep desktop resolution */
    DISPLAY_FULLSCREEN_NATIVE, /* switch to the closest resolution */
}
windowing_mode_t;

typedef struct SDL_t {
    SDL_Window * window;
    SDL_Renderer * renderer;
    SDL_Texture * screen_texture;
    palette_t palette;
    windowing_mode_t display_mode;
    int pixel_scale;
    int pixel_format;
} SDL_t;

static struct SDL_t sdl;

/* ---[ FORWARD DECLARATION OF PRIVATE FUNCTIONS ]-------------------------- */

static void _PrintSystemInfos();
static void _SDL_ToggleWindowingMode(windowing_mode_t mode);
static void _SDL_BlitVideoToScreen(int x, int y, u8 * src, u32 * dest);

/* -------------------------------------------------------------------------
 * Execution functions
 * ------------------------------------------------------------------------- */

void InitPlatform(int argc, const char ** argv) {
    memset(&x86, 0, sizeof(X86_t));
    memset(&sdl, 0, sizeof(struct SDL_t));

    x86.State.argc = argc;
    x86.State.argv = (char **) argv;
    x86.Video.backbuffer = 0;
    x86.Video.frontbuffer = 0;
    x86.Video.with_vsync = true;

    sdl.pixel_scale = 2;

    /* SLD_INIT_TIMER is mandatory to use SDL_GetPerformanceXXXXXX() */
    if( SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER | SDL_INIT_AUDIO) != 0) {
        LogError("InitPlatform: Unable to initialize SDL: %s", SDL_GetError());
        SDL_Quit();
        exit(EXIT_FAILURE);
    }

    SDL_LogSetPriority(SDL_LOG_CATEGORY_APPLICATION, SDL_LOG_PRIORITY_INFO);
    if (SDL_SetThreadPriority(SDL_THREAD_PRIORITY_HIGH))
        LogError("InitPlaform: thread priority (%s)", SDL_GetError());

    x86.Cpu.L1_cache_line_bytes = SDL_GetCPUCacheLineSize();
}

void ClosePlatform() {
    SDL_DestroyTexture(sdl.screen_texture);
    SDL_DestroyRenderer(sdl.renderer);
    SDL_DestroyWindow(sdl.window);
    SDL_Quit();
}

void FatalError(const char * fmt, ...) {
    ClosePlatform();
    va_list args;
    va_start(args, fmt);
    SDL_LogMessageV(SDL_LOG_CATEGORY_APPLICATION, SDL_LOG_PRIORITY_CRITICAL, fmt, args);
    va_end(args);
    exit(EXIT_FAILURE);
}

void ParseDefaultExeOptions() {
    const char * scale_str = CheckExeOption("-scale");

    if (scale_str) {
        int scale = atoi(scale_str);
        if(scale > 0)
            sdl.pixel_scale = scale;
        else
            LogError("CLI: -scale with invalid argument: not a number or out of range");
    }
    if (CheckExeOption("-debug")) {
        LogDebug("CLI: debug mode");
        SDL_LogSetPriority(SDL_LOG_CATEGORY_APPLICATION, SDL_LOG_PRIORITY_DEBUG);
    }
    if (CheckExeOption("-novsync")) {
        LogDebug("CLI: no vsync");
        x86.Video.with_vsync = false;
    }
    if (CheckExeOption("-window")) {
        LogDebug("CLI: window mode");
        sdl.display_mode = DISPLAY_WINDOW;
    }
    if (CheckExeOption("-infos")) {
        _PrintSystemInfos();
        ClosePlatform();
        exit(EXIT_SUCCESS);
    }
    if (CheckExeOption("-fullscreen")) {
        LogDebug("CLI: fullscreen desktop mode");
        sdl.display_mode = DISPLAY_FULLSCREEN_DESKTOP;
    }
    if (CheckExeOption("-fullres")) {
        LogDebug("CLI: fullscreen display mode");
        sdl.display_mode = DISPLAY_FULLSCREEN_NATIVE;
    }
    if (CheckExeOption("-h") || CheckExeOption("-?")
        || CheckExeOption("-help") || CheckExeOption("--help") ) {
        Console("%s options:", x86.State.argv[0]);
        Console("\t-debug         force debug level infos");
        Console("\t-fullres       fullscreen (switch to the closest resolution)");
        Console("\t-fullscreen    fullscreen desktop (keep current resolution");
        Console("\t-infos         print platform infos on console");
        Console("\t-novsync       disable synchronization to vertical retrace");
        Console("\t-window        force windowing display");
        Console("\t-scale <n>     window mode pixel size (default: x%d)", sdl.pixel_scale);
        Console(" ");
        ClosePlatform();
        exit(EXIT_SUCCESS);
    }
}

/* -------------------------------------------------------------------------
 * Time functions
 * ------------------------------------------------------------------------- */

inline u32 GetTime() {
    return (u32)SDL_GetTicks();
}

// do not trust Sleep() better use an alternative busy loop
// TODO: to be improved for windows and linux machines
// see https://www.geisswerks.com/ryan/FAQS/timing.html
// see https://stackoverflow.com/questions/23258650/sleep1-and-sdl-delay1-takes-15-ms
void Pause(int ms) {
    int end_time = GetTime() + ms;
    while (GetTime() < end_time) {
        SDL_Delay(0);
    }
}

/* -------------------------------------------------------------------------
 * Video functions
 * ------------------------------------------------------------------------- */

static const u32 _vga_palette[MAX_COLORS] = {
    0x000000, 0x0002aa, 0x14aa00, 0x00aaaa, 0xaa0003, 0xaa00aa, 0xaa5500, 0xaaaaaa,
    0x555555, 0x5555ff, 0x55ff55, 0x55ffff, 0xff5555, 0xfd55ff, 0xffff55, 0xffffff,
    0x000000, 0x101010, 0x202020, 0x353535, 0x454545, 0x555555, 0x656565, 0x757575,
    0x8a8a8a, 0x9a9a9a, 0xaaaaaa, 0xbababa, 0xcacaca, 0xdfdfdf, 0xefefef, 0xffffff,
    0x0004ff, 0x4104ff, 0x8203ff, 0xbe02ff, 0xfd00ff, 0xfe00be, 0xff0082, 0xff0041,
    0xff0008, 0xff4105, 0xff8200, 0xffbe00, 0xffff00, 0xbeff00, 0x82ff00, 0x41ff01,
    0x24ff00, 0x22ff42, 0x1dff82, 0x12ffbe, 0x00ffff, 0x00beff, 0x0182ff, 0x0041ff,
    0x8282ff, 0x9e82ff, 0xbe82ff, 0xdf82ff, 0xfd82ff, 0xfe82df, 0xff82be, 0xff829e,
    0xff8282, 0xff9e82, 0xffbe82, 0xffdf82, 0xffff82, 0xdfff82, 0xbeff82, 0x9eff82,
    0x82ff82, 0x82ff9e, 0x82ffbe, 0x82ffdf, 0x82ffff, 0x82dfff, 0x82beff, 0x829eff,
    0xbabaff, 0xcabaff, 0xdfbaff, 0xefbaff, 0xfebaff, 0xfebaef, 0xffbadf, 0xffbaca,
    0xffbaba, 0xffcaba, 0xffdfba, 0xffefba, 0xffffba, 0xefffba, 0xdfffba, 0xcaffbb,
    0xbaffba, 0xbaffca, 0xbaffdf, 0xbaffef, 0xbaffff, 0xbaefff, 0xbadfff, 0xbacaff,
    0x010171, 0x1c0171, 0x390171, 0x550071, 0x710071, 0x710055, 0x710039, 0x71001c,
    0x710001, 0x711c01, 0x713900, 0x715500, 0x717100, 0x557100, 0x397100, 0x1c7100,
    0x097100, 0x09711c, 0x067139, 0x037155, 0x007171, 0x005571, 0x003971, 0x001c71,
    0x393971, 0x453971, 0x553971, 0x613971, 0x713971, 0x713961, 0x713955, 0x713945,
    0x713939, 0x714539, 0x715539, 0x716139, 0x717139, 0x617139, 0x557139, 0x45713a,
    0x397139, 0x397145, 0x397155, 0x397161, 0x397171, 0x396171, 0x395571, 0x394572,
    0x515171, 0x595171, 0x615171, 0x695171, 0x715171, 0x715169, 0x715161, 0x715159,
    0x715151, 0x715951, 0x716151, 0x716951, 0x717151, 0x697151, 0x617151, 0x597151,
    0x517151, 0x51715a, 0x517161, 0x517169, 0x517171, 0x516971, 0x516171, 0x515971,
    0x000042, 0x110041, 0x200041, 0x310041, 0x410041, 0x410032, 0x410020, 0x410010,
    0x410000, 0x411000, 0x412000, 0x413100, 0x414100, 0x314100, 0x204100, 0x104100,
    0x034100, 0x034110, 0x024120, 0x014131, 0x004141, 0x003141, 0x002041, 0x001041,
    0x202041, 0x282041, 0x312041, 0x392041, 0x412041, 0x412039, 0x412031, 0x412028,
    0x412020, 0x412820, 0x413120, 0x413921, 0x414120, 0x394120, 0x314120, 0x284120,
    0x204120, 0x204128, 0x204131, 0x204139, 0x204141, 0x203941, 0x203141, 0x202841,
    0x2d2d41, 0x312d41, 0x352d41, 0x3d2d41, 0x412d41, 0x412d3d, 0x412d35, 0x412d31,
    0x412d2d, 0x41312d, 0x41352d, 0x413d2d, 0x41412d, 0x3d412d, 0x35412d, 0x31412d,
    0x2d412d, 0x2d4131, 0x2d4135, 0x2d413d, 0x2d4141, 0x2d3d41, 0x2d3541, 0x2d3141,
    0x000000, 0x000000, 0x000000, 0x000000, 0x000000, 0x000000, 0x000000, 0x000000
};

void SetVGAMode()
{
    x86.Video.frontbuffer = MemAlloc(W*H);
    x86.Video.backbuffer = MemAlloc(W*H);
    
    for (int i = 0; i < MAX_COLORS; i++)
        SetColorPalette(i, _vga_palette[i]);

    LogDebug("video: framebuffer resolution is %dx%d", W, H);

    uint32_t flags = SDL_WINDOW_SHOWN;

    sdl.window = SDL_CreateWindow(TITLE,
        SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
        W * sdl.pixel_scale, H * sdl.pixel_scale, flags);

    SDL_ShowCursor(SDL_ENABLE);

    /* create renderer and texture */

    flags = SDL_RENDERER_ACCELERATED;
    if (x86.Video.with_vsync)
        flags |= SDL_RENDERER_PRESENTVSYNC;

    SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "nearest");

    sdl.pixel_format = SDL_GetWindowPixelFormat(sdl.window);
    LogDebug("video: pixel format is '%s'",
        SDL_GetPixelFormatName(sdl.pixel_format));

    if (sdl.pixel_format != SDL_PIXELFORMAT_RGB888)
        FatalError("video: unknown or not supported pixel format: %s",
            SDL_GetPixelFormatName(sdl.pixel_format));
    
    sdl.renderer = SDL_CreateRenderer(sdl.window, -1, flags);
    sdl.screen_texture = SDL_CreateTexture(sdl.renderer, sdl.pixel_format,
        SDL_TEXTUREACCESS_STREAMING, W, H);

    SDL_RenderSetLogicalSize(sdl.renderer, W, H);

    _SDL_ToggleWindowingMode(sdl.display_mode);
}

void BeginFrameLoop() {
    assert(x86.Video.backbuffer != 0);
    UpdatePlatformInteractions();
}

void EndFrameLoop() {
    DrawPalette();
    SwapScreenBuffer();
}

void SwapScreenBuffer() {
    u8 * screen_pixels;
    int pitch;
    void ** pixels = (void **)&screen_pixels;

    memcpy(x86.Video.frontbuffer, x86.Video.backbuffer, W*H);

    if (! SDL_LockTexture(sdl.screen_texture, NULL, pixels, &pitch)) {
        switch( pitch ) {
            case (W*4):
                _SDL_BlitVideoToScreen(0, 0, x86.Video.frontbuffer, (u32 *)screen_pixels);
                break;

            default:
                FatalError("video: Unsupported pitch %d (width %d)", pitch, W);
        }

        SDL_UnlockTexture(sdl.screen_texture);
        SDL_RenderCopy(sdl.renderer, sdl.screen_texture, NULL, NULL);
        SDL_RenderPresent(sdl.renderer);
    }
    else
        LogError("video: Unable to lock screen texture");
}

// void TakeScreenshot() {
//     u32 * screen_pixels;
//     int pitch;
//     void ** pixels = (void **)&screen_pixels;

//     if (! SDL_LockTexture(sdl.screen_texture, NULL, pixels, &pitch)) {
//         switch (pitch) {
//             case (W*4):
//                 SaveTGA(screen_pixels, W, H, 24);
//                 break;

//             default:
//                 LogError("screenshot: Unsupported pitch %d (width %d)", pitch, W);
//         }

//         SDL_UnlockTexture(sdl.screen_texture);
//     }
//     else
//         LogError("screenshot: Unable to lock screen texture");
// }

/* -------------------------------------------------------------------------
 * Pixels
 * ------------------------------------------------------------------------- */

/* -------------------------------------------------------------------------
 * Palette
 * ------------------------------------------------------------------------- */

/* rgb as 0xRRGGBB value */
void SetColorPalette(int index, u32 rgb888) {
    assert(index >= 0 && index < MAX_COLORS );
    sdl.palette.colors[index].v = rgb888;
}

void SavePalette(palette_t * palette_out) {
    memcpy(palette_out->colors, sdl.palette.colors, sizeof(palette_t));
}

void RestorePalette(const palette_t * palette) {
    memcpy(sdl.palette.colors, palette->colors, sizeof(palette_t));
}

/* -------------------------------------------------------------------------
 * Interactivity
 * ------------------------------------------------------------------------- */

void UpdatePlatformInteractions() {
    const Uint8 * state = SDL_GetKeyboardState(NULL);
    x86.Input.button_state[BTN_UP] = state[SDL_SCANCODE_UP] != 0;
    x86.Input.button_state[BTN_DOWN] = state[SDL_SCANCODE_DOWN] != 0;
    x86.Input.button_state[BTN_LEFT] = state[SDL_SCANCODE_LEFT] != 0;
    x86.Input.button_state[BTN_RIGHT] = state[SDL_SCANCODE_RIGHT] != 0;

    SDL_Event event;
    if (SDL_PollEvent(&event)) {
        switch (event.type) {
            case SDL_QUIT:
                x86.Input.exit_event = true;
                x86.Input.last_event = EVT_QUIT;
                break;

            case SDL_KEYDOWN:
                switch( event.key.keysym.sym )
                {
                case SDLK_ESCAPE:
                    x86.Input.exit_event = true;
                    x86.Input.last_event = KEY_ESC;
                    break;

                case SDLK_RETURN:
                    if( event.key.keysym.mod & KMOD_CTRL )
                        x86.Input.last_event =  KEY_CTRL_ENTER;
                    else if( event.key.keysym.mod & KMOD_ALT )
                        x86.Input.last_event =  KEY_ALT_ENTER;
                    else
                        x86.Input.last_event = KEY_ENTER;
                    break;

                case SDLK_UP: x86.Input.last_event = KEY_UP; break;
                case SDLK_DOWN: x86.Input.last_event = KEY_DOWN; break;
                case SDLK_LEFT: x86.Input.last_event = KEY_LEFT; break;
                case SDLK_RIGHT: x86.Input.last_event = KEY_RIGHT; break;
                case SDLK_PAGEDOWN: x86.Input.last_event = KEY_PAGEDOWN; break;
                case SDLK_PAGEUP: x86.Input.last_event = KEY_PAGEUP; break;
                case SDLK_END: x86.Input.last_event = KEY_END; break;
                case SDLK_HOME: x86.Input.last_event = KEY_HOME; break;
                default:
                    x86.Input.last_event = event.key.keysym.sym;
                } 
                break;
        }
        return;
    }
    x86.Input.last_event = EVT_NONE;
}

void ExecuteDefaultInteractions() {
    int event = GetLastEvent();
    switch (event)
    {
        case KEY_ALT_ENTER:
        case KEY_CTRL_ENTER:
            if(sdl.display_mode == DISPLAY_WINDOW) {
                if(event == KEY_ALT_ENTER)
                    _SDL_ToggleWindowingMode(DISPLAY_FULLSCREEN_DESKTOP);
                else
                    _SDL_ToggleWindowingMode(DISPLAY_FULLSCREEN_NATIVE);
            } else
                _SDL_ToggleWindowingMode(DISPLAY_WINDOW);
            break;

        case 'c': /* show color palette */
        case 'C':
            x86.State.show_palette = (x86.State.show_palette + 1) % 3;
            if (!x86.State.show_palette) ClearScreen(0);
            break;

        case 'p': /* pause */
        case 'P':
            x86.State.pause = ! x86.State.pause;
            break;

        case 's': /* screenshot */
        case 'S':
            TakeScreenshot();
            break;

        case KEY_TAB:
            x86.State.show_fps = ! x86.State.show_fps;
            break;

        default: /* push back the event and delegate */
            x86.Input.last_event = event;
            break;
    }
}

/* -------------------------------------------------------------------------
 * Numbers
 * ------------------------------------------------------------------------- */


/* -------------------------------------------------------------------------
 * Console
 * ------------------------------------------------------------------------- */

void Console(const char * fmt, ...) {
    va_list args;
    va_start(args, fmt);
    SDL_LogMessageV(SDL_LOG_CATEGORY_APPLICATION, SDL_LOG_PRIORITY_INFO, fmt, args);
    va_end(args);
}

void LogDebug(const char * fmt, ...) {
    va_list args;
    va_start(args, fmt);
    SDL_LogMessageV(SDL_LOG_CATEGORY_APPLICATION, SDL_LOG_PRIORITY_DEBUG, fmt, args);
    va_end(args);
}

void LogError(const char * fmt, ...) {
    va_list args;
    va_start(args, fmt);
    SDL_LogMessageV(SDL_LOG_CATEGORY_APPLICATION, SDL_LOG_PRIORITY_ERROR, fmt, args);
    va_end(args);
}

/* -------------------------------------------------------------------------
 * Memory
 * ------------------------------------------------------------------------- */

void * MemAllocAlign(int size, int align) {
    return _mm_malloc(size, align);
}

void * MemAlloc(int size) {
    return _mm_malloc(size, x86.Cpu.L1_cache_line_bytes);
}

void MemFree(void * area) {
    if(area) _mm_free(area);
}


/* -------------------------------------------------------------------------
 * Private functions
 * ------------------------------------------------------------------------- */

#  ifdef X86
struct features_t {
    const char *name;
    SDL_bool (*test)();
} _x86_feats[] = {
    {"RDTSC", SDL_HasRDTSC},
    {"MMX", SDL_HasMMX},
    {"SSE", SDL_HasSSE},
    {"SSE2", SDL_HasSSE2},
    {"SSE3", SDL_HasSSE3},
    {"SSE41", SDL_HasSSE41},
    {"SSE42", SDL_HasSSE42},
    {"AVX", SDL_HasAVX},
    {"AVX2", SDL_HasAVX2},
    {"AVX512F", SDL_HasAVX512F}
};
#  endif /* X86 */

static void _PrintSystemInfos() {
    int mode_count = SDL_GetNumDisplayModes(0);

    Console("Available system RAM %d Mb", SDL_GetSystemRAM());
    Console("CPU Cache Line: %d-bytes\n", x86.Cpu.L1_cache_line_bytes);

#  ifdef X86
    Console("CPU Features: ");
    for(int i = 0; i < sizeof(_x86_feats)/sizeof(_x86_feats[0]); i++) {
        if(_x86_feats[i].test())
            Console("%s ", _x86_feats[i].name);
    }
    Console("\n");
#  endif

    Console("CPU Perf Frequency: %I64u Hz\n", SDL_GetPerformanceFrequency());

    u64 acc = 0;
    u64 count = 0;
    u64 delta;
    for(int i = 0; i < 1000; i++) {
        delta = SDL_GetPerformanceCounter();
        delta = SDL_GetPerformanceCounter()-delta;
        acc += delta;
        count++;
    }
    f64 microsec = (1000.0f * 1000.0f* (f64)acc)/(f64)SDL_GetPerformanceFrequency();
    Console("CPU Perf Counter delay: %g micro-seconds\n", microsec);

    Console("SDL2 platform has %d video modes", mode_count);

    for (int i = 0; i < mode_count; i++) {
        SDL_DisplayMode dm;
        if( SDL_GetDisplayMode(0, i, &dm) < 0) {
            LogError("console_infos: SDL_GetDisplayMode failed (%d)", SDL_GetError());
            continue;
        }
        Console("video mode #%02d : %dx%d %s", i, dm.w, dm.h,
            SDL_GetPixelFormatName(dm.format));
    }
}

static void _SDL_ToggleWindowingMode(windowing_mode_t mode)
{
    #define WINDOW_TOGGLE_DELAY  100

    switch( mode ) {
        case DISPLAY_WINDOW:
            SDL_Delay(WINDOW_TOGGLE_DELAY); /* wait for current callback to finish */
            SDL_SetWindowFullscreen(sdl.window, 0);
            SDL_SetWindowSize(sdl.window, W * sdl.pixel_scale, H * sdl.pixel_scale);
            SDL_ShowCursor(SDL_ENABLE);
            break;

        case DISPLAY_FULLSCREEN_DESKTOP:
            SDL_SetWindowFullscreen(sdl.window, SDL_WINDOW_FULLSCREEN_DESKTOP);
            SDL_ShowCursor(SDL_DISABLE);
            break;
        
        case DISPLAY_FULLSCREEN_NATIVE:
            SDL_Delay(WINDOW_TOGGLE_DELAY); /* wait for current callback to finish */
            SDL_SetWindowSize(sdl.window, W, H); /* to let SDL select the closest resolution */
            SDL_SetWindowFullscreen(sdl.window, SDL_WINDOW_FULLSCREEN);
            SDL_ShowCursor(SDL_DISABLE);
            break;
    }

    sdl.display_mode = mode;

    int display_index = SDL_GetWindowDisplayIndex(sdl.window);
    SDL_DisplayMode dm;
    if (SDL_GetCurrentDisplayMode(display_index, &dm) != 0) {
        LogDebug("SDL_GetCurrentDisplayMode failed: %s", SDL_GetError());
        return;
    }
    LogDebug("video: resolution on display #%02d is %dx%d @ %d Hz", display_index, dm.w, dm.h, dm.refresh_rate);
}


/* blit the video 8-bit chunky frontbuffer to a 32-bit texture */
static void _SDL_BlitVideoToScreen(int x, int y, u8 * src, u32 * dest) {
    /*
    if(video.warping) {
        static float itime = 0;
        itime += .3f;

        pix32 c;
        v2s * uv  = video.warping;
        int r, g, b;
        for(int j = 0; j < H; j++) {
            for(int i = 0; i < W; i++) {

                if(uv->u < 0  || uv->u >= W || uv->v < 0 || uv->v >= H) {
                    *(dest++) = 0;
                }
                else
                {
                    int offset = uv->u + uv->v * W;
                    c.v = sdl.palette.colors[src[offset]].v;

                    r = c.r + sin(uv->v + itime) * 10;
                    g = c.g + sin(uv->v + itime) * 10; 
                    b = c.b + sin(uv->v + itime) * 10;

                    if(r < 0) r = 0; else if(r > 255) r = 255;
                    if(g < 0) g = 0; else if(g > 255) g = 255;
                    if(b < 0) b = 0; else if(b > 255) b = 255;

                    c.r = r;
                    c.g = g;
                    c.b = b;

                    *(dest++) = c.v; 
                }
                uv++;
            }
        }
    } else*/
    if( x || y) {
        const int clip_left = (x < 0) ? -x : 0;
        const int clip_top = (y < 0) ? -y : 0;
        const int clip_right = M_MIN(W - x, W);
        const int clip_bottom = M_MIN(H - y, H);

        u32 * d = dest;
        memset(d, 0, W * H * 4);

        for(int j = clip_top; j < clip_bottom; j++) {
            u8 * s = src + clip_left + j * W;
            u32 * d = dest + x + clip_left + (y+j) * W;
            for(int i = clip_right - clip_left; i > 0; i--)
                *(d++) = sdl.palette.colors[*(s++)].v;
        }
    } else {
        for(int j = W*H; j > 0; j--) 
            *(dest++) = sdl.palette.colors[*(src++)].v;
    }

// #  if !defined(DISABLE_LOAD_TRACKING)
//     if (! x86.State.show_fps) return;

//     int index = video.frame_count;
//     for(int k = 0; k < FRAME_MAX_COUNT; k++) {
        
//         u32 x = 60 * video.frame_load_history[index] / video.frame_time_history[index];

//         index = (index + 1) & (FRAME_MAX_COUNT-1);

//         x = (x >= W-4) ? W-4 : x;
        
//         dest = destination + 2 + (k+2) * W;
//         for(int i = 0; i < x; i++)
//             *(dest++) = 0xFF00006D;
//         *(dest++) = 0xFFA0A0FF;
//         for(int i = x ; i < 60; i++) 
//             *(dest++) = 0xFF00002D;
//         destination[62 + (k+2) * W] = 0xFF00FF00;
//     }
// #  endif
}

# endif /* X_IMPLEMENTATION */

#endif /* X86_SDL_CORE_H */