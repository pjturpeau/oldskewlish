/**
 * samples/breakout/breakout.c
 * 
 * Copyleft (c) Pierre-Jean Turpeau 11/2020
 * <pierrejean AT turpeau DOT net>
 *
 * breakout style game mock-up, mainly because I like simple physics effects 
 * found in pico-8 Breakout Hero https://www.lexaloffle.com/bbs/?pid=53977
 */
#define X_IMPLEMENTATION

#define TITLE "-[ breakout ]-"

#include <x86/x.h>
#include <x86/draw.h>
#include "particles.h"

#include "ball.h"
#include "paddle.h"

// bricks color: jaune, rouge, vert, bleu, cyan, gris, orange, rose
// +1 variante plus claire par couleur (ou blanc) pour l'effet 3D minimaliste

// fonds: bleu, rouge, vert, mauve foncés 
// +1 variante plus foncée pour l'ombre


u8 explode_colors[] = {5, 4, 3, 2, 1};
u8 tail_colors[] = {5, 12, 13, 14, 10, 15};

typedef struct {
    
    int x, y, w, h;
    int type;
    //image * texture;
    bool alive;
}
brick_t;

int bricks_count;
brick_t * bricks;

void TailParticles(int x, int y) {
    int count = 10;
    while(count--) {
        float r = 2.5f;
        AddOneParticle(
            x + GetFloatRandomValue(-r, r),
            y + GetFloatRandomValue(-r, r),
            20.f + GetFloatRandomValue(0.f, 15.f),
            1.f,
            1.f,
            0, 0, 0, NULL, 
            tail_colors, sizeof(tail_colors));
    }
}

void ExportParticles(int x, int y) {
    int count = 10;
    while(count--) {
        AddOneParticle(
            x,
            y,
            30.f + GetFloatRandomValue(0.f, 25.f),
            1.f,
            6.f,
            -.1f,
            GetFloatRandomValue(0.f, 2.f),
            GetFloatRandomValue(0.F, M_TAU),
            NULL,
            explode_colors, sizeof(explode_colors));
    }
}

void UpdateGame(float elapsed_time, float time_step) {

    UpdateBall(time_step);
    UpdatePaddle();

    TailParticles(ball.pos.x, ball.pos.y);

    //update_bricks(time_step);
    
    // int e = last_event();
    // if(e == ' ')
    //     shake(10);

    // if(e == 'x')
    //     ExportParticles(paddle.x, paddle.y);

    // if(button(BTN_LEFT) || button(BTN_RIGHT) || button(BTN_UP) || button(BTN_DOWN))
    //     TailParticles(paddle.x, paddle.y);

    UpdateParticles(time_step * .1f);

    ClearScreen(0);

#define WB 20
#define HB 8

    DrawRectangle_c(10, 10, 10+WB, 10+HB, 5);

    DrawParticles();
    DrawBall();
    DrawPaddle();
}


int main(int argc, char **argv) {
    InitPlatform(argc, (const char **) argv);
    ParseDefaultExeOptions();
    SetVGAMode();

    InitPaddle();
    InitBall();

    u32 prev_time = GetTime();

    while (! IsPlatformExit()) {
        BeginFrameLoop();
        ExecuteDefaultInteractions();

        if (!IsPaused()) {
            UpdateGame(GetTime(), GetTime() - prev_time);
            prev_time = GetTime();
        }

        EndFrameLoop();
    };

    ClosePlatform();

    Console("fps = %d", x86.Time.fps);
    return EXIT_SUCCESS;
}
