/**
 * samples/pixelation/pixelation.c
 * 
 * Copyleft (c) Pierre-Jean Turpeau 02/2018
 * <pierrejean AT turpeau DOT net>
 *
 */
#define X_IMPLEMENTATION

#define TITLE "-[ pixelate ]-"
#include <x86/x.h>
#include <x86/image.h>
#include <x86/timeline.h>

extern unsigned char minitel_data[];
extern unsigned int  minitel_size;

image_t image;
float pixelate_factor = 1.0;

palette_t black_palette;
palette_t image_palette; 

void AnimateTimeline(u32 system_time) {
    UpdateTimelineTime(system_time);

    FadePaletteTime(1500, 1500, &black_palette, &image_palette);
    AnimateFloatLerpTime(1500, 2000, &pixelate_factor, 20, 1);

    FadePaletteTime (1500+2000+1000, 2000, &image_palette, &black_palette);
    AnimateFloatLerpTime(1500+2000+1000, 2000, &pixelate_factor, 1, 20);

    JumpTimelineTo(1500, 1500+2000+1000+2000+1000);
}

int main(int argc, char **argv) {
    InitPlatform(argc, (const char **) argv);
    ParseDefaultExeOptions();

    SetVGAMode();

    ClearPalette(0);
    SavePalette(&black_palette);    

    LoadBMP(&image, &image_palette, minitel_data, minitel_size);
    RestorePalette(&image_palette);

    InitTimeline();

    while(!IsPlatformExit()) {
        BeginFrameLoop();
        ExecuteDefaultInteractions();

            if (!IsPaused()) {
                AnimateTimeline(GetTime());
                DrawPixelateImage(&image, 0, 0, pixelate_factor);
            }

        EndFrameLoop();
    };

    FreeImageData(&image);
    ClosePlatform();

    Console("fps = %d\n", x86.Time.fps);
    return EXIT_SUCCESS;    
}
