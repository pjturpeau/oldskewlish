/**
 * samples/floor/floor.c
 * 
 * Copyleft (c) Pierre-Jean Turpeau 01/2020
 * <pierrejean AT turpeau DOT net>
 * 
 * Very primitive floor-plane, not as polished as a true mode7...
 */
#define X_IMPLEMENTATION 1

#define TITLE "-[ floor plane ]-"

#include <x86/x.h>
#include <x86/image.h>
#include <x86/fp16.h>

#include <math.h>

extern unsigned char texture_data[];
extern unsigned int  texture_size;

#define FLOOR_PLANE_FOV (60 * M_PI / 180) /* 60 degrees in radians */
#define SKY_SIZE  40

image_t texture;
int total_shading_levels;

void DrawFloorPlane(float elapsed_time)
{
    float time = elapsed_time * 0.001;

    /* camera position in the texture */
    int user_x = 256 * cos(time);
    int user_y = 256 * sin(time);

    int camera_height = 150 + 100 * sin(time * 0.5) * sin(time * 1.25);

    float user_angle = M_PI * sin(time * 0.25f) * sin(time*0.15);

    float angle_left  = user_angle - FLOOR_PLANE_FOV/2;
    float angle_right = user_angle + FLOOR_PLANE_FOV/2;

    const float RENDERING_HEIGHT_INV = 1.0f / (H - SKY_SIZE - 1);

    u8 * video = GetBackbuffer() + SKY_SIZE * W;

    /* draw the floor plane */
    for(int y = SKY_SIZE; y < H; y++) {

        int ray_length = 100 * camera_height / (y - SKY_SIZE + 1);

        /* this whole part could be pre-calc in a total_shading_levels array */
        float height_ratio = 1.0 - (float) (y - SKY_SIZE) * RENDERING_HEIGHT_INV;
        int shading_level = (total_shading_levels-1) * (height_ratio * height_ratio * height_ratio); /* quad easing */ 
        shading_level *= texture.color_count;

        /* this part should be optimized using pre-calc fixed point sin/cos */
        fp16 tex_startx = FloatToFP16(ray_length * sin(angle_left));
        fp16 tex_starty = FloatToFP16(ray_length * cos(angle_left));

        fp16 tex_endx = FloatToFP16(ray_length * sin(angle_right));
        fp16 tex_endy = FloatToFP16(ray_length * cos(angle_right));

        fp16 tex_xadd = (tex_endx - tex_startx) / W;
        fp16 tex_yadd = (tex_endy - tex_starty) / H;

        fp16 tex_x = tex_startx;
        fp16 tex_y = tex_starty;

        for( int i = 0; i < W; i++ ) {
            int itx = FP16ToInt(tex_x) + user_x;
            int ity = FP16ToInt(tex_y) + user_y;

            *(video++) = texture.pixels[(itx&0xFF) + ((ity<<8)&0xFF00)] + shading_level;

            tex_x += tex_xadd;
            tex_y += tex_yadd;
        }
    }

    /* clear the sky part */
    video = GetBackbuffer();
    for(int j = 0 ; j < SKY_SIZE; j++, video += W)
        memset(video, 0, W);
    
    /* todo: add stars in the sky */
}

int main(int argc, char **argv) {
    InitPlatform(argc, (const char **) argv);
    ParseDefaultExeOptions();

    SetVGAMode();

    palette_t palette;

    LoadBMP(&texture, &palette, texture_data, texture_size);
    RestorePalette(&palette);

    /* use the whole palette for the shadows */
    total_shading_levels = MAX_COLORS / texture.color_count;

    DarkenPaletteToBlack(0, texture.color_count, total_shading_levels);

    while(!IsPlatformExit()) {
        BeginFrameLoop();
        ExecuteDefaultInteractions();

            if (! IsPaused())
                DrawFloorPlane(GetTime());

        EndFrameLoop();
    };

    ClosePlatform();

    Console("fps = %d", x86.Time.fps);
    return EXIT_SUCCESS;
}
