/**
 * samples/checkbrd/rotarycb.h
 * 
 * Copyleft (c) Pierre-Jean Turpeau 04/2020
 * <pierrejean AT turpeau DOT net>
 * 
 * rotozoom checker board
 */
#ifndef ROTARY_CB_H
#define ROTARY_CB_H

#include <math.h>
#include <x86/image.h>

#define TEXTURE_SIZE 256

extern void InitRotaryCheckerBoard();
extern void UpdateRotaryCheckerBoard(float dt);
extern void DrawRotaryCheckerBoard();

/* ---[ IMPLEMENTATION ]---------------------------------------------------- */

# ifdef X_IMPLEMENTATION

static int position_x;
static int position_y;

static float angle;
static float scale_factor;
static float time_speed;

static image_t * texture;

/** texture has to be 256x256x8 bits */
void InitRotaryCheckerBoard() {
    position_x = 0;
    position_y = 0;
    angle = 0.f;
    scale_factor = 1.f;
    time_speed = 1.f;

    texture = CreateImage(TEXTURE_SIZE, TEXTURE_SIZE);

    for(int j = 0; j < TEXTURE_SIZE; j++) {
        bool p1 = j < TEXTURE_SIZE/2 ? true: false; 
        for(int i = 0; i < TEXTURE_SIZE; i++) {
            bool p2 = i < TEXTURE_SIZE/2 ? p1 : !p1;
            SetImagePixel(texture, i, j, p2 ? 1 : 0);
        }
    }
}

void UpdateRotaryCheckerBoard(float dt) {
    // angle += dt * time_speed;
    angle = dt * time_speed;

    scale_factor = 1.75 + cos(angle *0.75) - 0.5 * sin(angle *1.25);
    
    /* position of the center of the framebuffer in the texture coordinate system */
    position_x = (TEXTURE_SIZE>>1) * cos(angle * 0.75);
    position_y = (TEXTURE_SIZE>>1) * sin(angle * 2);
}

# define RCB_NB_LAYERS 4

void DrawRotaryCheckerBoard()
{
    int Ax[RCB_NB_LAYERS], Ay[RCB_NB_LAYERS];
    int dx_DX[RCB_NB_LAYERS], dy_DX[RCB_NB_LAYERS];
    int dx_DY[RCB_NB_LAYERS], dy_DY[RCB_NB_LAYERS];

    float scale = (1<<16) * scale_factor;

    float Axr = scale * ((position_x - (W>>1)) * cos(angle)
            + (position_y - (H>>1)) * sin(angle));
    
    float Ayr = -scale * ((position_x - (W>>1)) * sin(angle)
            + (position_y - (H>>1)) * cos(angle));

    float dx_DXr =  scale * W * cos(angle) / W;
    float dy_DXr = -scale * W * sin(angle) / W;

    float dx_DYr = scale * H * sin(angle) / H;
    float dy_DYr = scale * H * cos(angle) / H;

    float zoom_factor = 1.f;

    for(int k = 0; k < RCB_NB_LAYERS; k++) {    

        Ax[k] = Axr;
        Ay[k] = Ayr; 

        dx_DX[k] = zoom_factor * dx_DXr;
        dy_DX[k] = zoom_factor * dy_DXr;

        dx_DY[k] = zoom_factor * dx_DYr;
        dy_DY[k] = zoom_factor * dy_DYr;

        zoom_factor *= 1.5f;
    }

    int dest_offset = 0;

    int x[RCB_NB_LAYERS], y[RCB_NB_LAYERS];

    uint8_t c;
    int k;

    for(int j = H; j > 0; j--)
    {
        for(k = 0; k < RCB_NB_LAYERS; k++) {
            x[k] = Ax[k];
            y[k] = Ay[k];
        }

        for(int i = W; i > 0; i--)
        {
            for(k = 0; k < RCB_NB_LAYERS; k++) {
                c = texture->pixels[((x[k]>>16)&0xFF) + ((y[k]>>8)&0xFF00)];
                // c = (((x[k]>>16)&0xFF) ^ ((y[k]>>16)&0xFF)) & 128;
                if(c) { c =/* c - 128 +*/ k+1; break; }
            }

            GetBackbuffer()[dest_offset++] = c;

            for(k = 0; k < RCB_NB_LAYERS; k++) {
                x[k] += dx_DX[k];
                y[k] += dy_DX[k];
            }
        }

        for(k = 0; k < RCB_NB_LAYERS; k++) {
            Ax[k] += dx_DY[k];
            Ay[k] += dy_DY[k];
        }
    }
}

# endif /* X_IMPLEMENTATION */

#endif /* ROTARY_CB_H */
