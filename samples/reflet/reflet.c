/**
 * samples/water-mirror/water-mirror.c
 * 
 * Copyleft (c) Pierre-Jean Turpeau 05/2020
 * <pierrejean AT turpeau DOT net>
 *
 */
#define X_IMPLEMENTATION

#define TITLE "-[ water reflect ]-"
#include <x86/x.h>
#include <x86/image.h>

extern unsigned char new_york_data[];
extern unsigned int  new_york_size;

image_t image;
palette_t palette;

void DrawReflet()
{
    DrawImage_c(&image, 0, 0);

    const int water_height = H - image.h;

    static float ht = 0;
    ht += 0.15f;

    static float vt = 0;
    vt += .1f;

    for(int j = 0; j < water_height; j++) {

        /* vertical movements */
        int y = water_height - j + sin(j/3.f +vt)*j/20.f;

        /* horizontal ripples */
        int x = cos(j/4.f+ht)*j/25.f;

        if(y < 0) y = 0; else if(y >= H) y = H-1;

        u32 * src = (u32 *) (GetBackbuffer() + y * W + x);
        u32 * dst = (u32 *) (GetBackbuffer() + (image.h + j) * W);

        for(int i = 0; i < W/4; i++) {
            *(dst++) = *(src++)
                | 0x80808080; /* move color index to the darker set */
        }
    }
}


int main(int argc, char ** argv) {
    InitPlatform(argc, (const char **) argv);
    ParseDefaultExeOptions();

    SetVGAMode();

    LoadBMP(&image, &palette, new_york_data, new_york_size);

    /* image has 128 useful colors, now generate a darker set for the water */

    for(int k = 0; k < MAX_COLORS/2; k++) {
        palette.colors[k+128].r = palette.colors[k].r / 2;
        palette.colors[k+128].g = palette.colors[k].g / 2;
        palette.colors[k+128].b = palette.colors[k].b / 2;
    }

    RestorePalette(&palette);

    while(!IsPlatformExit()) {
        BeginFrameLoop();
        ExecuteDefaultInteractions();

        if (! IsPaused()) {
            DrawReflet();
        }

        EndFrameLoop();
    };

    FreeImageData(&image);
    ClosePlatform();

    Console("fps = %d", x86.Time.fps);
    return 0;
}
