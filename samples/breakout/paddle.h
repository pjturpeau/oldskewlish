
#include <x86/x.h>
#include <x86/draw.h>

v2s paddle;

#define PADDLE_W 30
#define PADDLE_H 10

int _paddle;

void InitPaddle() {
    paddle.x = W/2;
    paddle.y = H/2;
    _paddle = W/2 - PADDLE_W/2;
}

void DrawPaddle() {
    DrawRectangle(_paddle, H-PADDLE_H-5, _paddle+PADDLE_W, H-5, 12);
}

void UpdatePaddle() {
    
    if(IsButtonPressed(BTN_LEFT)) {
        _paddle -= 5;
    } else if(IsButtonPressed(BTN_RIGHT)) {
        _paddle += 5;
    }

    if(IsButtonPressed(BTN_UP))
        paddle.y -= 2;
    if(IsButtonPressed(BTN_DOWN))
        paddle.y += 2;
}