
#define X_IMPLEMENTATION 1

#include "test.h"

#define NRUNS 5
#define NLOOPS 10000000 /* big number to ensure ramp-up of CPU throttle */

/* this test evaluate the impact of branching on clamping/saturation */

/* trivial branching option */
unsigned char clamp1(int n) {
    return (n > 255) ? 255 : n;
}

/* first non branching option */
unsigned char clamp2(int n) {
    return (-(n >= 0) & n) | -(n >= 255);
}

/* second non branching option */
unsigned char clamp3(int n) {
    n &= -(n >= 0);
    return n | ~-!(n & -256);
}

typedef unsigned char (*clamp_func)(int n);

int rand_lut[65536];

void test_clamp(clamp_func f) {
    int ticks[NRUNS];

    /* run 10 times to ensure CPU ramp-up */
    for(int k = 0; k < NRUNS; k++) {
        int start = read_time();
        for(int i = 0; i < NLOOPS; i++) {
            int value = rand_lut[i&65535];
            unsigned char v = f(value);
        }
        int end = read_time();
        ticks[k] = end-start;
    }

    /* keep the best value */
    int best = ticks[0];
    for(int k = 1; k < NRUNS; k++)
        if(ticks[k] < best) best = ticks[k];
    
    Console("%u ticks - %.1f ticks/call\n", best, (float)best/(float)NLOOPS);
}

int main(int argc, char **argv) {
    InitPlatform(argc, (const char **) argv);
    ParseDefaultExeOptions();

    uint32_t start, end;

    /* generate random numbers */

    float low = 0, high = 0;
    for(int i = 0; i < 65536; i++) {
        rand_lut[i] = rand() / (RAND_MAX / 512);
        if(rand_lut[i] > 255) high++; else low++;
    }
    /* display repartition ratio around 255 */
    Console("<= 255 : %.2f%%, > 255 : %.2f%%\n",
        100.f*low/65536, 100.f*high/65526);

    test_clamp(clamp1);
    test_clamp(clamp2);
    test_clamp(clamp3);

    /* 
     * Core i5-6300U (gcc i686 -O0/O2/O3 i686 MinGW_x86_64)
     * -O0 : clamp1 < clamp2 < clamp3
     * -O1, O2, O3, Ofast : clamp1 == clamp2 < clamp3
     * 
     * A < B, means A has lower ticks per calls.
     */

    ClosePlatform();
    return EXIT_SUCCESS;
}
