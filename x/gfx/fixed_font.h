/**
 * x/gfx/fixed_font.h
 * 
 * Copyleft (c) Pierre-Jean Turpeau 05/2020
 * <pierrejean AT turpeau DOT net>
 * 
 * See: https://lazyfoo.net/tutorials/SDL/41_bitmap_fonts/index.php
 */
#ifndef X_GFX_FIXED_FONT_H
#define X_GFX_FIXED_FONT_H

# include <x/x.h>
# include <x/gfx/image.h>
# include <x/gfx/sprite.h>

# define FONT_CHAR_COUNT 256

/* font descriptor */
typedef struct {
    image_t * image;
    int cell_w;
    int cell_h;
    rect_t chars[FONT_CHAR_COUNT];
} fixed_font_t;

/**
 * Configure the instance using the given bitmap:
 *  - precompute all characters position and size in the bitmap
 *
 * A bitmap font is created from a bitmap in which several pixel content help to
 * give the details of the font (font infos):
 *  - the 1st pixel value gives the cells width
 *  - the 2nd pixel value gives the cells height
 *  - the 3rd pixel value gives the ASCII code of the first cell in the bitmap
 */
extern void init_fixed_font(fixed_font_t * font, image_t * image);

extern void fixed_char(const fixed_font_t * font, int x, int y, int c, u32 fg_color, u32 bg_color);

/* ---[ IMPLEMENTATION ]---------------------------------------------------- */

# ifdef X_IMPLEMENTATION

void init_fixed_font(fixed_font_t * font, image_t * image) {
    font->image = image;

    SDL_memset(font->chars, 0, sizeof(font->chars));

    /* fist pixel is the cell width */
    font->cell_w = font->image->pixels[0];
    assert(font->cell_w);

    /* second pixel is the cell height */
    font->cell_h = font->image->pixels[1];
    assert(font->cell_h);

    /* third pixel is the first ascii code */
    int start_char = font->image->pixels[2];

    /* clear font info to avoid problems with masks generation */
    *((u32*)font->image->pixels) = 0;

    /* if masks have been generated, there's a good chance that we need to fix
     * values poluted by font infos pixels... */
    if(image->masks)
        *((u32*)font->image->masks) = 0xFFFFFFFF;

    int cell_cols = font->image->w / font->cell_w;
    int cell_rows = font->image->h / font->cell_h;

    int cur_char = start_char;

    /* initialize undefined chars */
    for(int i = 0; i < start_char; i++) {
        font->chars[i].x = 0;
        font->chars[i].y = 0;
        font->chars[i].w = 0;
        font->chars[i].h = 0;
    }

    /* lookup each char */
    for(int j = 0; j < cell_rows; j++) {
        for(int i = 0; i < cell_cols; i++) {
            
            /* setup initial char pos and size */
            font->chars[cur_char].x = i * font->cell_w;
            font->chars[cur_char].y = j * font->cell_h;
            font->chars[cur_char].w = font->cell_w;
            font->chars[cur_char].h = font->cell_h;

            cur_char++;
            
            /* when the first ascii cell in the bitmap is not 0 */
            if( cur_char >= 256 ) {
                j = cell_rows; /* break the outerloop */
                break; /* break the innerloop */
            }
        }
    }

    console_debug("fixed font: w=%d, h=%d, start_ascii=0x%X", font->cell_w, font->cell_h, start_char);
}

void fixed_char(const fixed_font_t * font, int x, int y, int c, u32 fg_color, u32 bg_color)
{
    const rect_t * area = &(font->chars[c]);
    image_t * sprite = font->image;

    assert(sprite->pixels);
    assert(sprite->masks);

   if(x < -area->w || x > W || y < -area->h || y > H)
        return;

    const int clip_left = (x < 0) ? -x : 0;
    const int clip_top = (y < 0) ? -y : 0;
    const int clip_right = M_MIN(W - x, area->w);
    const int clip_bottom = M_MIN(H - y, area->h);

    const int words_count = (clip_right - clip_left) >> 2;

    fg_color |= (fg_color << 8);
    fg_color |= (fg_color << 16);

    bg_color |= (bg_color << 8);
    bg_color |= (bg_color << 16);

    u32 src_ofs = clip_left + area->x + (clip_top + area->y) * sprite->w;

    for(int j = clip_top; j < clip_bottom; j++, src_ofs += sprite->w) {
        u32 * src32 = (u32 *) (sprite->pixels + src_ofs);
        u32 * msk32 = (u32 *) (sprite->masks  + src_ofs);
        u32 * dst32 = (u32 *) (backbuffer() + x + clip_left + ( y + j) * W);

        for(int i = words_count; i > 0; i--, dst32++)
            *dst32 = (*(src32++) & fg_color) | (bg_color & *(msk32++));
    }
}

# endif /* X_IMPLEMENTATION */

#endif /* X_GFX_FONT_H */
