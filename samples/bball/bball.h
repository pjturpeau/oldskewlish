/**
 * bball/bball.h
 * 
 * Copyleft (c) Pierre-Jean Turpeau 01/2020
 * <pierrejean AT turpeau DOT net>
 * 
 * Perpetual motion bouncing ball (no energy loss during transfers)
 * 
 * Based on a simplified finite state machine:
 * 1. the ball fall until it hit the ground
 * 2. the ball compress itself on the ground
 * 3. the ball bounce up back until equilibrium
 * 3.A. the ball spring strech/compress according to ball elasticity
 *
 * Float only. Not really optimized for low-end CPU.
 */
#ifndef BOUNCING_BALL_H
#define BOUNCING_BALL_H

#include <x86/fsm.h>
#include <x86/easing.h>

typedef struct 
{
    fsm_t fsm;

    v2f pos;
    v2f velocity; /* kinetic */
    v2f acceleration; /* gravitation */
    v2f radius; /* depends on mass and elasticity */
    float mass;
    float dt;
    float restitution_factor; /* when the ball hit the floor */
    float elasticity;
    float stiffness; /* to compute compression time: the lower the softer */
    float max_elasticity;

    /* time fields for some transitions and spring animation */
    float duration_time; // expected duration of a transition
    float elapsed_time;
}
bouncing_ball;

extern void InitBouncingBall(bouncing_ball * ball);
extern void UpdateBouncingBall(bouncing_ball * ball, float elapsed_time);

/* ---[ IMPLEMENTATION ]---------------------------------------------------- */

#ifdef X_IMPLEMENTATION

static void _InitialState(fsm_t * fsm, fsm_action_t action);

void InitBouncingBall(bouncing_ball * ball) {
    InitStateMachine(&ball->fsm, _InitialState);
}

void UpdateBouncingBall(bouncing_ball * ball, float elapsed_time) {
    static float last_time = 0; 
    ball->dt = elapsed_time - last_time;
    ball->elapsed_time += ball->dt;
    ExecuteStateMachine(&ball->fsm);
    last_time = elapsed_time;
}

static void _StateBounceUpAndDown(fsm_t * fsm, fsm_action_t action);
static void _StateCompress(fsm_t * fsm, fsm_action_t action);
static void _StateStretchUp(fsm_t * fsm, fsm_action_t action);

static void _MoveBall(bouncing_ball * ball);
static void _AnimateSpringBall(bouncing_ball * ball);

static void _InitialState(fsm_t * fsm, fsm_action_t action) {
    bouncing_ball * ball = (bouncing_ball *) fsm;
    if (action == E_ENTRY) {
        ball->pos.x = W/2;
        ball->pos.y = 80;
        ball->velocity.x = 0.5;
        ball->velocity.y = 0;
        ball->acceleration.x = 0;
        ball->acceleration.y = 0.1;
        ball->mass = 50;
        ball->radius.x = ball->radius.y = ball->mass;

        /* even 1.0f doesn't give perpetual motion due to approximation and 
         * simplifications in the current implementation */
        ball->restitution_factor = 1.f;
        ball->stiffness = 12.0f;

        SwitchStateMachineState(fsm, _StateBounceUpAndDown);
    }
}

static void _StateBounceUpAndDown(fsm_t * fsm, fsm_action_t action) {
    bouncing_ball * ball = (bouncing_ball *) fsm;
    if (action == E_RUN) {
        _AnimateSpringBall(ball); /* jelly effect */
        _MoveBall(ball); /* move when in air */

        /* when the ball hit the ground, the ball compress itself */
        if(ball->velocity.y > 0 && ball->pos.y + ball->radius.y >= H-1) {
            SwitchStateMachineState(fsm, _StateCompress);
        }
    }
}

static void _StateCompress(fsm_t * fsm, fsm_action_t action) {
    bouncing_ball * ball = (bouncing_ball *) fsm;
    if (action == E_ENTRY) {
        /* compute compression animation parameters */
        ball->elapsed_time = 0;
        ball->max_elasticity = ball->velocity.y * ball->mass / ball->stiffness;

        /* normally it's for complete compression/extension of the spring */
        ball->duration_time = M_PI * sqrt(ball->mass / ball->stiffness);

        ball->elasticity = ball->pos.y + ball->radius.y - (H-1);

        ball->radius.x = ball->mass + ball->elasticity;
        ball->radius.y = ball->mass - ball->elasticity;
    } else if (action == E_RUN) {
        /* animate compression and compute distortion accordingly */
        float elasticity = FloatLerp(ball->elasticity, ball->max_elasticity,
            ease_out_quad(ball->elapsed_time/ball->duration_time));

        ball->radius.x = ball->mass + elasticity;
        ball->radius.y = ball->mass - elasticity;

        ball->pos.y = (H-1) - ball->radius.y;

        /* at the end of compression, the ball stretch up */
        if (ball->elapsed_time >= ball->duration_time)
            SwitchStateMachineState(fsm, _StateStretchUp);
    }
}

static void _StateStretchUp(fsm_t * fsm, fsm_action_t action) {
    bouncing_ball * ball = (bouncing_ball *) fsm;
    if (action == E_ENTRY) {
        /* the following line doesn't ensure perpetual motion even with a 
         * restitution factor of 1 because of roundings and approximations. */ 
        /* ball->velocity.y = - ball->restitution_factor * ball->velocity.y; */

        /* ensure a restitution energy that allows perpetual motion */
        ball->velocity.y = - GetFloatRandomValue(4.5f, 6.2f);

        ball->elapsed_time = 0;
        ball->duration_time = 150;

        SwitchStateMachineState(fsm, _StateBounceUpAndDown);
    }
}

static void _MoveBall(bouncing_ball * ball) {
    ball->velocity.x += ball->acceleration.x * ball->dt;
    ball->velocity.y += ball->acceleration.y * ball->dt;
    ball->pos.x += ball->velocity.x * ball->dt;
    ball->pos.y += ball->velocity.y * ball->dt;
    if (ball->velocity.x > 0 && ball->pos.x + ball->radius.x >= W-1) {
        ball->pos.x = W-1 - ball->radius.x;
        ball->velocity.x = -ball->velocity.x;
    } else if (ball->velocity.x < 0 && ball->pos.x - ball->radius.x <= 0) {
        ball->pos.x = ball->radius.x;
        ball->velocity.x = -ball->velocity.x;
    }
}

/* gives the jelly effect */
static void _AnimateSpringBall(bouncing_ball * ball) {
    float elasticity = 0;

    if (ball->elapsed_time < ball->duration_time) { 
        float t = ball->elapsed_time / ball->duration_time;
        elasticity = ball->max_elasticity * exp(-t * 6) * cos( 8 * M_TAU * t);
    }

    ball->radius.x = ball->mass + elasticity;
    ball->radius.y = ball->mass - elasticity;
}

# endif /* IMPLEMENTATION */

#endif /* BOUNCING_BALL_H */
