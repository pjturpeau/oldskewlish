/**
 * x/3d/m4f.h
 * 
 * Copyleft (c) Pierre-Jean Turpeau 12/2019
 * <pierrejean AT turpeau DOT net>
 * 
 * Column-major matrices tailored to pre-multiply vectors.
 * Camera metrix is left-hand crafted.
 * World matrix is right-hand crafted.
 * 
 * See the following resources:
 * https://www.ntu.edu.sg/home/ehchua/programming/opengl/CG_BasicsTheory.html
 * https://www.3dgep.com/understanding-the-view-matrix/#Memory_Layout_of_Column-Major_Matrices
 */
#ifndef X_MATH_M4F_H
#define X_MATH_M4F_H

#include <x/math/v3f.h>

#include <math.h>

/**
 * m00 m01 m02 m03
 * m10 m11 m12 m13
 * m20 m21 m22 m23
 * m30 m31 m32 m33
 */
typedef union
{
    /* C arrays are row major */
    float data[4][4]; /* rows, cols */
    
    /* keep numbering consistent with usual mathematic notation */
    struct { 
        float m00, m01, m02, m03; /* row 0 */
        float m10, m11, m12, m13;
        float m20, m21, m22, m23;
        float m30, m31, m32, m33; /* row 3 */
    };

    struct {
        v3f row0;
        v3f row1;
        v3f row2;
        v3f row3;
    };
}
m4f;

void m4f_zero(m4f * m) {
    for(int j = 0; j < 4; j++)
        for(int i = 0; i < 4; i++)
            m->data[i][j] = 0.0f;
}

void m4f_identity(m4f * m) {
    m->m00 = 1; m->m01 = 0; m->m02 = 0; m->m03 = 0;
    m->m10 = 0; m->m11 = 1; m->m12 = 0; m->m13 = 0;
    m->m20 = 0; m->m21 = 0; m->m22 = 1; m->m23 = 0;
    m->m30 = 0; m->m31 = 0; m->m32 = 0; m->m33 = 1;
}

void m4f_console(m4f * m) {
    for( int row = 0; row < 4; row++ )
        console("%f %f %f %f",
            m->data[row][0],
            m->data[row][1],
            m->data[row][2],
            m->data[row][3]
        );
}

/**
 * Set 'm' as translation matrix:
 * 
 *     1 0 0 p.x
 * m = 0 1 0 p.y
 *     0 0 1 p.z
 *     0 0 0 1
 */
void m4f_translation(m4f * m, v3f p) {
    m4f_identity(m);
    m->m03 = p.x;
    m->m13 = p.y;
    m->m23 = p.z;
}

/** post-multiply 'v' by 'm' */
void m4f_vec_mul(v3f * result, const v3f * v, const m4f * m) {
    result->x = v->x * m->m00 + v->y * m->m10 + v->z * m->m20 + v->w * m->m30;
    result->y = v->x * m->m01 + v->y * m->m11 + v->z * m->m21 + v->w * m->m31;
    result->z = v->x * m->m02 + v->y * m->m12 + v->z * m->m22 + v->w * m->m32;
    result->w = v->x * m->m03 + v->y * m->m13 + v->z * m->m23 + v->w * m->m33;
}

void m4f_mul(m4f * result, m4f * a, m4f * b) {
    m4f_vec_mul(&result->row0, &a->row0, b);
    m4f_vec_mul(&result->row1, &a->row1, b);
    m4f_vec_mul(&result->row2, &a->row2, b);
    m4f_vec_mul(&result->row3, &a->row3, b);
}

/**
 *           m00 m01 m02 m03   v.x
 *  result = m10 m11 m12 m13 * v.y
 *           m20 m21 m22 m23   v.z
 *           m30 m31 m32 m33   1.0
 * 
 * Identity axiom is used for the 'w' component of a position vector
 */     
void m4f_mul_pos_vec(v3f * result, const m4f * m, const v3f * v) {
    result->x = m->m00 * v->x + m->m01 * v->y + m->m02 * v->z + m->m03;
    result->y = m->m10 * v->x + m->m11 * v->y + m->m12 * v->z + m->m13;
    result->z = m->m20 * v->x + m->m21 * v->y + m->m22 * v->z + m->m23;
}

/** Null axiom is used for the 'w' component of a direction vector */
void m4f_mul_dir_vec(v3f * result, const m4f * m, const v3f * v) {
    result->x = m->m00 * v->x + m->m01 * v->y + m->m02 * v->z;
    result->y = m->m10 * v->x + m->m11 * v->y + m->m12 * v->z;
    result->z = m->m20 * v->x + m->m21 * v->y + m->m22 * v->z;
}

/**
 *           1        0        0
 * Rx(a) =   0      cosf(a)   -sinf(a)
 *           0      sinf(a)    cosf(a)
 *
 *
 *          cosf(a)    0      sinf(a)
 * Ry(a) =   0        1        0
 *         -sinf(a)    0      cosf(a)
 *
 *
 *         cosf(a)   -sinf(a)    0
 * Rz(a) = sinf(a)    cosf(a)    0
 *           0        0        1
 *
 *
 * ROT = Ry * Rx * Rz;
 */
void m4f_rotation(m4f * m, float rad_x, float rad_y, float rad_z)
{
    float csx = cosf(rad_x);
    float snx = sinf(rad_x);

    m->m00 = 1; m->m01 =   0; m->m02 =   0;
    m->m10 = 0; m->m11 = csx; m->m12 = -snx;
    m->m20 = 0; m->m21 = snx; m->m22 =  csx;
    //TODO: to complete
}

/**
 * zaxis = normal(cam_target - cam_pos)
 * xaxis = normal(cross(up_vector, zaxis))
 * yaxis = cross(zaxis, xaxis)
 * 
 *    xaxis.x                yaxis.x                zaxis.x                 0
 *    xaxis.y                yaxis.y                zaxis.y                 0
 *    xaxis.z                yaxis.z                zaxis.z                 0
 *    -dot(xaxis, cam_pos)   -dot(yaxis, cam_pos)   -dot(zaxis, cam_pos)    1
 */
void m4f_look_at_left_handed(m4f * result, v3f * eye_position, v3f * eye_direction, v3f * up_vector) {
    v3f xaxis, yaxis, zaxis;

    Vector3fSub(&zaxis, eye_direction, eye_position);
    Vector3fNormalize(&zaxis);

    Vector3fCrossProduct(&xaxis, up_vector, &zaxis);
    Vector3fNormalize(&xaxis);

    Vector3fCrossProduct(&yaxis, &zaxis, &xaxis);

    result->m00 = xaxis.x;  result->m01 = yaxis.x;  result->m02 = zaxis.x; result->m03 = 0.0f; 
    result->m10 = xaxis.y;  result->m11 = yaxis.y;  result->m12 = zaxis.y; result->m13 = 0.0f;
    result->m20 = xaxis.z;  result->m21 = yaxis.z;  result->m22 = zaxis.z; result->m23 = 0.0f;

    result->m30 = - Vector3fDotProduct(&xaxis, eye_position);
    result->m31 = - Vector3fDotProduct(&yaxis, eye_position);
    result->m32 = - Vector3fDotProduct(&zaxis, eye_position);
    result->m33 = 1.0f;
}


/** todo: illustrate the matrix */
void m4f_perspective_fov_right_handed(m4f * result, float fov, float aspect_ratio, float znear, float zfar) {
    float y_scale = (float)(1.0f / tan(fov * 0.5f)); /* TODO: fast tan ? */
    float q = zfar / (zfar - znear);

    result->m00 = y_scale / aspect_ratio; result->m01 = 0; result->m02 = 0; result->m03 = 0;
    result->m10 = 0; result->m11 = y_scale; result->m12 = 0; result->m13 = 0;
    result->m20 = 0; result->m21 = 0; result->m22 = q; result->m23 = -1.0f;
    result->m30 = 0; result->m31 = 0; result->m32 = q * znear; result->m33 = 0;
}

#endif /* X_MATH_M4F_H */
