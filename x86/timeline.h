/**
 * x86/timeline.h
 *
 * Copyleft (c) Pierre-Jean Turpeau 04/2020 <pierrejean AT turpeau DOT net>
 *
 * No smart API. Reduced to the minimum.
 * It works with absolute timings.
 */
#ifndef X86_TIMELINE_H
#define X86_TIMELINE_H

# include <x86/x.h>

/* ---[ DEFINITIONS ]------------------------------------------------------- */

typedef float (*interp_f32_func)(float from, float to, float factor);
typedef void  (*action_func)();

/* ---[ DECLARATIONS ]------------------------------------------------------ */

extern void InitTimeline();
extern void UpdateTimelineTime(u32 system_time);          // update the time reference
extern void JumpTimelineTo(u32 target_time, u32 when);

extern void FadePaletteTime(u32 start_time, u32 duration, palette_t * from, palette_t * to);
extern void AnimateFloatLerpTime(u32 start_time, u32 duration, f32 * var_ptr, f32 from, f32 to);
extern void AnimateFloatTime(u32 start_time, u32 duration, f32 * var_ptr, f32 from, f32 to, interp_f32_func func);

extern void ExecuteActionOnTime(u32 when, action_func func);

/* ---[ IMPLEMENTATION ]---------------------------------------------------- */

# ifdef X_IMPLEMENTATION 

typedef struct {
    s32 start_time;
    s32 start_shift;
    s32 current_time;
    s32 previous_time;
} timeline_t;

timeline_t _timeline;

void InitTimeline() {
    _timeline.start_time = -1;
    _timeline.start_shift = 0;
    _timeline.current_time = 0;
    _timeline.previous_time = 0;
}

void UpdateTimelineTime(u32 system_time) {
    if (_timeline.start_time == -1) {
        _timeline.start_time = system_time - _timeline.start_shift;
    }
    _timeline.previous_time = _timeline.current_time;
    _timeline.current_time = system_time - _timeline.start_time;
}

void JumpTimelineTo(u32 timeline_time, u32 when) {
    if (_timeline.current_time >= when) {
        _timeline.start_time = -1;
        _timeline.start_shift = timeline_time;
    }
}

void FadePaletteTime(u32 start_time, u32 duration, palette_t * from, palette_t * to) {
    s32 elapsed_time = _timeline.current_time - start_time;
    if (elapsed_time >= 0 && elapsed_time <= duration) {
        FadePalette(from, to, ((f32)elapsed_time)/((f32)duration));
    }
}

void AnimateFloatLerpTime(u32 start_time, u32 duration, f32 * var_ptr, f32 from, f32 to) {
    AnimateFloatTime(start_time, duration, var_ptr, from, to, FloatLerp);
}

void AnimateFloatTime(u32 start_time, u32 duration, f32 * var_ptr, f32 from, f32 to, interp_f32_func func) {
    s32 elapsed_time = _timeline.current_time - start_time;
    if (elapsed_time >= 0 && elapsed_time <= duration) {
        *var_ptr = func(from, to, ((f32)elapsed_time)/((f32)duration));
    } else if (elapsed_time > 0 && elapsed_time >= duration) {
        *var_ptr = to;
    }
}

void ExecuteActionOnTime(u32 when, action_func func) {
    if (_timeline.previous_time < when && _timeline.current_time >= when)
        func();
}

# endif /* X_IMPLEMENTATION */

#endif /* X86_TIMELINE_H */
