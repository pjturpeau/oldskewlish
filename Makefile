default: release

release:
	[ -d release ] && rm -r release || true;
	mkdir release && cd release; cmake -DCMAKE_BUILD_TYPE=Release -G "Unix Makefiles" ..

debug:
	[ -d debug ] && rm -r debug || true;
	mkdir debug && cd debug; cmake -DCMAKE_BUILD_TYPE=Debug -G "Unix Makefiles" ..

clean:
	rm -rf release
	rm -rf debug
	rm -f CMakeCache.txt

