/**
 * x86/font.h
 * 
 * Copyleft (c) Pierre-Jean Turpeau 12/2019
 * <pierrejean AT turpeau DOT net>
 * 
 * See: https://lazyfoo.net/tutorials/SDL/41_bitmap_fonts/index.php
 */
#ifndef X86_FONT_H
#define X86_FONT_H

# include <x86/x.h>
# include <x86/image.h>

/* ---[ DEFINITIONS ]------------------------------------------------------- */

# define FONT_CHAR_COUNT 256

// simplified proportional font
typedef struct {
    image_t image;
    int line_height;
    int space_width; // width of the space ' ' char
    int spacing;     // for now, used for horizontal and vertical spacing
    rect_t chars[FONT_CHAR_COUNT];
} font_t;

typedef struct {
    image_t image;
    int cell_w;
    int cell_h;
    v2s chars[FONT_CHAR_COUNT];
} mono_font_t;
/* ---[ DECLARATIONS ]------------------------------------------------------ */

extern void LoadProportionalFont(font_t * font, palette_t *pal, u8 * image_data, int image_size);
extern void FreeProportionalFont(font_t * font);
extern void DrawProportionalText(const font_t * font, int x, int y, const char * str); // Draw a transparent multi-line (\n) string
extern v2s  GetProportionalTextSize(const font_t * font, const char * text);           // returns the width and height needed to draw the given text

extern void LoadMonospaceFont(mono_font_t * font, palette_t *pal, u8 * image_data, int image_size);
extern void FreeMonospaceFont(mono_font_t * font);
extern void DrawMonospaceChar(const mono_font_t * font, int x, int y, int c, u32 fg_color, u32 bg_color);

/* ---[ IMPLEMENTATION ]---------------------------------------------------- */

# ifdef X_IMPLEMENTATION

/**
 * Configure the instance using the given bitmap:
 *  - precompute all characters position and size in the bitmap
 *  - compute the line height
 *  - compute the space width
 *
 * A bitmap font is created from a bitmap in which several pixel content help to
 * give the details of the font:
 *  - the 1st pixel value gives the cells width
 *  - the 2nd pixel value gives the cells height
 *  - the 3rd pixel value gives the ASCII code of the first cell in the bitmap
 *
 * The 'masked_image' shall have correctly initialized masks. 
 */
void LoadProportionalFont(font_t * font, palette_t *pal, u8 * image_data, int image_size) {
    LoadBMP(&(font->image), pal, image_data, image_size);
    GenerateSpriteMask(&(font->image));
    
    font->spacing = 1; /* default */

    memset(font->chars, 0, FONT_CHAR_COUNT * sizeof(rect_t));

    /* first pixel is the cell width */
    int cell_w = font->image.pixels[0];
    assert(cell_w);

    /* second pixel is the cell height */
    int cell_h = font->image.pixels[1];
    assert(cell_h);

    /* third pixel is the first ascii code */
    int start_char = font->image.pixels[2];

    int cell_cols = font->image.w / cell_w;
    int cell_rows = font->image.h / cell_h;

    // now clear font info pixel and reset mask
    *((u32 *)font->image.pixels) = 0;
    *((u32 *)font->image.masks) = 0xFFFFFFFF;
    
    int top_line = cell_h;
    int base_line = 0;

    int cur_char = start_char;

    /* initialize undefined chars */
    for(int i = 0; i < start_char; i++) {
        font->chars[i].x = 0;
        font->chars[i].y = 0;
        font->chars[i].w = 0;
        font->chars[i].h = 0;
    }

    /* lookup each char */
    for(int j = 0; j < cell_rows; j++) {
        for(int i = 0; i < cell_cols; i++) {
            
            /* setup initial char pos and size */
            font->chars[cur_char].x = i * cell_w;
            font->chars[cur_char].y = j * cell_h;
            font->chars[cur_char].w = cell_w;
            font->chars[cur_char].h = cell_h;

            /* compute the char left position */
            for(int x = 0; x < cell_w; x++) {
                for(int y = 0; y < cell_h; y++) {

                    /* look for the first non transparent pixel */
                    u8 col =
                        GetImagePixel(&(font->image), i * cell_w + x, j * cell_h + y);
                    
                    if( col ) { /* not a transparent pixel */
                        font->chars[cur_char].x += x; /* store the position */
                        x = cell_w; /* break the outer loop */
                        break; /* break the inner loop */
                    }
                }
            }

            /* compute the char right limit (width) */
            for(int x = cell_w - 1; x > 0; x--) {
                for(int y = 0; y < cell_h; y++) {

                    int px = i * cell_w + x;

                    /* look for the last non transparent pixel */
                    u8 col = GetImagePixel(&(font->image), px, j * cell_h + y);
                    
                    if( col ) {
                        font->chars[cur_char].w = px + 1 - font->chars[cur_char].x;
                        x = -1; /* break the outer loop */
                        break; /* break the inner loop */
                    }
                }
            }

            /* compute the top line */
            for( int y = 0; y < cell_h; y++ ) {
                for( int x = 0; x < cell_w; x++ ) {

                    u8 col = GetImagePixel(&(font->image), i * cell_w + x, j * cell_h + y);

                    if( col ) {
                        if(y < top_line)
                            top_line = y;
                        y = cell_h;
                        break;
                    }
                }
            }

            /* compute the base line */
            if( cur_char == 'Z' ) {
                for(int y = cell_h - 1; y >= 0; y--) {
                    for(int x = 0; x < cell_w; x++) {

                        u8 col = GetImagePixel(&(font->image), i * cell_w + x, j * cell_h + y);

                        if( col ) {
                            base_line = y;
                            y = -1;
                            break;
                        }
                    }
                }
            }

            cur_char++;
            
            /* when the first ascii cell in the bitmap is not 0 */
            if( cur_char >= 256 ) {
                j = cell_rows; /* break the outerloop */
                break; /* break the innerloop */
            }
        }
    }

    font->space_width = cell_w >> 1;
    font->line_height = base_line - top_line + 1;

    /* squeeze useless top pixels */
    for( int i = 0; i < FONT_CHAR_COUNT; ++i ) {
        font->chars[i].y += top_line;
        font->chars[i].h -= top_line;
    }

    LogDebug("LoadProportionalFont: cell_w=%d, cell_h=%d, start_ascii=0x%X", cell_w, cell_h, start_char);
    LogDebug("LoadProportionalFont: space width=%d, baseline=%d, topline=%d, line height=%d", font->space_width, base_line, top_line, font->line_height);
}

void FreeProportionalFont(font_t *font) {
    FreeImageData(&(font->image));
}

void DrawProportionalText(const font_t * font, int x, int y, const char * str) {
    int draw_x = x;
    int draw_y = y;

    for( int c = *str; c != '\0'; c = *(++str)) {
        switch(c) {
            case ' ':
                draw_x += font->space_width;
                break;
            case '\n':
                draw_x = x;
                draw_y += font->line_height + font->spacing;
                break;
            default: {
                const rect_t * def = &(font->chars[c]);
                DrawSpriteRect_c(&(font->image), draw_x, draw_y, def);
                draw_x += def->w + font->spacing;
                break;
            }
        }
    }
}

v2s GetProportionalTextSize(const font_t * font, const char * text) {
    v2s result;

    result.w = 0;
    result.h = font->line_height;

    int w = 0;
    
    for( int c = *text; c != '\0'; c = *(++text)) {
        switch(c) {
            case ' ':
                w += font->space_width;
                break;
            case '\n':
                result.w = w > result.w ? w : result.w;
                w = 0;
                result.h += font->line_height + font->spacing;
                break;
            default: {
                const rect_t * def = &(font->chars[c]);
                w += def->w + font->spacing;
                break;
            }
        }
    }
    result.w = w > result.w ? w : result.w;
    return result;
}

void LoadMonospaceFont(mono_font_t * font, palette_t *pal, u8 * image_data, int image_size) {
    LoadBMP(&(font->image), pal, image_data, image_size);
    GenerateSpriteMask(&(font->image));

    memset(font->chars, 0, sizeof(font->chars));

    font->cell_w = font->image.pixels[0];       // first pixel is the cell width
    assert(font->cell_w);

    // DrawMonospaceChar copy char by blocks for 4 masked pixels...
    if ((font->cell_w % 4) != 0)
        FatalError("LoadMonospaceFont: char width shall be mutiple of 4, here it is %d", font->cell_w);

    font->cell_h = font->image.pixels[1];       // second pixel is the cell height
    assert(font->cell_h);

    int start_char = font->image.pixels[2];    // third pixel is the first ascii code

    // now reset font info pixels and masks
    *((u32*)font->image.pixels) = 0;
    *((u32*)font->image.masks) = 0xFFFFFFFF;

    int cell_cols = font->image.w / font->cell_w;
    int cell_rows = font->image.h / font->cell_h;

    int cur_char = start_char;

    // lookup each char
    for(int j = 0; j < cell_rows; j++) {
        for(int i = 0; i < cell_cols; i++) {
            
            // setup initial char pos
            font->chars[cur_char].x = i * font->cell_w;
            font->chars[cur_char].y = j * font->cell_h;

            cur_char++;
            
            // when the first ascii cell in the bitmap is not 0
            if( cur_char >= 256 ) {
                j = cell_rows; /* break the outerloop */
                break; /* break the innerloop */
            }
        }
    }
    LogDebug("LoadMonospaceFont: cell_w=%d, cell_h=%d, start_ascii=0x%X", font->cell_w, font->cell_h, start_char);
}

void FreeMonospaceFont(mono_font_t * font) {
    FreeImageData(&(font->image));
}

void DrawMonospaceChar(const mono_font_t * font, int x, int y, int c, u32 fg_color, u32 bg_color) {
    const v2s * area = &(font->chars[c]);
    const image_t * sprite = &(font->image);

    assert(sprite->pixels);
    assert(sprite->masks);

    if(x < -area->w || x > W || y < -area->h || y > H)
        return;

    const int clip_left = (x < 0) ? -x : 0;
    const int clip_top = (y < 0) ? -y : 0;
    const int clip_right = M_MIN(W - x, font->cell_w);
    const int clip_bottom = M_MIN(H - y, font->cell_h);

    const int words_count = (clip_right - clip_left) >> 2;

    // copy char by set of 4 pixels, hence 4 colors mask
    fg_color |= (fg_color << 8);
    fg_color |= (fg_color << 16);

    bg_color |= (bg_color << 8);
    bg_color |= (bg_color << 16);

    u32 src_ofs = clip_left + area->x + (clip_top + area->y) * sprite->w;

    for(int j = clip_top; j < clip_bottom; j++, src_ofs += sprite->w) {
        u32 * src32 = (u32 *) (sprite->pixels + src_ofs);
        u32 * msk32 = (u32 *) (sprite->masks  + src_ofs);
        u32 * dst32 = (u32 *) (GetBackbuffer() + x + clip_left + ( y + j) * W);

        for(int i = words_count; i > 0; i--, dst32++)
            *dst32 = (*(src32++) & fg_color) | (bg_color & *(msk32++));
    }
}

# endif /* X_IMPLEMENTATION */

#endif /* X86_FONT_H */
