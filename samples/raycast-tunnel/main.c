/**
 * raycast-tunnel/main.c
 * Copyleft (c) Pierre-Jean Turpeau 01/2020
 * <pierrejean AT turpeau DOT net>
 */
#define X_IMPLEMENTATION 1

#define TITLE "-[ raycasting tunnel ]-"

#include <x/x.h>
#include <x/gfx/bmp.h>

/* including to ensure compilation of implementations */
#include <x/3d/m3f.h>
#include <x/math/v3f.h>
#include <x/math/numbers.h>

#define INCBIN_PREFIX
#define INCBIN_STYLE INCBIN_STYLE_SNAKE
#include <incbin/incbin.h>
INCBIN_EXTERN(texture);

extern void rt_interpolation(float delta_time);
extern void rt_not_optimized(float delta_time);

image_t * g_texture;

int fx_version = 0;

void rendering_func(float dt) {
    if(last_event() == ' ') {
        fx_version++;
        fx_version %= 2;
    }
    switch(fx_version) {
        case 0:
            rt_interpolation(dt);
            break;
        case 1:
            rt_not_optimized(dt);
            break;
    }
}

void start() {
    console("use [SPACE] to test various algorithms (interpolations, not optimized)\n");

    g_texture = create_bmp(texture_data, texture_size);
    restore_palette(g_texture->palette);
    main_loop(rendering_func);
}

INCBIN(texture, "images/texture256x256.bmp");
